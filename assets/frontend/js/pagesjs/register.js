var postApp = angular.module('postApp',[]);


postApp.controller('postController', function ($scope, $http){
});

 $(document).ready(function(){
     $("form[name='registration']").validate({
    
       
       // Specify validation rules
    rules: {
     
      firstname:"required",
      lastname: "required",
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 5
      },
      cpass : {
                minlength : 5,
		equalTo : "#password",
                required:true,
	   }
    },
    // Specify validation error messages
    messages: {
      firstname: "Please enter your firstname",
      lastname:  "Please enter your lastname",
      password:{
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      cpass:"Password does not match",
      email: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
    

 });

 function checkEmail()
 {
  //alert('here');

  var emailId = $('#emailId').val();

  $.ajax({

       url:site_url+'home/checkExistEmail',
       method:'POST',
       dataType:'JSON',
       data:{emailId:emailId},
       success:function(data){
            
            if(data.status==1)
            {
               $('#emailId').val(' ');
              $('#msg').text('Oopps..!!!,Email Already Exist..!');

            }
       }


  });
 }

