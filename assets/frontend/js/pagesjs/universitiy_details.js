
var app = angular.module('postApp',[]);

app.controller('postController',function($scope,$http,$compile){
     
     $scope.userReview={};
        
        
        
        $scope.userReview.fullname=$('#fullname').val();
        $scope.userReview.mobile=$('#mobile_no').val();
        $scope.userReview.emailId=$('#email_id').val();
        $scope.userReview.review=$('#review').val();
        $scope.userReview.city=$('#city').val();
        $scope.userReview.rate=$('.userRate').val();

   $scope.formSubmit=function(){
    console.log($scope.userReviewForm)

       if($scope.userReviewForm.$valid)
        {
           
           $scope.userReview.rate=$('.userRate').val();
           $scope.userReview.universityId=$('#universityId').val();
           $scope.userReview.universityName=$('#universityName').val();


          $http({

                method:'POST',
                dataType:'JSON',
                url:site_url+'universities/saveUserUniversityReviews',
                data:$scope.userReview,
                headers:{'Content-Type':'application/json'}
           }).success(function(data){
                   
                   if(data.status==1)
                   {        
                            $scope.userReview.fullname='';
                            $scope.userReview.mobile='';
                            $scope.userReview.emailId='';
                            $scope.userReview.review='';
                            $scope.userReview.city='';
                            $scope.userReview.rate='';
                            $scope.userReviewForm.$setPristine();

                          swal("Success!", "Your review saved with us.", "success");
                   }

                   if(data.status==2)
                   {

                     //swal("Oops!", "Please login first!", "error");
                       window.location = site_url+'login';
                   }
                   if(data.status==3)
                   {

                     swal("Oops!", "You have already submited review for this university!", "error");
                     
                   }
           });
           

       }



   }
});










    
  $(document).ready(function(){
        
   
    $(".click-callback").stars({ 

                click: function(i) {
                   // alert("Star " + i + " selected.");
                    $('#userRate').val(i);
                }
            });


});
