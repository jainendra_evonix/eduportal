var postApp = angular.module('postApp',[]);


postApp.controller('postController', function ($scope, $window,$http){


   // $('#myModal').addClass('show');
   // $('#myModal').modal('show');


$scope.brochure={};
$scope.apply={};
     
   //  alert($('#fullname').val());
    

    $scope.brochure.fullname=$('#fullname').val();
    $scope.brochure.emailId=$('#useremai').val();
    $scope.apply.applyFullname=$('#fullname').val();
    $scope.apply.applyEmailId=$('#useremai').val();
 
     $scope.submitForm = function(){


             var universityId = $('.brochureInfo').data('id');

             $scope.brochure.universityId=universityId;
             $scope.brochure.pagelocation = 'allUniversities';
         

          if($scope.instantRegister.$valid)
          {

             //console.log($scope.brochure); saveBrochureEnquiry

            $http({
               
               method:"POST",
               dataType:'json',
               url: site_url+'universities/saveBrochureofUniversity',
               data:$scope.brochure,
               headers:{'Content-Type':'application/json'}
            }).success(function(data){ 



                   $('#instantRegister').find('input,textarea,select').val('').end();
                   if(data.status==1)
                   {         
                    
                             
                            $('#modalClass').removeClass( "model-custom" ).addClass("modal-md");

                            
 
                            $('#modalBody').hide();
                            $('#modalSuccess').hide();
                            $('#brochureLoginForm').show().empty().html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h3 class="modal-title text-center text-primary">Member Login</h3></div><form method="post"><div class="modal-body" style="overflow: auto;"><h4 class="text-center">You are already registered with us.</h4><form name="login" ><h5 class="text-center"><br>Please enter your password to sign in<br><br></h5><h4 class="text-center text-primary mb20">'+data.emailId+'</h4><input type="hidden" id="email" value='+data.emailId+'><div class="form-group row " ><div class="col-md-10" style="margin:0px auto; float: none;"> <label for="pass" class="col-md-2">Password: </label><div class="col-md-10"><input type="password" name="password" id="paas" value="" class="form-control " ></div></div></div><div class="row"><div class="col-md-4" style="margin:10px auto 0px auto; float: none;"><button type="button" class="btn btn-primary btn-block" onclick="login()">Submit</button></div></div></div></form></div></div>');
                   }
             
              if(data.status==3 || data.status==2)
                  {   
                    
                      
                      $('.list-enqu-btn').find('.temp').removeAttr('data-target').removeClass('temp');
                      $('#addButton').empty().html('<ul><li><a href="<?php echo base_url(); ?>logout" class="v3-menu-sign"><i class="fa fa-sign-in"></i>Log out</a></li></ul>');
                    
                   $('#modalBody').hide();
                   $('#modalSuccess').show();
                      $('#modalSuccess').html('<div class="modal-header" id="thankyou"><button type="button" class="close responseModal" id="close_modal_leadform" data-dismiss="modal">×</button><h4 class="modal-title text-center">THANK YOU</h4><p style="text-align:center;"><span class="h5" style="color: #263a78">Details has been sent to your registered email id. You can download the brochure for '+data.universityName+',  '+data.uniCity+ ' by clicking below link</span></p><li align="center"><a target="_blank" href="https://images.static-collegedunia.com/public/college_data/images/pdfcol/1437989147ProspectusMBBS2015.pdf">University Brochure </a></li><p></p></div>');
                      

                  }
                


            });
        

          }


         
         
     }


   $scope.submitApplyForm = function(){


           var universityId = $('.brochureInfo').data('id');
           
             $scope.apply.universityId=universityId;

          if($scope.instantApply.$valid)
          {

             //console.log($scope.brochure);

            $http({
               
               method:"POST",
               dataType:'json',
               url: site_url+'universities/applyToUniversity',
               data:$scope.apply,
               headers:{'Content-Type':'application/json'}
            }).success(function(data){ 
                      
                      $('#instantApply').find('input,textarea,select').val('').end();
                 /*  if(data.status==1)
                   {         


                          
                            window.location=site_url+'login';
                   }*/

                   if(data.status==1)
                   {          


                          //  $('#modalClass2').removeClass( "model-custom" ).addClass("modal-md");
                            $('#applyModalBody').hide();
                            $('#applyModalSuccess').hide();
                            $('#applyLoginForm').show().empty().html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h3 class="modal-title text-center text-primary">Member Login</h3></div><form method="post"><div class="modal-body" style="overflow: auto;"><h4 class="text-center">You are already registered with us.</h4><form name="login" ><h5 class="text-center"><br>Please enter your password to sign in<br><br></h5><h4 class="text-center text-primary mb20">'+data.emailId+'</h4><input type="hidden" id="email" value='+data.emailId+'><div class="form-group row " ><div class="col-md-10" style="margin:0px auto; float: none;"> <label for="pass" class="col-md-2">Password: </label><div class="col-md-10"><input type="password" name="password" id="paas" value="" class="form-control " ></div></div></div><div class="row"><div class="col-md-4" style="margin:10px auto 0px auto; float: none;"><button type="button" class="btn btn-primary btn-block" onclick="applyLogin()">Submit</button></div></div></div></form></div></div>');
                           

                   }
             
              if(data.status==3 || data.status==2 )
                  {   
                    
                      
                      $('.list-enqu-btn').find('.temp').removeAttr('data-target').removeClass('temp');
                      $('#addButton').empty().html('<ul><li><a href="<?php echo base_url(); ?>logout" class="v3-menu-sign"><i class="fa fa-sign-in"></i>Log out</a></li></ul>');
                    
                   $('#applyModalBody').hide();
                   $('#applyModalSuccess').show();
                      $('#applyModalSuccess').html('<div class="modal-header" id="thankyou"><button type="button" class="close responseModal" id="close_modal_leadform" data-dismiss="modal">×</button><h4 class="modal-title text-center">THANK YOU</h4><p style="text-align:center;"><span class="h5" style="color: #263a78">Details has been sent to your registered email id. You can download the brochure for '+data.universityName+',  '+data.uniCity+ 'by clicking below link</span></p><li align="center"><a target="_blank" href="https://images.static-collegedunia.com/public/college_data/images/pdfcol/1437989147ProspectusMBBS2015.pdf"></a></li><p></p></div>');
                      

                  }
                


            });
        

          }



   }




 var loadCity = function () {
 
            $http({
                 
                 method:'POST',
                 dataType:'json',
                 url:site_url+'universities/getCitiesforModal',
                 headers:{'Content-Type':'application/json'}
             }).success(function(data){
                    
                    $scope.cities=data;
                  
                    

             });


             $http({
                 
                 method:'POST',
                 dataType:'json',
                 url:site_url+'universities/getCoursesforModal',
                 headers:{'Content-Type':'application/json'}
             }).success(function(data){
                    
                    $scope.courses=data;
                    
                    

             });
   
};
// and fire it after definition
loadCity();





});


    var hash = {};
    var obj={};

function ChangeUrl(){

		 console.log(hash)
		 var i=0;   
         var url = site_url+'colleges/search?'; 
         var newUrl='';
         $.each(hash,function(key,val){
          
         
	         if(i==0)
	          	newUrl = newUrl+key+"="+val; 
	          else
	          	newUrl = newUrl+"&"+key+"="+val;
	          console.log(newUrl)
	           i++;
          })
         console.log('Main URL==>'+url+newUrl)
 
	 history.pushState(obj, obj.Page, url+newUrl);
	}


	
	$(document).ready(function(){
    
    
	 $('.location').click(function(){

      //  alert('here')
       
        var location = [];
        var courses = [];
        var specialization = [];
      
        $('#location input:checked').each(function (){
           if($(this).prop('checked')){
           	location.push($(this).val().split(' ').join('-'));
                var id = $('.location'+location).val();
              }
            });
        
//console.log(location);

        $('#courses input:checked').each(function (){ 
            
            if($(this).prop('checked')){
                courses.push($(this).val());
            }
            
            $('#specialization input:checked').each(function(){
	    if($(this).prop('checked')){
	   		specialization.push($(this).val());
	   }
	  });
           
        
        });
	     
	     $.ajax({
             
             url:site_url+'colleges/searchColleges',
             methos:'GET',
             dataType: 'json',
             data:{location:location,courses:courses,specialization:specialization},
             success:function(data){
             	
                $('#searchedUniversities').html(''); 
               $('#searchedUniversities').html(data.universities);
             }

	     });
           
	     location=location.join(' ');
	   
	     console.log(location);
	    

	     hash['location']= location;
	     ChangeUrl();

	 });




      
      $('.courses').click(function(){
       //  alert()
      	 var courses = [];
         var location = [];
         var specialization = [];
         
	    $('#courses input:checked').each(function(){
	    if($(this).prop('checked')){
	   		courses.push($(this).val());
	   }
	  });
          
          $('#location input:checked').each(function(){
	    if($(this).prop('checked')){
	   		location.push($(this).val());
	   }
	  });
          
          $('#specialization input:checked').each(function(){
	    if($(this).prop('checked')){
	   		specialization.push($(this).val());
	   }
	  });
          
         // console.log(courses)
           $.ajax({
             
             url:site_url+'colleges/searchColleges',
             methos:'GET',
             dataType: 'json',
             data:{courses:courses,location:location,specialization:specialization},
             success:function(data){
             	
                $('#searchedUniversities').html(''); 
               $('#searchedUniversities').html(data.universities);
             }

	     });
           

	   courses = courses.join(' ');
	   hash['courses'] = courses;
	    ChangeUrl(); 

      });


      $('.specialization').click(function(){
       //  alert()
      	 var courses = [];
         var location = [];
         var specialization = [];
	    $('#courses input:checked').each(function(){
	    if($(this).prop('checked')){
	   		courses.push($(this).val());
	   }
	  });
          $('#location input:checked').each(function(){
	    if($(this).prop('checked')){
	   		location.push($(this).val());
	   }
	  });
          
          $('#specialization input:checked').each(function(){
	    if($(this).prop('checked')){
	   		specialization.push($(this).val());
	   }
	  });
          
         // console.log(courses)
           $.ajax({
             
             url:site_url+'colleges/searchColleges',
             methos:'GET',
             dataType: 'json',
             data:{courses:courses,location:location,specialization:specialization},
             success:function(data){
             	
                $('#searchedUniversities').html(''); 
               $('#searchedUniversities').html(data.universities);
             }

	     });
           

	   specialization = specialization.join(' ');
	   hash['specialization'] = specialization;
	    ChangeUrl(); 

      });
  
$(function(){
    

  $("div.holder").jPages({
    containerID : "searchedUniversities"
  });

});


});


$(document).ready(function(){
    
    $('.brochureInfo').click(function(){

      $(this).addClass('temp');

    })


    $('.applyInfo').click(function(){

      $(this).addClass('temp');

    })

   });  

function login(){

  

 var emailId = $('#email').val();
 var pass = $('#paas').val();

 alert(pass+" "+emailId)

 if((pass !==null && pass!==''))
 {
 
  $.ajax({

     url:site_url+'home/checkLoginForUniversityBrochure',
     method:'POST',
     dataType:'json',
     data:{emailaddress:emailId,password:pass},

     
     success:function(data){
           
           if(data.status==1)
           {           

                        
                       $('.list-enqu-btn').find('.temp').removeAttr('data-target').removeClass('temp');
                       $('#addButton').empty().html('<ul><li><a href="<?php echo base_url(); ?>logout" class="v3-menu-sign"><i class="fa fa-sign-in"></i>Log out</a></li></ul>');
                       $('#modalBody').hide();
                       $('#brochureLoginForm').hide();

                      $('#modalSuccess').show();
                      $('#modalSuccess').html('<div class="modal-header" id="thankyou"><button type="button" class="close responseModal" id="close_modal_leadform" data-dismiss="modal">×</button><h4 class="modal-title text-center">THANK YOU</h4><p style="text-align:center;""><span class="h5" style="color: #263a78">Brochure of '+data.univaersityName+',  '+data.uniCity+ ' sent on your email-id, Addmission cell will contact to you soon.</span></p><p></p></div>');
                     
                      
                       

           }

           if(data.status==2)
           {
              
              swal("Oops!", "Password was incorrect!", "error");

           }

     }


  });
}else{

   swal("Oops!", "Please enter password!", "error");
}
}


function applyLogin(){

  

 var emailId = $('#email').val();
 var pass = $('#paas').val();

 alert(pass+" "+emailId)

 if((pass !==null && pass!==''))
 {
 
  $.ajax({

     url:site_url+'home/checkLoginForApplyUniversity',
     method:'POST',
     dataType:'json',
     data:{emailaddress:emailId,password:pass},

     
     success:function(data){
           
           if(data.status==1)
           {           

                        
                       $('.list-enqu-btn').find('.temp').removeAttr('data-target').removeClass('temp');
                       $('#addButton').empty().html('<ul><li><a href="<?php echo base_url(); ?>logout" class="v3-menu-sign"><i class="fa fa-sign-in"></i>Log out</a></li></ul>');
                       $('#applyModalBody').hide();
                       $('#applyLoginForm').hide();

                      $('#applyModalSuccess').show();
                      $('#applyModalSuccess').html('<div class="modal-header" id="thankyou"><button type="button" class="close responseModal" id="close_modal_leadform" data-dismiss="modal">×</button><h4 class="modal-title text-center">THANK YOU</h4><p style="text-align:center;""><span class="h5" style="color: #263a78">Applied to '+data.univaersityName+',  '+data.uniCity+ ' sent on your email-id, Addmission cell will contact to you soon.</span></p><p></p></div>');
                     
                      
                       

           }

           if(data.status==2)
           {
              
              swal("Oops!", "Password was incorrect!", "error");

           }

     }


  });
}else{

   swal("Oops!", "Please enter password!", "error");
}
}





//function for show showModalForm
function showModalForm()
{


  $('#modalBody').show();
  $('#modalClass').removeClass( "modal-md" ).addClass( "model-custom" );
  $('#brochureLoginForm').hide();
  $('#modalSuccess').hide();
   
   /*$('#modalBody').modal('open');
   $('#brochureLoginForm').modal('hide');
   $('#modalSuccess').modal('hide');*/
}

	