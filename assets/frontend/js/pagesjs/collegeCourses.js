var postApp = angular.module('postApp',[]);
//angular.module('ui.bootstrap.demo', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);


postApp.controller('postController', function ($scope, $http){
    
    //inject service 
   
   
    $scope.brochure={};
     
   //  alert($('#fullname').val());
    

    $scope.brochure.fullname=$('#fullname').val();
    $scope.brochure.emailId=$('#useremai').val();


   
   
     $scope.submitForm = function(){


             var universityId = $('.brochureInfo').data('id');

             $scope.brochure.universityId=universityId;

         

          if($scope.instantRegister.$valid)
          {

            $http({
               
               method:"POST",
               dataType:'json',
               url: site_url+'universities/saveBrochureEnquiry',
               data:$scope.brochure,
               headers:{'Content-Type':'application/json'}
            }).success(function(data){ 



           $('#instantRegister').find('input,textarea,select').val('').end();
                   if(data.status==1)
                   {         
                   	
                             
                   	        $('#modalClass').removeClass( "model-custom" ).addClass("modal-md");

                            
 
                            $('#modalBody').hide();
                            $('#modalSuccess').hide();
                   	        $('#brochureLoginForm').show().empty().html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h3 class="modal-title text-center text-primary">Member Login</h3></div><form method="post"><div class="modal-body" style="overflow: auto;"><h4 class="text-center">You are already registered with us.</h4><form name="login" ><h5 class="text-center"><br>Please enter your password to sign in<br><br></h5><h4 class="text-center text-primary mb20">'+data.emailId+'</h4><input type="hidden" id="email" value='+data.emailId+'><div class="form-group row " ><div class="col-md-10" style="margin:0px auto; float: none;"> <label for="pass" class="col-md-2">Password: </label><div class="col-md-10"><input type="password" name="password" id="paas" value="" class="form-control " ></div></div></div><div class="row"><div class="col-md-4" style="margin:10px auto 0px auto; float: none;"><button type="button" class="btn btn-primary btn-block" onclick="login()">Submit</button></div></div></div></form></div></div>');
                   }


                   

                  if(data.status==3 || data.status==2)
                  {   
                  	
                      
                      $('.list-enqu-btn').find('.temp').removeAttr('data-target').removeClass('temp');
                      $('#addButton').empty().html('<ul><li><a href="<?php echo base_url(); ?>logout" class="v3-menu-sign"><i class="fa fa-sign-in"></i>Log out</a></li></ul>');
                    
                   $('#modalBody').hide();
                   $('#modalSuccess').show();
                      $('#modalSuccess').html('<div class="modal-header" id="thankyou"><button type="button" class="close responseModal" id="close_modal_leadform" data-dismiss="modal">×</button><h4 class="modal-title text-center">THANK YOU</h4><p style="text-align:center;"><span class="h5" style="color: #263a78">Details has been sent to your registered email id. You can download the brochure for '+data.colgName+',  '+data.colgCity+ 'by clicking below link</span></p><li align="center"><a target="_blank" href="https://images.static-collegedunia.com/public/college_data/images/pdfcol/1437989147ProspectusMBBS2015.pdf">MBBS Prospectus </a></li><p></p></div>');
                      

                  }
                


            });
        

          }


         
         
     }
 
   // function for load city
            var loadCity = function () {
 
            $http({
                 
                 method:'POST',
                 dataType:'json',
                 url:site_url+'universities/getCitiesforModal',
                 headers:{'Content-Type':'application/json'}
             }).success(function(data){
                    console.log(data)
                    $scope.cities=data;
                  
                    

             });


             $http({
                 
                 method:'POST',
                 dataType:'json',
                 url:site_url+'universities/getCoursesforModal',
                 headers:{'Content-Type':'application/json'}
             }).success(function(data){
                    
                    $scope.courses=data;
                    
                    

             });
   
};
// and fire it after definition
loadCity();


    
});



    var getCollege='fetchCollege';
    var universityid;
    var university;
    var collegeName;
    var collegeId;
    var level;
    var courseId; 
    var hash = {};
    var obj={};
    var university ={};

function ChangeUrl(){

		// console.log(university)
		 var i=0;   
         var url = site_url+'university/'+university+'/'+'college/'+collegeName+'/search?'; 
         var newUrl='';
         $.each(hash,function(key,val){
          
         
	         if(i==0)
	          	newUrl = newUrl+key+"="+val; 
	          else
	          	newUrl = newUrl+"&"+key+"="+val;
	         // console.log(newUrl)
	           i++;
          })
         
         var link =  url + newUrl + '&id='+id;
     //    console.log(link)
         console.log('Main URL==>'+url+newUrl)
 
	 history.pushState(obj, obj.Page, link);
	}


	
	$(document).ready(function(){


		
	 $('.location').click(function(){

      

     
     var id = $('#univrID').val();
     var universityName = $('#uniName').val().split(' ').join('-');
       
      // alert(universityName)
   

        
        var location = [];
        var courses = [];
        var specialization = [];

      universityid =id;   
      university=universityName;

        $('#location input:checked').each(function (){
           if($(this).prop('checked')){
           	location.push($(this).val().split(' ').join('-'));
                var id = $('.location'+location).val();
              }
            });
        
        $('#courses input:checked').each(function (){ 
            
            if($(this).prop('checked')){
                courses.push($(this).val().split(' ').join('-'));
            }
            
            $('#specialization input:checked').each(function(){
	    if($(this).prop('checked')){
	   		specialization.push($(this).val().split(' ').join('-'));
	   }
	  });
           
        
        });
	     
	     $.ajax({
             
             url:site_url+'universities/'+university+'/'+'search',
             methos:'GET',
             dataType: 'json',
             data:{location:location,courses:courses,specialization:specialization,id:id},
             success:function(data){
             	
                $('#searchedUniversities').html(''); 
               $('#searchedUniversities').html(data.universities);
             }

	     });
           
	     location=location.join(' ');
	      hash['location']= location;

	     ChangeUrl();

	 });

      
      $('.courses').click(function(){
        
      	 var courses = [];
         var location = [];
         var specialization = [];
      // var id = $('#univrID').val();
      id = $('#colgID').val();
      collegeName = $('#colgName').val().split(' ').join('-');
     var universityName = $('#uniName').val().split(' ').join('-');
     universityid =id;  
     university=universityName;

     
         
	    $('#courses input:checked').each(function(){
	    if($(this).prop('checked')){
	   		courses.push($(this).val());
	   }
	  });
          
          $('#location input:checked').each(function(){
	    if($(this).prop('checked')){
	   		location.push($(this).val());
	   }
	  });
          
          $('#specialization input:checked').each(function(){
	    if($(this).prop('checked')){
	   		specialization.push($(this).val());
	   }
	  });
          
         // console.log(courses)
           $.ajax({
             
             url:site_url+'university/'+university+'/'+'college/'+collegeName+'/innerSearch2',
             methos:'GET',
             dataType: 'json',
             data:{courses:courses,location:location,specialization:specialization,id:id},
             success:function(data){
             	
                $('#searchedUniversities').html(''); 
               $('#searchedUniversities').html(data.searchedCourses);
             }

	     });
           

	   courses = courses.join(' ');
	   hash['courses'] = courses;
	    ChangeUrl(); 

      });


      $('.specialization').click(function(){
       //  alert()
      	 var courses = [];
         var location = [];
         var specialization = [];
          // var id = $('#univrID').val();
          id = $('#colgID').val();
      collegeName = $('#colgName').val().split(' ').join('-');
     var universityName = $('#uniName').val().split(' ').join('-');
     universityid =id;  
     university=universityName;
	    $('#courses input:checked').each(function(){
	    if($(this).prop('checked')){
	   		courses.push($(this).val());
	   }
	  });
          $('#location input:checked').each(function(){
	    if($(this).prop('checked')){
	   		location.push($(this).val());
	   }
	  });
          
          $('#specialization input:checked').each(function(){
	      if($(this).prop('checked')){
	   		specialization.push($(this).val());
	   }
	  });
          
         // console.log(courses)
           $.ajax({
             
             url:site_url+'university/'+university+'/'+'college/'+collegeName+'/innerSearch2',
             methos:'GET',
             dataType: 'json',
             data:{courses:courses,location:location,specialization:specialization,id:id},
             success:function(data){
             	   
                 $('#searchedUniversities').html(''); 
               $('#searchedUniversities').html(data.searchedCourses);
             }

	     });
           

	   specialization = specialization.join(' ');
	   hash['specialization'] = specialization;
	    ChangeUrl(); 

      });
  



});
	

 $(document).ready(function(){
    
    $('.brochureInfo').click(function(){

     	$(this).addClass('temp');

    })



  $('.responseModal').click(function(){

  	

    })

 });




function login(){

	

 var emailId = $('#email').val();
 var pass = $('#paas').val();

 alert(pass+" "+emailId)

 if((pass !==null && pass!==''))
 {
 
  $.ajax({

     url:site_url+'home/checkLoginFn',
     method:'POST',
     dataType:'json',
     data:{emailaddress:emailId,password:pass},

     
     success:function(data){
           
           if(data.status==1)
           {
                  
                      $('.list-enqu-btn').find('.temp').removeAttr('data-target').removeClass('temp');
                       $('#addButton').empty().html('<ul><li><a href="<?php echo base_url(); ?>logout" class="v3-menu-sign"><i class="fa fa-sign-in"></i>Log out</a></li></ul>');
                     $('#modalBody').hide();
                     $('#brochureLoginForm').hide();

                     $('#modalSuccess').show();
                      $('#modalSuccess').html('<div class="modal-header" id="thankyou"><button type="button" class="close responseModal" id="close_modal_leadform" data-dismiss="modal">×</button><h4 class="modal-title text-center">THANK YOU</h4><p style="text-align:center;""><span class="h5" style="color: #263a78">Details has been sent to your registered email id. You can download the brochure for '+data.colgName+',  '+data.colgCity+ ' by clicking below link</span></p><li align="center" ><a target="_blank" href="https://images.static-collegedunia.com/public/college_data/images/pdfcol/1437989147ProspectusMBBS2015.pdf"> '+data.courseName+' in '+data.spec_course +'</a></li><p></p></div>');
                     
                      
                       

           }

           if(data.status==2)
           {
              
              swal("Oops!", "Password was incorrect!", "error");

           }

     }


  });
}else{

	 swal("Oops!", "Please enter password!", "error");
}
}



//function for show showModalForm
function showModalForm()
{


	$('#modalBody').show();
	$('#modalClass').removeClass( "modal-md" ).addClass( "model-custom" );
	$('#brochureLoginForm').hide();
	$('#modalSuccess').hide();
   
   /*$('#modalBody').modal('open');
   $('#brochureLoginForm').modal('hide');
   $('#modalSuccess').modal('hide');*/
}