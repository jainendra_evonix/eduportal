<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_model extends CI_Model {


    public function __construct() {
        parent::__construct();
     
    }


   public function generateRandomString($length = 8) {    
            $characters   = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            return $randomString;
    }

     public function check_admin_forgot_password($emailaddress) {
        $query = $this->db->query("SELECT * FROM tbl_admin WHERE username='$emailaddress'");
        //echo $query->num_rows(); exit;
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }







    public function check_user_login($emailaddress, $password) {
        $query = $this->db->query("SELECT * FROM tbl_admin WHERE username='$emailaddress' and 
        password='".md5($password)."'");
        //echo $query->num_rows(); exit;
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }



     public function update_password($emailaddress,$npassword) {
        $newpassword = array(
        'password' => md5($npassword),
        'isused' => 1
        );
        $this->db->where('username', $emailaddress);
        $this->db->update('tbl_admin', $newpassword);
        //print_r($this->db->last_query()); exit;
        return TRUE;
    }


    public function getalluniversitydata() {


            $query = $this->db->query('select * from tbl_university where deleted=0 order by id desc');
            
            if($query->num_rows() > 0)
            {
                $data=$query->result();
                return $data;
            }
            else
            {
                return FALSE;
            }
    }


    /*function  for get all programs*/
    public function getAllMasterPrograms()
    {

           $masterPrograms = $this->db->select('*')
                                      ->from('mst_courses')
                                      ->get()->result();
                                      
                                      return $masterPrograms;

                                     // echo "<pre>"; print_r($masterPrograms); exit;

    }


    /*function for getMasterCourseBYId*/
    public function getMasterCourseBYId($id)
    {

        $masterPrograms = $this->db->select('*')
                                      ->from('mst_courses')
                                      ->where('mst_courses.id',$id)
                                      ->get()->row();
                                      
                                      return $masterPrograms;

    }


    /*function for delete master course*/
    public function deleteMasterCourse($deletingId)
    {

        $deleteMasterCourse = $this->db->where('id',$deletingId)
                                        ->delete('mst_courses');

                                       return $deleteMasterCourse;
    }

    public function getalluniversitycount() {
            $query = $this->db->query('select * from tbl_university where deleted=0 order by id desc');
            
            return $query->num_rows();
    }

    public function getallcoursecount() {
            $query = $this->db->query('select * from tbl_course where deleted=0 order by id desc');
            
            return $query->num_rows();
    }

    public function getallspecializationcount() {
            $query = $this->db->query('select * from tbl_specialization where deleted=0 order by id desc');
            
            return $query->num_rows();
    }

    public function getallasccoursecount() 
      {
            $query = $this->db->query('select tc.*, tca.id as tcaid, tca.enabled as ascenabled, ts.name as specializationname from tbl_course tc inner join tbl_clg_crs_spec_association tac on tc.id=tac.courseid inner join tbl_college_association tca on tac.course_association_id=tca.id inner join tbl_specialization ts on tac.specializationid=ts.id where tc.enabled=1 and tc.deleted=0 and tca.deleted=0');
            
            return $query->num_rows();
    }

    public function deleteuniversitydata($deletingId) {
        $isdelete= array('deleted'=>'1');
        $query = $this->db->query('select * from tbl_university where id='.$deletingId);
       if($query->num_rows() > 0)
        {
            $this->db->where('id',$deletingId);
            $this->db->update('tbl_university', $isdelete);
            return true;
        }
        else
        {
            return false;
        }
    }

  

    public function deletespecializationdata($deletingId) {
        $isdelete= array('deleted'=>'1');
        $query = $this->db->query('select * from tbl_specialization where id='.$deletingId);
       if($query->num_rows() > 0)
        {
            $this->db->where('id',$deletingId);
            $this->db->update('tbl_specialization', $isdelete);
            return true;
        }
        else
        {
            return false;
        }
    }

    public function deleteasccoursedata($deletingId) {
        $isdelete= array('deleted'=>'1');
        $query = $this->db->query('select * from tbl_college_association where id='.$deletingId);
        if($query->num_rows() > 0)
            {
                $this->db->where('id',$deletingId);
                $this->db->update('tbl_college_association', $isdelete);
            }

        $query = $this->db->query('select * from tbl_clg_crs_spec_association where id='.$deletingId);
        if($query->num_rows() > 0)
            {
                $this->db->where('id',$deletingId);
                $this->db->update('tbl_clg_crs_spec_association', $isdelete);
            }

        return true;
    }

    public function deletecollegedata($deletingId) {

        $isdelete= array('deleted'=>'1');
        $query = $this->db->query('select * from tbl_college where id='.$deletingId);
       if($query->num_rows() > 0)
        {
            $this->db->where('id',$deletingId);
            $this->db->update('tbl_college', $isdelete);
            return true;
        }
        else
        {
            return false;
        }
    }

    public function get_college_association($copyId) {
        $query = $this->db->query("SELECT * FROM tbl_college_association WHERE id='$copyId'");
        //echo $query->num_rows(); exit;
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_clg_crs_spec_association($copyId) {
        $query = $this->db->query("SELECT * FROM tbl_clg_crs_spec_association WHERE course_association_id='$copyId'");
        //echo $query->num_rows(); exit;
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getuniversitydata($universityid) {

         
             $universityInfo = $this->db->select('*')
                                       ->from('tbl_university')
                                       ->where('deleted',0)
                                       ->where('id',$universityid)
                                       ->get()->row();

                                       return $universityInfo;
    }


    public function getspecializationdata($specid) {
            $query = $this->db->query('select * from tbl_specialization where deleted=0 and id='.$specid);
            $data = $query->row();
            if($query->num_rows() > 0)
            {
                return $data;
            }
            else
            {
                return FALSE;
            }
    }

    public function getCollegeData($collegeId) {

            $collegeData = $this->db->select('tbl_college.*,tbl_university.name as universityName,tbl_university.id as universityId')
                                    ->from('tbl_college')
                                    ->join('tbl_university','tbl_college.universityid=tbl_university.id')
                                    ->where('tbl_college.deleted',0)
                                    ->where('tbl_college.id',$collegeId)
                                    ->get()->row();

             //  echo "<pre>"; print_r($collegeData); exit;

                                    return $collegeData;


    }

    public function submit_university($data) {
        if ($this->db->insert('tbl_university', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function update_university($data) {
        $id = $data['id'];
          $this->db->where('id',$id);
        if ($this->db->update('tbl_university',$data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getallcoursesdata() {
            $query = $this->db->query('select * from tbl_course where deleted=0 order by id desc');
            
            if($query->num_rows() > 0)
            {
                $data=$query->result();
                return $data;
            }
            else
            {
                return FALSE;
            }
    }

    public function getallspecializationdata() {
            $query = $this->db->query('select * from tbl_specialization where deleted=0 order by id desc');
            
            if($query->num_rows() > 0)
            {
                $data=$query->result();
                return $data;
            }
            else
            {
                return FALSE;
            }
    }

    public function getallcollegedata() {
            $query = $this->db->query('select tc.*, tu.name as universityname, tu.id as uniId from tbl_college tc inner join tbl_university tu on tc.universityid=tu.id where tc.deleted=0 order by tc.id desc');
            
            if($query->num_rows() > 0)
            {
                $data=$query->result();
                return $data;
            }
            else
            {
                return FALSE;
            }
    }

    public function submit_course($data) {
        if ($this->db->insert('tbl_course', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function update_course($data) {
        $id = $data['id'];
          $this->db->where('id',$id);
        if ($this->db->update('tbl_course',$data)) {
            return true;
        } else {
            return false;
        }
    }

    public function submit_specialization($data) {
        if ($this->db->insert('tbl_specialization', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function update_specialization($data) {
        $id = $data['id'];
          $this->db->where('id',$id);
        if ($this->db->update('tbl_specialization',$data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getuniversitycollegedata()
         {
            $collegeuniversity=$this->input->post("collegeuniversity");
            $query="select * from tbl_college where universityid ='$collegeuniversity' and enabled=1 and deleted=0 order by id desc";
            $result=$this->db->query($query);
            return $result;
          }

    public function getcourseassociationdata()
         {
            $eId=$this->input->post("eId");
            $qry="select tcsa.*, tca.universityid, tca.collegeid from tbl_clg_crs_spec_association tcsa inner join tbl_college_association tca on tcsa.course_association_id=tca.id where tcsa.course_association_id=".$eId;

            $query = $this->db->query($qry);
            $data = $query->row();
            if($query->num_rows() > 0)
            {
                return $data;
            }
            else
            {
                return FALSE;
            }
            //return $query->result_array();
          }

    public function submit_college($data) {
        if ($this->db->insert('tbl_college', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchalluniversitydata() {

            $q = $this->db->query("select * from tbl_university where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select University';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function fetchallcollegedata() {
            $q = $this->db->query("select * from tbl_college where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select College';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function fetchallcoursesdata() {
            $q = $this->db->query("select * from tbl_course where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select Course';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    /*public function fetchallspecdata() {
            $query = $this->db->query('select * from tbl_specialization where enabled=1 and deleted=0 order by id desc');
            
            if($query->num_rows() > 0)
            {
                $data=$query->result();
                return $data;
            }
            else
            {
                return FALSE;
            }
    }*/

    public function fetchallspecdata() {
            $q = $this->db->query("select * from tbl_specialization where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select Specialization';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function fetchalldegreedata() {
            $q = $this->db->query("select * from tbl_degree where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select Degree';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function fetchallcredentialdata() {
            $q = $this->db->query("select * from tbl_credential where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select Credential';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function fetchallmodedata() {
            $q = $this->db->query("select * from tbl_modeofstudy where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select Mode of Study';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function fetchallmediumdata() {
            $q = $this->db->query("select * from tbl_medium where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select Medium';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function fetchallrecognitiondata() {
            $q = $this->db->query("select * from tbl_recognition where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select Recognition';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function fetchallaccreditationdata() {
            $q = $this->db->query("select * from tbl_accreditation where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select Accreditation';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function fetchallownershipdata() {
            $q = $this->db->query("select * from tbl_ownership where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select Ownership';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function fetchallcoursestatusdata() {
            $q = $this->db->query("select * from tbl_course_status where enabled=1 and deleted=0 order by id desc");

        if($q->num_rows() > 0) {
            $data['']='Select Course Status';
            foreach($q->result() as $row) {
                $data[$row->id] = $row->name;
            }
            return $data;
         }
        else {
            return false;
        }
    }

    public function submit_courseassociation($course,$collegecourse) {
        /*print_r($collegecourse);
        echo $collegecourse['totalfees'];
        exit;*/

        $this->db->insert('tbl_college_association', $course);
        $course_insert_id = $this->db->insert_id();

        $collegecoursedata = array(
            'course_association_id' => $course_insert_id,
            'courseid' => $collegecourse['courseid'],
            'specializationid' => $collegecourse['specializationid'],
            'credentialid' => $collegecourse['credentialid'],
            'degreeid' => $collegecourse['degreeid'],
            'modeofstudyid' => $collegecourse['modeofstudyid'],
            'duration' => $collegecourse['duration'],
            'mediumid' => $collegecourse['mediumid'],
            'recognitionid' => $collegecourse['recognitionid'],
            'coursestatusid' => $collegecourse['coursestatusid'],
            'accreditationid' => $collegecourse['accreditationid'],
            'ownershipid' => $collegecourse['ownershipid'],
            'totalfees' => $collegecourse['totalfees'],
            'eligibility' => $collegecourse['eligibility'],
            'coursestructure' => $collegecourse['coursestructure'],
            'enabled' => $collegecourse['enabled'],
            'timestamp' => $collegecourse['timestamp']
        );

        $this->db->insert('tbl_clg_crs_spec_association', $collegecoursedata);

        return true;
    }

    public function update_courseassociation($course,$collegecourse) {
        $id = $course['id'];

        $this->db->where('id',$id);
        $this->db->update('tbl_college_association',$course);

        $collegecoursedata = array(
            'courseid' => $collegecourse['courseid'],
            'specializationid' => $collegecourse['specializationid'],
            'credentialid' => $collegecourse['credentialid'],
            'degreeid' => $collegecourse['degreeid'],
            'modeofstudyid' => $collegecourse['modeofstudyid'],
            'duration' => $collegecourse['duration'],
            'mediumid' => $collegecourse['mediumid'],
            'recognitionid' => $collegecourse['recognitionid'],
            'coursestatusid' => $collegecourse['coursestatusid'],
            'accreditationid' => $collegecourse['accreditationid'],
            'ownershipid' => $collegecourse['ownershipid'],
            'totalfees' => $collegecourse['totalfees'],
            'eligibility' => $collegecourse['eligibility'],
            'coursestructure' => $collegecourse['coursestructure'],
            'enabled' => $collegecourse['enabled']
        );

        $this->db->where('course_association_id',$id);
        $this->db->update('tbl_clg_crs_spec_association',$collegecoursedata);

        return true;
    }

    public function getallasccoursesdata() {
            $query = $this->db->query('select tc.*, tca.id as tcaid, tca.enabled as ascenabled, ts.name as specializationname from tbl_course tc inner join tbl_clg_crs_spec_association tac on tc.id=tac.courseid inner join tbl_college_association tca on tac.course_association_id=tca.id inner join tbl_specialization ts on tac.specializationid=ts.id where tc.enabled=1 and tc.deleted=0 and tca.deleted=0');
            
            if($query->num_rows() > 0)
            {
                $data=$query->result();
                return $data;
            }
            else
            {
                return FALSgetAllProfessionalCouncilsE;
            }
    }

    /*============ START JAINENDRA's FUNCTIONS's ===============*/
    


   /*============== fucntion for get userinfo ===========*/

   public function getUserInfo($user_id)
   {
     
      $userInfo = $this->db->select('*')
                           ->from('tbl_admin')
                           ->where('id',$user_id)
                           ->get()->row();
              
                      return $userInfo;



   }


   /*======== function for getAllProfessionalCouncils =========*/

    public function getAllProfessionalCouncils()
    {

        $professionalCouncils = $this->db->select('*')
                                     ->from('mst_professional_councils')
                                     ->where('isEnable',0)
                                     ->get()->result();

                                    return $professionalCouncils;



    }


/*function for get all countries*/
 
 public function getAllCountries()
 {

     $countries = $this->db->select('*')
                           ->from('mst_countries')
                           ->where('id',101)
                           ->get()->row();

                           return $countries;

         }


         /*function for getAllStates*/

         public function getAllStates()
         {

           $states = $this->db->select('*')
                              ->from('mst_states')
                              ->where('country_id',101)
                              ->get()->result();

                              return $states;
         }


         /*function for save unioversity logo*/

         public function saveUniversityLogo($fileName,$user_id)
         {

             $data = array(
                   
                   'user_id' => $user_id,
                   'logo_name' => $fileName
                  );
       
       //echo "<pre>"; print_r($data); exit;
           $saveLo = $this->db->insert('tbl_university_logo',$data);
                      

         }

         /*function for save banner image */

         public function saveUniversityBannerImage($fileName,$user_id,$universityname)
         {

            $data = array(

            //  'user_id' => $user_id,
                'name' => $universityname,
                'banner_img' => $fileName,
                );

            $saveBanner = $this->db->insert('tbl_university',$data);
                $last_id = $this->db->insert_id();
          }


    /*function for save university info */
    public function saveUniversityInfo($universityInfo)
    {

        $saved = $this->db->insert('tbl_university',$universityInfo);

           return $saved;


    }

    /*function for save college info */

    public function saveCollegeInfo($collegeInfo)
    {

     $saved = $this->db->insert('tbl_college',$collegeInfo);

      return $saved;

    }




    /*function for get getAccreditions*/
    public function getAccreditions()
    {


      $accreditation = $this->db->select('*')
                                ->from('tbl_accreditation')
                                ->get()->result(); 

                                return $accreditation;

    }

    /*function for get state cities*/

    public function getStateCities($stateName)
    {


    $cities = $this->db->select('*')
                            ->from('mst_cities')
                            ->where('city_state',$stateName)
                            ->get()->result();


     // echo "<pre>"; print_r($cities); exit;
                            return $cities;


    }

    /*function for university saveGalleryImages*/

    public function saveGalleryImages($universityname,$fileName)
    {
      
      $data = array(
                
                'university_name' => $universityname,
                'image_name' => $fileName
                 );

             $saveImages = $this->db->insert('tbl_university_gallery',$data);

              return $saveImages;

    }


  /*function for add college galleryImages*/

  public function saveCollegeGalleryImages($collegeName,$fileName)
  {
      
           $data = array(
                
                'college_name' => $collegeName,
                'image_name' => $fileName
                 );

             $saveImages = $this->db->insert('tbl_college_gallery',$data);

              return $saveImages;


  }

   

    /*function for getUniversityCategory*/

    public function getUniversityCategory()
    {
        
        $categories = $this->db->select('*')
                              ->from('tbl_university_type')
                              ->get()->result();

                              return $categories;
    }


    /*function for getAllUniversities*/
    public function getAllUniversities()
    {
     $universities = $this->db->select('*')
                                    ->from('tbl_university')
                                    ->where('enabled','1')
                                    ->where('deleted',0)
                                    ->order_by('id','DESC')
                                    ->get()->result();

                                    return $universities;
    }


    /*function for get programs*/
    public function getProgramType()
    {

         $programTypes = $this->db->select('*')
                                  ->from('mst_course_type')
                                  ->where('isEnable',1)
                                   ->get()->result();

                                  return $programTypes;
    }



/*function for getStreams*/

public function getStream()
{

  $streams = $this->db->select('*')
                      ->from('mst_streams')
                      ->get()->result();

                      return $streams;


}

    /*function for save master courses*/

    public function saveMasterCourses($data)
    {

     
     $saveCourse = $this->db->insert('mst_courses',$data);

            return $saveCourse;



    }

  /*function for updateMasterCourses*/
  public function updateMasterCourses($data,$mastercourseId)
  {
     
    $update = $this->db->where('id',$mastercourseId)
                       ->update('mst_courses',$data);


  }

    /*function for update college info */
    public function updateCollegeInfo($collegeId,$collegeInfo)
    {

        $updateCollegeInfo  = $this->db->where('id',$collegeId)
                                       ->update('tbl_college',$collegeInfo);

                                 //      echo "<pre>"; print_r($this->db->last_query()); exit;

                      return $updateCollegeInfo;


    }


    /*function for get all colleges*/

    public function getAllColleges()
    {

     $colleges = $this->db->select('tbl_college.*,tbl_university.name as universityName')
                                    ->from('tbl_college')
                                    ->join('tbl_university','tbl_college.universityid=tbl_university.id')
                                    ->where('tbl_college.deleted',0)
                                    ->get()->result();
                              return $colleges;
                                   

    }



    /*function for get all master courses*/

    public function getAllMasterCourses()
    {


       $masterCourses = $this->db->select('*')
                                 ->from('mst_courses')
                                 ->get()->result();

                                 return $masterCourses;

    }


    /*function for getCollegeByUniversityID*/
    public function getCollegeByUniversityID($universityId)
    {
              
              $collegeInfo = $this->db->select('*')
                                      ->from('tbl_college')
                                      ->where('universityid',$universityId)
                                      ->get()->result();
                    
                     return $collegeInfo;
                                     // echo "<pre>"; print_r($collegeInfo); exit();

    }




    /*function for saveUniversitiesCollegesCourses*/

    public function saveUniversitiesCollegesCourses($data)
    {
       
       $save = $this->db->insert('tbl_universities_colleges_courses',$data);

           return $save;



    }


    /*function for  updateUniversityInfo*/

    public function updateUniversityInfo($universityInfo,$universityId)
    {

      //  echo $universityId; 
      
     //echo "<pre>"; print_r($universityInfo); exit;

        $updateUniversityInfo = $this->db->where('id',$universityId)
                                         ->update('tbl_university',$universityInfo);

                                         return $updateUniversityInfo;
    }



    /*function for saveProgramType*/
    public function saveProgramType($data)
    {
      
       $save = $this->db->insert('mst_course_type',$data);

           return  $save;
    }


    /*function for save saveStreams*/
    public function saveStreams($data)
    {
    $save = $this->db->insert('mst_streams',$data);

        return $save;
    }


    /*funtion for getCollegeCourses*/
    public function getCollegeCourses($colgId)
    {

      $collegesCourses = $this->db->select('tbl_universities_colleges_courses.college_id,tbl_university.id as universityId,tbl_universities_colleges_courses.id,tbl_university.name as uniName,tbl_college.name as colgName,tbl_universities_colleges_courses.course_name,tbl_universities_colleges_courses.course_specialization,tbl_universities_colleges_courses.course_shortname,mst_courses.course_stream,mst_courses.course_type')
                                  ->from('tbl_universities_colleges_courses')  
                                  ->join('mst_courses','mst_courses.course_name=tbl_universities_colleges_courses.course_name','left') 
                                  ->join('tbl_college','tbl_universities_colleges_courses.college_id=tbl_college.id')
                                  ->join('tbl_university','tbl_college.universityid=tbl_university.id')
                                  ->where('tbl_universities_colleges_courses.college_id',$colgId)
                                //  ->group_by('tbl_university.name')
                                  ->get()->result(); 

                                return $collegesCourses;
                                

    }
    

    /*function addedCollegeCourses*/

    public function addedCollegeCourses($colgId)
    {

     $courses = $this->db->select('mst_courses.course_type,mst_courses.course_stream,tbl_universities_colleges_courses.course_name,tbl_universities_colleges_courses.course_name,tbl_universities_colleges_courses.course_specialization,tbl_universities_colleges_courses.course_shortname')
                         ->from('tbl_universities_colleges_courses')
                         ->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name')
                         ->where('tbl_universities_colleges_courses.college_id',$colgId)
                         ->get()->result();
                         return $courses;


    }






    /*function for getAllUniversitiesColleges*/

    public function getAllUniversitiesColleges($universityId)
    {
       
      // echo $universityId; exit;
      $colleges = $this->db->select('*')
                           ->from('tbl_college')
                           ->where('universityid',$universityId)
                           ->get()->result();
 
                          // echo "<pre>"; print_r($this->db->last_query()); exit;
 
                        return $colleges;



    }

    /*function for getCourseDuration*/

    public function getCourseDuration()
    {

       $coursesDuartion = $this->db->select('*')
                                   ->from('mst_course_duration')
                                   ->get()->result();

                                   return $coursesDuartion;


    }

    /*function for getModeofStudy*/

    public function getModeofStudy()
    {
        $modes = $this->db->select('*')
                           ->from('tbl_modeofstudy')
                           ->get()->result();

                           return $modes;
    }


    /*function for get all course info*/
  public function getAllCourseInfo($id)
  {
      
     // echo $id; exit;
 
      $allCourseInfo =  $this->db->select('tbl_universities_colleges_courses.*,mst_courses.course_type,mst_courses.course_stream,tbl_college.name as colgName,tbl_university.name as universityName')
                         ->from('tbl_universities_colleges_courses')
                         ->join('tbl_college','tbl_universities_colleges_courses.college_id=tbl_college.id')
                         ->join('tbl_university','tbl_college.universityid=tbl_university.id')
                         ->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name')
                         ->where('tbl_universities_colleges_courses.id',$id)
                         ->get()->row();

                            
                      return $allCourseInfo;

                         // echo "<pre>"; print_r($allCourseInfo); exit;



  }



  /*function for updateCollegeCourseInfo*/

  public function updateCollegeCourseInfo($id,$data)
  {

    //echo "<pre>here"; print_r($data); exit;
     $updateCourseInfo = $this->db->where('id',$id)
                                 ->update('tbl_universities_colleges_courses',$data);

                                 return $updateCourseInfo;

  }


    /*function for delete course*/

      public function deleteCourse($deleteId)
      {
            
        

       $delCourse = $this->db->where('id',$deleteId)
                             ->delete('tbl_universities_colleges_courses');
 
 //echo "<pre>"; print_r($this->db->last_query()); exit;
                             return $delCourse;




      }

  

}

?>