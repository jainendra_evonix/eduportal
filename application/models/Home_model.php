<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // function for register user

    public function registerUser($userInfo) {

        $saveData = $this->db->insert('tbl_user', $userInfo);

        return $saveData;
    }

    /* ========= FUNCTION FOR ACTIVATE LOGIN ======== */

    public function activateLogin($data, $token) {

        $status = $this->db->where('activation_token', $token)
                ->update('tbl_user', $data);

        return $status;
    }
    
    
    
    public function checkLogin($email,$password)
    {
        
       
        $matchpass = md5($password);
        // echo $email.'  '.$matchpass; exit;
        $userData = $this->db->select('*')
                            ->from('tbl_user')
                            ->where('user_email',$email)
                            ->where('user_password',$password)
                            ->where('flag',1)
                            ->get()->row();
             
        
           return $userData;
        
    }
   
   /*function for get all top university*/
    public function getTopUniversities() {


        $universities = $this->db->select('*')
                        ->from('tbl_university')
                        ->where('enabled', 1)
                        ->order_by('rand()')
                        ->limit(10)
                        ->get()->result();


        return $universities;
        //  echo "<pre>"; print_r($universities); exit();
    }
   
   /*function for get top colleges*/

   public function getTopColleges()
   {
        $colleges = $this->db->select('*')
                              ->from('tbl_university')
                               ->where('enable',1)
                               ->order_by('rand()')
                               ->limit(10)
                               ->get()->result();

                               return $colleges;

   } 


    public function getCities(){  

        $cities = $this->db->select('id,city')
                        ->from('tbl_university')
                        ->order_by('city','asc')
                        ->group_by('city')
                        ->get()->result();

        // print_r($this->db->last_query()); exit;

        return $cities;
    }

    public function getCollegesCities()
    {
      
       $cities = $this->db->select('id,city')
                        ->from('tbl_college')
                        ->order_by('city','asc')
                        ->group_by('city')
                        ->get()->result();

        // print_r($this->db->last_query()); exit;

        return $cities;

    }

    /* function for get all courses list */

    public function getCourses() {

        $courses = $this->db->select('*')
                        ->from('mst_course_type')
                        ->order_by('course_type')
                        ->get()->result();

        return $courses;
    }

    /*function for get all courses name*/
    
    public function getCoursesName()
    {
       $courses = $this->db->select('*')
                        ->from('mst_courses')
                        ->order_by('course_name')
                        ->get()->result();

        return $courses;
    }
    /* =============function for get all universities=========== */

    public function getAllUniversity() {
      


        $universities = $this->db->select('tbl_university.id as uId, tbl_university.name as uniName,tbl_university.city,tbl_university.state,tbl_university.country,tbl_university.logo_img,tbl_university.banner_img,tbl_university.approved_by,tbl_university.accredited_name,tbl_university.brochure_name')
                        ->from('tbl_university')
                        ->where('deleted',0)
                        ->get()->result();

        return $universities;

        //  echo "<pre>"; print_r($universities); exit();
    }


    /*==============function for get all colleges============*/
    public function getAllColleges($universityId)
    {

      $colleges = $this->db->select('tbl_university.id as uId, tbl_university.name as uniName,tbl_college.id as cId, tbl_college.name as colgName,tbl_college.city,tbl_college.stateName,tbl_college.country,tbl_college.logoname,tbl_college.bannerName,tbl_college.approvedby,tbl_college.accreditions,tbl_college.brochure_name,mpc.g_name')
                        ->join('tbl_university','tbl_college.universityid=tbl_university.id')
                        ->join('mst_professional_councils mpc','tbl_college.approvedby=mpc.id')
                        ->from('tbl_college')
                        ->where('tbl_college.universityid',$universityId)
                        ->where('tbl_college.deleted',0)
                        ->get()->result(); 

                        return $colleges;
     //echo "<pre>"; print_r($colleges); exit();         
  
    }

    /* ============== function for getSpecialization  =============== */
    public function getSpecialization()
    {
        $specialization = $this->db->select('*')
                                   ->from('mst_courses')
                                   ->get()->result();
        
              return $specialization;
        
    }
    
    public function searchLocation(){
         if (!empty($_GET)){

            //  echo "<pre>"; print_r($_GET); exit;
            $query = 'select * from tbl_university where city  IN ';

            foreach ($_GET['location'] as $key) {

                $query .= "city='$key'";

                $query .= ' OR ';
            }

            $q = rtrim($query, ' OR ');

            $result = $this->db->query($q);
            echo $this->db->last_query();
            exit;
            $searchedResult = $result->result();

            //  echo "<pre>"; print_r($searchedResult); exit;


            return $searchedResult;
        }
    }

    /* ===== function commonSearch ====== */
        
            // [uId] => 10
            // [uniName] => Sharada University
            // [city] => Andaman and Nicobar Islands
            // [state] => Uttar Pradesh
            // [country] => India
            // [logo_img] => Sharda_University-Webp_net-compress-image.jpg
            // [banner_img] => Sharada_University-banner2.jpg
            // [approved_by] => UGC
            // [accredited_name] => NAAC Grade A
            // [brochure_name] => 

 // function for home search 

    public function searchColleges($searched)
    {
             
/*
             $colleges = $this->db->select('*')
                                  ->from('tbl_college')
                                  ->where('tbl_college.name LIKE',$searched)
                                  ->get()->result();

                                  echo "<pre>"; print_r($this->db->last_query()); exit;*/

                                  $this->db->select('tbl_university.id as uniId,tbl_university.name as uniName,tbl_college.id as cId, tbl_college.name as colgName,tbl_college.city,tbl_college.stateName,tbl_college.country,tbl_college.logoname,tbl_college.bannerName,tbl_college.approvedby,tbl_college.accreditions,tbl_college.brochure_name,mpc.g_name');
                                  $this->db->join('tbl_university','tbl_college.universityid=tbl_university.id');
                                  $this->db->join('mst_professional_councils mpc','tbl_college.approvedby=mpc.id');
                                  $this->db->join('tbl_universities_colleges_courses','tbl_college.id=tbl_universities_colleges_courses.college_id');
                                  $this->db->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name');
                                  $this->db->join('mst_course_type','mst_courses.course_type=mst_course_type.course_type');
                                  $this->db->where('tbl_college.name LIKE','%'.$searched.'%'); 
                                  $this->db->group_by('tbl_college.id');
                                 $query = $this->db->get('tbl_college');

                        //  echo "<pre>"; print_r($this->db->last_query()); exit;


                       
                        if($query->num_rows() > 0){
                           // echo 1; exit;
                         $searches = $query->result();

                       //  echo "<pre>"; print_r($searches); exit;
                         return $searches; 
                        } else {
                            return FALSE;
                        }

    }


    /*function for search universites*/

    public function searchUniversites($searched)
    {


        $this->db->select('tbl_university.id as uId, tbl_university.name as uniName,tbl_university.city,tbl_university.state,tbl_university.country,tbl_university.logo_img,tbl_university.banner_img,tbl_university.approved_by,tbl_university.accredited_name,tbl_university.brochure_name');
                           $this->db->join('tbl_universities_colleges_courses','tbl_university.id=tbl_universities_colleges_courses.university_id');
                            $this->db->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name');
                            $this->db->join('mst_course_type','mst_courses.course_type=mst_course_type.course_type');
                             $this->db->join('tbl_universities_colleges_courses tutcc','tbl_university.id=tutcc.university_id');
                            $this->db->join('mst_courses mc','tutcc.course_name=mc.course_name');
                            $this->db->join('mst_course_type mct','mc.course_type=mct.course_type');
                             $this->db->where('tbl_university.name LIKE','%'.$searched.'%'); 
                             $this->db->group_by('tbl_university.id');
                             $query = $this->db->get('tbl_university');
                        // echo "<pre>"; print_r($this->db->last_query()); exit;

                        if($query->num_rows() > 0){
                           // echo 1; exit;
                         $searches = $query->result();

                       //  echo "<pre>"; print_r($searches); exit;
                         return $searches; 
                        } else {
                            return FALSE;
                        }

    }

// function home search universities

    public function homeSearchUniversityResult($searched)
    {
               
                 $this->db->select('tbl_university.id as uId, tbl_university.name as uniName,tbl_university.city,tbl_university.state,tbl_university.country,tbl_university.logo_img,tbl_university.banner_img,tbl_university.approved_by,tbl_university.accredited_name,tbl_university.brochure_name');
                           $this->db->join('tbl_universities_colleges_courses','tbl_university.id=tbl_universities_colleges_courses.university_id');
                            $this->db->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name');
                            $this->db->join('mst_course_type','mst_courses.course_type=mst_course_type.course_type');
                             $this->db->join('tbl_universities_colleges_courses tutcc','tbl_university.id=tutcc.university_id');
                            $this->db->join('mst_courses mc','tutcc.course_name=mc.course_name');
                            $this->db->join('mst_course_type mct','mc.course_type=mct.course_type');

                            if($searched)
                            {
                               $this->db->where('tbl_university.name LIKE','%'.$searched.'%'); 
                            }
                            $this->db->group_by('tbl_university.id');
                             $query = $this->db->get('tbl_university');
                      //  echo "<pre>"; print_r($this->db->last_query()); exit;

                        if($query->num_rows() > 0){
                           // echo 1; exit;
                         $searches = $query->result();

                      //  echo "<pre>"; print_r($searches); exit;
                         return $searches; 
                        } else {
                            return FALSE;
                        }
 
    }



    // function for home search colleges
     
     public function homeSearchCollegesResult($searched)
     {
           
                        $this->db->select('tbl_university.id as uniId,tbl_university.name as uniName,tbl_college.id as cId, tbl_college.name as colgName,tbl_college.city,tbl_college.stateName,tbl_college.country,tbl_college.logoname,tbl_college.bannerName,tbl_college.approvedby,tbl_college.accreditions,tbl_college.brochure_name,mpc.g_name');
                                  $this->db->join('tbl_university','tbl_college.universityid=tbl_university.id');
                                  $this->db->join('mst_professional_councils mpc','tbl_college.approvedby=mpc.id');
                                  $this->db->join('tbl_universities_colleges_courses','tbl_college.id=tbl_universities_colleges_courses.college_id');
                                  $this->db->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name');
                                  $this->db->join('mst_course_type','mst_courses.course_type=mst_course_type.course_type');
                                  $this->db->where('tbl_college.name LIKE','%'.$searched.'%'); 
                                  $this->db->group_by('tbl_college.id');
                                 $query = $this->db->get('tbl_college');

                        //  echo "<pre>"; print_r($this->db->last_query()); exit;


                       
                        if($query->num_rows() > 0){
                           // echo 1; exit;
                         $searches = $query->result();

                       // echo "<pre>"; print_r($searches); exit;
                         return $searches; 
                        } else {
                            return FALSE;
                        }


     }


    public function commonSearch($location,$courses,$specialization)
    {
       // echo "<pre>"; print_r($specialization); exit;

         $location = str_replace('-',' ',$location);

                           $this->db->select('tbl_university.id as uId, tbl_university.name as uniName,tbl_university.city,tbl_university.state,tbl_university.country,tbl_university.logo_img,tbl_university.banner_img,tbl_university.approved_by,tbl_university.accredited_name,tbl_university.brochure_name');
                           $this->db->join('tbl_universities_colleges_courses','tbl_university.id=tbl_universities_colleges_courses.university_id');
                            $this->db->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name');
                            $this->db->join('mst_course_type','mst_courses.course_type=mst_course_type.course_type');
                             $this->db->join('tbl_universities_colleges_courses tutcc','tbl_university.id=tutcc.university_id');
                            $this->db->join('mst_courses mc','tutcc.course_name=mc.course_name');
                            $this->db->join('mst_course_type mct','mc.course_type=mct.course_type');
                            

                       if(!empty($location)){

                           
                           $this->db->where_in('tbl_university.city',$location);        
                        }
                        if(!empty($courses))
                          {
                            $this->db->where_in('mst_course_type.course_type',$courses); 
                           
                          }

                        if(!empty($specialization)){

                               $this->db->where_in('mc.course_name',$specialization); 
                             }

                      
                               $this->db->group_by('tbl_university.id');
                              $query = $this->db->get('tbl_university');


                      //  echo "<pre>"; print_r($this->db->last_query()); exit;
                        if($query->num_rows() > 0){
                           // echo 1; exit;
                         $searches = $query->result();

                        // echo "<pre>"; print_r($searches); exit;
                         return $searches; 
                        } else {
                            return FALSE;
                        }
             
   } 
    
    

// function for inner search 
   public function innerCommonSearch($location,$courses,$specialization,$universityId)
   {

          
          //echo 'search'; exit;

         $location = str_replace('-',' ',$location);

          $this->db->select('tbl_university.id as uniId,tbl_university.name as uniName,tbl_college.id as cId, tbl_college.name as colgName,tbl_college.city,tbl_college.stateName,tbl_college.country,tbl_college.logoname,tbl_college.bannerName,tbl_college.approvedby,tbl_college.accreditions,tbl_college.brochure_name,mpc.g_name');
              $this->db->join('tbl_university','tbl_college.universityid=tbl_university.id');
              $this->db->join('mst_professional_councils mpc','tbl_college.approvedby=mpc.id');
              $this->db->join('tbl_universities_colleges_courses','tbl_college.id=tbl_universities_colleges_courses.college_id');
              $this->db->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name');
              $this->db->join('mst_course_type','mst_courses.course_type=mst_course_type.course_type');
              
                       if(!empty($location)){
                           $this->db->where_in('tbl_college.city',$location);   
                        }
                       if(!empty($courses))
                        {
                            
                            
                            $this->db->where_in('mst_course_type.course_type',$courses); 
               
                        }
                          
                        if(!empty($specialization)){
                         
                          $this->db->where_in('mc.course_name',$specialization); 
                        }

                      $this->db->where('tbl_college.universityid',$universityId); 
                      $this->db->group_by('tbl_college.id');
                      $query = $this->db->get('tbl_college');

                    // echo "<pre>"; print_r($this->db->last_query()); exit;


                       
                        if($query->num_rows() > 0){
                           // echo 1; exit;
                         $searches = $query->result();

                       //  echo "<pre>"; print_r($searches); exit;
                         return $searches; 
                        } else {
                            return FALSE;
                        }



   }
  // function for search colleges by courses,locations and sterams

   public function commonSearchColleges($location,$courses,$specialization)
   {           
       //echo 'esle'; exit();


              $location = str_replace('-',' ',$location);

          $this->db->select('tbl_university.id as uniId,tbl_university.name as uniName,tbl_college.id as cId, tbl_college.name as colgName,tbl_college.city,tbl_college.stateName,tbl_college.country,tbl_college.logoname,tbl_college.bannerName,tbl_college.approvedby,tbl_college.accreditions,tbl_college.brochure_name,mpc.g_name');
              $this->db->join('tbl_university','tbl_college.universityid=tbl_university.id');
              $this->db->join('mst_professional_councils mpc','tbl_college.approvedby=mpc.id');
              $this->db->join('tbl_universities_colleges_courses','tbl_college.id=tbl_universities_colleges_courses.college_id');
              $this->db->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name');
              $this->db->join('mst_course_type','mst_courses.course_type=mst_course_type.course_type');
              
                       if(!empty($location)){

                           $this->db->where_in('tbl_college.city',$location);   
                        }
                       if(!empty($courses))
                        {
                            $this->db->where_in('mst_course_type.course_type',$courses); 
               
                        }
                          
                        if(!empty($specialization)){
                         
                          $this->db->where_in('mc.course_name',$specialization); 
                        }

                     
                      $this->db->group_by('tbl_college.id');
                      $query = $this->db->get('tbl_college');

                 // echo "<pre>"; print_r($this->db->last_query()); exit;


                       
                        if($query->num_rows() > 0){
                           // echo 1; exit;
                         $searches = $query->result();

                       //  echo "<pre>"; print_r($searches); exit;
                         return $searches; 
                        } else {
                            return FALSE;
                        }
   }



 // function for search college courses

   public function searchCollegeCourses($location,$courses,$specialization,$colgId)
   {   


                         
                            $this->db->select('`tucc`.*, `mc`.*, `tucc`.`id`');
                            $this->db->join('mst_courses mc','tucc.course_name=mc.course_name');
              
                       if(!empty($courses))
                        {
                            
                          
                            $this->db->where_in('mc.course_type',$courses); 
               
                        }
                          
                        if(!empty($specialization)){

                          $this->db->where_in('mc.course_name',$specialization); 
                        }

                      $this->db->where('tucc.college_id',$colgId); 
                     
                      $query = $this->db->get('tbl_universities_colleges_courses tucc');

                        // echo "<pre>"; print_r($this->db->last_query()); exit;
                        if($query->num_rows() > 0){
                           // echo 1; exit;
                         $searches = $query->result();

                       // echo "<pre>"; print_r($searches); exit;
                         return $searches; 
                        } else {
                            return FALSE;
                        }


   }
   
   //function for search colleges of university by streams

  public function searchCollegeStream($location,$courses,$specialization,$universityId)
  {                             


                             $this->db->select('tu.id as uniId, tu.name as uniName,tu.logo_img,tu.banner_img,tu.city as ucity,tu.state as ustate,tu.approved_by as uapproved,tu.accredited_name as uaccredited,tc.id as colgId, tc.name as colgName,mc.course_name,tc.logoname,tc.bannerName,tc.brochure_name,tc.type,tc.accreditions as caccreditions,tc.approvedby as capprovedby,tc.city as ccity,tc.stateName as cstate,mpc.g_name');
                           // $this->db->from('tbl_universities_colleges_courses tucc');
                           $this->db->join('tbl_college tc','tucc.college_id=tc.id');
                           $this->db->join('tbl_university tu','tucc.university_id=tu.id');
                           $this->db->join('mst_professional_councils mpc','tc.approvedby=mpc.id');
                           $this->db->join('mst_courses mc','tucc.course_name=mc.course_name');
                          
               
                       if(!empty($courses))
                        {
                            
                          
                            $this->db->where_in('mc.course_type',$courses); 
               
                        }
                          
                        if(!empty($specialization)){

                          $this->db->where_in('mc.course_name',$specialization); 
                        }

                      $this->db->where('tucc.university_id',$universityId); 

                           $this->db->group_by('tucc.college_id');
                     
                      $query = $this->db->get('tbl_universities_colleges_courses tucc');

                        // echo "<pre>"; print_r($this->db->last_query()); exit;
                        if($query->num_rows() > 0){
                           // echo 1; exit;
                         $searches = $query->result();

                     // echo "<pre>here"; print_r($searches); exit;
                         return $searches; 
                        } else {
                            return FALSE;
                        }


  }



    
    // function for get university info
    
    public function getUniversityInfo($id)
    {
       //  echo "model";  exit;
        $uniInfo = $this->db->select('*')
                            ->from('tbl_university')
                            ->where('id',$id)
                            ->get()->row();
                            
                    return $uniInfo;
                            
        
       
        
    }
    
    // fuinction for getAllUniversityCourses
    
    public function getAllUniversityCourses($id)
    {

             $universityCourses = $this->db->select('mst_courses.course_name,mst_courses.course_type,mst_courses.course_stream,tbl_universities_colleges_courses.course_shortname,tbl_universities_colleges_courses.id as tblUniColgCourseId')
                              ->from('tbl_universities_colleges_courses')
                              ->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name')
                              ->where('tbl_universities_colleges_courses.university_id',$id)
                              ->group_by('tbl_universities_colleges_courses.course_name')
                              ->get()->result();
                 //echo "<pre>"; print_r($universityCourses); exit;
                           return $universityCourses;

       //   
                             
             return $allCourses;
        
    }
    
    // function for getCollegesofUni 
    public function getCollegesofUni($id)
    {
        $allColleges = $this->db->select('*')
                               ->from('tbl_college')
                               ->where('universityid',$id)
                               ->get()->result();

                            //   echo "<pre>"; print_r($this->db->last_query()); exit;
        
          return $allColleges;
                               
        
    }


    //function for get course info

    public function getAllCourseInfo($lid)
    {
      

      $courseInfo = $this->db->select('tbl_universities_colleges_courses.course_name,tbl_college.name as colgName,tbl_university.name as universityName')
                               ->from('tbl_universities_colleges_courses')
                           //    ->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name')
                               ->join('tbl_universities_colleges_courses','tbl_universities_colleges_courses.college_id=tbl_college.id')
                               ->join('tbl_university','tbl_university.id=tbl_college.universityid')
                               ->where('tbl_universities_colleges_courses.id',$lid)
                               ->get()->row();

                               echo "<pre>"; print_r($this->db->last_query()); exit;
                              
                            echo "<pre>"; print_r($courseInfo); exit;

                           return $courseInfo;
                            //  echo "<pre>"; print_r($courseInfo); exit;

    }



    /*function for  getAllStreams*/

   
    public function getAllStreams()
    {

        $streams = $this->db->select('*')
                            ->from('mst_courses')
                            ->get()->result();

                            return $streams;
  

    }


    /*function for get all college info*/

    public function getCollgeInfo($id)
    {
     
     //echo $colgId; exit;

          $collegeInfo = $this->db->select('tbl_university.name as universityName,tbl_college.*,mst_professional_councils.g_name')
                                   ->from('tbl_college')
                                   ->join('tbl_university','tbl_college.universityid=tbl_university.id') 
                                   ->join('mst_professional_councils','tbl_college.approvedby=mst_professional_councils.id')
                                   ->where('tbl_college.id',$id)
                                   ->get()->row();
                                    
                                  return $collegeInfo;

                                 // echo "<pre>"; print_r($this->db->last_query()); exit;


    }


    // function for get all colleges
    public function getAllCollege()
    {

           
          $this->db->select('tbl_university.id as uniId,tbl_university.name as uniName,tbl_college.id as cId, tbl_college.name as colgName,tbl_college.city,tbl_college.stateName,tbl_college.country,tbl_college.logoname,tbl_college.bannerName,tbl_college.approvedby,tbl_college.accreditions,tbl_college.brochure_name,mpc.g_name');
              $this->db->join('tbl_university','tbl_college.universityid=tbl_university.id');
              $this->db->join('mst_professional_councils mpc','tbl_college.approvedby=mpc.id');
              $this->db->join('tbl_universities_colleges_courses','tbl_college.id=tbl_universities_colleges_courses.college_id');
              $this->db->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name');
              $this->db->join('mst_course_type','mst_courses.course_type=mst_course_type.course_type');
                       $this->db->group_by('tbl_college.id');
                      $query = $this->db->get('tbl_college');

                 // echo "<pre>"; print_r($this->db->last_query()); exit;


                       
                        if($query->num_rows() > 0){
                           // echo 1; exit;
                         $searches = $query->result();

                       //  echo "<pre>"; print_r($searches); exit;
                         return $searches; 
                        } else {
                            return FALSE;
                        }
                                    
                                  return $colleges;
    }

  // function for  getCollgeofUniversity

    public function getCollgeofUniversity($universityId)
    {
        
        $collegeInfo = $this->db->select('tbl_university.name as universityName,tbl_college.*,mst_professional_councils.g_name')
                                   ->from('tbl_college')
                                   ->join('tbl_university','tbl_college.universityid=tbl_university.id') 
                                   ->join('mst_professional_councils','tbl_college.approvedby=mst_professional_councils.id')
                                   ->where('tbl_university.id',$universityId)
                                   ->get()->row();
                                    
                                  return $collegeInfo;

    }

    /*function for getAllCollegeCourses*/

    public function getAllCollegeCourses($colgId)
    {
        
        //echo $colgId; exit;
      $colgCourses = $this->db->select('mst_courses.course_name,mst_courses.course_type,tbl_universities_colleges_courses.course_shortname,tbl_universities_colleges_courses.id as tblUniColgCourseId')
                              ->from('tbl_universities_colleges_courses')
                              ->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name')
                              ->where('tbl_universities_colleges_courses.college_id',$colgId)
                              ->group_by('tbl_universities_colleges_courses.course_name')
                              ->get()->result();

                              return $colgCourses;

                              // echo "<pre>"; print_r($colgCourses); exit;



    }


    /*function for getAllCollegeGalleryImages*/

    public function getAllCollegeGalleryImages($colgId)
    {
              // echo $colgId;exit;
              
         $galleryIamges = $this->db->select('tbl_college_gallery.*')
                                   ->from('tbl_college_gallery')
                                   ->join('tbl_college','tbl_college.name=tbl_college_gallery.college_name')
                                   ->where('tbl_college.id',$colgId)
                                   ->get()->result();
                                   // echo "<pre>"; print_r($galleryIamges); exit;
                                   return $galleryIamges;
                                  // echo "<pre>"; print_r($galleryIamges); exit;


    }


   /*function for getAllCollegeCoursesStreams*/

   public function getAllCollegeCoursesStreams($colgId)
   {

          $colgStreams = $this->db->select('mst_courses.course_stream,tbl_universities_colleges_courses.college_id')
                                  ->from('tbl_universities_colleges_courses')
                                  ->join('mst_courses','tbl_universities_colleges_courses.course_name=mst_courses.course_name')
                                  ->where('tbl_universities_colleges_courses.college_id',$colgId)
                                  ->group_by('mst_courses.course_stream')
                                  ->get()->result();
                              
                           //   echo "<pre>"; print_r($colgStreams); exit;
                              return $colgStreams;

   }


/*function for getAllCollegesCourses*/

public function getAllCollegesCourses($courseName,$cid)
{
    
  // echo $courseName; exit;


    $collegeCourses = $this->db->select('tucc.*,mc.*,tucc.id')
                               ->from('tbl_universities_colleges_courses tucc')
                               ->join('mst_courses mc','tucc.course_name=mc.course_name')
                               ->where('tucc.college_id',$cid)
                               ->where('tucc.course_name',$courseName)
                               ->get()->result();
                                
                                return $collegeCourses;
                               //echo "<pre>"; print_r($this->db->last_query()); exit;

}



public function getAllCollegesStreams($streamName,$collegeId)
{
    
    //echo $streamName." ".$collegeId; exit;

    $collegeCourses = $this->db->select('tucc.*,mc.*,tucc.id')
                               ->from('tbl_universities_colleges_courses tucc')
                               ->join('mst_courses mc','tucc.course_name=mc.course_name')
                               ->where('tucc.college_id',$collegeId)
                               ->where('mc.course_stream',$streamName)
                               ->get()->result();
                                

                               // echo "<pre>"; print_r($collegeCourses); exit;
                                return $collegeCourses;

}




/*function for view all course info*/
 
 public function getCourseInfo($cid)
 {

   $courseInfo = $this->db->select('tucc.*,mst_courses.course_type,mst_courses.course_stream,tc.name as colgName,tc.logoname as colgLogo,tc.address as colgAddress,tc.city as colgCity,tc.stateName,mpc.g_name,tu.name as universityName,tu.id as uniId,tc.url,tu.email,tc.country,tc.contactNo,tc.faxno,tc.address as colgAdd')
                          ->from('tbl_universities_colleges_courses tucc')
                          ->join('mst_courses','tucc.course_name=mst_courses.course_name')
                          ->join('tbl_college tc','tucc.college_id=tc.id')
                          ->join('mst_professional_councils mpc','tc.approvedby=mpc.id')
                          ->join('tbl_university tu','tc.universityid=tu.id')
                          ->where('tucc.id',$cid)
                          ->get()->row();
                          
                          return $courseInfo;
                        //  echo "<pre>"; print_r($courseInfo); exit;

 }


 /*function for getAllCollegsByCourse*/
 public function getAllCollegsByCourse($uniId,$courseName)
 {

//  echo $courseName; exit;

      $colleges = $this->db->select('tu.id as uniId, tu.name as uniName,tu.logo_img,tu.banner_img,tu.city as ucity,tu.state as ustate,tu.approved_by as uapproved,tu.accredited_name as uaccredited,tc.id as colgId, tc.name as colgName,mc.course_name,tc.logoname,tc.bannerName,tc.brochure_name,tc.type,tc.accreditions as caccreditions,tc.approvedby as capprovedby,tc.city as ccity,tc.stateName as cstate,mpc.g_name')
                           ->from('tbl_universities_colleges_courses tucc')
                           ->join('tbl_college tc','tucc.college_id=tc.id')
                           ->join('tbl_university tu','tucc.university_id=tu.id')
                           ->join('mst_professional_councils mpc','tc.approvedby=mpc.id')
                           ->join('mst_courses mc','tucc.course_name=mc.course_name')
                           ->where('tucc.course_name',$courseName)
                           ->where('tucc.university_id',$uniId)
                           ->group_by('tucc.college_id')
                           ->get()->result();
                        
                       // echo "<pre>"; print_r($this->db->last_query()); exit;
                           return $colleges;


 }



/*function for getAllCollegsByStream*/
 public function getAllCollegsByStream($uniId,$streamName)
 {

//  echo $courseName; exit;

      $colleges = $this->db->select('tu.id as uniId, tu.name as uniName,tu.logo_img,tu.banner_img,tu.city as ucity,tu.state as ustate,tu.approved_by as uapproved,tu.accredited_name as uaccredited,tc.id as colgId, tc.name as colgName,mc.course_name,tc.logoname,tc.bannerName,tc.brochure_name,tc.type,tc.accreditions as caccreditions,tc.approvedby as capprovedby,tc.city as ccity,tc.stateName as cstate,mpc.g_name')
                           ->from('tbl_universities_colleges_courses tucc')
                           ->join('tbl_college tc','tucc.college_id=tc.id')
                           ->join('tbl_university tu','tucc.university_id=tu.id')
                           ->join('mst_professional_councils mpc','tc.approvedby=mpc.id')
                           ->join('mst_courses mc','tucc.course_name=mc.course_name')
                           ->where('mc.course_stream',$streamName)
                           ->where('tucc.university_id',$uniId)
                           ->group_by('tucc.college_id')
                           ->get()->result();
                        
                      //  echo "<pre>"; print_r($colleges); exit;
                           return $colleges;


 }




/*function for get brochure of college*/
public function getBrochureColg($uniId)
{

   
   $collegebrochure = $this->db->select('tbl_college.*,tbl_university.name as universityName')
                               ->from('tbl_college')
                               ->join('tbl_university','tbl_college.universityid=tbl_university.id')
                               ->where('tbl_college.id',$uniId)
                               ->get()->row();
                              
                           //   echo "<pre>"; print_r($this->db->last_query()); exit;

                       return $collegebrochure;
  

}

 /*funtion for get brochure of university*/

 public function getBrochure($universityId)
 {    
    $brochureName = $this->db->select('tbl_university.brochure_name as uniBrochure,tbl_university.name as universityName,tbl_college.brochure_name as colgBrochure,tbl_college.type,tbl_college.name as collegeName,tbl_college.city as colgCity')
                             ->from('tbl_university')
                             ->join('tbl_college','tbl_university.id=tbl_college.universityid')
                             ->where('tbl_university.id',$universityId)
                             ->get()->row();
                                 

                              //   echo "<pre>"; print_r($this->db->last_query()); exit;

                            //    echo "<pre>"; print_r($brochureName); exit;
                                return $brochureName;

                              /* if($brochureName->type=='department')
                               {
                                   
                                  $brochure=$brochureName->colgBrochure;

                               }else{

                                $brochure=$brochureName->uniBrochure;
                               }
                               return $brochure;*/
                           //  echo "<pre>"; print_r($brochureName); exit;

 }

 /*function for create new user*/

  public function createUser($data)
  {
    // echo "<pre>"; print_r($data); exit;
      $save = $this->db->insert('tbl_user',$data);

         $lastInsertedId = $this->db->insert_id();
           //echo $lastInsertedId; exit;

               return $lastInsertedId;

  }
    
    public function createUser2($data)
    {
    // echo "<pre>"; print_r($data); exit;
      $save = $this->db->insert('tbl_user',$data);

         $lastInsertedId = $this->db->insert_id();

            $lastUserData = $this->db->select('*')
                                      ->from('tbl_user')
                                      ->where('id',$lastInsertedId)
                                      ->get()->row();
           //echo $lastInsertedId; exit;

               return $lastUserData;

   }
   /*function for tbl_course_enq*/
  
  public function saveBrochureEnquiry($enqData)
  {
     $save = $this->db->insert('tbl_course_enq',$enqData);
          return $save;

  }

  /*function for get userinfo */
  public function getUerInfo($user_id)
  {
    
     $userdata = $this->db->select('*')
                          ->from('tbl_user')
                          ->where('id',$user_id)
                          ->get()->row();

                          return $userdata;

  }

  /*function for user existance */
  public function checkUserExistance($emailId)
  {

    $userInfo =  $this->db->select('*')
                          ->from('tbl_user')
                          ->where('user_email',$emailId)
                          ->get()->row();

                          return $userInfo;

  }


  /*function for get courses and colleges name*/
  public function getCoursesAndCollegesName()
  {    
    
       $courses = $this->db->select('*')
                            ->from('mst_courses')
                            ->order_by('rand()')
                            ->limit(4)
                            ->get()->result();
                          //  echo "<pre>"; print_r($courses); exit;

            return $courses;



  }

  /*function for getCollegesByCourse*/
  public function getCollegesByCourse($course)
  {
      $colleges = $this->db->select('tc.*,mpc.g_name,tu.id as uniId,tu.name as uniName')
                           ->from('tbl_universities_colleges_courses tucc')
                           ->join('tbl_college tc','tc.id=tucc.college_id')
                           ->join('tbl_university tu','tc.universityid=tu.id')
                           ->join('mst_professional_councils mpc','tc.approvedby=mpc.id')
                           ->where('tucc.course_name',$course)
                           ->limit(3)
                           ->get()->result();

                           return $colleges;


  }


  /*function for check exist email*/

  function checkExistEmail($email)
  {

    $checkEmail = $this->db->select('*')
                           ->from('tbl_user')
                           ->where('tbl_user.user_email',$email)
                           ->get()->row();
                          
                         
                           return $checkEmail;

  }

 //function for check existance user college review
         
  public function checkExistanceUserCollegeReview($colgId,$user_id)
  {
      
      $check = $this->db->select('*')
                        ->from('tbl_college_reviews')
                        ->where('user_id',$user_id)
                        ->where('college_id',$colgId)
                        ->get()->row();

                        return $check;

  }
  
//function for check existance user university review
public function checkExistanceUserUniversityReview($universityId,$user_id)
  {
      
      $check = $this->db->select('*')
                        ->from('tbl_university_reviews')
                        ->where('user_id',$user_id)
                        ->where('university_id',$universityId)
                        ->get()->row();

                        return $check;

  }

  // function for save user college review
  public function saveUserReview($userReviewData)
  {

   $saveData = $this->db->insert('tbl_college_reviews',$userReviewData);

              return $saveData;

  }
// function for save user university review
  public function saveUserUiversityReview($userReviewData)
  {

   $saveData = $this->db->insert('tbl_university_reviews',$userReviewData);

              return $saveData;

  }

  // function for get college rating
  public function getAllRatingofCollege($colgId)
  {

     $colgRating = $this->db->select('*')
                           ->from('tbl_college_reviews')
                           ->where('college_id',$colgId)
                           ->order_by('id','DESC')
                           ->get()->result();
                           
                           return $colgRating;
                         // echo "<pre>"; print_r($colgRating); exit;
   }
   // function for get college rating
  public function getAllRatingofUniversity($id)
  {

     $uniRating = $this->db->select('*')
                           ->from('tbl_university_reviews')
                           ->where('university_id',$id)
                           ->order_by('id','DESC')
                           ->get()->result();
                           
                           return $uniRating;
                         // echo "<pre>"; print_r($colgRating); exit;
   }



   //function for saveUnivesityBrochureDownloads

    public function saveUnivesityBrochureDownloads($userWithBrochureInfo)
    {
            
           // echo "<pre>"; print_r($userWithBrochureInfo); exit;
      $saveBrochureDownloadInfo = $this->db->insert('tbl_university_brochure_downloads',$userWithBrochureInfo);

                      return $saveBrochureDownloadInfo;

    }

    //function for saveUnivesityApplicantInfo

    public function saveUnivesityApplicantInfo($candidateInfo)
    {
          
          $saveApplicantInfo = $this->db->insert('tbl_apply_to_university',$candidateInfo);
                     return $saveApplicantInfo;

    }
   

    public function saveCollegeApplicantInfo($candidateInfo)
    {
          
        //  echo "<pre>"; print_r($candidateInfo); exit;
          $saveApplicantInfo = $this->db->insert('tbl_apply_to_college',$candidateInfo);
                     return $saveApplicantInfo;

    }


    // function for get user reviews

    public function getUserUniversityReviews($user_id)
    { 
       $userReviews = $this->db->select('*')
                               ->from('tbl_university_reviews')
                               ->join('tbl_university tu','tbl_university_reviews.university_id=tu.id')
                               ->where('tbl_university_reviews.user_id',$user_id)
                               ->group_by('tbl_university_reviews.university_id')
                               ->get()->result();
                      return $userReviews;
                               // echo "<pre>"; print_r($userReviews); exit;

    }


    // function for get all user applied colleges

    public function getAllAppliedCollegs($user_id)
    {

         $appliedColleges = $this->db->select('*')
                                     ->from('tbl_apply_to_college')
                                     ->join('tbl_college tc','tbl_apply_to_college.college_id=tc.id')
                                     ->group_by('tbl_apply_to_college.college_id')
                                     ->where('tbl_apply_to_college.user_id',$user_id)
                                     ->get()->result();
                                       
                                       return $appliedColleges;
                                     // echo "<pre>"; print_r($appliedColleges); exit();

    }


}
?>