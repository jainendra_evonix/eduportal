<?php 

class My_front extends My_base{
	
 
 function __construct() {

        parent::__construct();
        $this->front_js_css();
       // $this->data['folder_type'] = FRONTENDFOLDERNAME;
      //  $this->data['contents'] = Content::find('all', array('conditions' => array('status = ? ', 1)));
       // $this->data['shirtcategory'] = Shirtcategory::find('all', array('conditions' => array('status = ?', '1'), 'limit' => '3', 'order' => 'updated desc'));
      //  $this->data['categories'] = Category::find('all', array('conditions' => array('status = ? and showinfilter = ?', 1, 1)));
    }

  


    function front_js_css(){
    	//echo 23; exit;
        array_push($this->scripts['css'], 'frontend/css/font-awesome.min.css', 'frontend/css/custom.css', 'frontend/css/materialize.css', 'frontend/css/custom.css','frontend/css/style.css','frontend/css/bootstrap.css','frontend/css/responsive.css');
         
 
        /*if envirment is development then load non minified js */

        array_push($this->scripts['js'], "frontend/js/bootstrap.js", 'frontend/js/materialize.min.js', "frontend/js/custom.js","frontend/js/sweetalert.min.js");

        //echo "<pre>"; print_r($this->scripts); exit;
    }

    function _api_require_user($params = array()) {
        if (empty($params['except']))
            $params['except'] = array();
        if (empty($params['only']))
            $params['only'] = array();
        if (count($params['except']) > 0 AND in_array($this->data['action'], $params['except']))
            return TRUE;
        if (count($params['only']) > 0 AND !in_array($this->data['action'], $params['only']))
            return TRUE;
        if ($this->session->userdata(SITE_NAME . '_user_data') && $this->session->userdata[SITE_NAME . '_user_data']['user_id'] > 0) {
            return TRUE; //user is logged in    
        }
        if ($this->data['action'] == 'checkout') {
            $this->session->set_userdata(SITE_NAME . '_cart_redirect', '1');
        }
        redirect('login');
    }

    function pagination($count = 0, $url = '', $limit = 0) {
        $config['base_url'] = $url;
        $config['total_rows'] = $count;
        $config['per_page'] = $limit;

        $config['full_tag_open'] = '<div class="pull-right"><ul class="pagination" style="margin-right:10px">';
        $config['full_tag_close'] = '</ul></div>';

        $config['prev_link'] = '&laquo; &nbsp;';
        $config['prev_tag_open'] = '<li class="gray-bg">';
        $config['prev_tag_close'] = '</li>';

        $config['next_link'] = '&raquo; &nbsp;';
        $config['next_tag_open'] = '<li class="gray-bg">';
        $config['next_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li><span class="">';
        $config['cur_tag_close'] = '</span></li>';

//        $config['last_link'] = 'Last';
//        $config['first_link'] = 'First';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';



        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        return $config;
    }



}
?>