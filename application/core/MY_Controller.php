<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }
    
    public function check_session($logged_in_data) {
  
//        $logged_in_userdata = $this->session->userdata(base_url() . 'logged_in_userdata');
//        $logged_in_merchantdata = $this->session->userdata(base_url() . 'logged_in_merchantdata');
        if (!empty($logged_in_data)) {
            return TRUE;
        } else {

           // redirect('Admin');
        }
    }

}