<?php 
error_reporting(E_STRICT | E_ALL);
ini_set('display_errors', '1');

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adwords extends CI_Controller{

function __construct()
 {
  parent::__construct(); 
  //load our new Adwords library
  $this->load->library('My_adwords');

 }

 function index()
 {
   $user = new My_adwords();
   $user->GetCampaigns();
 
 }


}