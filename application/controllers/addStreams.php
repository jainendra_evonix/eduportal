<?php  if($this->session->flashdata('courseSaved')){   echo '<script> 
                   swal({
                        title: "Course added successfully!",
                       /* text: "Verify your email!",*/
                        icon: "success",
                        button: "Ok",
                      }); </script>';
  }








  if(!empty($postdata)){
    
    $programType = isset($postdata) && !empty($postdata['programType']) ? $postdata['programType'] : '';
    $stream = isset($postdata) && !empty($postdata['stream']) ? $postdata['stream'] : '';
    $courseName = isset($postdata) && !empty($postdata['universityname']) ? $postdata['universityname'] : '';
    $mastercourseId = isset($postdata) && !empty($postdata['mastercourseId']) ? $postdata['mastercourseId'] : '';
   
    
    }else{

    // echo "<pre>"; print_r($universityInfo); exit();

  $programType = isset($masterProgram) && !empty($masterProgram->course_type) ? $masterProgram->course_type : '';
  $stream = isset($masterProgram) && !empty($masterProgram->stream_name) ? $masterProgram->stream_name : '';
  $courseName = isset($masterProgram) && !empty($masterProgram->course_name) ? $masterProgram->course_name : '';
  $mastercourseId = isset($masterProgram) && !empty($masterProgram->id) ? $masterProgram->id : '';
  



}



?>
<style type="text/css">
  
  .error{

    color: red;
    font-size: 14px;
  }
  .select-wrapper input.select-dropdown{padding: 5px 10px;}
</style>
<!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
          <ul>
            <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="<?php echo base_url() ?>admin/adduniversity"> Add Stream </a> </li>
            <li class="page-back"><a href="<?php echo base_url() ?>admin/allcollege"><i class="fa fa-backward" aria-hidden="true"></i> View All</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>Add Stream</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="<?php echo base_url() ?>admin/addStream">Add New</a> </li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url() ?>admin/allStream"><i class="material-icons">subject</i>View All</a> </li>
            </ul>
            <!-- Dropdown Structure -->





            <div class="split-row">
              <div class="col-md-12">
                <div class="ad-inn-page" style="margin: 30px 0px;">
                  <div class="tab-inn ad-tab-inn">
                    <div class="hom-cre-acc-left hom-cre-acc-right">
                      <div class="">
                        <?php echo $this->session->flashdata('successmsg');  ?>
                        <?php echo $this->session->flashdata('errormsg');  ?>

                        
                          <div class="pg-elem-inn ele-btn col-md-12" >
                          
                           <form id="example-advanced-form"  method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/editProgram" novalidate style="padding: 35px 20px; margin-bottom: 30px;">
                              <div class="input_fields_wrap">
                             
                             <div class="row">
                                   <div class="col-md-3 form-group">
                                        <label>Program Type</label>
                                          <select name="programType[]" class="choosen-select" value="" data-validation="required">
                                          <!--  <option value="">select program type </option> -->
                                           <?php foreach ($ProgramType as $key) { ?>
                                            <option value="<?php echo $key->id; ?>" <?php if($key->id==$programType){echo 'selected';} ?>><?php echo $key->course_type; ?></option>
                                            
                                       <?php    } ?>

                                         </select> 
                                         <input type="hidden" name="mastercourseId" value="<?php echo $mastercourseId; ?>">
                                        <?php echo form_error('programType', '<div class="error">', '</div>'); ?>   
                                  </div>
                                   
                                   <div class="col-md-4 form-group">
                                        <label>Program Stream</label>
                                          <select name="stream[]" class="choosen-select" value="" data-validation="required">
                                          
                                           <?php foreach ($streams as $key) { ?>
                                            <option value="<?php echo $key->id; ?>" <?php if($key->id==$stream){ echo 'selected'; } ?>><?php echo $key->stream_name; ?></option>
                                            
                                       <?php    } ?>

                                         </select> 


                                        <?php echo form_error('stream', '<div class="error">', '</div>'); ?>   
                                  </div>
                                 
                                   <div class="col-md-4 form-group">
                                        <label>Course Name</label>
                                            <input type="text" class="validate form-control" id="courseName" value="<?php echo $courseName; ?>" name="courseName[]" placeholder="Enter course name" value="" data-validation="required"  data-validation-error-msg="Please enter course name" >
                                        <?php echo form_error('courseName', '<div class="error">', '</div>'); ?>   
                                  </div>
                                 <!--  <div class="col-md-3 form-group">
                                        <label>Course Specialization</label>
                                            <input type="text" class="validate form-control" id="courseSpec" name="courseSpec[]" value="" data-validation="required"  data-validation-error-msg="Please enter course specialization" >
                                        <?php echo form_error('courseSpec', '<div class="error">', '</div>'); ?>   
                                  </div> -->
                                  <!-- <div class="col-md-3 form-group">
                                        <label>Course Short Name</label>
                                            <input type="text" class="validate form-control" id="courseShortName" name="courseShortName[]" value="" data-validation="required"  data-validation-error-msg="Please enter course short name" >
                                        <?php echo form_error('courseShortName', '<div class="error">', '</div>'); ?>   
                                  </div> -->
                                  <div class="col-md-1 form-group">
                                            <a href="javascript:void(0);" onclick="getCourseTypes();"><i class="fa fa-plus-circle mt30 add_field_button" aria-hidden="true"></i></a>
  
                                  </div>
                                </div>

                              </div>



                                   

                                  <div class="row mTB30">
                                      <div class="col-md-12">
                                          <button class="btn btn-primary btn-block" type="Submit">Submit</button>
                                      </div>    
                                    </div>  
                            </form>

                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>






  <!--== BOTTOM FLOAT ICON ==-->
  <!-- <section>
    <div class="fixed-action-btn vertical">
      <a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
      <ul>
        <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
        <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
        <li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
        <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
      </ul>
    </div>
  </section>


