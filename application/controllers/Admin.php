<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends MY_Controller {


    	
    	public function __construct() {
            parent::__construct();
           
          /* if(!$this->session->userdata('user_id')){

                redirect(base_url().'admin');
            }*/
         
        }


     public function index()
     {

         // echo 123; exit;
            $this->load->view('admin/allcss');
            $this->load->view('admin/adminheader');
            $this->load->view('admin/adminlogin');
            $this->load->view('admin/adminfooter');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/adminlogin.js');
     

     }


     public function adminforgotpassword()
        {
            $this->load->view('admin/allcss');
            $this->load->view('admin/adminheader');
            $this->load->view('admin/adminforgotpassword');
            $this->load->view('admin/adminfooter');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/adminforgotpassword.js');
        }
        
        public function check_login() {
            $_POST = json_decode(file_get_contents('php://input'), true);
            //print_r($_POST); exit;
            if ($this->input->post()) {
                $this->form_validation->set_rules('emailaddress', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
                $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');

                if ($this->form_validation->run() == TRUE) {
                $emailaddress = $this->input->post('emailaddress');
                $password = $this->input->post('password');
                $response = $this->admin_model->check_user_login($emailaddress, $password);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $afterlogin = array(
                            'user_id' =>$response->id,
                            'username' => $response->username,
                            'fname' => $response->fname,
                            'lname' => $response->lname,
                            'loginsession' => true
                        );
                        
                    $this->session->set_userdata($afterlogin);
                    $data = array('status' => '1');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Wrong email id or password combination !.</div>');
                    }
                } else {
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
            }
            //print_r($this->form_validation->get_field_data()); exit;
           //print_r($data);            
        }
        else {
            $data = array('status' => '3');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }



    public function check_forgot_password() {

            $_POST = json_decode(file_get_contents('php://input'), true);
            if ($this->input->post()) {
                $this->form_validation->set_rules('emailaddress', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
                if ($this->form_validation->run() == TRUE) {
                $emailaddress = $this->input->post('emailaddress');
                    $randomkeystring = $this->generateRandomString();
                    $url = base_url();
                $response = $this->admin_model->check_admin_forgot_password($emailaddress);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    //$data = array('status' => '1');
                    //$updatepassword = $this->admin_model->update_password($emailaddress,$randomkeystring);

                    $this->admin_model->update_password($emailaddress,$randomkeystring);
                    
                    //send mail
                    require 'vendor/autoload.php';
                    
                    $sendgrid = new SendGrid("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");
                    $email = new SendGrid\Email();
                    
                    $email->addTo($emailaddress)
                         ->setFrom('support@evonixtech.com')
                         ->setFromName('EduPortal')
                         ->setSubject("New Password")
                         ->setHtml("Your new password is as below<br />Password = $randomkeystring<br /><a href='".$url."admin'>Click here to login</a><br /><br /> <br />Regards, <br />Support Team.");
           
                    if($sendgrid->send($email)) {
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success">An email has been sent to '.$emailaddress.'. Please login using your new password.</div>');
                } 
                else {
                    $data = array('status' => '4', 'message' => '<div class="alert alert-danger">Error in sending Email.</div>');
                    }
                    }
                    else {
                    $data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>The email id you have entered does not exist !.</div>');
                    }
                } else {
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
            }           
        }
        else {
            $data = array('status' => '3');
       }
        print_r(json_encode($data));
        }



        public function generateRandomString($length = 8) {    
                $characters   = 'abcdefghijklmnopqrstuvwxyz0123456789';
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, strlen($characters) - 1)];
                }
                return $randomString;
        }



        public function dashboard()
        {
          
       //   echo 123; exit;
              
            $user_id =$this->session->userdata('user_id'); 
            
            $data['userInfo'] = $this->getUserInfo($user_id);
          //  echo "<pre>"; print_r($session_data); exit;

            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/dashboard');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/dashboard.js');
        }

        public function getuniversitycount()
        {
            $universitycount=$this->admin_model->getalluniversitycount();

            $results[] = array(
                'universitycount' => $universitycount
                 );

            echo json_encode($results);
        }

        public function getcoursecount()
        {
            $coursecount=$this->admin_model->getallcoursecount();

            $results[] = array(
                'coursecount' => $coursecount
                 );

            echo json_encode($results);
        }

        public function getspecializationcount()
        {
            $specializationcount=$this->admin_model->getallspecializationcount();

            $results[] = array(
                'specializationcount' => $specializationcount
                 );

            echo json_encode($results);
        }

        public function alluniversity()
        { 
           
            $user_id =$this->session->userdata('user_id'); 

            $data['userInfo'] = $this->getUserInfo($user_id);

           // echo "<pre>"; print_r($data['userInfo']); exit;

            $this->load->view('admin/allcss');
            $this->load->view('admin/header',$data);
            $this->load->view('admin/alluniversity');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/alluniversity.js');
        }

  /*function for show all programs*/
  public function allPrograms()
  {

       if(!$this->session->userdata('user_id'))
            {
                redirect(base_url().'admin/');

            }

            $user_id =$this->session->userdata('user_id'); 
            $data['userInfo'] = $this->getUserInfo($user_id);

            $this->load->view('admin/allcss');
            $this->load->view('admin/header',$data);
            $this->load->view('admin/allPrograms');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/allPrograms.js');
  }



        function getalluniversity(){
                 //echo 123; exit;


            $universitydata=$this->admin_model->getalluniversitydata();
            //print_r($universitydata);
            foreach($universitydata as $k=>$v){
            //print_r($v); exit;

                $time=strtotime($v->timestamp);
            $day=date("d",$time);
            $month=date("M",$time);
            $year=date("Y",$time);

                    $results[] = array(
                'srno' => $k+1,
                'id' => $v->id,
                'name' => $v->name,
                'address' => $v->address,
                'url' => $v->url,
                'enabled' => $v->enabled,
                'day' => $day,
                'month' => $month,
                'year' => $year
                 );
            }
            echo json_encode($results);
        }

     
     /*function get all master programs */
     public function getPrograms()
     {


      $masterPrograms=$this->admin_model->getAllMasterPrograms();
         // echo "<pre>"; print_r($masterPrograms); exit;
            //print_r($universitydata);
            foreach($masterPrograms as $k=>$v){
            //print_r($v); exit;

              /*  $time=strtotime($v->timestamp);
            $day=date("d",$time);
            $month=date("M",$time);
            $year=date("Y",$time);*/

                    $results[] = array(
                'srno' => $k+1,
                'id' => $v->id,
                'CourseType' => $v->course_type,
                'StreamName' => $v->course_stream,
                'CourseName' => $v->course_name,
               
               
                 );

           

            }
           //  echo "<pre>"; print_r($results); exit;
            echo json_encode($results);
        }


     
/*function for edit program*/
  
  public function editProgram($id='')
  {
     
         // echo $id; exit;
            
             if(!empty($_POST))
          {

         // echo "<pre>"; print_r($_POST);  exit;

            $programeType = $this->input->post('programType');
            $stream = $this->input->post('stream');
            $courseName = $this->input->post('courseName');
            $mastercourseId = $this->input->post('mastercourseId');


              foreach ($programeType as $key => $program)
                  {
                    $cstreamName = $stream[$key];
                    $courseN =   $courseName[$key];
                   

                              $data = array(
                                             'course_type'=>$program,
                                             'course_stream'=>$cstreamName,
                                              'course_name' => $courseN,
                                             
                                          );

                                          //  echo "<pre>"; print_r($data);

                                  $update = $this->admin_model->updateMasterCourses($data,$mastercourseId);          
                             }//exit();
     
                      
                      $this->session->set_flashdata('courseUpdated','Course has been updated successfully');

                      redirect(base_url().'admin/allPrograms');


          }






            $user_id =$this->session->userdata('user_id'); 
            $data['userInfo'] = $this->getUserInfo($user_id);
            $data['ProgramType'] = $this->admin_model->getProgramType();  
            $data['streams'] = $this->admin_model->getStream();  
            $data['masterProgram'] = $this->admin_model->getMasterCourseBYId($id);

           // echo "<pre>"; print_r($data['masterProgram']); exit;
          //  $data['allUniversities'] = $this->admin_model->getAllUniversities();

            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/editProgram');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/editProgram.js');




  }


  /*function for delete program*/
  public function deleteProgram()
  {
    $deletingId = $this->input->post('deletingId');

   // echo $deletingId; exit;
     

    $deleteMasterCourse = $this->admin_model->deleteMasterCourse($deletingId);
      
      if($deleteMasterCourse){
        $this->session->set_flashdata('masterCourseDeleted','Master Course has been deleted');
         }

  }




        function deleteuniversity(){
          
            $deletingId=$this->input->post('deletingId');
            if($this->admin_model->deleteuniversitydata($deletingId))
            {
                $this->session->set_flashdata("universityDelete", '<div class="alert alert-success" style="font-size:13px;    padding:10px;width:35%;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>University has been deleted successfully!</div>');
                
            }
            else{
                
                $this->session->set_flashdata("errormsg", '<div class="alert alert-danger" style="color:#a94442;font-size:13px;    padding:10px;width:35%;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Error while deleting university!</div>');
                //$data =0;
            }
        }

         // function for get user info
          
          public function getUserInfo($user_id)
          {

           // echo $user_id;

           return   $userInfo = $this->admin_model->getUserInfo($user_id);



          }



        public function adduniversity()
        {
            if(!$this->session->userdata('user_id')){
                redirect(base_url().'admin');
            }

            


            if(!empty($_POST)){
                 
              //   echo "<pre>"; print_r($_POST); exit;

               $validate=$this->formValidation();
                      
                   if ($this->form_validation->run() == FALSE)
                    {
                            $data['postdata'] = $this->input->post();
                            $data['errors'] = validation_errors();
                    }
                    else
                    {
                        $universityInfo = array(

                          'name' => $this->input->post('universityname'),
                          'about' =>htmlentities($this->input->post('about')),
                          'shortname' =>$this->input->post('universityshortname'),
                          'url' =>$this->input->post('universityurl'),
                          'approved_by' =>$this->input->post('approvedby'),
                          'accredited_name' =>$this->input->post('accreditions'),
                          'brochure_name' =>$this->input->post('brochurename'),
                          'logo_img' =>$this->input->post('logoname'),
                          'banner_img' =>$this->input->post('bannerName'),
                          'university_type' =>$this->input->post('universityType'),
                          'established' =>$this->input->post('established'),
                          'city' =>$this->input->post('city'),
                          'state' =>$this->input->post('stateName'),
                          'country' =>$this->input->post('country'),
                          'address' =>$this->input->post('address'),
                          'phone_no' =>$this->input->post('contactNo'),
                          'fax' =>$this->input->post('faxno'),
                          'meta_title' =>$this->input->post('titleTag'),
                          'meta_keywords' =>$this->input->post('metaKeyword'),
                          'meta_description' =>$this->input->post('metaDescription'),
                          'meta_robot' =>$this->input->post('metaRobot'),
                          'enabled' =>1
                    
                        );

                  $saveUniversityInfo = $this->admin_model->saveUniversityInfo($universityInfo);

                 if($saveUniversityInfo)
                 {
                      $this->session->set_flashdata('universitySaved','University Added Successfully');

                      redirect(base_url().'admin/alluniversity');
                 }
            }
       }
            $data['accreditionss'] = $this->admin_model->getAccreditions();
            $data['countries'] = $this->admin_model->getAllCountries();
            $data['states'] = $this->admin_model->getAllStates();
            $user_id = $this->session->userdata('user_id'); 
            $data['userInfo'] = $this->getUserInfo($user_id);
            $data['professionalCouncils'] = $this->admin_model->getAllProfessionalCouncils();
            $data['universitiesType'] = $this->admin_model->getUniversityCategory();
     
            $this->load->view('admin/allcss');
            $this->load->view('admin/header',$data);
            $this->load->view('admin/adduniversity',$data);
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/adduniversity.js');
        }

        public function submituniversity() {
            $_POST = json_decode(file_get_contents('php://input'), true);
           
            print_r($_POST); exit;
            //print_r($_FILES); exit;
            
            if ($this->input->post()) {
                $this->form_validation->set_rules('universityname', 'Name', 'trim|required|trim|xss_clean|strip_tags');
                $this->form_validation->set_rules('address', 'Address', 'trim|required|trim|xss_clean|strip_tags');
                if ($this->form_validation->run() == TRUE) {
                    /*print_r('if');
                    exit;*/

                date_default_timezone_set('UTC');
                $info['name'] = $this->input->post('universityname');
                $info['shortname'] = $this->input->post('universityshortname');
                $info['url'] = $this->input->post('universityurl');
                $info['address'] = $this->input->post('address');
                $info['timestamp'] = date("Y-m-d H:i:s");
                
                
                $status = $this->input->post('enabled');
                if(is_null($status)) {
                    //echo 'is_null';
                    $info['enabled'] = 1;
                    //echo $info['enabled'];
                } else {
                    //echo 'not_is_null';
                    $info['enabled'] = $this->input->post('enabled');
                    //echo $info['enabled'];
                }           
                //exit;
                
                /*if ($status == "on") {
                    $info['enabled'] = 1;
                } else {
                    $info['enabled'] = 0;
                }*/
                
                $response = $this->admin_model->submit_university($info);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>University has been added successfully.</div>');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0');
                    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while adding university.</div>');
                    }
                } else {
            
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                
            }
           //print_r($data);            
        }
        else {
            
            $data = array('status' => '3');
            $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }

        public function edituniversity($editingId='')
        {



            if(!empty($_POST))
            {

               //echo "<pre>"; print_r($_POST); exit;

                 $validate=$this->formValidation();
                      
                   if ($this->form_validation->run() == FALSE)
                    {
                        
                            $data['postdata'] = $this->input->post();
                            $data['errors'] = validation_errors();

                          //  echo "<pre>"; print_r($data['errors']); exit;
                    }
                    else
                    {
                        
                         $universityId = $this->input->post('universityId');
                        $universityInfo = array(

                          'name' => $this->input->post('universityname'),
                          'about'=>htmlentities($this->input->post('about')),
                          'shortname' =>$this->input->post('universityshortname'),
                          'url' =>$this->input->post('universityurl'),
                          'email' =>$this->input->post('email'),
                          'approved_by' =>$this->input->post('approvedby'),
                          'accredited_name' =>$this->input->post('accreditions'),
                          'brochure_name' =>$this->input->post('bannerName'),
                          'logo_img' =>$this->input->post('logoname'),
                          'banner_img' =>$this->input->post('bannerName'),
                          'university_type' =>$this->input->post('universityType'),
                          'established' =>$this->input->post('established'),
                          'city' =>$this->input->post('city'),
                          'state' =>$this->input->post('stateName'),
                          'country' =>$this->input->post('country'),
                          'address' =>$this->input->post('address'),
                          'fax' =>$this->input->post('faxno'),
                          'phone_no' =>$this->input->post('contactNo'),
                          'meta_title' => $this->input->post('titleTag'),
                          'meta_keywords' => $this->input->post('metaKeyword'),
                          'meta_description' => $this->input->post('metaDescription'),
                          'meta_robot' => $this->input->post('metaRobot'),
                          'updated_date' =>date('Y-m-d h:i:s'),

                    
                        );
                 /*  echo "<pre>PostData"; print_r($_POST); ;
                   echo "<pre>"; print_r($universityInfo); exit;*/
                  $updateUniversityInfo = $this->admin_model->updateUniversityInfo($universityInfo,$universityId);

                 if($updateUniversityInfo)
                 {
                      $this->session->set_flashdata('universityUpdated','University Updated Successfully');

                      redirect(base_url().'admin/alluniversity');
                 }
            }




            } //else{

              $user_id = $this->session->userdata('user_id'); 

             $data['userInfo'] = $this->getUserInfo($user_id);
             

            $data['accreditionss'] = $this->admin_model->getAccreditions();
            $data['countries'] = $this->admin_model->getAllCountries();
            $data['states'] = $this->admin_model->getAllStates();
            $data['professionalCouncils'] = $this->admin_model->getAllProfessionalCouncils();
            $data['universitiesType'] = $this->admin_model->getUniversityCategory();
            $data['universityInfo'] = $this->admin_model->getuniversitydata($editingId);

          //  echo "<pre>"; print_r($data['getdata']); exit;
            
          /*  $data = array(
                'getdata' => $getdata
            );*/
            
            $this->load->view('admin/allcss',$data);
            $this->load->view('admin/header',$data);
            $this->load->view('admin/edituniversity',$data);
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/edituniversity.js');
       // }
        }

        public function updateuniversity() {
            $_POST = json_decode(file_get_contents('php://input'), true);
           
            //print_r($_POST); exit;
            //print_r($_FILES); exit;
            
            if ($this->input->post()) {
                $this->form_validation->set_rules('universityname', 'Name', 'trim|required|trim|xss_clean|strip_tags');
                $this->form_validation->set_rules('address', 'Address', 'trim|required|trim|xss_clean|strip_tags');
                if ($this->form_validation->run() == TRUE) {
                    /*print_r('if');
                    exit;*/

                $info['name'] = $this->input->post('universityname');
                $info['shortname'] = $this->input->post('universityshortname');
                $info['url'] = $this->input->post('universityurl');
                $info['address'] = $this->input->post('address');
                $info['id'] = $this->input->post('hiddenid');
                
                
                $status = $this->input->post('enabled');
                if(is_null($status)) {
                    //echo 'is_null';
                    $info['enabled'] = 1;
                    //echo $info['enabled'];
                } else {
                    //echo 'not_is_null';
                    $info['enabled'] = $this->input->post('enabled');
                    //echo $info['enabled'];
                }           
                //exit;
                
                /*if ($status == "on") {
                    $info['enabled'] = 1;
                } else {
                    $info['enabled'] = 0;
                }*/
                
                $response = $this->admin_model->update_university($info);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>University has been updated successfully.</div>');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0');
                    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while updating university.</div>');
                    }
                } else {
            
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                
            }
           //print_r($data);            
        }
        else {
            $data = array('status' => '3');
            $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }

        public function allcourses()
        {
             $user_id =$this->session->userdata('user_id'); 
            
            $data['userInfo'] = $this->getUserInfo($user_id);

            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/allcourses');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/allcourses.js');
        }

        function getallcourses() {
            $coursedata=$this->admin_model->getallcoursesdata();
            //print_r($coursesdata);
            foreach($coursedata as $k=>$v){
            //print_r($v); exit;

                $time=strtotime($v->timestamp);
            $day=date("d",$time);
            $month=date("M",$time);
            $year=date("Y",$time);

                    $results[] = array(
                'srno' => $k+1,
                'id' => $v->id,
                'name' => $v->name,
                'enabled' => $v->enabled,
                'day' => $day,
                'month' => $month,
                'year' => $year
                 );
            }
            echo json_encode($results);
        }

     

        public function addcourse()
        {
             $user_id =$this->session->userdata('user_id'); 
            
            $data['userInfo'] = $this->getUserInfo($user_id);

            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/addcourse');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/addcourse.js');
        }

        public function submitcourse() {
            $_POST = json_decode(file_get_contents('php://input'), true);
           
            //print_r($_POST); exit;
            //print_r($_FILES); exit;
            
            if ($this->input->post()) {
                $this->form_validation->set_rules('coursename', 'Name', 'trim|required|trim|xss_clean|strip_tags');
                if ($this->form_validation->run() == TRUE) {
                    /*print_r('if');
                    exit;*/

                date_default_timezone_set('UTC');
                $info['name'] = $this->input->post('coursename');
                $info['shortname'] = $this->input->post('courseshortname');
                $info['timestamp'] = date("Y-m-d H:i:s");
                
                
                $status = $this->input->post('enabled');
                if(is_null($status)) {
                    //echo 'is_null';
                    $info['enabled'] = 1;
                    //echo $info['enabled'];
                } else {
                    //echo 'not_is_null';
                    $info['enabled'] = $this->input->post('enabled');
                    //echo $info['enabled'];
                }           
                //exit;
                
                /*if ($status == "on") {
                    $info['enabled'] = 1;
                } else {
                    $info['enabled'] = 0;
                }*/
                
                $response = $this->admin_model->submit_course($info);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Course has been added successfully.</div>');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0');
                    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while adding course.</div>');
                    }
                } else {
            
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                
            }
           //print_r($data);            
        }
        else {
            $data = array('status' => '3');
            $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }

     

        public function updatecourse() {
            $_POST = json_decode(file_get_contents('php://input'), true);
           
            //print_r($_POST); exit;
            //print_r($_FILES); exit;
            
            if ($this->input->post()) {
                $this->form_validation->set_rules('coursename', 'Name', 'trim|required|trim|xss_clean|strip_tags');
                if ($this->form_validation->run() == TRUE) {
                    /*print_r('if');
                    exit;*/

                $info['name'] = $this->input->post('coursename');
                $info['shortname'] = $this->input->post('courseshortname');
                $info['id'] = $this->input->post('hiddenid');
                
                
                $status = $this->input->post('enabled');
                if(is_null($status)) {
                    //echo 'is_null';
                    $info['enabled'] = 1;
                    //echo $info['enabled'];
                } else {
                    //echo 'not_is_null';
                    $info['enabled'] = $this->input->post('enabled');
                    //echo $info['enabled'];
                }           
                //exit;
                
                /*if ($status == "on") {
                    $info['enabled'] = 1;
                } else {
                    $info['enabled'] = 0;
                }*/
                
                $response = $this->admin_model->update_course($info);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Course has been updated successfully.</div>');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0');
                    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while updating course.</div>');
                    }
                } else {
            
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                
            }
           //print_r($data);            
        }
        else {
            $data = array('status' => '3');
            $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }

        public function addspecialization()
        {
           if(!$this->session->userdata('user_id'))
           {
            redirect(base_url().'admin');
           }

           $user_id = $this->session->userdata('user_id');
           $data['userInfo'] = $this->getUserInfo($user_id);


            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/addspecialization');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/addspecialization.js');
        }

        public function submitspecialization() {
            $_POST = json_decode(file_get_contents('php://input'), true);
           
            //print_r($_POST); exit;
            //print_r($_FILES); exit;
            
            if ($this->input->post()) {
                $this->form_validation->set_rules('specializationname', 'Name', 'trim|required|trim|xss_clean|strip_tags');
                if ($this->form_validation->run() == TRUE) {
                    /*print_r('if');
                    exit;*/

                date_default_timezone_set('UTC');
                $info['name'] = $this->input->post('specializationname');
                $info['shortname'] = $this->input->post('specializationshortname');
                $info['timestamp'] = date("Y-m-d H:i:s");
                
                
                $status = $this->input->post('enabled');
                if(is_null($status)) {
                    //echo 'is_null';
                    $info['enabled'] = 1;
                    //echo $info['enabled'];
                } else {
                    //echo 'not_is_null';
                    $info['enabled'] = $this->input->post('enabled');
                    //echo $info['enabled'];
                }           
                //exit;
                
                /*if ($status == "on") {
                    $info['enabled'] = 1;
                } else {
                    $info['enabled'] = 0;
                }*/
                
                $response = $this->admin_model->submit_specialization($info);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Specialization has been added successfully.</div>');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0');
                    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while adding specialization.</div>');
                    }
                } else {
            
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                
            }
           //print_r($data);            
        }
        else {
            $data = array('status' => '3');
            $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }

        public function editspecialization($editingId) {
             $user_id =$this->session->userdata('user_id'); 
            
            $data['userInfo'] = $this->getUserInfo($user_id);
            
            $getdata=$this->admin_model->getspecializationdata($editingId);
            
            $data = array(
                'getdata' => $getdata
            );
            
            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/editspecialization', $data);
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/editspecialization.js');
        }

        public function updatespecialization() {
            $_POST = json_decode(file_get_contents('php://input'), true);
           
            //print_r($_POST); exit;
            //print_r($_FILES); exit;
            
            if ($this->input->post()) {
                $this->form_validation->set_rules('specializationname', 'Name', 'trim|required|trim|xss_clean|strip_tags');
                if ($this->form_validation->run() == TRUE) {
                    /*print_r('if');
                    exit;*/

                $info['name'] = $this->input->post('specializationname');
                $info['shortname'] = $this->input->post('specializationshortname');
                $info['id'] = $this->input->post('hiddenid');
                
                
                $status = $this->input->post('enabled');
                if(is_null($status)) {
                    //echo 'is_null';
                    $info['enabled'] = 1;
                    //echo $info['enabled'];
                } else {
                    //echo 'not_is_null';
                    $info['enabled'] = $this->input->post('enabled');
                    //echo $info['enabled'];
                }           
                //exit;
                
                /*if ($status == "on") {
                    $info['enabled'] = 1;
                } else {
                    $info['enabled'] = 0;
                }*/
                
                $response = $this->admin_model->update_specialization($info);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Specialization has been updated successfully.</div>');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0');
                    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while updating specialization.</div>');
                    }
                } else {
            
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                
            }
           //print_r($data);            
        }
        else {
            $data = array('status' => '3');
            $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }

        public function allspecialization()
        {
             $user_id =$this->session->userdata('user_id'); 
            
            $data['userInfo'] = $this->getUserInfo($user_id);

            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/allspecialization');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/allspecialization.js');
        }

        function getallspecialization() {
            $specializationdata=$this->admin_model->getallspecializationdata();
            //print_r($specializationdata);
            foreach($specializationdata as $k=>$v){
            //print_r($v); exit;

                $time=strtotime($v->timestamp);
            $day=date("d",$time);
            $month=date("M",$time);
            $year=date("Y",$time);

                    $results[] = array(
                'srno' => $k+1,
                'id' => $v->id,
                'name' => $v->name,
                'enabled' => $v->enabled,
                'day' => $day,
                'month' => $month,
                'year' => $year
                 );
            }
            echo json_encode($results);
        }

        function deletespecialization() {
            //$_POST = json_decode(file_get_contents('php://input'), true);
            //echo "asd".$this->input->post('id'); exit;
            $deletingId=$this->input->post('deletingId');
            if($this->admin_model->deletespecializationdata($deletingId))
            {
                $this->session->set_flashdata("successmsg", '<div class="alert alert-success" style="font-size:13px;    padding:10px;width:35%;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Specialization has been deleted successfully!</div>');
                //$data = 1;
            }
            else{
                
                $this->session->set_flashdata("errormsg", '<div class="alert alert-danger" style="color:#a94442;font-size:13px;    padding:10px;width:35%;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Error while deleting specialization!</div>');
                //$data =0;
            }
        }

        public function addcollege()
        {

       
         if(!empty($_POST))
         {
           
              

                $type = $this->input->post('type');

                if($type=='college'){

                $validate=$this->formValidationForCollege();
                      
                   if ($this->form_validation->run() == FALSE)
                    {

                       // echo 'false'; exit;
                            $data['postdata'] = $this->input->post();
                            $data['errors'] = validation_errors();
                    }else
                     {

                       // echo 'true'; exit;
                     $collegeInfo = array(

                       'universityid' => $this->input->post('universityId'),
                       'name' =>  trim($this->input->post('collegeName')),
                       'shortname' =>trim($this->input->post('collegeSrtName')),
                       'url' => $this->input->post('collegeUrl'),
                       'email' => $this->input->post('email'),
                       'about' => htmlentities($this->input->post('about')),
                       'city' => $this->input->post('city'),
                       'address' =>$this->input->post('address') ,
                       'country' => $this->input->post('country'),
                       'stateName' => $this->input->post('stateName'),
                       'contactNo' => $this->input->post('contactNo'),
                       'college_type' => $this->input->post('collegeType'),
                       'accreditions' => $this->input->post('accreditions'),
                       'approvedby' => $this->input->post('approvedby'),
                       'faxno' => $this->input->post('faxno'),
                       'established' => $this->input->post('established'),
                       'logoname' => $this->input->post('logoname'),
                       'bannerName' => $this->input->post('bannerName'),
                       'brochure_name' => $this->input->post('brochurename'),
                       'meta_titile' => $this->input->post('titleTag'),
                       'meta_keywords' => $this->input->post('metaKeyword'),
                       'meta_description' => $this->input->post('metaDescription'),
                       'meta_robot' => $this->input->post('metaRobot'),
                       'enabled' =>1
                      
                      
                     );

                   // echo "<pre>"; print_r($collegeInfo); exit;
                  $saveCollegeInfo = $this->admin_model->saveCollegeInfo($collegeInfo);

                 if($saveCollegeInfo)
                 {
                      $this->session->set_flashdata('collegeSaved','College Added Successfully');

                      redirect(base_url().'admin/allcollege');
                 }
              }
            }else{


                   $this->formValidationForDeparment();

                   if ($this->form_validation->run() == FALSE)
                    {

                            $data['postdata'] = $this->input->post();
                            $data['errors'] = validation_errors();

                    }else
                     {
                             

                      // echo 'valid'; exit;


                         $collegeInfo = array(

                       'universityid' => $this->input->post('universityId'),
                       'name' =>  trim($this->input->post('departmentName')),
                      
                      
                       'email' => $this->input->post('emailDept'),
                       'about' => htmlentities($this->input->post('aboutDept')),
                       'contactNo' => $this->input->post('contactNoDept'),
                       'faxno' => $this->input->post('faxnoDept'),
                       'established' => $this->input->post('establishedDept'),
                       'bannerName' => $this->input->post('bannerNameDept'),
                       'brochure_name' => $this->input->post('brochurenameDept'),
                       'meta_titile' => $this->input->post('titleTagDept'),
                       'meta_keywords' => $this->input->post('metaKeywordDept'),
                       'meta_description' => $this->input->post('metaDescriptionDept'),
                       'meta_robot' => $this->input->post('metaRobotDept'),
                       'department' => 1,
                       'enabled' => 1
                      
                      
                     );

                   // echo "<pre>"; print_r($collegeInfo); exit;
                  $saveCollegeInfo = $this->admin_model->saveCollegeInfo($collegeInfo);

                 if($saveCollegeInfo)
                 {
                      $this->session->set_flashdata('departmentSaved','Department Added Successfully');

                      redirect(base_url().'admin/allcollege');
                 }

                

                     }


            }
         }

            $user_id =$this->session->userdata('user_id'); 
            $data['allUniversities'] = $this->admin_model->getAllUniversities();
             $data['states'] = $this->admin_model->getAllStates();
            $data['countries'] = $this->admin_model->getAllCountries();


            $data['accreditionss'] = $this->admin_model->getAccreditions();
            $data['professionalCouncils'] = $this->admin_model->getAllProfessionalCouncils();
            $data['universitiesType'] = $this->admin_model->getUniversityCategory();

            
            $data['userInfo'] = $this->getUserInfo($user_id);

            


            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/addCollege', $data);
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/addCollege.js');
         
        }

        public function getuniversitycollege()
        {
            $collegeuniversity=$this->input->post('collegeuniversity');
            $query=$this->admin_model->getuniversitycollegedata();
            echo '<option value="">Select College</option>';
            foreach($query->result() as $row)
            { 
                echo "<option value='".$row->id."'>".$row->name."</option>";
            }
        }

        public function editcourseassociation()
        {
            $eId=$this->input->post('eId');
            $data['fetchalluniversitydata']=$this->admin_model->fetchalluniversitydata();
            $data['fetchallcollegedata']=$this->admin_model->fetchallcollegedata();
            $data['fetchallcoursesdata']=$this->admin_model->fetchallcoursesdata();
            $data['fetchallspecdata']=$this->admin_model->fetchallspecdata();
            $data['fetchalldegreedata']=$this->admin_model->fetchalldegreedata();
            $data['fetchallcredentialdata']=$this->admin_model->fetchallcredentialdata();
            $data['fetchallmodedata']=$this->admin_model->fetchallmodedata();
            $data['fetchallmediumdata']=$this->admin_model->fetchallmediumdata();
            $data['fetchallrecognitiondata']=$this->admin_model->fetchallrecognitiondata();
            $data['fetchallcoursestatusdata']=$this->admin_model->fetchallcoursestatusdata();
            $data['fetchallaccreditationdata']=$this->admin_model->fetchallaccreditationdata();
            $data['fetchallownershipdata']=$this->admin_model->fetchallownershipdata();

            $data['info']=$this->admin_model->getcourseassociationdata();

            /*echo '<form class="" style="background:#ffffff;" name="addcae" ng-submit="submitformeditcourse()" novalidate><input type="text" class="validate" id="caefees" name="caefees" ng-model="cae.caefees" ng-pattern="/^[0-9]*$/" value="'.$getdata->totalfees.'" required><span ng-show="submitted && addcae.caefees.$error.required"  class="help-block has-error ng-hide">Fees is required.</span><span ng-show="submitted && addcae.caefees.$error.pattern"  class="help-block has-error ng-hide">Please enter a valid number.</span><span class="help-block has-error ng-hide" ng-show="caefeesError">{{caefeesError}}</span><label for="list_name">Total Fees</label><div class="row"><div class="input-field col s12 v2-mar-top-40"><input class="input-btn" type="submit" value="Submit" ng-click="submitted=true"></div></div></form>';*/

            /*$array = array(
                    'totalfees' => $getdata->totalfees
                );
                $data = json_encode($array);
                echo $data;*/

            $this->load->view("admin/pagewisejs/modal", $data);
        }

        public function editcamodal()
        {
            $this->load->view('admin/editcamodal');
        }

        public function submitcollege() {
            $_POST = json_decode(file_get_contents('php://input'), true);
           
            //print_r($_POST); exit;
            //print_r($_FILES); exit;
            
            if ($this->input->post()) {
                $this->form_validation->set_rules('collegename', 'Name', 'trim|required|trim|xss_clean|strip_tags');
                $this->form_validation->set_rules('address', 'Address', 'trim|required|trim|xss_clean|strip_tags');
                if ($this->form_validation->run() == TRUE) {
                    /*print_r('if');
                    exit;*/

                date_default_timezone_set('UTC');
                $info['universityid'] = $this->input->post('collegeuniversity');
                $info['name'] = $this->input->post('collegename');
                $info['shortname'] = $this->input->post('collegeshortname');
                $info['url'] = $this->input->post('collegeurl');
                $info['address'] = $this->input->post('address');
                $info['timestamp'] = date("Y-m-d H:i:s");
                
                
                $status = $this->input->post('enabled');
                if(is_null($status)) {
                    //echo 'is_null';
                    $info['enabled'] = 1;
                    //echo $info['enabled'];
                } else {
                    //echo 'not_is_null';
                    $info['enabled'] = $this->input->post('enabled');
                    //echo $info['enabled'];
                }           
                //exit;
                
                /*if ($status == "on") {
                    $info['enabled'] = 1;
                } else {
                    $info['enabled'] = 0;
                }*/
                
                $response = $this->admin_model->submit_college($info);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>College has been added successfully.</div>');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0');
                    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while adding college.</div>');
                    }
                } else {
            
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                
            }
           //print_r($data);            
        }
        else {
            $data = array('status' => '3');
            $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }

        public function editcollege($collegeId='')
         {
              
              if(!empty($_POST))
              {
                  
                  //  echo "<pre>"; print_r($_POST); exit;

                    $type = $this->input->post('type');

                    
                     if($type=='college'){

                     

                    $validate=$this->formValidationForCollege();
                      
                   if ($this->form_validation->run() == FALSE)
                    {
                            $data['postdata'] = $this->input->post();
                            $data['errors'] = validation_errors();
                    }else
                     {

                       // echo 'true'; exit;
                        $collegeId = $this->input->post('collegeId');
                     $collegeInfo = array(

                       'universityid' => $this->input->post('universityId'),
                       'name' =>  trim($this->input->post('collegeName')),
                       'shortname' =>trim($this->input->post('collegeSrtName')),
                       'url' => $this->input->post('collegeUrl'),
                       'email' => $this->input->post('email'),
                       'about' => htmlentities($this->input->post('about')),
                       'city' => $this->input->post('city'),
                       'address' =>$this->input->post('address') ,
                       'country' => $this->input->post('country'),
                       'stateName' => $this->input->post('stateName'),
                       'contactNo' => $this->input->post('contactNo'),
                       'college_type' => $this->input->post('collegeType'),
                       'accreditions' => $this->input->post('accreditions'),
                       'approvedby' => $this->input->post('approvedby'),
                       'faxno' => $this->input->post('faxno'),
                       'established' => $this->input->post('established'),
                       'logoname' => $this->input->post('logoname'),
                       'bannerName' => $this->input->post('bannerName'),
                       'enabled' =>1
                      
                     );
                         
                    // echo $collegeId;  exit; 

                     //echo "<pre>"; print_r($collegeInfo); exit;

                  $updateCollegeInfo = $this->admin_model->updateCollegeInfo($collegeId,$collegeInfo);

                 if($updateCollegeInfo)
                 {
                      $this->session->set_flashdata('collegeUpdated','College info  has been updated successfully');

                      redirect(base_url().'admin/allcollege');
                 }
              }
             }else{
                       
                      
                   $this->formValidationForDeparment();

                   if ($this->form_validation->run() == FALSE)
                    {

                            $data['postdata'] = $this->input->post();
                            $data['errors'] = validation_errors();

                    }else
                     {

                      $deptId = $this->input->post('collegeId');

                      
                    $collegeInfo = array(

                       'universityid' => $this->input->post('universityId'),
                       'name' =>  trim($this->input->post('departmentName')),
                      
                      
                       'email' => $this->input->post('emailDept'),
                       'about' => htmlentities($this->input->post('aboutDept')),
                       'contactNo' => $this->input->post('contactNoDept'),
                       'faxno' => $this->input->post('faxnoDept'),
                       'established' => $this->input->post('establishedDept'),
                       'bannerName' => $this->input->post('bannerNameDept'),
                       'brochure_name' => $this->input->post('brochurenameDept'),
                       'meta_titile' => $this->input->post('titleTagDept'),
                       'meta_keywords' => $this->input->post('metaKeywordDept'),
                       'meta_description' => $this->input->post('metaDescriptionDept'),
                       'meta_robot' => $this->input->post('metaRobotDept'),
                       'type' => 'department',
                       'enabled' => 1
                      
                      
                     );

                 //  echo "<pre>"; print_r($collegeInfo); exit;
                  $saveCollegeInfo = $this->admin_model->updateCollegeInfo($deptId,$collegeInfo);

                 if($saveCollegeInfo)
                 {
                      $this->session->set_flashdata('departmentUpdated','Department Added Successfully');

                      redirect(base_url().'admin/allcollege');
                 }

                

                     }






             }

              }else{

            $user_id =$this->session->userdata('user_id'); 
            $data['userInfo'] = $this->getUserInfo($user_id);
            $data['allUniversities'] = $this->admin_model->getAllUniversities();
            $data['states'] = $this->admin_model->getAllStates();
            $data['countries'] = $this->admin_model->getAllCountries();
            $data['accreditionss'] = $this->admin_model->getAccreditions();
            $data['professionalCouncils'] = $this->admin_model->getAllProfessionalCouncils();
            $data['universitiesType'] = $this->admin_model->getUniversityCategory();
            $data['collegeInfo']=$this->admin_model->getcollegedata($collegeId);

            $type=$data['collegeInfo']->type;
           // echo $type; exit;

            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            if($type!='department'){
            $this->load->view('admin/editcollege', $data);
            $this->load->view('admin/pagewisejs/addCollege.js');

             }else{
               
               $this->load->view('admin/editDepartment', $data);
               $this->load->view('admin/pagewisejs/editDepartment.js');

             }
            $this->load->view('admin/alljs');
            
              }
        }

        public function updatecollege() {
            $_POST = json_decode(file_get_contents('php://input'), true);
           
            //print_r($_POST); exit;
            //print_r($_FILES); exit;
            
            if ($this->input->post()) {
                $this->form_validation->set_rules('collegename', 'Name', 'trim|required|trim|xss_clean|strip_tags');
                if ($this->form_validation->run() == TRUE) {
                    /*print_r('if');
                    exit;*/

                $info['universityid'] = $this->input->post('collegeuniversity');
                $info['name'] = $this->input->post('collegename');
                $info['shortname'] = $this->input->post('collegeshortname');
                $info['url'] = $this->input->post('collegeurl');
                $info['address'] = $this->input->post('address');
                $info['id'] = $this->input->post('hiddenid');
                
                
                $status = $this->input->post('enabled');
                if(is_null($status)) {
                    //echo 'is_null';
                    $info['enabled'] = 1;
                    //echo $info['enabled'];
                } else {
                    //echo 'not_is_null';
                    $info['enabled'] = $this->input->post('enabled');
                    //echo $info['enabled'];
                }           
                //exit;
                
                /*if ($status == "on") {
                    $info['enabled'] = 1;
                } else {
                    $info['enabled'] = 0;
                }*/
                
                $response = $this->admin_model->update_college($info);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>College has been updated successfully.</div>');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0');
                    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while updating college.</div>');
                    }
                } else {
            
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                
            }
           //print_r($data);            
        }
        else {
            $data = array('status' => '3');
            $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }

        function deleteasccourse() {
            //$_POST = json_decode(file_get_contents('php://input'), true);
            //echo "asd".$this->input->post('id'); exit;
            $deletingId=$this->input->post('deletingId');
            if($this->admin_model->deleteasccoursedata($deletingId))
            {
                $this->session->set_flashdata("successmsg", '<div class="alert alert-success" style="font-size:13px;    padding:10px;width:45%;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Course association has been deleted successfully!</div>');
                //$data = 1;
            }
            else{
                
                $this->session->set_flashdata("errormsg", '<div class="alert alert-danger" style="color:#a94442;font-size:13px;    padding:10px;width:45%;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Error while deleting course association!</div>');
                //$data =0;
            }
        }

        function createcopyasccourse() {
            //$_POST = json_decode(file_get_contents('php://input'), true);
            //echo "asd".$this->input->post('id'); exit;
            $copyId=$this->input->post('copyId');

            date_default_timezone_set('UTC');
            $ca = $this->admin_model->get_college_association($copyId);
            if($ca) {
                $course['universityid']=$ca->universityid;
                $course['collegeid']=$ca->collegeid;
                $course['enabled']=$ca->enabled;
                $course['deleted']=$ca->deleted;
                $course['timestamp'] = date("Y-m-d H:i:s");
            }

            $ccsa = $this->admin_model->get_clg_crs_spec_association($copyId);
            if($ccsa) {
                $collegecourse['courseid']=$ccsa->courseid;
                $collegecourse['specializationid']=$ccsa->specializationid;
                $collegecourse['credentialid']=$ccsa->credentialid;
                $collegecourse['degreeid']=$ccsa->degreeid;
                $collegecourse['modeofstudyid']=$ccsa->modeofstudyid;
                $collegecourse['duration']=$ccsa->duration;
                $collegecourse['mediumid']=$ccsa->mediumid;
                $collegecourse['recognitionid']=$ccsa->recognitionid;
                $collegecourse['coursestatusid']=$ccsa->coursestatusid;
                $collegecourse['accreditationid']=$ccsa->accreditationid;
                $collegecourse['ownershipid']=$ccsa->ownershipid;
                $collegecourse['totalfees']=$ccsa->totalfees;
                $collegecourse['eligibility']=$ccsa->eligibility;
                $collegecourse['coursestructure']=$ccsa->coursestructure;
                $collegecourse['enabled']=$ccsa->enabled;
                $collegecourse['deleted']=$ccsa->deleted;
                $collegecourse['timestamp'] = date("Y-m-d H:i:s");
            }

            $response = $this->admin_model->submit_courseassociation($course,$collegecourse);
            if ($response) {
                //echo '<pre>';print_r($response);exit;
                $this->session->set_flashdata("successmsg", '<div class="alert alert-success" style="font-size:13px;    padding:10px;width:45%;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Course association has been copied successfully!</div>');
                }
                else {
                $this->session->set_flashdata("errormsg", '<div class="alert alert-danger" style="color:#a94442;font-size:13px;    padding:10px;width:45%;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Error while copying course association!</div>');
                }
        }

        public function collegeassociation()
        {
             $user_id =$this->session->userdata('user_id'); 
            
            $data['userInfo'] = $this->getUserInfo($user_id);
            $fetchalluniversitydata=$this->admin_model->fetchalluniversitydata();
            $fetchallcollegedata=$this->admin_model->fetchallcollegedata();
            $fetchallcoursesdata=$this->admin_model->fetchallcoursesdata();
            $fetchallspecdata=$this->admin_model->fetchallspecdata();
            //echo "<pre>"; print_r($fetchallspecdata); exit;
            $fetchalldegreedata=$this->admin_model->fetchalldegreedata();
            $fetchallcredentialdata=$this->admin_model->fetchallcredentialdata();
            $fetchallmodedata=$this->admin_model->fetchallmodedata();
            $fetchallmediumdata=$this->admin_model->fetchallmediumdata();
            $fetchallrecognitiondata=$this->admin_model->fetchallrecognitiondata();
            $fetchallcoursestatusdata=$this->admin_model->fetchallcoursestatusdata();
            $fetchallaccreditationdata=$this->admin_model->fetchallaccreditationdata();
            $fetchallownershipdata=$this->admin_model->fetchallownershipdata();

            $data = array(
                'fetchalluniversitydata' => $fetchalluniversitydata,
                'fetchallcollegedata' => $fetchallcollegedata,
                'fetchallcoursesdata' => $fetchallcoursesdata,
                'fetchallspecdata' => $fetchallspecdata,
                'fetchalldegreedata' => $fetchalldegreedata,
                'fetchallcredentialdata' => $fetchallcredentialdata,
                'fetchallmodedata' => $fetchallmodedata,
                'fetchallmediumdata' => $fetchallmediumdata,
                'fetchallrecognitiondata' => $fetchallrecognitiondata,
                'fetchallcoursestatusdata' => $fetchallcoursestatusdata,
                'fetchallaccreditationdata' => $fetchallaccreditationdata,
                'fetchallownershipdata' => $fetchallownershipdata
            );

            $this->load->view('admin/collegeassociation', $data);
        }

        public function courseassociation()
        {
             $user_id =$this->session->userdata('user_id'); 
            
            $data['userInfo'] = $this->getUserInfo($user_id);

            $fetchalluniversitydata=$this->admin_model->fetchalluniversitydata();
            $fetchallcollegedata=$this->admin_model->fetchallcollegedata();
            $fetchallcoursesdata=$this->admin_model->fetchallcoursesdata();
            $fetchallspecdata=$this->admin_model->fetchallspecdata();
            //echo "<pre>"; print_r($fetchallspecdata); exit;
            $fetchalldegreedata=$this->admin_model->fetchalldegreedata();
            $fetchallcredentialdata=$this->admin_model->fetchallcredentialdata();
            $fetchallmodedata=$this->admin_model->fetchallmodedata();
            $fetchallmediumdata=$this->admin_model->fetchallmediumdata();
            $fetchallrecognitiondata=$this->admin_model->fetchallrecognitiondata();
            $fetchallcoursestatusdata=$this->admin_model->fetchallcoursestatusdata();
            $fetchallaccreditationdata=$this->admin_model->fetchallaccreditationdata();
            $fetchallownershipdata=$this->admin_model->fetchallownershipdata();

            $data = array(
                'fetchalluniversitydata' => $fetchalluniversitydata,
                'fetchallcollegedata' => $fetchallcollegedata,
                'fetchallcoursesdata' => $fetchallcoursesdata,
                'fetchallspecdata' => $fetchallspecdata,
                'fetchalldegreedata' => $fetchalldegreedata,
                'fetchallcredentialdata' => $fetchallcredentialdata,
                'fetchallmodedata' => $fetchallmodedata,
                'fetchallmediumdata' => $fetchallmediumdata,
                'fetchallrecognitiondata' => $fetchallrecognitiondata,
                'fetchallcoursestatusdata' => $fetchallcoursestatusdata,
                'fetchallaccreditationdata' => $fetchallaccreditationdata,
                'fetchallownershipdata' => $fetchallownershipdata
            );
            
            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/courseassociation', $data);
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/courseassociation.js');
        }

        public function submitcourseassociation() {
            $_POST = json_decode(file_get_contents('php://input'), true);
           
            /*echo $this->input->post('cauniversity');
            echo '<br>';
            print_r($this->input->post('eligibility'));
            echo '<br>';
            print_r($this->input->post('cajobdescription'));
            echo '<br>';
            echo $this->input->post('jobdescription');
            echo '<br>';
            print_r($_POST); exit;*/
            //print_r($_FILES); exit;
            
            if ($this->input->post()) {
                $this->form_validation->set_rules('eligibility', 'Eligibility', 'trim|required|trim|xss_clean|strip_tags');
                if ($this->form_validation->run() == TRUE) {
                    /*print_r('if');
                    exit;*/

                date_default_timezone_set('UTC');
                $course['universityid'] = $this->input->post('cauniversity');
                $course['collegeid'] = $this->input->post('cacollege');
                $course['timestamp'] = date("Y-m-d H:i:s");

                $collegecourse['courseid'] = $this->input->post('cacourse');
                $collegecourse['specializationid'] = $this->input->post('caspec');
                $collegecourse['credentialid'] = $this->input->post('cacredential');
                $collegecourse['degreeid'] = $this->input->post('cadegree');
                $collegecourse['modeofstudyid'] = $this->input->post('camode');
                $collegecourse['duration'] = $this->input->post('caduration');
                $collegecourse['mediumid'] = $this->input->post('camedium');
                $collegecourse['recognitionid'] = $this->input->post('carecognition');
                $collegecourse['coursestatusid'] = $this->input->post('cacoursestatus');
                $collegecourse['accreditationid'] = $this->input->post('caaccreditation');
                $collegecourse['ownershipid'] = $this->input->post('caownership');
                $collegecourse['totalfees'] = $this->input->post('cafees');
                $collegecourse['eligibility'] = $this->input->post('eligibility');
                $collegecourse['coursestructure'] = $this->input->post('coursestructure');
                $collegecourse['timestamp'] = date("Y-m-d H:i:s");          
                
                $status = $this->input->post('enabled');
                if(is_null($status)) {
                    //echo 'is_null';
                    $course['enabled'] = 1;
                    $collegecourse['enabled'] = 1;
                    //echo $course['enabled'];
                } else {
                    //echo 'not_is_null';
                    $course['enabled'] = $this->input->post('enabled');
                    $collegecourse['enabled'] = $this->input->post('enabled');
                    //echo $course['enabled'];
                }           
                //exit;
                
                /*if ($status == "on") {
                    $course['enabled'] = 1;
                } else {
                    $course['enabled'] = 0;
                }*/
                
                $response = $this->admin_model->submit_courseassociation($course,$collegecourse);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Course has been associated successfully.</div>');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0');
                    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while associating course.</div>');
                    }
                } else {
            
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                
            }
           //print_r($data);            
        }
        else {
            $data = array('status' => '3');
            $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }

        public function updatecourseassociation() {
            //$_POST = json_decode(file_get_contents('php://input'), true);
            
            if ($this->input->post()) {
                $this->form_validation->set_rules('eligibility', 'Eligibility', 'trim|required|trim|xss_clean|strip_tags');
                if ($this->form_validation->run() == TRUE) {
                    /*print_r('if');
                    exit;*/

                $course['universityid'] = $this->input->post('caeuniversity');
                $course['collegeid'] = $this->input->post('caecollege');

                $course['id'] = $this->input->post('hiddenid');

                $collegecourse['courseid'] = $this->input->post('caecourse');
                $collegecourse['specializationid'] = $this->input->post('caespec');
                $collegecourse['credentialid'] = $this->input->post('caecredential');
                $collegecourse['degreeid'] = $this->input->post('caedegree');
                $collegecourse['modeofstudyid'] = $this->input->post('caemode');
                $collegecourse['duration'] = $this->input->post('caeduration');
                $collegecourse['mediumid'] = $this->input->post('caemedium');
                $collegecourse['recognitionid'] = $this->input->post('caerecognition');
                $collegecourse['coursestatusid'] = $this->input->post('caecoursestatus');
                $collegecourse['accreditationid'] = $this->input->post('caeaccreditation');
                $collegecourse['ownershipid'] = $this->input->post('caeownership');
                $collegecourse['totalfees'] = $this->input->post('caefees');
                $collegecourse['eligibility'] = $this->input->post('eligibility');
                $collegecourse['coursestructure'] = $this->input->post('coursestructure');          
                
                $status = $this->input->post('enabled');
                if(is_null($status)) {
                    //echo 'is_null';
                    $course['enabled'] = 1;
                    $collegecourse['enabled'] = 1;
                    //echo $course['enabled'];
                } else {
                    //echo 'not_is_null';
                    $course['enabled'] = $this->input->post('enabled');
                    $collegecourse['enabled'] = $this->input->post('enabled');
                    //echo $course['enabled'];
                }           
                //exit;
                
                /*if ($status == "on") {
                    $course['enabled'] = 1;
                } else {
                    $course['enabled'] = 0;
                }*/
                
                $response = $this->admin_model->update_courseassociation($course,$collegecourse);
                if ($response) {
                    //echo '<pre>';print_r($response);exit;
                    $data = array('status' => '1');
                    $this->session->set_flashdata('successmsg', '<div class="alert alert-success"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Course association has been updated successfully!</div>');
                    redirect(base_url().'admin/addcollege');
                    }
                    else {
                    //$data = array('status' => '0');
                    $data = array('status' => '0');
                    $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while updating course association!</div>');
                    redirect(base_url().'admin/addcollege');
                    }
                } else {
            
                $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                
            }
           //print_r($data);            
        }
        else {
            $data = array('status' => '3');
            $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');
       }
        //print_r($data); exit;
        print_r(json_encode($data));
        //echo json_encode($data);
        }

        function getallasccourses() {
            $asccoursesdata=$this->admin_model->getallasccoursesdata();
            //echo "<pre>"; print_r($asccoursesdata); exit;
            if(!empty($asccoursesdata)) {
            foreach($asccoursesdata as $k=>$v){
            //print_r($v); exit;

                    $results[] = array(
                'srno' => $k+1,
                'id' => $v->id,
                'tcaid' => $v->tcaid,
                'ascenabled' => $v->ascenabled,
                'name' => $v->name,
                'specializationname' => $v->specializationname,
                'checkarray' => 1
                 );
            }
        }
        else {
            $results[] = array(
                'checkarray' => 0
                 ); 
        }
            echo json_encode($results);
        }

        public function allcollege()
        {
             $user_id =$this->session->userdata('user_id'); 
            
            $data['userInfo'] = $this->getUserInfo($user_id);



            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/allcollege');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/allcollege.js');
        }

        function getallcollege() {

            $collegedata=$this->admin_model->getallcollegedata();
           // print_r($collegedata); exit();
            foreach($collegedata as $k=>$v){
            //print_r($v); exit;

                $time=strtotime($v->created_date);
            $day=date("d",$time);
            $month=date("M",$time);
            $year=date("Y",$time);

                    $results[] = array(
                'srno' => $k+1,
                'id' => $v->id,
                'link' => base_url().'admin/viewCourses/'.$v->id,
                'universityname' => $v->universityname,
                'name' => $v->name,
                'address' => $v->address,
                'url' => $v->url,
                'enabled' => $v->enabled,
                'day' => $day,
                'month' => $month,
                'year' => $year
                 );
            }
            echo json_encode($results);
        }

        
        /*function for view all college courses*/

        public function viewCourses($colgId)
        {

           // echo $colgId.'  '.$universityId; exit;

            $user_id =$this->session->userdata('user_id'); 
            $data['userInfo'] = $this->getUserInfo($user_id);

            $data['universityAndCollegeInfo'] = $this->admin_model->getCollegeData($colgId);

           // echo "<pre>"; print_r($data['universityAndCollegeInfo']); exit;



            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/allcollegeCourses',$data);
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/allcollegeCourses.js');
        }




        function deletecollege() {
            //$_POST = json_decode(file_get_contents('php://input'), true);
            //echo "asd".$this->input->post('id'); exit;
            $deletingId=$this->input->post('deletingId');
            if($this->admin_model->deletecollegedata($deletingId))
            {
                $this->session->set_flashdata("collegeDeletd",'College deleted successfully');
                //$data = 1;
            }
            else{
                
                $this->session->set_flashdata("errormsg", '<div class="alert alert-danger" style="color:#a94442;font-size:13px;    padding:10px;width:35%;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Error while deleting college!</div>');
                //$data =0;
            }
        }




       public function formValidation()
       {

          
            $this->form_validation->set_rules('universityname', 'University Name', 'required');
            $this->form_validation->set_rules('universityshortname', 'University Short Name', 'required');
            $this->form_validation->set_rules('universityurl', 'Uversityurl Url', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('stateName', 'State Name', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('contactNo', 'Contact No', 'required');
            $this->form_validation->set_rules('universityType', 'University Type', 'required');
            $this->form_validation->set_rules('accreditions', 'Accreditions', 'required');
            $this->form_validation->set_rules('approvedby', 'Approvedby', 'required');
            $this->form_validation->set_rules('faxno', 'faxno', 'required');
            $this->form_validation->set_rules('established', 'Established', 'required');
            $this->form_validation->set_rules('about', 'About', 'required');
            $this->form_validation->set_rules('logoname', 'Logoname', 'required');
            $this->form_validation->set_rules('bannerName', 'BannerName', 'required');

            $this->form_validation->set_rules('metaKeyword', 'Meta Keyword', 'required');
            $this->form_validation->set_rules('titleTag', 'Title Tag', 'required');
            $this->form_validation->set_rules('metaDescription', 'Meta Description', 'required');
            $this->form_validation->set_rules('metaRobot', 'Meta Robot', 'required');
            $this->form_validation->set_rules('brochurename', 'Brochure', 'required');

       }




       public function formValidationForCollege()
       {



            $this->form_validation->set_rules('universityId', 'University Id', 'required');
            $this->form_validation->set_rules('collegeName', 'College Name', 'required');
            $this->form_validation->set_rules('collegeSrtName', 'College Short Name', 'required');
            $this->form_validation->set_rules('collegeUrl', 'College Website', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('stateName', 'State', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('contactNo', 'Contact No', 'required');
            $this->form_validation->set_rules('collegeType', 'College Type', 'required');
            $this->form_validation->set_rules('accreditions', 'Accreditions', 'required');
            $this->form_validation->set_rules('approvedby', 'Approved By', 'required');
            $this->form_validation->set_rules('faxno', 'Fax No', 'required');
            $this->form_validation->set_rules('established', 'Established', 'required');
            $this->form_validation->set_rules('about', 'About', 'required');
            $this->form_validation->set_rules('logoname', 'Logoname', 'required');
            $this->form_validation->set_rules('bannerName', 'BannerName', 'required');

            $this->form_validation->set_rules('metaKeyword', 'Meta Keyword', 'required');
            $this->form_validation->set_rules('titleTag', 'Title Tag', 'required');
            $this->form_validation->set_rules('metaDescription', 'Meta Description', 'required');
            $this->form_validation->set_rules('metaRobot', 'Meta Robot', 'required');
            $this->form_validation->set_rules('brochurename', 'Brochure Name', 'required');
           // / $this->form_validation->set_rules('bannerName', 'BannerName', 'required');


       }

/*function for validate department*/

function formValidationForDeparment()
{

    $this->form_validation->set_rules('universityId', 'University Id', 'required');
    $this->form_validation->set_rules('type', 'Type', 'required');
    $this->form_validation->set_rules('departmentName', 'Department Name ', 'required');
    $this->form_validation->set_rules('aboutDept', 'About Department', 'required');
    $this->form_validation->set_rules('contactNoDept', 'Contact No. ', 'required');
    $this->form_validation->set_rules('emailDept', 'Email Dept', 'required');
    $this->form_validation->set_rules('faxnoDept', 'Fax No.', 'required');
    $this->form_validation->set_rules('establishedDept', 'Department established', 'required');
    $this->form_validation->set_rules('metaKeywordDept', 'Meta Keyword', 'required');
    $this->form_validation->set_rules('titleTagDept', 'Title Tag', 'required');
    $this->form_validation->set_rules('metaDescriptionDept', 'Meta Description', 'required');
    $this->form_validation->set_rules('metaRobotDept', 'Meta Robot', 'required');
    $this->form_validation->set_rules('bannerNameDept', 'Banner Name', 'required');
    $this->form_validation->set_rules('brochurenameDept', 'Brochure Name', 'required');
    // $this->form_validation->set_rules('universityId', 'University Id', 'required');




}


     /*================= start Jainendra's functions ==============*/
/*function for upload university logo*/
    function uploadLogo()
    {

                  $user_id = $this->session->userdata('user_id');
                  $universityname = $this->input->post('universityname');
                  //echo $universityname; exit;
                   

                   // echo   $user_id; 
                    $new_name ='logo';
                    $config['upload_path']          = './uploads/logo/universities/';
                    $config['allowed_types']        = 'jpg|png|jpeg';
                    $config['max_size']             = 0;
                    $config['max_width']            = 220;
                    $config['max_height']           = 220;
                    $config['min_width']            = 119;
                    $config['min_height']           = 119;
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = $universityname.'-'.$_FILES["logo-0"]["name"];

                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                  //  echo "<pre>";print_r($_FILES["logo-0"]["name"]);
                    if (!$this->upload->do_upload("logo-0"))
                    {
                            $error = array('error' => $this->upload->display_errors());
                           
                        //  print_r($error['error']);
                             
                            if($error['error']="The image you are attempting to upload doesn't fit into the allowed dimensions.")
                            {
                                  
                                $data = array('status'=>'1');
                                print_r(json_encode($data));
                            }
                  
                           
                    }
                    else
                    {  

                         $data = array('upload_data' => $this->upload->data());

                            foreach ($data as $key => $value) {
                                $fileName=($value['file_name']);
                            }
                            if($data)
                            {
                          // $save = $this->admin_model->saveUniversityLogo($fileName,$user_id); 
                           $data = array('upload_data' => $this->upload->data());

                            $data = array('status'=>'2','fileName'=>$fileName);
                                print_r(json_encode($data));
                      }

            }



     
         }




/*function for upload university brochure*/
function uploadBrochure()
    {     

               //   echo "<pre>"; print_r($_FILES); exit;

                  $user_id = $this->session->userdata('user_id');
                  $universityname = $this->input->post('universityname');
                  //echo $universityname; exit;
                   

                   // echo   $user_id; 
                    $new_name ='logo';
                    $config['upload_path']          = './uploads/brochure/universities/';
                    $config['allowed_types']        = 'pdf';
                    $config['max_size']             = 1024;
                  
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = $universityname.'-'.$_FILES["brochure-0"]["name"];

                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                  //  echo "<pre>";print_r($_FILES["logo-0"]["name"]);
                    if (!$this->upload->do_upload("brochure-0"))
                    {
                            $error = array('error' => $this->upload->display_errors());
                           
                        // print_r($error['error']);
                             
                            if($error['error']="The image you are attempting to upload doesn't fit into the allowed dimensions.")
                            {
                                  
                                $data = array('status'=>'1');
                                print_r(json_encode($data));
                            }
                  
                           
                    }
                    else
                    {     
                       // $fileName = $universityname.'-'.$_FILES["logo-0"]["name"];

                      //  echo $fileName; exit;
                           
                            $data = array('upload_data' => $this->upload->data());

                            foreach ($data as $key => $value) {
                                $fileName=($value['file_name']);
                            }

                           
                            if($data)
                            {
                          // $save = $this->admin_model->saveUniversityLogo($fileName,$user_id); 
                           $data = array('upload_data' => $this->upload->data());

                            $data = array('status'=>'2','fileName'=>$fileName);
                                print_r(json_encode($data));
                      }

            }



     
         }



/*function for upload college brochure*/
function uploadCollegeBrochure()
    {     

               //   echo "<pre>"; print_r($_FILES); exit;

                  $user_id = $this->session->userdata('user_id');
                  $universityname = $this->input->post('collegeName');
                  //echo $universityname; exit;
                   

                   // echo   $user_id; 
                    $new_name ='logo';
                    $config['upload_path']          = './uploads/brochure/colleges/';
                    $config['allowed_types']        = 'pdf';
                    $config['max_size']             = 1024;
                  
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = $universityname.'-'.$_FILES["brochure-0"]["name"];

                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                  //  echo "<pre>";print_r($_FILES["logo-0"]["name"]);
                    if (!$this->upload->do_upload("brochure-0"))
                    {
                            $error = array('error' => $this->upload->display_errors());
                           
                        // print_r($error['error']);
                             
                            if($error['error']="The image you are attempting to upload doesn't fit into the allowed dimensions.")
                            {
                                  
                                $data = array('status'=>'1');
                                print_r(json_encode($data));
                            }
                  
                           
                    }
                    else
                    {     
                       // $fileName = $universityname.'-'.$_FILES["logo-0"]["name"];

                      //  echo $fileName; exit;


                           
                            $data = array('upload_data' => $this->upload->data());
                         //    echo "<pre>"; print_r($data); exit;

                            foreach ($data as $key => $value) {
                                $fileName=($value['file_name']);
                            }

                           
                            if($data)
                            {
                          // $save = $this->admin_model->saveUniversityLogo($fileName,$user_id); 
                           $data = array('upload_data' => $this->upload->data());

                            $data = array('status'=>'2','fileName'=>$fileName);
                                print_r(json_encode($data));
                      }

            }



     
         }
     

    function uploadCollegeLogo()
    {

                  $user_id = $this->session->userdata('user_id');
                  $collegename = $this->input->post('collegeName');
                  
                //  echo $collegename; exit;
                   

                   // echo   $user_id; 
                    $new_name ='logo';
                    $config['upload_path']          = './uploads/logo/colleges/';
                    $config['allowed_types']        = 'jpg|png|jpeg';
                    $config['max_size']             = 0;
                    $config['max_width']            = 120;
                    $config['max_height']           = 120;
                    $config['min_width']            = 119;
                    $config['min_height']           = 119;
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = $collegename.'-'.$_FILES["logo-0"]["name"];

                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                  //  echo "<pre>";print_r($_FILES["logo-0"]["name"]);
                    if (!$this->upload->do_upload("logo-0"))
                    {
                            $error = array('error' => $this->upload->display_errors());
                           
                        //  print_r($error['error']);
                             
                            if($error['error']="The image you are attempting to upload doesn't fit into the allowed dimensions.")
                            {
                                  
                                $data = array('status'=>'1');
                                print_r(json_encode($data));
                            }
                 
                           
                    }
                    else
                    {     
                       // $fileName = $collegename.'-'.$_FILES["logo-0"]["name"];
                           
                            $data = array('upload_data' => $this->upload->data());

                             foreach ($data as $key => $value) {
                                $fileName=($value['file_name']);
                            }
 

                            if($data)
                            {
                          // $save = $this->admin_model->saveUniversityLogo($fileName,$user_id); 
                           $data = array('upload_data' => $this->upload->data());

                            $data = array('status'=>'2','fileName'=>$fileName);
                                print_r(json_encode($data));
                      }

            }



     
         }



        /*function for upload banner*/
        public function uploadBanner()
        {

             $universityname = $this->input->post('universityname');

             $user_id = $this->session->userdata('user_id');
                   

                   // echo   $user_id; 
                    
                    $config['upload_path']          = './uploads/banner/universities/';
                    $config['allowed_types']        = 'jpg|png|jpeg';
                    $config['max_size']             = 0;
                    $config['max_width']            = 1920;
                    $config['max_height']           = 1080;
                    /*$config['min_width']            = 273;
                    $config['min_height']           = 129;*/
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = $universityname.'-'.$_FILES["banner-0"]["name"];

                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                  //  echo "<pre>";print_r($_FILES["logo-0"]["name"]);

                    if (!$this->upload->do_upload("banner-0"))
                    {
                            $error = array('error' => $this->upload->display_errors());
                           
                        //  print_r($error['error']);
                             
                            if($error['error']="The image you are attempting to upload doesn't fit into the allowed dimensions.")
                            {
                                  
                                $data = array('status'=>'1');
                                print_r(json_encode($data));
                            }
                  
                           
                    }
                    else
                    {     
                        //$fileName = $universityname.'-'.$_FILES["banner-0"]["name"];
                           
                            $data = array('upload_data' => $this->upload->data());
                             foreach ($data as $key => $value) {
                                $fileName=($value['file_name']);
                            }
                            if($data)
                            {
                         
                           $data = array('upload_data' => $this->upload->data());

                            $data = array('status'=>'2','fileName'=>$fileName);
                                print_r(json_encode($data));
                      }

            }

        

        }


        public function uploadCollegeBanner()
        {

             $collegeName = $this->input->post('collegeName');

             $user_id = $this->session->userdata('user_id');
                   


                   // echo   $user_id; 
                    
                    $config['upload_path']          = './uploads/banner/colleges/';
                    $config['allowed_types']        = 'jpg|png|jpeg';
                    $config['max_size']             = 0;
                    $config['max_width']            = 1920;
                    $config['max_height']           = 1080;
                    $config['min_width']            = 273;
                    $config['min_height']           = 129;
                    $config['overwrite'] = TRUE;
                    $config['file_name'] = $collegeName.'-'.$_FILES["banner-0"]["name"];

                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                  //  echo "<pre>";print_r($_FILES["logo-0"]["name"]);

                    if (!$this->upload->do_upload("banner-0"))
                    {
                            $error = array('error' => $this->upload->display_errors());
                           
                        //  print_r($error['error']);
                             
                            if($error['error']="The image you are attempting to upload doesn't fit into the allowed dimensions.")
                            {
                                  
                                $data = array('status'=>'1');
                                print_r(json_encode($data));
                            }
                  
                           
                    }
                    else
                    {     
                        //$fileName = $universityname.'-'.$_FILES["banner-0"]["name"];
                           
                            $data = array('upload_data' => $this->upload->data());
                             foreach ($data as $key => $value) {
                                $fileName=($value['file_name']);
                            }
                            if($data)
                            {
                         
                           $data = array('upload_data' => $this->upload->data());

                            $data = array('status'=>'2','fileName'=>$fileName);
                                print_r(json_encode($data));
                      }

            }

        

        }



         function getStateCities()
         {

            //roll no. C182001
             $stateName = $this->input->post('stateName');

             $data['cities'] = $this->admin_model->getStateCities($stateName);

            // echo "<pre>"; print_r($data['cities']); exit;

             $stateCities = $this->load->view('admin/citiesOptions',$data,true);

               $stateCities2 = array('cities'=>$stateCities);

               echo json_encode($stateCities2); 
              
         }
        /*function for upload gallery images*/

        function uploadGalleryImages()
        {


            //echo "<pre>"; print_r($_FILES); exit;

             $universityname = $this->input->post('universityname');

             $user_id = $this->session->userdata('user_id');
              
                  
                  $config = array(
                'upload_path'   => './uploads/galleryImages/universities/',
                'allowed_types' => 'jpg|gif|png',
                'overwrite'     => 1, 
                'min_width' => 750,
                'max_height' => 500,
                'max_width'   => 750,
                'max_height' => 500,
                'overwrite' => TRUE,
                                     
               );
                  
                   $images = array();

            foreach ($_FILES as $key => $image) {
                $_FILES['images[]']['name']= $image['name'];
                $_FILES['images[]']['type']= $image['type'];
                $_FILES['images[]']['tmp_name']= $image['tmp_name'];
                $_FILES['images[]']['error']= $image['error'];
                $_FILES['images[]']['size']= $image['size'];

                $fileName = $universityname .'-'. $image['name'];

                $images[] = $fileName;

                $config['file_name'] = $fileName;

                $this->upload->initialize($config);
                $flag = false;
                $dataerror = "";
                $datasucc ="";
                    if (!$this->upload->do_upload($key))
                    {
                            $error = array('error' => $this->upload->display_errors());
                            $flag = true;
                        //  print_r($error['error']);
                             
                            if($error['error']="The image you are attempting to upload doesn't fit into the allowed dimensions.")
                            {
                                  
                                $dataerror = array('status'=>'1');
                                // print_r(json_encode($arr));
                            }
                  
                           
                    }
                    else{     

                           $data = array('upload_data' => $this->upload->data());

                        //   echo "<pre>"; print_r($data); exit;
                            foreach ($data as $key => $value) {
                                $fileName=($value['file_name']);
                            }
                            if($data)
                            {   $flag = true;
                                $datasucc = array('status' =>2);
                                $saveNames = $this->admin_model->saveGalleryImages($universityname,$fileName);
                         
                            $data = array('upload_data' => $this->upload->data());
  
                            }

                      }


            }
             if($flag)
                {
                    if($dataerror){
                       
                    $error = array('error'=>$dataerror);
                     print_r(json_encode($error));
                 }

                     if($datasucc)
                     {
                        
                       $success = array('success' => $datasucc);
                       print_r(json_encode($success));

                     }

                }

           // echo "<pre>"; print_r($images); exit;

      }


      
      /*function for upload college gallery images */

        function uploadCollegeGalleryImages()
        {

          //  echo "<pre>"; print_r($_FILES);

             $collegeName = $this->input->post('collegeName');

             $user_id = $this->session->userdata('user_id');
              
                  
                  $config = array(
                'upload_path'   => './uploads/galleryImages/colleges/',
                'allowed_types' => 'jpg|gif|png',
                'overwrite'     => 1, 
                'min_width' => 750,
                'max_height' => 500,
                'max_width'   => 750,
                'max_height' => 500,
                'overwrite' => TRUE,
                                     
               );
                  
                   $images = array();

            foreach ($_FILES as $key => $image) {
                $_FILES['images[]']['name']= $image['name'];
                $_FILES['images[]']['type']= $image['type'];
                $_FILES['images[]']['tmp_name']= $image['tmp_name'];
                $_FILES['images[]']['error']= $image['error'];
                $_FILES['images[]']['size']= $image['size'];

               $fileName = $collegeName .'-'. $image['name'];

                $images[] = $fileName;

                $config['file_name'] = $fileName;

                $this->upload->initialize($config);
                $flag = false;
                $dataerror = "";
                $datasucc ="";
                    if (!$this->upload->do_upload($key))
                    {
                            $error = array('error' => $this->upload->display_errors());
                            $flag = true;
                        //  print_r($error['error']);
                             
                            if($error['error']="The image you are attempting to upload doesn't fit into the allowed dimensions.")
                            {
                                  
                                $dataerror = array('status'=>'1');
                                // print_r(json_encode($arr));
                            }
                  
                           
                    }
                    else{    


                           $data = array('upload_data' => $this->upload->data());
                         // echo "<pre>"; print_r($data); 

                            foreach ($data as $key => $value) {
                                $fileName=($value['file_name']);

                                 //echo $fileName;
                            }//exit;
                            if($data)
                            {   $flag = true;
                                $datasucc = array('status' =>2);
                                $saveNames = $this->admin_model->saveCollegeGalleryImages($collegeName,$fileName);
                         
                            $data = array('upload_data' => $this->upload->data());
  
                            }

                      }


            }
             if($flag)
                {
                    if($dataerror){
                       
                    $error = array('error'=>$dataerror);
                     print_r(json_encode($error));
                 }

                     if($datasucc)
                     {
                        
                       $success = array('success' => $datasucc);
                       print_r(json_encode($success));

                     }

                }

           // echo "<pre>"; print_r($images); exit;

      }




     /*function for add programs*/

     public function addPrograms()
     {

           if(!$this->session->userdata('user_id'))
            {
                redirect(base_url().'admin/');

            }

          if(!empty($_POST))
          {

        // echo "<pre>"; print_r($_POST);  exit;

            $programeType = $this->input->post('programType');
            $stream = $this->input->post('stream');
            $courseName = $this->input->post('courseName');
           


              foreach ($programeType as $key => $program)
                  {
                    $cstreamName = $stream[$key];
                    $courseN =   $courseName[$key];
                   

                              $data = array(
                                             'course_type'=>$program,
                                             'course_stream'=>$cstreamName,
                                              'course_name' => $courseN,
                                             
                                          );

                                          //  echo "<pre>"; print_r($data);

                                  $save = $this->admin_model->saveMasterCourses($data);          
                             }//exit();
     
                      
                      $this->session->set_flashdata('courseSaved','Course has been saved successfully');

                      redirect(base_url().'admin/allPrograms');


          }else{


            $user_id =$this->session->userdata('user_id'); 
            
            $data['userInfo'] = $this->getUserInfo($user_id);
            $data['ProgramType'] = $this->admin_model->getProgramType();  
            $data['streams'] = $this->admin_model->getStream();  
          //  $data['allUniversities'] = $this->admin_model->getAllUniversities();

            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/addPrograms');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/addPrograms.js');
       }
     }

    public function getProgramType()
    {
        
        

       $data['ProgramType'] = $this->admin_model->getProgramType();

       //echo  "<pre>"; print_r($data['ProgramType']); exit;

       $programsType = $this->load->view('admin/programType',$data,true);

               $options = array('programs'=>$programsType);

               echo json_encode($options); 


    }



   public function getStreams()
    {
        
      

       $data['streamsData'] = $this->admin_model->getStream();

      // echo  "<pre>"; print_r($data['streamsData']); exit;

       $streams = $this->load->view('admin/streamsOptions',$data,true);

               $options = array('streams'=>$streams);

               echo json_encode($options); 


    }




    public function getCoursesName()
    {

        $data['courses']=$this->admin_model->getAllMasterCourses();
          $courses = $this->load->view('admin/coursesOptions',$data,true);

               $options = array('courses'=>$courses);

               echo json_encode($options); 
    }



    /*function for add courses to university/college */

    public function addCourses($colgId='',$universityId='')
    {
          
         // echo $colgId.'   '.$universityId; exit;

       if(!$this->session->userdata('user_id'))
       {

         redirect(base_url().'admin');
       }


       if(!empty($_POST))
       {

          // echo "<pre>"; print_r($_POST); exit;

            $this->form_validation->set_rules('university', 'University Name', 'required');
            $this->form_validation->set_rules('college', 'College Name', 'required');
            $this->form_validation->set_rules('course', 'Course Name', 'required');
            $this->form_validation->set_rules('courseSpec', 'Course Specialization Name', 'required');
            $this->form_validation->set_rules('courseShortName', 'Course Short Name', 'required');
            $this->form_validation->set_rules('courseDuration', 'Course Duration', 'required');
            $this->form_validation->set_rules('courseFee', 'Course Fee', 'required');
            $this->form_validation->set_rules('feeStructure', 'Fee Structure', 'required');
            $this->form_validation->set_rules('courseEligibility', 'Course Eligibility', 'required');


            if($this->form_validation->run() == FALSE)
                    {
                      //  echo 'here'; exit;
                            $data['postdata'] = $this->input->post();
                            $data['errors'] = validation_errors();

                           // echo "<pre>"; print_r($data['errors']); exit;
                    }else{


                           $university = $this->input->post('university');
                           $college = $this->input->post('college');
                           $course = $this->input->post('course');
                           $courseSpec = $this->input->post('courseSpec');
                           $courseShortName = $this->input->post('courseShortName');
                           $courseDuration = $this->input->post('courseDuration');
                           $modeofstudy = $this->input->post('modeofstudy');
                           $courseFee = $this->input->post('courseFee');
                           $feeStructure = htmlentities($this->input->post('feeStructure'));
                           $courseEligibility = htmlentities($this->input->post('courseEligibility'));
                          // $courseShortName = $this->input->post('courseShortName');
                         

                          

                                 $data = array(
                               'university_id' =>$university,
                               'college_id' => $college,
                               'course_name' => $course,
                               'course_specialization' =>$courseSpec,
                               'course_shortname' => $courseShortName,
                               'course_duration ' => $courseDuration,
                               'study_mode ' => $modeofstudy,
                               'course_total_fee ' => $courseFee,
                               'course_fee_structure ' => $feeStructure,
                               'course_eligibility ' => $courseEligibility,

                           );
                          //   echo "<pre>"; print_r($data); exit();

                               
                               $this->admin_model->saveUniversitiesCollegesCourses($data);
                          
                         
                            $this->session->set_flashdata('courseCreated','College Courses added Successfully');

                            redirect(base_url().'admin/viewCourses/'.$college);
                    }



       }//else{

            $user_id =$this->session->userdata('user_id'); 
            $data['userInfo'] = $this->getUserInfo($user_id);
            $data['masterCourses'] = $this->admin_model->getAllMasterCourses();
            $data['universityAndCollegeInfo'] = $this->admin_model->getCollegeData($colgId);
           // echo "<pre>"; print_r($data['masterCourses']); exit;
            $data['allColleges'] = $this->admin_model->getAllColleges();
            $data['allUniversities'] = $this->admin_model->getAllUniversities();
            $data['courseDuration'] = $this->admin_model->getCourseDuration();
            $data['modeofstudy'] = $this->admin_model->getModeofStudy();

            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/addCourses',$data);
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/addCourses.js');
      //  }



    }


   /*function for getCollegeByUniversityID*/

   public function getCollegeByUniversityID()
   {
      $universityId = $this->input->post('universityId');

      $data['collegeData'] = $this->admin_model->getCollegeByUniversityID($universityId);

      $optionsView = $this->load->view('admin/collegesDropdown',$data,true);
      $options = array('colleges'=>$optionsView);

               echo json_encode($options); 


   }


   /*function for load view of collegeCourses*/
   public function allCollegeCourses()
   {

     

            $user_id =$this->session->userdata('user_id'); 
            $data['userInfo'] = $this->getUserInfo($user_id);

            // echo "<pre>"; print_r($data['collegeCourses']); exit;



            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/allcollegeCourses');
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/allcollegeCourses.js');


 

   }


/*function for get all college courses*/
public function getCollegeCourses($colgId)
{
 
  // $colgId = 4;

    $collegeCourses = $this->admin_model->getCollegeCourses($colgId);

       // echo "<pre>"; print_r($collegeCourses); exit;

         foreach($collegeCourses as $k=>$v){

              $results[] = array(

                  
                'srno' => $k+1,
                'id' => $v->id,
                'universityName' => $v->uniName,
                'colgName' => $v->colgName,
                'course_name' => $v->course_name,
                'course_specialization' => $v->course_specialization,
                'course_shortname' => $v->course_shortname,
                'course_stream' => $v->course_stream,
                'course_type' => $v->course_type
                
                 );

            }
           // echo "<pre>"; print_r($results); exit;
            echo json_encode($results);
        }


/*function for get load view show universities collegs*/

   public function viewColleges($universityId)
   {

    // echo $universityId; exit;
            $user_id =$this->session->userdata('user_id'); 
            $data['userInfo'] = $this->getUserInfo($user_id);
            
            $data['universitiyId'] = array('id'=>$universityId);
            $data['universityInfo'] = $this->admin_model->getuniversitydata($universityId);
     

       // echo "<pre>"; print_r($allUniversityColleges); exit;


            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/university_colleges',$data);
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/university_colleges.js');




   }


  /*function get universitiy colleges */

  public function getColleges($universityId='')
  {
      
     $allUniversityColleges = $this->admin_model->getAllUniversitiesColleges($universityId);

    //  echo "<pre>"; print_r($allUniversityColleges); exit;

        
        foreach($allUniversityColleges as $k=>$v){
          

                    $results[] = array(

                'url' =>  base_url().'admin/viewCourses/'.$v->id,     
                'srno' => $k+1,
                'id' => $v->id,
                'universityid' => $v->universityid,
                'colgName' => $v->name,
               /* 'course_name' => $v->course_name,
                'course_specialization' => $v->course_specialization,
                'course_shortname' => $v->course_shortname,
                'course_stream' => $v->course_stream,
                'course_type' => $v->course_type*/
                
                );
            
        //    echo "<pre>"; print_r($results); 
           

            } //exit;
         echo json_encode($results);
  }


  /*function for view college courses*/
  public function viewCollegeCourses()
  {
   $_POST = json_decode(file_get_contents('php://input'), true);
   // echo "<pre>"; print_r($_POST); exit;

   $colgId = $this->input->post('colgId');

   $collegeCourses = $this->admin_model->addedCollegeCourses($colgId);

       $data = array('status' =>1,'courses'=>$collegeCourses);
    echo json_encode($data);

  }



   /*function for add stream */

   public function addStream()
   {
       
        if(!empty($_POST))
        {

            // echo "<pre>"; print_r($_POST); exit;


            //  $this->form_validation->set_rules('programType[]', 'Program Type', 'required');
              $this->form_validation->set_rules('programStream[]', 'Program Stream', 'required');
            
            if($this->form_validation->run() == FALSE)
                    {
                       // echo 'here'; exit;
                            $data['postdata'] = $this->input->post();
                            $data['errors'] = validation_errors();
                    }else{
                      
                     //  $programType= $this->input->post('programType');
                       $programStream= $this->input->post('programStream');

                      // echo "<pre>"; print_r($programType);
                       // echo  "<pre>"; print_r($programStream); exit;


                       /*foreach ($programType as $key => $value) {
                            
                         $data = array(

                          'course_type' => $value,
                          'isEnable' =>1
                      );

                         $saveProgramType = $this->admin_model->saveProgramType($data);
                       }*///exit;
                     
                     foreach ($programStream as $key => $value) {
                         $data = array(

                            'stream_name' => $value
                        );

                        
                         $saveStreams = $this->admin_model->saveStreams($data);
                     }  


                      $this->session->set_flashdata('saveStreams','Program type and Stream saved');

                      redirect(base_url().'admin/addPrograms');
                }


        }


            $user_id =$this->session->userdata('user_id'); 
            $data['userInfo'] = $this->getUserInfo($user_id);

           
            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/addStreams',$data);
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/addStream.js');

   }


   /*function for edit college course*/

   public function editCourse($id='')
   {
       


          if(!empty($_POST)){

           //   echo "<pre>"; print_r($_POST); exit;

                  $this->form_validation->set_rules('university', 'University Name', 'required');
                  $this->form_validation->set_rules('college', 'College Name', 'required');
                  $this->form_validation->set_rules('course', 'Course Name', 'required');
                  $this->form_validation->set_rules('courseSpec', 'Course Specialization Name', 'required');
                  $this->form_validation->set_rules('courseShortName', 'Course Short Name', 'required');
                  $this->form_validation->set_rules('courseDuration', 'Course Duration', 'required');
                  $this->form_validation->set_rules('courseFee', 'Course Fee', 'required');
                  $this->form_validation->set_rules('feeStructure', 'Fee Structure', 'required');
                  $this->form_validation->set_rules('courseEligibility', 'Course Eligibility', 'required');


            if($this->form_validation->run() == FALSE)
                    {
                      //  echo 'here'; exit;
                            $data['postdata'] = $this->input->post();
                            $data['errors'] = validation_errors();

                           // echo "<pre>"; print_r($data['errors']); exit;
                    }else{
                          
                            $id = $this->input->post('id');
                           $university = $this->input->post('university');
                           $college = $this->input->post('college');
                           $course = $this->input->post('course');
                           $courseSpec = $this->input->post('courseSpec');
                           $courseShortName = $this->input->post('courseShortName');
                           $courseDuration = $this->input->post('courseDuration');
                           $modeofstudy = $this->input->post('modeofstudy');
                           $courseFee = $this->input->post('courseFee');
                           $feeStructure = htmlentities($this->input->post('feeStructure'));
                           $courseEligibility = htmlentities($this->input->post('courseEligibility'));
                          // $courseShortName = $this->input->post('courseShortName');
                         

                          

                                 $data = array(
                               'university_id' =>$university,
                               'college_id' => $college,
                               'course_name' => $course,
                               'course_specialization' =>$courseSpec,
                               'course_shortname' => $courseShortName,
                               'course_duration ' => $courseDuration,
                               'study_mode ' => $modeofstudy,
                               'course_total_fee ' => $courseFee,
                               'course_fee_structure ' => $feeStructure,
                               'course_eligibility ' => $courseEligibility,

                           );
                          //   echo "<pre>"; print_r($data); exit();

                               
                               $this->admin_model->updateCollegeCourseInfo($id,$data);
                          
                         
                            $this->session->set_flashdata('courseUpdated','Course Info Updated Successfully');

                            redirect(base_url().'admin/viewCourses/'.$college);
                    }

                  }
             
             $user_id =$this->session->userdata('user_id'); 
            
            $data['userInfo'] = $this->getUserInfo($user_id);
            $data['masterCourses'] = $this->admin_model->getAllMasterCourses();
            $data['courseDuration'] = $this->admin_model->getCourseDuration();
            $data['modeofstudy'] = $this->admin_model->getModeofStudy();

            $data['allCourseInfo'] = $this->admin_model->getAllCourseInfo($id);
           
         
            $this->load->view('admin/allcss');
            $this->load->view('admin/header', $data);
            $this->load->view('admin/editCourse',$data);
            $this->load->view('admin/alljs');
            $this->load->view('admin/pagewisejs/editCourse.js');
       
      }



      /*function for delete course*/

      public function deleteCourse()
      {
            
        $deleteId = $this->input->post('deletingId');

       $delCourse = $this->admin_model->deleteCourse($deleteId);

       if($delCourse)
       {
        $this->session->set_flashdata('couseDeleted','Course has been deleted successfully');

        
       }




      }
  
 function logout() {
              //$baseurl=base_url();
            $this->session->sess_destroy();
            redirect(base_url().'admin');
        }

    }