<?php
ob_start();
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Universities extends My_front
  {
  	
  	function __construct()
  	{
  		
  		parent::__construct();
  	}



  	public function index()
  	{
          
         $this->data['cities']= $this->home_model->getCities(); 
         $this->data['courses']= $this->home_model->getCourses(); 
         $this->data['specialization'] = $this->home_model->getSpecialization();

         // echo "<pre>"; print_r($this->data['specialization']); exit;

         $this->data['universities']= $this->home_model->getAllUniversity();
       // echo "<pre>"; print_r($this->data['cities']); exit();
          $total_row =count($this->home_model->getAllUniversity());
          
                

         $view = 'universities';

         array_push($this->scripts['css'], "frontend/css/jPages.css");
         array_push($this->scripts['js'], "frontend/js/jPages.js");
         array_push($this->scripts['js'], "frontend/js/pagesjs/universities.js");
         $this->display_view('frontend',$view,$this->data);

  	}

  	public function university()
  	{
         // echo 123; exit;
          
          $view = 'universities_details';
          $this->display_view('frontend',$view,$this->data);


  	}


// function for home seach
    public function homesearch(){

      
        $type = $this->input->get('type');
     
        $searched= $this->input->get('searchedKeyword');


             //   echo 'here'; exit;

     if(!empty($type)){

               array_push($this->scripts['css'], "frontend/css/jPages.css");
               array_push($this->scripts['js'], "frontend/js/jPages.js");
               if($type=='university')
               {

                  // echo 'college search'; exit;
                 $this->data['cities']= $this->home_model->getCities();
                 $this->data['courses']= $this->home_model->getCourses();
                 $this->data['specialization'] = $this->home_model->getSpecialization();

                  $this->data['universities'] = $this->home_model->homeSearchUniversityResult($searched);

                  $view = "universities";

                   array_push($this->scripts['js'], "frontend/js/pagesjs/universities.js");
                
                  
                   $this->display_view('frontend',$view,$this->data);

               }

                if($type=='college')
                {
                   $this->data['cities']= $this->home_model->getCities();
                   $this->data['courses']= $this->home_model->getCourses();
                   $this->data['specialization'] = $this->home_model->getSpecialization();

                  $this->data['colleges'] = $this->home_model->homeSearchCollegesResult($searched);
                   
                   $view="allColleges";
                   array_push($this->scripts['js'],"frontend/js/pagesjs/allColleges.js");

                    $this->display_view('frontend',$view,$this->data);
                   }
           }else{

            redirect(base_url());
           }
         }


  	 function search()
  	 {
        $location = $this->input->get('location');
      


     		if($this->input->is_ajax_request()){

                  
            $pagelocation = $this->input->get('getCollege') ? $this->input->get('getCollege'):'';
      			$location = $this->input->get('location') ? $this->input->get('location'): '';
      			$courses  = $this->input->get('courses') ? $this->input->get('courses'):'';
      			$specialization = $this->input->get('specialization') ? $this->input->get('specialization'):'';
                     
               $data['searched']= $this->commonSearch($location,$courses,$specialization);
             
             
              
              $searchedUni= $this->load->view('frontend/searchedUniversitiesView',$data,true);
                     
              $searchedResult = array('universities'=>$searchedUni);

              echo json_encode($searchedResult); 
          

               
  		}else{
         
    		$loc=$this->input->get('location') ? $this->input->get('location'): '';
  			$cour=$this->input->get('courses') ? $this->input->get('courses') : '';
        $spec=$this->input->get('specialization') ?  $this->input->get('specialization'): '';
  			$searchedKeywords=$this->input->get('searchedKeyword') ?  $this->input->get('searchedKeyword'): '';
        $universitiesId = $this->input->get('id') ?  $this->input->get('id'): '';
         
         // echo "<pre>"; print_r($searchedKeywords); exit;
       // echo $universitiesId; exit;

          			$location = !empty($loc)?explode(' ', $loc):'';
          			$courses = !empty($cour)? explode(' ', $cour):'';
          			$specialization = !empty($spec)?explode(' ', $spec):'';
               
                          $this->data['cities']= $this->home_model->getCities();
                          $this->data['courses']= $this->home_model->getCourses();
                          $this->data['specialization'] = $this->home_model->getSpecialization();
                          $this->data['loc']=$location;
                          $this->data['cour']=$courses;
                          $this->data['spec']=$specialization;



        

  			$this->data['universities'] = $this->commonSearch($location,$courses,$specialization,$universitiesId);
      
             $view = "universities";

             array_push($this->scripts['js'], "frontend/js/pagesjs/universities.js");
          
  		      
  		       $this->display_view('frontend',$view,$this->data);
  		}

      }

      function commonSearch($location,$courses,$specialization){

         	       
          $searched = $this->home_model->commonSearch($location,$courses,$specialization);

      
          return $searched;

      }

      //function for search colleges by filter

      public function searchColleges()
      {

         //  echo "<pre>"; print_r($_GET); exit;
                array_push($this->scripts['css'], "frontend/css/jPages.css");
                array_push($this->scripts['js'], "frontend/js/jPages.js"); 
        if($this->input->is_ajax_request()){

                  
            $pagelocation = $this->input->get('getCollege') ? $this->input->get('getCollege'):'';
            $location = $this->input->get('location') ? $this->input->get('location'): '';
            $courses  = $this->input->get('courses') ? $this->input->get('courses'):'';
            $specialization = $this->input->get('specialization') ? $this->input->get('specialization'):'';
                     
               $data['searched']= $this->commonSearchColleges($location,$courses,$specialization);

              //s echo "<pre>"; print_r($data['searched']); exit;
             
              $searchedUni= $this->load->view('frontend/searcheduniversityColleges',$data,true);
                    
              $searchedResult = array('universities'=>$searchedUni);

              echo json_encode($searchedResult); 
          

               
      }else{


        $loc=$this->input->get('location') ? $this->input->get('location'): '';
        $cour=$this->input->get('courses') ? $this->input->get('courses') : '';
        $spec=$this->input->get('specialization') ?  $this->input->get('specialization'): '';
        $searchedKeywords=$this->input->get('searchedKeyword') ?  $this->input->get('searchedKeyword'): '';
        $universitiesId = $this->input->get('id') ?  $this->input->get('id'): '';
         
         // echo "<pre>"; print_r($searchedKeywords); exit;
       // echo $universitiesId; exit;

                $location = !empty($loc)?explode(' ', $loc):'';
                $courses = !empty($cour)? explode(' ', $cour):'';
                $specialization = !empty($spec)?explode(' ', $spec):'';
               
                          $this->data['cities']= $this->home_model->getCities();
                          $this->data['courses']= $this->home_model->getCourses();
                          $this->data['specialization'] = $this->home_model->getSpecialization();
                          $this->data['loc']=$location;
                          $this->data['cour']=$courses;
                          $this->data['spec']=$specialization;

        $this->data['colleges'] = $this->commonSearchColleges($location,$courses,$specialization);

       // echo "<pre>"; print_r($this->data['colleges']); exit;
      
              $view="allColleges";
             array_push($this->scripts['js'],"frontend/js/pagesjs/allColleges.js");
          
            
             $this->display_view('frontend',$view,$this->data);
      }

     } 



        
        function commonSearchColleges($location,$courses,$specialization){

                 
          $searched = $this->home_model->commonSearchColleges($location,$courses,$specialization);

      
          return $searched;

      }


      // function for show all colleges
      public function allColleges()
      {

           $this->data['cities']= $this->home_model->getCities();
           $this->data['courses']= $this->home_model->getCourses();
           $this->data['specialization'] = $this->home_model->getSpecialization();
           $this->data['colleges'] = $this->home_model->getAllCollege();
           $view="allColleges";

                array_push($this->scripts['css'], "frontend/css/jPages.css");
                array_push($this->scripts['js'], "frontend/js/jPages.js"); 
             array_push($this->scripts['js'],"frontend/js/pagesjs/allColleges.js");
             $this->display_view('frontend',$view,$this->data);

      }


    // function for inner search
      public function innerSearch()
      {

          $location = $this->input->get('location');
          $universityId = $this->input->get('id') ? $this->input->get('id'): '';


        if($this->input->is_ajax_request()){
            
            $pagelocation = $this->input->get('getCollege') ? $this->input->get('getCollege'):'';
            $location = $this->input->get('location') ? $this->input->get('location'): '';
            $courses  = $this->input->get('courses') ? $this->input->get('courses'):'';
            $specialization = $this->input->get('specialization') ? $this->input->get('specialization'):'';
            $universityId=$this->input->get('id');
                     
               $data['searched']= $this->home_model->innerCommonSearch($location,$courses,$specialization,$universityId);
             
             
              
              $searchedUni=$this->load->view('frontend/searcheduniversityColleges',$data,true);
                     
              $searchedResult = array('universities'=>$searchedUni);

              echo json_encode($searchedResult); 
          

               
      }else{

        //echo 'here'; exit;

        $loc=$this->input->get('location') ? $this->input->get('location'): '';
        $cour=$this->input->get('courses') ? $this->input->get('courses') : '';
        $spec=$this->input->get('specialization') ?  $this->input->get('specialization'): '';
        $universityId = $this->input->get('id') ?  $this->input->get('id'): '';

       // echo $universityId; exit;

        $location = !empty($loc)?explode(' ', $loc):'';
        $courses = !empty($cour)? explode(' ', $cour):'';
        $specialization = !empty($spec)?explode(' ', $spec):'';
               
                          $this->data['cities']= $this->home_model->getCities();
                          $this->data['courses']= $this->home_model->getCourses();
                          $this->data['specialization'] = $this->home_model->getSpecialization();
                          $this->data['loc']=$location;
                          $this->data['cour']=$courses;
                          $this->data['spec']=$specialization;
                          $this->data['universityInfo']=$this->home_model->getUniversityInfo($universityId);



        

        $this->data['colleges'] = $this->home_model->innerCommonSearch($location,$courses,$specialization,$universityId);
      
             $view = "colleges";

             array_push($this->scripts['js'], "frontend/js/pagesjs/collegeCourses.js");
          
            
             $this->display_view('frontend',$view,$this->data);
      }

      }


      
   // function for searchCoursesofCollege

      public function innerSearch2()
      {

          
          $location = $this->input->get('location');
          $universityId = $this->input->get('id') ? $this->input->get('id'): '';


        if($this->input->is_ajax_request()){

            
            $pagelocation = $this->input->get('getCollege') ? $this->input->get('getCollege'):'';
            $location = $this->input->get('location') ? $this->input->get('location'): '';
            $courses  = $this->input->get('courses') ? $this->input->get('courses'):'';
            
            $specialization = $this->input->get('specialization') ? $this->input->get('specialization'):'';

            $colgId=$this->input->get('id');
                $data['universityAndCollegeInfo'] = $this->home_model->getCollgeInfo($colgId);     
               $data['collegeCourses']= $this->home_model->searchCollegeCourses($location,$courses,$specialization,$colgId);
                
              // echo "<pre>"; print_r($data['collegeCourses']); exit;
             
              
              $searchedCourses=$this->load->view('frontend/searchedCollegesCourses',$data,true);

             // / echo "<pre>"; print_r($searchedCourses); exit;
                     
              $searchedResult = array('searchedCourses'=>$searchedCourses);

              echo json_encode($searchedResult); 
          

               
      }else{

        // echo 'here'; exit;

        $loc=$this->input->get('location') ? $this->input->get('location'): '';
        $cour=$this->input->get('courses') ? $this->input->get('courses') : '';
        $spec=$this->input->get('specialization') ?  $this->input->get('specialization'): '';
        $colgId = $this->input->get('id') ?  $this->input->get('id'): '';

       // echo $colgId; exit;

        $location = !empty($loc)?explode(' ', $loc):'';
        $courses = !empty($cour)? explode(' ', $cour):'';
        $specialization = !empty($spec)?explode(' ', $spec):'';
               
                          $this->data['cities']= $this->home_model->getCities();
                          $this->data['courses']= $this->home_model->getCoursesName(); 
                          $this->data['specialization'] = $this->home_model->getSpecialization();
                          $this->data['loc']=$location;
                          $this->data['cour']=$courses;
                          $this->data['spec']=$specialization;
                          $this->data['universityInfo']=$this->home_model->getUniversityInfo($universityId);
               // echo "<pre>"; print_r($cour); exit;

          $this->data['courseName'] = $cour;
        
        $this->data['universityAndCollegeInfo'] = $this->home_model->getCollgeInfo($colgId);     
        $this->data['collegeCourses'] = $this->home_model->searchCollegeCourses($location,$courses,$specialization,$colgId);
               
              // echo "<pre>"; print_r($this->data['collegeCourses']); exit();
             $view = "collegeCourses";

             array_push($this->scripts['js'], "frontend/js/pagesjs/collegeCourses.js");
          
            
             $this->display_view('frontend',$view,$this->data);
      }


      }


// function for search university colleges by streams

      public function searches()
      {
            
      // echo 'sesrchxdfsdfds'; exit;

           $location = $this->input->get('location');
           $universityId = $this->input->get('id') ? $this->input->get('id'): '';
             


          if($this->input->is_ajax_request()){

            
           
            $location = $this->input->get('location') ? $this->input->get('location'): '';
            $courses  = $this->input->get('courses') ? $this->input->get('courses'):'';
            
            $specialization = $this->input->get('specialization') ? $this->input->get('specialization'):'';

            $universityId=$this->input->get('id');
                $data['universityAndCollegeInfo'] = $this->home_model->getCollgeofUniversity($universityId);     
               $data['colleges']= $this->home_model->searchCollegeStream($location,$courses,$specialization,$universityId);
                
             // echo "<pre>"; print_r($data['universityAndCollegeInfo']); exit;
             
              
              $searchedColleges=$this->load->view('frontend/universityStreamColleges',$data,true);

             // / echo "<pre>"; print_r($searchedCourses); exit;
                     
              $searchedResult = array('searchedCourses'=>$searchedColleges);

              echo json_encode($searchedResult); 
          

               
      }else{

           // echo 'here'; exit;

        $loc=$this->input->get('location') ? $this->input->get('location'): '';
        $cour=$this->input->get('courses') ? $this->input->get('courses') : '';
        $spec=$this->input->get('specialization') ?  $this->input->get('specialization'): '';
        $universityId = $this->input->get('id') ?  $this->input->get('id'): '';

      

        $location = !empty($loc)?explode(' ', $loc):'';
        $courses = !empty($cour)? explode(' ', $cour):'';
        $specialization = !empty($spec)?explode(' ', $spec):'';

               
                          $this->data['cities']= $this->home_model->getCities();
                          $this->data['courses']= $this->home_model->getCoursesName(); 
                          $this->data['specialization'] = $this->home_model->getSpecialization();
                          $this->data['loc']=$location;
                          $this->data['cour']=$courses;
                          $this->data['spec']=$specialization;
                          $this->data['university']=$this->home_model->getUniversityInfo($universityId);
               // echo "<pre>"; print_r($cour); exit;

          $this->data['courseName'] = $cour;
        
        $this->data['universityAndCollegeInfo'] = $this->home_model->getCollgeofUniversity($universityId);     
        $this->data['colleges'] = $this->home_model->searchCollegeStream($location,$courses,$specialization,$universityId);
               
            // echo "<pre>"; print_r($this->data['colleges']); exit;

             $view = "collegesByStream";

             array_push($this->scripts['js'], "frontend/js/pagesjs/collegeCourses.js");
          
            
             $this->display_view('frontend',$view,$this->data);
      }


    }

        
      // function for load university detail view 
      
      public function universityDetails($uniName,$id)
      {
           
          // get university name
         $this->data['universityInfo'] = $this->home_model->getUniversityInfo($id);
         // get all courses of university
         $this->data['allCourses'] = $this->home_model->getAllUniversityCourses($id);
         //get all colleges of university
         $this->data['allColleges'] = $this->home_model->getCollegesofUni($id);
         $this->data['allStreams'] = $this->home_model->getAllStreams();
         $this->data['userUniversityReview'] = $this->home_model->getAllRatingofUniversity($id);
  
           array_push($this->scripts['js'], "frontend/js/stars.min.js");
            array_push($this->scripts['js'], "frontend/js/pagesjs/universitiy_details.js");
          $view = 'universities_details';

          $this->display_view('frontend',$view,$this->data);
         
          
      }



      /*function for load from university college info view*/
      public function university_college($uniName,$courseName,$cShrtName,$colgId)
      {
         
       //echo $uniName.' '.$courseName.' '.$cShrtName.' '.$colgId; exit; 
         
        $this->data['collegeInfo'] = $this->home_model->getCollgeInfo($colgId);
        $this->data['allCourses']  = $this->home_model->getAllCollegeCourses($colgId); 
        $this->data['collegeGalleryImages'] = $this->home_model->getAllCollegeGalleryImages($colgId);
        $this->data['collegeCoursesStreams'] = $this->home_model->getAllCollegeCoursesStreams($colgId);
        $view = 'college_details';

          $this->display_view('frontend',$view,$this->data);




      }

      /*function for load  from home page college info view*/
      public function college_info($colgName,$colgId)
      {  

        // print_r($this->uri->segment('n')); exit;

       

     //  echo $colgName.' '.$colgId; exit; 
         
        $this->data['collegeInfo'] = $this->home_model->getCollgeInfo($colgId);
        $this->data['allCourses']  = $this->home_model->getAllCollegeCourses($colgId); 
        $this->data['collegeGalleryImages'] = $this->home_model->getAllCollegeGalleryImages($colgId);
        $this->data['collegeCoursesStreams'] = $this->home_model->getAllCollegeCoursesStreams($colgId);
        $this->data['colgAverageRating'] = $this->home_model->getAllRatingofCollege($colgId);
       
   
        array_push($this->scripts['js'], "frontend/js/stars.min.js");
        array_push($this->scripts['js'], "frontend/js/pagesjs/collegeDetails.js");
        $view = 'college_details';

          $this->display_view('frontend',$view,$this->data);




      }
     
     // function for load all colleges by university course

      function universityCollegesCourse($uniName,$c,$courseName,$uniId,$courseId)
      {
          
        //echo $uniName.' '.$courseName.' '.$c.' '.$uniId.' '.$courseId;   exit;    
        
          
         $this->data['cities']= $this->home_model->getCollegesCities(); 
         $this->data['courses']= $this->home_model->getCourses(); 
         $this->data['specialization'] = $this->home_model->getSpecialization();
         $this->data['university']= $this->home_model->getUniversityInfo($uniId);
         $this->data['colleges']= $this->home_model->getAllCollegsByCourse($uniId,$courseName);
         $view = 'collegesByCourse';
         array_push($this->scripts['js'], "frontend/js/pagesjs/collegeCoursesofUniversity.js");
         $this->display_view('frontend',$view,$this->data);




      }

      // function for load colleges by stream

      public function universityCollegesStream($uniName,$uniId,$c,$streamName,$courseId)
      {

      //echo $uniName.' '.$uniId.' '.$c.' '.$streamName.' '.$courseId;   exit;    

     
         $this->data['cities']= $this->home_model->getCollegesCities(); 
         $this->data['courses']= $this->home_model->getCoursesName(); 
         $this->data['specialization'] = $this->home_model->getSpecialization();

         // echo "<pre>"; print_r($this->data['specialization']); exit;
         $this->data['university']= $this->home_model->getUniversityInfo($uniId);
         //  echo "<pre>"; print_r($this->data['university']); exit;
         $this->data['colleges']= $this->home_model->getAllCollegsByStream($uniId,$streamName);

        // echo "<pre>"; print_r($this->data['colleges']); exit();
         // $total_row =count($this->home_model->getAllCollegsByCourse($courseName));
          
                

         $view = 'collegesByStream';
         array_push($this->scripts['js'], "frontend/js/pagesjs/collegeStream.js");
         $this->display_view('frontend',$view,$this->data);





      }
      //function for get courseDetail view
      public function course($cshrtNBame,$cName,$lId)
      {
         // echo $cshrtNBame."  ".$cName.' '.$lId; exit;

        $this->data['getAllCourseInfo'] = $this->home_model->getAllCourseInfo($lId);
          
          $view = 'course_details';
          
          $this->display_view('frontend', $view,$this->data);
          
      }


     


      /*function for load view collegeCourses*/

      public function collegeCourses($colgName,$colgId,$colName,$cid,$type,$courseName,$courseId)
        {
            
          //  echo $courseName; exit;

           $this->data['courseName'] = $courseName;
           $this->data['universityAndCollegeInfo'] = $this->home_model->getCollgeInfo($cid);
           $this->data['collegeCourses'] = $this->home_model->getAllCollegesCourses($courseName,$cid);
           
         //  echo "<pre>"; print_r($this->data['collegeCourses']); exit;


         $this->data['cities']= $this->home_model->getCities(); 
         $this->data['courses']= $this->home_model->getCoursesName(); 
         $this->data['specialization'] = $this->home_model->getSpecialization();


         // echo "<pre>"; print_r($this->data['specialization']); exit;

         $this->data['universities']= $this->home_model->getAllUniversity();
       // echo "<pre>"; print_r($this->data['cities']); exit();
          $total_row =count($this->home_model->getAllUniversity());
          
                

         $view = 'collegeCourses';
         // array_push($this->scripts['js'], "frontend/js/jquery.validate.min.js");
          array_push($this->scripts['js'], "frontend/js/pagesjs/collegeCourses.js");
         $this->display_view('frontend',$view,$this->data);

            

      }

       //function for show courses by streams

      public function collegeCoursesByStream($uniName,$collegeType,$colgName,$streamType,$streamName,$collegeId)
      {

        // echo $uniName." " .$collegeType." ".$colgName." ".$streamType." ".$streamName." ".$collegeId; exit;

            $this->data['courseName'] = $streamName;
            $this->data['universityAndCollegeInfo'] = $this->home_model->getCollgeInfo($collegeId);
            $this->data['cities']= $this->home_model->getCities(); 
            $this->data['courses']= $this->home_model->getCoursesName(); 
            $this->data['specialization'] = $this->home_model->getSpecialization();

           $this->data['collegeCourses'] = $this->home_model->getAllCollegesStreams($streamName,$collegeId);


            $this->data['universities']= $this->home_model->getAllUniversity();
       // echo "<pre>"; print_r($this->data['cities']); exit();
          $total_row =count($this->home_model->getAllUniversity());
          
                

         $view = 'collegeCoursesByStream';
         // array_push($this->scripts['js'], "frontend/js/jquery.validate.min.js");
          array_push($this->scripts['js'], "frontend/js/pagesjs/collegeCoursesByStream.js");
         $this->display_view('frontend',$view,$this->data);
          
           // echo "<pre>"; print_r($this->data['collegeCourses']); exit;
            
      }

      /*function getCitiesforModal*/
      public function getCitiesforModal()
      {

          $this->data['cities']= $this->home_model->getCities();
          $arr = array();
          foreach ($this->data['cities'] as $key => $value) {
            $arr[] = array('id'=>$value->id,'city'=>$value->city);
          }
         
        //  echo "<pre>"; print_r($this->data['cities']);
           
          echo json_encode($arr);

      }



      public function getCoursesforModal()
      {


          $this->data['courses']= $this->home_model->getCoursesName();
          // echo "<pre>"; print_r($this->data['courses']); exit;
          $arr = array();
          foreach ($this->data['courses'] as $key => $value) {
            $arr[] = array('id'=>$value->id,'course'=>$value->course_name);
          }
         
          //echo "<pre>"; print_r($this->data['cities']);
           
          echo json_encode($arr);

      }


      /*function load course view detail*/

      public function courseDetail($course,$cid)
      {
        
      //  echo $cid; exit;
         
      $this->data['courseInfo'] = $this->home_model->getCourseInfo($cid);
      $view ='courseDetailView';
      $this->display_view('frontend',$view,$this->data);


      }


      /* function for get all colleges with course */
     public function university_courses($uniName,$courseName,$courseSname,$tcucId)
     {
      
      echo $uniName.' '.$courseName.' '.$courseSname.' '.$tcucId; exit;


        



     }


     /*function for save brochure enquiry*/

     public function saveBrochureEnquiry()
     {
           
           

        $_POST = json_decode(file_get_contents('php://input'), true);

     //  echo '<pre>'; print_r($_POST); exit;
      
        $page = $this->input->post('page');
         // echo $page; exit;
       $universityId = $this->input->post('universityId');
       $user_id =  isset($sessionData) && !empty($sessionData['user_id']) ? $sessionData['user_id'] : '';
       $fullName = $this->input->post('fullname');
       $emailId = $this->input->post('emailId');
       $mobileNo   = $this->input->post('mobileNo');
       $city   = $this->input->post('city');
       $course   = $this->input->post('course');
       $pageLocation = $this->input->post('pagelocation') ? $this->input->get('pagelocation') : '';
     // echo $pageLocation; exit;
        
      
      $new = explode('-', $universityId);
       
       if($page=='index')
       {
         //echo 'here'; exit;
         $uniId = $new[0];
         $courseName = $new[1];
         //echo $uniId; exit;
         $getBrochure = $this->home_model->getBrochureColg($uniId);
        // echo "<pre>"; print_r($getBrochure); exit;
          $collegeName = $getBrochure->name;
         
        $newGetBrocure = array(

                'uniBrochure' => $getBrochure->brochure_name,
                'universityName' => $getBrochure->universityName,
                'colgBrochure' => $getBrochure->brochure_name,
                'type' => $getBrochure->type,
                'collegeName' => $collegeName,
                'colgCity' => $getBrochure->city
                
                 );
            // echo "<pre>"; print_r($newGetBrocure); exit;
       }


       elseif($pageLocation=='allUniversities')
       {  
         // echo "elseif"; exit();
              
         $uniId = $this->input->get('universityId');

          $newGetBrocure = array(
                'uniBrochure' => $getBrochure->uniBrochure,
                'universityName' => $getBrochure->universityName,
                'colgBrochure' => $getBrochure->colgBrochure,
                'type' => $getBrochure->type,
                'collegeName' => $collegeName,
                'colgCity' => $getBrochure->colgCity
                 );

       }

       else{

         
         $uniId = $new[0];
          $courseName = $new[1];
          $coorseSpec = $new[2];
             $getBrochure = $this->home_model->getBrochure($uniId);

             // echo "<pre>"; print_r($getBrochure); exit();
         
            $collegeName = $getBrochure->collegeName;
          
            $newGetBrocure = array(
                'uniBrochure' => $getBrochure->uniBrochure,
                'universityName' => $getBrochure->universityName,
                'colgBrochure' => $getBrochure->colgBrochure,
                'type' => $getBrochure->type,
                'collegeName' => $collegeName,
                'colgCity' => $getBrochure->colgCity
                 );

       }
     
        
        $this->session->set_userdata($newGetBrocure);
         



    
       $newPass = mt_rand(10,600000000);

    
   $checkExistance = $this->home_model->checkUserExistance($emailId);
      

   
      $userData = array(
              
              'first_name' => $fullName,
              'user_email' => $emailId,
              'user_password' => $newPass,
              'flag' =>1
                );
         
           // echo "<pre>"; print_r($enqData); exit; 

          if(!$this->session->userdata('user_id'))
             {

                 if(!empty($checkExistance)){
                 
                     $userId = $checkExistance->id;
                   
                           if($page=='index')
                           {
                              $enqData = array(
                                  'user_tbl_id' => $userId,
                                  'university_name' => $getBrochure->universityName,
                                  'college_name' =>$collegeName,
                                  'city' => $city,
                                  'sel_course' => $course,
                                  'enquiry_type' => 1 );

                           }else{

                           $enqData = array(
                          'user_tbl_id' => $userId,
                          'city' => $city,
                          'sel_course' => $course,
                          'brochure_course' => $courseName,
                          'brochure_course_spe' => $coorseSpec,
                          'brochure_name' => $getBrochure->colgBrochure,
                          'enquiry_type' => 3
                          );
                        }
                           

                 $saveEnqueryData = $this->home_model->saveBrochureEnquiry($enqData);

                    $this->session->set_userdata($enqData);
                    $data = array('status' =>1,'emailId' =>$emailId);

                 

                 
              }else{
                   
                  
                
                $saveBrochureEnq = $this->home_model->createUser($userData);


                 if($page=='index')
                   {
                     
                     $enqData = array(

                      'user_tbl_id' => $userId,
                      'university_name' => $getBrochure->universityName,
                      'college_name' =>$collegeName,
                      'city' => $city,
                      'sel_course' => $course,
                      'enquiry_type' => 1
                    
                     );




                   }else{

                   $enqData = array(
                  'user_tbl_id' => $userId,
                  'city' => $city,
                  'sel_course' => $course,
                  'brochure_course' => $courseName,
                  'brochure_course_spe' => $coorseSpec,
                  'brochure_name' => $getBrochure->colgBrochure,
                  'enquiry_type' => 3
                  );
                }
                 $saveEnqueryData = $this->home_model->saveBrochureEnquiry($enqData);
                    

                $subject = $fullName.', Download Admission Brochure of '.$getBrochure->collegeName.',  '.$getBrochure->colgCity;

                $message = "Howdy " . $fullName. "<br><br>";
                $message.="Your account has been created, please notedown your email and login password <br>";
                $message.="Email :-".$emailId."<br>";
                $message.="Password :-".$newPass."<br>";  
                $message .= "Thank you for downloading brochure of one of the colleges listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                <br><br>";
             // $message .= "Thank you for registration.<br><br>";

               $message.= "Regards,"."<br><br>";
               $message.= "Team Studyatease,"."<br><br>";

                $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';

               $data = array('status' => 2,'emailId'=>$emailId);

             
                     $this->sendBrochureToMail($subject, $message, $emailId,$file);
           }
                
                

          }else{
                
                

            $userId = $this->session->userdata('user_id');

                 

                       if($page=='index')
                           {
                             
                             $enqData = array(

                              'user_tbl_id' => $userId,
                              'university_name' => $getBrochure->universityName,
                              'college_name' =>$collegeName,
                              'city' => $city,
                              'sel_course' => $course,
                              'enquiry_type' => 1
                            
                             );

                           }else{

                           $enqData = array(
                          'user_tbl_id' => $userId,
                          'city' => $city,
                          'sel_course' => $course,
                          'brochure_course' => $courseName,
                          'brochure_course_spe' => $coorseSpec,
                          'brochure_name' => $getBrochure->colgBrochure,
                          'enquiry_type' => 3
                          );
                        }

                 $saveEnqueryData = $this->home_model->saveBrochureEnquiry($enqData);

                $subject = $fullName.', Download Admission Brochure of '.$collegeName.',  '.$city;

              $message = "Howdy " . $fullName. "<br><br>";
              $message .= "Thank you for downloading brochure of one of the colleges listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                <br><br>";
             // $message .= "Thank you for registration.<br><br>";

            $message.= "Regards,"."<br><br>";
            $message.= "Team Studyatease,"."<br><br>";

             $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

           $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';
          // echo $file; // exit;
          // $file = "https://images.static-collegedunia.com/public/college_data/images/pdfcol/1437989147ProspectusMBBS2015.pdf";



              $this->sendBrochureToMail($subject, $message, $emailId,$file);
              
              $data = array('status' => 3,'colgName'=>$getBrochure->collegeName,'colgCity'=>$getBrochure->colgCity);

              

     // }
    
              }

          echo json_encode($data);
         // echo "<pre>hjehre"; print_r($getBrochure); exit;

      
     
  }

//function for save user reviews

  public function saveUserReviews()
  { 

       $_POST = json_decode(file_get_contents('php://input'), true);

          // echo "<pre>"; print_r($_POST); exit;
         $fullname = $this->input->post('fullname');
         $colgName = $this->input->post('colgName');
         $colgId = $this->input->post('colgId');
         $mobilNo = $this->input->post('mobile');
         $emailId = $this->input->post('emailId');
         $city = $this->input->post('city');
         $review = $this->input->post('review');
         $rate = $this->input->post('rate');
          

         $userReviewData = array(
             'college_id'=>$colgId,
             'user_rate'=>$rate,
             'fullname'=>$fullname,
             'email_id'=>$emailId,
             'mobile_no'=>$mobilNo,
             'city'=>$city,
             'review'=>$review,

         );

          $cName = preg_replace('/[^A-Za-z0-9\-\']/', '-',$colgName); 

          $current_url = base_url().'college/'.strtolower($cName).'/'.$colgId; 
     if($this->session->userdata('user_id'))
     {
         $user_id = $this->session->userdata('user_id');
         $userReviewData['user_id']=$user_id;

          $checkExistanceReview = $this->home_model->checkExistanceUserCollegeReview($colgId,$user_id);
              
              if(empty($checkExistanceReview))
              {
              // echo "<pre>"; print_r($userReviewData); exit;
               $saveUserReviews = $this->home_model->saveUserReview($userReviewData);
               if($this->session->userdata('collegeReviewsData'))
                  {
                       $this->session->unset_userdata('collegeReviewsData');
                  }
            $data = array('status'=>1);
          }else{
                $data = array('status'=>3);
          }

     }else{

          
             $this->session->set_userdata('lasturl',$current_url);
             $this->session->set_userdata('collegeReviewsData',$userReviewData);
                    $data = array('status'=>2);

           $data = array('status'=>2);
  

     }
        
      // $user_id = $this->session->userdata('user_id');
   
     echo json_encode($data);
        
  }


// function save user uiniversity review

 public function saveUserUniversityReviews()
 {

      $_POST = json_decode(file_get_contents('php://input'), true);

       //  echo "<pre>"; print_r($_POST);  exit;
       
         $universityId = $this->input->post('universityId');
         $universityName = $this->input->post('universityName');
         $fullname = $this->input->post('fullname');
         $mobilNo = $this->input->post('mobile');
         $emailId = $this->input->post('emailId');
         $city = $this->input->post('city');
         $review = $this->input->post('review');
         $rate = $this->input->post('rate');
        
         $userReviewData = array(
            
             'university_id'=>$universityId,
             'user_rate'=>$rate,
             'fullname'=>$fullname,
             'email_id'=>$emailId,
             'mobile_no'=>$mobilNo,
             'city'=>$city,
             'review'=>$review,

         );

         $uName = preg_replace('/[^A-Za-z0-9\-\']/', '-',$universityName); 

          $current_url = base_url().'universityDetails/'.strtolower($uName).'/'.$universityId; 
         
          
     if($this->session->userdata('user_id'))
     {
         $user_id = $this->session->userdata('user_id');
       
            $userReviewData['user_id']=$user_id;
            $checkExistanceReview = $this->home_model->checkExistanceUserUniversityReview($universityId,$user_id);
             
             if(empty($checkExistanceReview))
             {
                  $saveUserReviews = $this->home_model->saveUserUiversityReview($userReviewData);

           

                if($this->session->userdata('universityReviewsData'))
                {
                     $this->session->unset_userdata('universityReviewsData');
                }
                $data = array('status'=>1);
              }else{

                   $data = array('status'=>3);
              }

           
          
     }else{

            // echo "<pre>"; print_r(current_url()); exit;
          // echo 'else'; exit;
            $this->session->set_userdata('lasturl',$current_url);
            // print_r($this->session->userdata('lasturl')); exit;
           $this->session->set_userdata('universityReviewsData',$userReviewData);
                    $data = array('status'=>2);
  

    }
        
      // $user_id = $this->session->userdata('user_id');
   
     echo json_encode($data);
        
  }

 


    /* ============FUNCTION FOR SEND MAIL =========== */

      public function sendBrochureToMail($subject, $message, $emailId,$file){

       //   echo $emailId; exit;

          require 'vendor/autoload.php';

          $sendgrid = new SendGrid("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");
          $email = new SendGrid\Email();

            $email->addTo($emailId)
                  ->setFrom('Eduportal')
                  ->setFromName("Eduportal | Find Your Dream University")
                  ->setSubject($subject)
                  ->setHtml($message)
                  ->addAttachment($file);

        //  $sendgrid->send($email);
      }



// function for get keyword related search

public function autocomplete()
{
   // echo "<pre>"; print_r($_POST); exit;
    
    $type = $_POST['type'];


  $keyword = $_POST['keyword'];

  if($type=='college')
  {
    
    $data['list'] = $this->home_model->searchColleges($keyword);
    $data = $this->load->view('frontend/collegeListing',$data,true);
    echo $data;

  } 
  
 else if($type=='university')
 {


  $data['list'] = $this->home_model->searchUniversites($keyword);
  $data = $this->load->view('frontend/listing',$data,true);
  echo $data;
   }

} 
     

// function for apply to university
public function applyToUniversity()
{

   $_POST = json_decode(file_get_contents('php://input'), true);

         
         if(isset($_POST))
         {     

           $universityId = $this->input->post('universityId');
           $fullname = $this->input->post('applyFullname');
           $emailId = $this->input->post('applyEmailId');
           $mobileNo = $this->input->post('applyMobileNo');
           $city = $this->input->post('applyCity');
           $course = $this->input->post('applyCourse');


             $candidateInfo = array(

                       'university_id' => $universityId,
                       'fullName' => $fullname,
                       'emailId' => $emailId,
                       'mobileNo' => $mobileNo,
                       'city' => $city,
                       'course' => $course,
                     );
           
           $universityInfo = $this->home_model->getUniversityInfo($this->input->post('universityId'));
           
              if(!$this->session->userdata('user_id'))
              {

                 $checkUserExistance = $this->home_model->checkUserExistance($emailId);
                  if(!empty($checkUserExistance))
                  {
                       
                       $this->session->set_userdata('candidateInfo',$candidateInfo);

                      $data = array('status'=>1,'emailId'=>$emailId);

                  }else{ // create new account
                         
                               $newPass = mt_rand(10,600000000);

                               $newUser = array(

                                'first_name' => $fullname,
                                'user_email' => $emailId,
                                'user_password' => $newPass,
                                'flag' => 1,
                               );
                        // carete new user

                           $createdUserId = $this->home_model->createUser2($newUser);

                            if($createdUserId)
                            {        
                                  $userSession = array(

                                        'user_id' => $createdUserId->id,
                                        'f_name' => $createdUserId->first_name,
                                        'l_name' => $createdUserId->last_name,
                                        'user_email' => $createdUserId->user_email,
                                        'logged_in' => true
                                      );
                                     
                                    $this->session->set_userdata($userSession);
                                $candidateInfo['user_id']=$createdUserId->id;

                                 $saveUnivesityApplicantInfo = $this->home_model->saveUnivesityApplicantInfo($candidateInfo);

                                       if($saveUnivesityApplicantInfo)
                                       {
                                           $subject = $fullname.', Download Admission Brochure of '.$universityInfo->name.',  '.$universityInfo->city;

                                            $message = "Hi " . $fullname. "<br><br>";
                                            $message.="Your account has been created, please notedown your email and login password <br>";
                                            $message.="Email :-".$emailId."<br>";
                                            $message.="Password :-".$newPass."<br>";  
                                            $message .= "Thank you for downloading brochure of one of the university listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                                            <br><br>";
                                            $message.= "Regards,"."<br><br>";
                                           $message.= "Team Studyatease.com,"."<br><br>";

                                            $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                                            $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';

                                           $data = array('status' => 2,'emailId'=>$emailId);

                                         
                                                 $this->sendBrochureToMail($subject, $message, $emailId,$file);
                                                 $data = array('status'=>2,'emailId'=>$emailId);
                                       }

                            } 

                  }

            }else{

                         
                  $candidateInfo['user_id']=$this->session->userdata('user_id');
                  $saveUnivesityBrochureDownloads = $this->home_model->saveUnivesityApplicantInfo($candidateInfo);
                   if($saveUnivesityBrochureDownloads)
                   {
                      $subject = $fullname.', Download Admission Brochure of '.$universityInfo->name.',  '.$universityInfo->city;

                              $message = "Hi " . $fullname. "<br><br>";
                              $message .= "Thank you for downloading brochure of one of the university listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                                <br><br>";
                           
                            $message.= "Regards,"."<br><br>";
                            $message.= "Team Studyatease.com"."<br><br>";

                             $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                           $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';
                               $this->sendBrochureToMail($subject, $message, $emailId,$file);
                              
                              $data = array('status' => 3,'universityName'=>$universityInfo->name,'uniCity'=>$universityInfo->city);
                   }

            }

              

             echo json_encode($data);

         }
 


}


/*function for apply to college*/

public function applyToCollege()
{

   $_POST = json_decode(file_get_contents('php://input'), true);
          
        //  echo "<pre>"; print_r($_POST); exit;
         
         if(isset($_POST))
         {     

           $universityId = $this->input->post('universityId');
           $fullname = $this->input->post('applyFullname');
           $emailId = $this->input->post('applyEmailId');
           $mobileNo = $this->input->post('applyMobileNo');
           $city = $this->input->post('applyCity');
           $course = $this->input->post('applyCourse');


             $candidateInfo = array(

                       'college_id' => $universityId,
                       'fullName' => $fullname,
                       'emailId' => $emailId,
                       'mobileNo' => $mobileNo,
                       'city' => $city,
                       'course' => $course,
                     );
           
           $collegeInfo = $this->home_model->getCollgeInfo($this->input->post('universityId'));

               // echo "<pre>"; print_r($collegeInfo); exit;
           
              if(!$this->session->userdata('user_id'))
              {

                 $checkUserExistance = $this->home_model->checkUserExistance($emailId);
                  if(!empty($checkUserExistance))
                  {
                       
                       $this->session->set_userdata('candidateInfo',$candidateInfo);

                      $data = array('status'=>1,'emailId'=>$emailId);

                  }else{ // create new account
                           

                               $newPass = mt_rand(10,600000000);

                               $newUser = array(

                                'first_name' => $fullname,
                                'user_email' => $emailId,
                                'user_password' => $newPass,
                                'flag' => 1,
                               );
                        // carete new user
                            

                           $createdUserId = $this->home_model->createUser2($newUser);

                            // echo "<pre>"; print_r($createdUserId); exit;
                            if($createdUserId)
                            {        
                                  $userSession = array(

                                        'user_id' => $createdUserId->id,
                                        'f_name' => $createdUserId->first_name,
                                        'l_name' => $createdUserId->last_name,
                                        'user_email' => $createdUserId->user_email,
                                        'logged_in' => true
                                      );
                                     
                                    $this->session->set_userdata($userSession);
                                $candidateInfo['user_id']=$createdUserId->id;

                                 $saveCollegeApplicantInfo = $this->home_model->saveCollegeApplicantInfo($candidateInfo);

                                       if($saveCollegeApplicantInfo)
                                       {
                                           $subject = $fullname.', Download Admission Brochure of '.$collegeInfo->name.',  '.$collegeInfo->city;

                                            $message = "Hi " . $fullname. "<br><br>";
                                            $message.="Your account has been created, please notedown your email and login password <br>";
                                            $message.="Email :-".$emailId."<br>";
                                            $message.="Password :-".$newPass."<br>";  
                                            $message .= "Thank you for applying to the university listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                                            <br><br>";
                                            $message.= "Regards,"."<br><br>";
                                           $message.= "Team Studyatease.com,"."<br><br>";

                                            $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                                            $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';

                                           $data = array('status' => 2,'emailId'=>$emailId);

                                         
                                                 $this->sendBrochureToMail($subject, $message, $emailId,$file);
                                                 $data = array('status'=>2,'emailId'=>$emailId);
                                       }

                            } 

                  }

            }else{

                         
                  $candidateInfo['user_id']=$this->session->userdata('user_id');
                  $saveCollegeBrochureDownloads = $this->home_model->saveCollegeApplicantInfo($candidateInfo);
                   if($saveCollegeBrochureDownloads)
                   {
                      $subject = $fullname.', Download Admission Brochure of '.$collegeInfo->name.',  '.$collegeInfo->city;

                              $message = "Hi " . $fullname. "<br><br>";
                              $message .= "Thank you for applying to one of the university listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                                <br><br>";
                           
                            $message.= "Regards,"."<br><br>";
                            $message.= "Team Studyatease.com"."<br><br>";

                             $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                           $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';
                               $this->sendBrochureToMail($subject, $message, $emailId,$file);
                              
                              $data = array('status' => 3,'universityName'=>$collegeInfo->name,'uniCity'=>$collegeInfo->city);
                   }

            }

              

             echo json_encode($data);

         }
 


}

/*function for download university brochure*/
public function saveBrochureofUniversity()
{
      $_POST = json_decode(file_get_contents('php://input'), true);

           //  echo "<pre>"; print_r($_POST); exit;
         
         if(isset($_POST)){
               
               $universityId = $this->input->post('universityId');
               $fullname = $this->input->post('fullname');
               $emailId = $this->input->post('emailId');
               $mobileNo = $this->input->post('mobileNo');
               $city = $this->input->post('city');
               $course = $this->input->post('course');
          $universityBrochure = $this->home_model->getUniversityInfo($this->input->post('universityId'));

             // echo "<pre>"; print_r($universityBrochure); exit;

        $userWithBrochureInfo = array(
                  
                        'university_id' =>$universityId,
                        'fullname' => $fullname,
                        'email_id' => $emailId,
                        'mobile_no' => $mobileNo,
                        'city' => $city,
                        'course' => $course,
                        'university_brochure' => $universityBrochure->brochure_name
                       );
          
                    // echo "<pre>"; print_r($userWithBrochureInfo); exit;

            if(!$this->session->userdata('user_id'))
            {
                 
                  $checkUserExistance = $this->home_model->checkUserExistance($emailId);
                  if(!empty($checkUserExistance))
                  {
                       
                       $this->session->set_userdata('userBrochureInfo',$userWithBrochureInfo);

                      $data = array('status'=>1,'emailId'=>$emailId);

                  }else{ // create new account
                         
                               $newPass = mt_rand(10,600000000);

                               $newUser = array(

                                'first_name' => $fullname,
                                'user_email' => $emailId,
                                'user_password' => $newPass,
                                'flag' => 1,
                               );
                        // carete new user

                           $createdUserId = $this->home_model->createUser2($newUser);

                            if($createdUserId)
                            {        
                                  $userSession = array(

                                        'user_id' => $createdUserId->id,
                                        'f_name' => $createdUserId->first_name,
                                        'l_name' => $createdUserId->last_name,
                                        'user_email' => $createdUserId->user_email,
                                        'logged_in' => true
                                      );
                                     
                                    $this->session->set_userdata($userSession);
                                $userWithBrochureInfo['user_id']=$createdUserId->id;

                                 $saveUnivesityBrochureDownloads = $this->home_model->saveUnivesityBrochureDownloads($userWithBrochureInfo);

                                       if($saveUnivesityBrochureDownloads)
                                       {


                                            $subject = $fullname.', Download Admission Brochure of '.$universityBrochure->name.',  '.$universityBrochure->city;

                                            $message = "Hi " . $fullname. "<br><br>";
                                            $message.="Your account has been created, please notedown your email and login password <br>";
                                            $message.="Email :-".$emailId."<br>";
                                            $message.="Password :-".$newPass."<br>";  
                                            $message .= "Thank you for downloading brochure of one of the university listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                                            <br><br>";
                                            $message.= "Regards,"."<br><br>";
                                           $message.= "Team Studyatease.com,"."<br><br>";

                                            $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                                            $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';

                                           $data = array('status' => 2,'emailId'=>$emailId);

                                         
                                                 $this->sendBrochureToMail($subject, $message, $emailId,$file);
                                                 $data = array('status'=>2,'emailId'=>$emailId);
                                       }

                            } 

                  }

            }else{

                       //    echo 'login'; 

                  //  echo "<pre>"; print_r($userWithBrochureInfo); exit;

                  $userWithBrochureInfo['user_id']=$this->session->userdata('user_id');
                  $saveUnivesityBrochureDownloads = $this->home_model->saveUnivesityBrochureDownloads($userWithBrochureInfo);
                   if($saveUnivesityBrochureDownloads)
                   {
                      $subject = $fullname.', Download Admission Brochure of '.$universityBrochure->name.',  '.$universityBrochure->city;

                              $message = "Hi " . $fullname. "<br><br>";
                              $message .= "Thank you for downloading brochure of one of the university listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                                <br><br>";
                           
                            $message.= "Regards,"."<br><br>";
                            $message.= "Team Studyatease.com"."<br><br>";

                             $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                           $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';
                               $this->sendBrochureToMail($subject, $message, $emailId,$file);
                              
                              $data = array('status' => 3,'universityName'=>$universityBrochure->name,'uniCity'=>$universityBrochure->city);
                   }

            }


   }// isset close
    
      echo json_encode($data);
}


  } 
  ?>