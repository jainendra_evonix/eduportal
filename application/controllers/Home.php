<?php
ob_start();
// Author Jainendra +  hrushikesj
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends My_front {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {

        parent::__construct();

        $this->load->library('form_validation');
    }

    public function index(){

        $this->data['topUniversities'] = $this->home_model->getTopUniversities();
        $this->data['courses'] = $this->home_model->getCoursesAndCollegesName();
           
           $courseArray = [];
        foreach ($this->data['courses'] as $key => $value) {
            
        $colleges = $this->home_model->getCollegesByCourse($value->course_name);
           $courseArray[$value->course_name]=$colleges;

        }//exit;

        $this->data['collegesAndCourses'] = $courseArray;
            
         //   echo "<pre>"; print_r($courseArray);exit;

        //  $view ='index-header';
        $view = 'index';


        if ($this->session->flashdata('success')) {
            $sweetAlert = 'swal({
                        title: "Link Sent!",
                        text: "Verify your email!",
                        icon: "success",
                        button: "Ok",
                      });';
            array_push($this->scripts['embed'], $sweetAlert);
        }
       
        array_push($this->scripts['js'], "frontend/js/pagesjs/index.js");




        $this->display_view('frontend', $view, $this->data, true);
    }

    /* ============== FUNCTION FOR KEY STRING============== */

    public function generateRandomString($length = 16) {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    /* function for login */

    public function login() {

        // echo 123; exit;

        if(!$this->session->userdata('user_id'))
        {



        if ($this->session->flashdata('success')) {

            $sweetAlert = 'swal({
                        title: "Email Verified!",
                        text: "Login with your credential..",
                        icon: "success",
                        button: "Ok",
                      });';
            array_push($this->scripts['embed'], $sweetAlert);
        }

        if ($this->session->flashdata('error')) {


            $sweetAlert = 'swal({
                        title: "Email Verified!",
                        text: "Login with your credential..",
                        icon: "success",
                        button: "Ok",
                      });';

            $error = 'swal("Oops!", "Invalid email or  password!", "error")';
            array_push($this->scripts['embed'], $error);
        }

        $view = 'login';
      //  array_push($this->scripts['js'], "frontend/js/jquery.validate.min.js");
        array_push($this->scripts['js'], "frontend/js/pagesjs/login.js");
        $this->display_view('frontend', $view, $this->data);
  
   }else{
          
          redirect(base_url().'dashboard');

   }

     }
    /* function for load view registration */

    public function register() {

        $view = 'register';

        array_push($this->scripts['js'], "frontend/js/jquery.validate.min.js");
        array_push($this->scripts['js'], "frontend/js/pagesjs/register.js");

        $this->display_view('frontend', $view, $this->data, false);
    }

    public function search() {

        echo "<pre>";
        print_r($_GET);
        exit;
    }

//        function for userInfo
    public function userInfo() {
        date_default_timezone_set('Asia/Kolkata');
        if (isset($_POST)) {

            $randomkeystring = $this->generateRandomString();

            $userInfo = array(
                'first_name' => $this->input->post('firstname'),
                'last_name' => $this->input->post('lastname'),
                'user_email' => $this->input->post('email'),
                'user_password' => $this->input->post('password'),
                'activation_token' => $randomkeystring,
                'created_date' => date("Y-m-d H:i:s")
            );
//            echo '<pre>';
//            print_r($userInfo);
//            exit;
            $saveData = $this->home_model->registerUser($userInfo);


            $url = base_url() . "home/activate/" . $randomkeystring;
            $fname = $this->input->post('firstname');
            $lname = $this->input->post('lastname');
            $email = $this->input->post('email');
            $password = $this->input->post('user_password');

            $fullname = $fname . " " . $lname;



            $subject = "Thank you for registration";

            $message = "Dear " . $fullname . "<br><br>";
            $message .= "Greetings from Eduportal!<br><br>";
            $message .= "Thank you for registration.<br><br>";



            $message .= "Below are your login details .<br>";
            $message .= "<b> Email</b> : " . $email . " .<br> <b> Password</b>  :" . $password . ".<br>";



            $message .= "<br />To activate your account, Please click the below link. <br>";
            $message .= "<b> <a href=" . $url . "> Click Here.</a> </b><br><br>";



            $this->sendTOmail($subject, $message, $email);

            if ($saveData) {

                //  $this->session->set_flashdata('registersuccess','Your email verification link send on your mail please confirm email');

                $this->_show_message('Your email verification link send on your mail please confirm email', 'success');

                redirect(base_url() . 'home');
            }
        }
    }



/*function for check exist email*/
function checkExistEmail()
{

$email = $this->input->post('emailId');

$checkEmail = $this->home_model->checkExistEmail($email);
  
  if(!empty($checkEmail))
  {

     $data = array('status'=>1);


  }else{

    $data = array('status'=>2);
  }

echo json_encode($data);


}
    /* ============FUNCTION FOR SEND MAIL =========== */

    public function sendTOmail($subject, $message, $username) {

        require 'vendor/autoload.php';

        $sendgrid = new SendGrid("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");
        $email = new SendGrid\Email();

        $email->addTo($username)
                ->addCc(cc_email)
                ->setFrom('Eduportal')
                ->setFromName("Eduportal | Find Your Dream University")
                ->setSubject($subject)
                ->setHtml($message);

        $sendgrid->send($email);
    }

    /* ============= FUNCTION FOR ACTIVATE SIGNUP ACCOUNT ============= */

    public function activate($token) {

        $actvntm = date("Y-m-d H:i:s");

        $data = array(
            'flag' => 1,
            'activation_time' => $actvntm,
        );

        $status = $this->home_model->activateLogin($data, $token);

        if ($status) {
            $this->_show_message("Your email id has been verified. Login with your credentials", 'success');

            redirect(base_url() . 'home/login');
        }
    }

    // funtion for check login
    public function checkLogin() {
        
        $_POST = json_decode(file_get_contents('php://input'), true);
        // echo "<pre>"; print_r($_POST); exit();
       
          if ($this->input->post()) {
              
              
            $this->form_validation->set_rules('emailaddress', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
             
            if ($this->form_validation->run() == TRUE){

               // echo "hereValid"; exit;
                           
                $email =    $this->input->post('emailaddress');
                $password = $this->input->post('password');
                $loginUser = $this->home_model->checkLogin($email, $password);
                
                  if(!empty($loginUser)){
                  $userSession = array(
                    'user_id' => $loginUser->id,
                    'f_name' => $loginUser->first_name,
                    'l_name' => $loginUser->last_name,
                    'user_email' => $loginUser->user_email,
                    'logged_in' => true
                  );
                 
                $this->session->set_userdata($userSession);
                  
                  //redirect to  user university review
                  if($this->session->userdata('lasturl')){
                       
                      
                      $data = array('status' =>4,'lastUrl'=>$this->session->userdata('lasturl'));
                    }else{
                     $data = array('status' =>1);
                    }
               
            }else{
                $data = array('status' =>2);
                 
            }
          } else {

                 $data = array('status' => '3', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                }
        }
        print_r(json_encode($data));
    }



//brochure download login function
    public function checkLoginFn()
    {
       
    //  echo "<pre>"; print_r($this->session->userdata()); exit;

       if ($this->input->post()){
              
            $this->form_validation->set_rules('emailaddress', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
             
            if ($this->form_validation->run() == TRUE){

               // echo "hereValid"; exit;
                           
                $email =    $this->input->post('emailaddress');
                $password = $this->input->post('password');
                $loginUser = $this->home_model->checkLogin($email,$password);

               // echo "<pre>"; print_r($loginUser); exit;
                
                  if(!empty($loginUser)){

                        $userSession = array(
                          'user_id' => $loginUser->id,
                          'f_name' => $loginUser->first_name,
                          'l_name' => $loginUser->last_name,
                          'user_email' => $loginUser->user_email,
                          'logged_in' => true
                        );
                         
                        $this->session->set_userdata($userSession);

                        //  $userId = $checkExistance->id;

                           $enqData = array(

                            'user_tbl_id' => $this->session->userdata('user_tbl_id'),
                            'city' => $this->session->userdata('city'),
                            'sel_course' => $this->session->userdata('sel_course'),
                            'brochure_course' => $this->session->userdata('brochure_course'),
                            'brochure_course_spe' => $this->session->userdata('brochure_course_spe'),
                            'brochure_name' => $this->session->userdata('brochure_name')
                          );
                             echo "<pre>"; print_r($enqData); exit;

                             // $this->session->sess_destroy();
                         echo "<pre>";    print_r($this->session->all_userdata()); 

                          echo "<pre>"; print_r($enqData); exit;

                         // /sharayu234@gmail.com
                           $subject = $loginUser->first_name.' '.$loginUser->last_name.', Download Admission Brochure of '.$this->session->userdata('collegeName').',  '.$this->session->userdata('colgCity');

                      $message = "Howdy " . $loginUser->first_name.' '.$loginUser->last_name. "<br><br>";
                      $message .= "Thank you for downloading brochure of one of the colleges listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                        <br><br>";
                     // $message .= "Thank you for registration.<br><br>";

                    $message.= "Regards,"."<br><br>";
                    $message.= "Team Studyatease,"."<br><br>";

                     $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                    $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';

                        $saveEnqueryData = $this->home_model->saveBrochureEnquiry($enqData);
                        
                         $this->sendBrochureToMail($subject,$message,$email,$file);
                         $data = array('status' => '1','colgName'=>$this->session->userdata('collegeName'),'colgCity' => $this->session->userdata('colgCity'),'courseName'=>$this->session->userdata('sel_course'),'spec_course'=>$this->session->userdata('brochure_course_spe'));
                 
                
            }else{
                $data = array('status' => '2');
                 
            }
          } else {

                 $data = array('status' => '3', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                }
        }
         
         print_r(json_encode($data));

    }




      public function checkLoginForUniversityBrochure()
    {
       
    //  echo "<pre>"; print_r($this->session->userdata()); exit;

       if ($this->input->post()){
              
            $this->form_validation->set_rules('emailaddress', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
             
            if ($this->form_validation->run() == TRUE){

               // echo "hereValid"; exit;
                           
                $email =    $this->input->post('emailaddress');
                $password = $this->input->post('password');
                $loginUser = $this->home_model->checkLogin($email,$password);

               // echo "<pre>"; print_r($loginUser); exit;
                
                  if(!empty($loginUser)){

                        $userSession = array(
                          'user_id' => $loginUser->id,
                          'f_name' => $loginUser->first_name,
                          'l_name' => $loginUser->last_name,
                          'user_email' => $loginUser->user_email,
                          'logged_in' => true
                        );
                         
                        $this->session->set_userdata($userSession);
                       
                          $lastSessionData= $this->session->userdata('userBrochureInfo'); 
                          $lastSessionData['user_id']=$loginUser->id;
                          $universityId = $lastSessionData['university_id'];
                          $universityBrochure = $this->home_model->getUniversityInfo($universityId);

                         $subject = $loginUser->first_name.' '.$loginUser->last_name.', Download Admission Brochure of '.$universityBrochure->name.',  '.$universityBrochure->city;

                      $message = "Hi " . $loginUser->first_name.' '.$loginUser->last_name. "<br><br>";
                      $message .= "Thank you for downloading brochure of one of the university listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                        <br><br>";
                     // $message .= "Thank you for registration.<br><br>";

                    $message.= "Regards,"."<br><br>";
                    $message.= "Team Studyatease.com"."<br><br>";

                     $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                    $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';

                         // echo "<pre>"; print_r($lastSessionData); exit();

                        $saveEnqueryData = $this->home_model->saveUnivesityBrochureDownloads($lastSessionData);
                        
                         $this->sendBrochureToMail($subject,$message,$email,$file);
                         $data = array('status' => '1','univaersityName'=>$universityBrochure->name,'uniCity' => $universityBrochure->city,'courseName'=>$lastSessionData['course']);
                 
                
            }else{
                $data = array('status' => '2');
                 
            }
          } else {

                 $data = array('status' => '3', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                }
        }
         
         print_r(json_encode($data));

    }



// check login for apply to university
 

      public function checkLoginForApplyUniversity()
    {
       
    //  echo "<pre>"; print_r($this->session->userdata()); exit;

       if ($this->input->post()){
              
            $this->form_validation->set_rules('emailaddress', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
             
            if ($this->form_validation->run() == TRUE){

               // echo "hereValid"; exit;
                           
                $email =    $this->input->post('emailaddress');
                $password = $this->input->post('password');
                $loginUser = $this->home_model->checkLogin($email,$password);

               // echo "<pre>"; print_r($loginUser); exit;
                
                  if(!empty($loginUser)){

                        $userSession = array(
                          'user_id' => $loginUser->id,
                          'f_name' => $loginUser->first_name,
                          'l_name' => $loginUser->last_name,
                          'user_email' => $loginUser->user_email,
                          'logged_in' => true
                        );
                         
                        $this->session->set_userdata($userSession);
                       
                          $lastSessionData= $this->session->userdata('candidateInfo'); 

                        ///  echo "<pre>"; print_r($lastSessionData); exit;
                          $lastSessionData['user_id']=$loginUser->id;
                          $universityId = $lastSessionData['university_id'];
                          $universityBrochure = $this->home_model->getUniversityInfo($universityId);

                         $subject = $loginUser->first_name.' '.$loginUser->last_name.', Applied in '.$universityBrochure->name.',  '.$universityBrochure->city;

                      $message = "Hi " . $loginUser->first_name.' '.$loginUser->last_name. "<br><br>";
                      $message .= "Thank you for apply to one of the university listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                        <br><br>";
                     // $message .= "Thank you for registration.<br><br>";

                    $message.= "Regards,"."<br><br>";
                    $message.= "Team Studyatease.com"."<br><br>";

                     $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                    $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';

                        
                          //  $lastSessionData['university_id']=$universityId;
                         // tbl_apply_to_university
                          
                           //  echo "<pre>"; print_r($lastSessionData); exit();

                        $saveEnqueryData = $this->home_model->saveUnivesityApplicantInfo($lastSessionData);
                        
                         $this->sendBrochureToMail($subject,$message,$email,$file);
                         $data = array('status' => '1','univaersityName'=>$universityBrochure->name,'uniCity' => $universityBrochure->city,'courseName'=>$lastSessionData['course']);
                 
                
            }else{
                $data = array('status' => '2');
                 
            }
          } else {

                 $data = array('status' => '3', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                }
        }
         
         print_r(json_encode($data));

    }


   

// check login for apply to university
 

      public function checkLoginForApplyCollege()
    {
       
    //  echo "<pre>"; print_r($this->session->userdata()); exit;

       if ($this->input->post()){
              
            $this->form_validation->set_rules('emailaddress', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');
             
            if ($this->form_validation->run() == TRUE){

                $email =    $this->input->post('emailaddress');
                $password = $this->input->post('password');
                $loginUser = $this->home_model->checkLogin($email,$password);

               // echo "<pre>"; print_r($loginUser); exit;
                
                  if(!empty($loginUser)){

                        $userSession = array(
                          'user_id' => $loginUser->id,
                          'f_name' => $loginUser->first_name,
                          'l_name' => $loginUser->last_name,
                          'user_email' => $loginUser->user_email,
                          'logged_in' => true
                        );
                         
                        $this->session->set_userdata($userSession);
                       
                          $lastSessionData= $this->session->userdata('candidateInfo'); 

                         // echo "<pre>"; print_r($lastSessionData); exit;
                          $lastSessionData['user_id']=$loginUser->id;
                          $universityId = $lastSessionData['college_id'];
                          $CollegeBrochure = $this->home_model->getCollgeInfo($universityId);

                         $subject = $loginUser->first_name.' '.$loginUser->last_name.', Applied in '.$CollegeBrochure->name.',  '.$CollegeBrochure->city;

                      $message = "Hi " . $loginUser->first_name.' '.$loginUser->last_name. "<br><br>";
                      $message .= "Thank you for apply to one of the college listed on Studyatease.com. This is an automatically generated email to inform you that we would be consistently assisting you to get your admission target.
                        <br><br>";
                     // $message .= "Thank you for registration.<br><br>";

                    $message.= "Regards,"."<br><br>";
                    $message.= "Team Studyatease.com"."<br><br>";

                     $fileLoc =  getcwd().'/uploads/brochure/colleges/'; 

                    $file = $fileLoc.'asdasdasd-jainendrapalProfile.pdf';

                        
                          //  $lastSessionData['university_id']=$universityId;
                         // tbl_apply_to_university
                          
                           //  echo "<pre>"; print_r($lastSessionData); exit();

                        $saveEnqueryData = $this->home_model->saveCollegeApplicantInfo($lastSessionData);
                        
                         $this->sendBrochureToMail($subject,$message,$email,$file);
                         $data = array('status' => '1','univaersityName'=>$CollegeBrochure->name,'uniCity' => $CollegeBrochure->city,'courseName'=>$lastSessionData['course']);
                 
                
            }else{
                $data = array('status' => '2');
                 
            }
          } else {

                 $data = array('status' => '3', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
                }
        }
         
         print_r(json_encode($data));

    }




/* ============FUNCTION FOR SEND MAIL =========== */

      public function sendBrochureToMail($subject, $message, $emailId,$file){

       //   echo $emailId; exit;

          require 'vendor/autoload.php';

          $sendgrid = new SendGrid("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");
          $email = new SendGrid\Email();

            $email->addTo($emailId)
                  ->setFrom('Eduportal')
                  ->setFromName("Eduportal | Find Your Dream University")
                  ->setSubject($subject)
                  ->setHtml($message)
                  ->addAttachment($file);

          $sendgrid->send($email);
      }

    // function for load user dashboard
    public function dashboard() {

      
      $user_id  = $this->session->userdata('user_id');

       $this->data['userdata'] = $this->home_model->getUerInfo($user_id);
     

        
        $view = 'userprofile';
        $this->display_view('frontend',$view,$this->data,false);
    }

    //  Function for user profile
    public function userprofile() {

        $user_id  = $this->session->userdata('user_id');

       $this->data['userdata'] = $this->home_model->getUerInfo($user_id);

        $view = 'userprofile';
        $this->display_view('frontend', $view, $this->data, false);
    }

    public function appliedcolleges(){

        $user_id  = $this->session->userdata('user_id');

       $this->data['userdata'] = $this->home_model->getUerInfo($user_id);
       $this->data['allAppliedCollegs'] = $this->home_model->getAllAppliedCollegs($user_id);

        $view = 'appliedcolleges';
        $this->display_view('frontend', $view, $this->data, false);
    }

    public function yourreviews(){

        $user_id  = $this->session->userdata('user_id');

       $this->data['userdata'] = $this->home_model->getUerInfo($user_id);
       $this->data['userReviews'] = $this->home_model->getUserUniversityReviews($user_id);

        $view = 'yourreviews';
        $this->display_view('frontend', $view, $this->data, false);
    }



 public function logout()
 {

       $this->session->sess_destroy();
            redirect(base_url());
    


 }

 /*function for test*/

 public function employeeData()
 {


    echo "here"; exit;
 }


}

?>