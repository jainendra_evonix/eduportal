<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
  
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['login'] = 'home/login';
//$route['adwords'] = 'adwords/index';
$route['register'] = 'home/register';
$route['adminforgotpassword'] = 'admin/adminforgotpassword';

$route['india-universites'] = 'universities';
$route['logout'] = 'home/logout';
$route['university'] = 'universities/university';
$route['admin'] = 'admin';
$route['dashboard'] = 'home/dashboard';

$route['userprofile'] = 'home/userprofile';
$route['appliedcolleges'] = 'home/appliedcolleges';
$route['yourreviews'] = 'home/yourreviews';
$route['dashboard'] = 'home/dashboard';
$route['homesearch'] = 'universities/homesearch';

$route['universityDetails/(:any)/(:num)'] = "universities/universityDetails/$1/$2";
$route['course/(:any)/(:any)/(:num)'] = 'universities/course/$1/$2/$3';
$route['university/(:any)/(:any)/(:any)/(:num)'] = "universities/university_college/$1/$2/$3/$4";
$route['college/(:any)/(:num)'] = "universities/college_info/$1/$2";
$route['university/(:any)/(:any)/(:any)/(:num)/(:num)'] = "universities/universityCollegesCourse/$1/$2/$3/$4/$5";

$route['university/(:any)/(:num)/(:any)/(:any)/(:num)'] = "universities/universityCollegesStream/$1/$2/$3/$4/$5";

$route['university/(:any)/(:any)/(:any)/(:num)/(:any)/(:any)/(:num)'] = "universities/collegeCourses/$1/$2/$3/$4/$5/$6/$7";
$route['university/(:any)/(:any)/(:any)/(:any)/(:any)/(:num)'] = "universities/collegeCoursesByStream/$1/$2/$3/$4/$5/$6";
$route['course/(:any)/(:num)'] = "universities/courseDetail/$1/$2";
//$route['university/(:any)/(:any)/(:any)/(:num)'] = "universities/university_courses/$1/$2/$3/$4";
$route['universities/search']="universities/search";
$route['allColleges']="universities/allColleges";
$route['colleges/searchColleges']="universities/searchColleges";
$route['colleges/search']="universities/searchColleges";
$route['universities/searches/(:any)']="universities/searches/$1";
$route['universities/(:any)/(:any)']="universities/innerSearch/$1/$2";
$route['university/(:any)/(:any)/(:any)/(:any)']="universities/innerSearch2/$1/$2";

//$route['universities/university'] ='university';

