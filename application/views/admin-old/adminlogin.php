<section class="tz-register">
		<div class="log-in-pop-full">
			<div class="log-in-pop-center">
				<a href="#" class="pop-close" data-dismiss="modal"><img src="<?php echo base_url(); ?>assets/images/cancel.png" alt="" />
				</a>
				<h4>Login</h4>
				<p>Please enter your authenticated credentials to login.</p>
				<div id="message"></div>
				<form class="s12" name="userLogin" ng-submit="submitForm()" novalidate>
					<div>
						<div class="input-field s12">
							<input type="email" class="validate" id="emailaddress" ng-model="user.emailaddress" name="emailaddress" required>
							<label>Email ID (required)</label>
							<span ng-show="submitted && userLogin.emailaddress.$invalid" class="help-block has-error ng-cloak">Valid Email ID is required.</span>
      						<span class="help-block has-error ng-cloak" ng-show="emailaddressError">{{emailaddressError}}</span>
						</div>
					</div>
					<div>
						<div class="input-field s12">
							<input type="password" class="validate" id="password" ng-model="user.password" name="password" required>
							<label>Password (required)</label>
							<span ng-show="submitted && userLogin.password.$error.required"  class="help-block has-error ng-cloak">Password is required.</span>
        					<span class="help-block has-error ng-cloak" ng-show="passwordError">{{passwordError}}</span>
						</div>
					</div>
					<div>
						<div class="input-field s4">
							<!-- <input type="submit" value="Register" class="waves-effect waves-light log-in-btn"> -->
							<input type="submit" value="Login" class="waves-effect waves-light log-in-btn" ng-click="submitted = true">
						</div>
					</div>
					<div>
						<div class="input-field s12"> <a href="<?php echo base_url();?>adminforgotpassword">Forgot password?</a></div>
					</div>
				</form>
			</div>
		</div>
</section>