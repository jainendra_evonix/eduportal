<!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
          <ul>
            <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> Add College</a> </li>
            <li class="page-back"><a href="<?php echo base_url() ?>admin/allcollege"><i class="fa fa-backward" aria-hidden="true"></i> View All</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>Add New College</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="<?php echo base_url() ?>admin/addcollege">Add New</a> </li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url() ?>admin/allcollege"><i class="material-icons">subject</i>View All</a> </li>
            </ul>
            <!-- Dropdown Structure -->
            <div class="split-row">
              <div class="col-md-12">
                <div class="ad-inn-page">
                  <div class="tab-inn ad-tab-inn">
                    <div class="hom-cre-acc-left hom-cre-acc-right">
                      <div class="">
                        <?php echo $this->session->flashdata('successmsg');  ?>
                        <?php echo $this->session->flashdata('errormsg');  ?>
                        <div class="pg-elem">
                          <div class="pg-elem-inn ele-btn">
                            <div>
                              <ul class="tabs pg-ele-tab">
                                <li class="tab col s3"><a class="active" href="#basic">Basic Details</a> </li>
                                <li class="tab col s3"><a href="#course">Courses(<?php echo $coursecount; ?>)</a> </li>
                                <li class="tab col s3"><span>Accreditation</span> </li>
                                <li class="tab col s3"><span>Highlights</span> </li>
                              </ul>
                            </div>
                            <div id="basic" class="col s12">
                              <form class="" style="background:#ffffff;" name="addcollege" ng-submit="submitform()" novalidate>
                              <div class="row">
                                <div class="input-field col s12">
                                  <?php echo form_dropdown('collegeuniversity',$fetchalluniversitydata,'', 'class="browser-default" ng-model="college.collegeuniversity" id="collegeuniversity" required'); ?>
                                  <span ng-show="submittedclg && addcollege.collegeuniversity.$error.required"  class="help-block has-error ng-hide">Please select university.</span>
                                  <span class="help-block has-error ng-hide" ng-show="collegeuniversityError">{{collegeuniversityError}}</span>
                                </div>
                              </div>

                                <div class="row">
                                  <div class="input-field col s12">
                                    <input type="text" class="validate" id="collegename" name="collegename" ng-model="college.collegename" maxlength="50" required>
                                    <span ng-show="submittedclg && addcollege.collegename.$error.required"  class="help-block has-error ng-hide">Name is required.</span>
                                    <span class="help-block has-error ng-hide" ng-show="collegenameError">{{collegenameError}}</span>
                                    <label for="list_name">Name</label>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="input-field col s12">
                                    <input type="text" class="validate" id="collegeshortname" name="collegeshortname" ng-model="college.collegeshortname" maxlength="20" required>
                                    <span ng-show="submittedclg && addcollege.collegeshortname.$error.required"  class="help-block has-error ng-hide">Short name is required.</span>
                                    <span class="help-block has-error ng-hide" ng-show="collegeshortnameError">{{collegeshortnameError}}</span>
                                    <label for="list_name">Short Name</label>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="input-field col s12">
                                    <input type="text" class="validate" id="collegeurl" name="collegeurl" ng-model="college.collegeurl" ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/" required>
                                    <span ng-show="submittedclg && addcollege.collegeurl.$error.required"  class="help-block has-error ng-hide">URL is required.</span>
                                    <span ng-show="submittedclg && addcollege.collegeurl.$error.pattern"  class="help-block has-error ng-hide">Please enter a valid url.</span>
                                    <span class="help-block has-error ng-hide" ng-show="collegeurlError">{{collegeurlError}}</span>
                                    <label for="list_phone">URL</label>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="input-field col s12">
                                    <textarea id="address" name="address" class="materialize-textarea" rows="4" cols="50" style="resize:none;" ng-model="college.address" required></textarea>
                                    <span ng-show="submittedclg && addcollege.address.$error.required"  class="help-block has-error ng-hide">Address is required.</span>
                                    <span class="help-block has-error ng-hide" ng-show="addressError">{{addressError}}</span>
                                    <label for="email">Address</label>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="input-field col s12">
                                    <select id="enabled" name="enabled" ng-model="college.enabled" class="browser-default" required>
                                      <option value="" disabled selected>Select Status</option>
                                      <option value="1">Active</option>
                                      <option value="0">Non-Active</option>
                                    </select>
                                    <span ng-show="submittedclg && addcollege.enabled.$error.required"  class="help-block has-error ng-hide">Please select status.</span>
                                    <span class="help-block has-error ng-hide" ng-show="enabledError">{{enabledError}}</span>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="input-field col s12 v2-mar-top-40">
                                    <input class="input-btn" type="submit" value="Submit" ng-click="submittedclg=true">
                                  </div>
                                </div>
                              </form>
                            </div>
                            <div id="course" class="col s12">
                              <!--<p>
                              </p>-->
                              <div class="pg-elem-t">
                                <ul class="collapsible" data-collapsible="accordion">
                                  <li>
                                    <div class="collapsible-header">Add Course</div>
                                    <div class="collapsible-body" style="display:none;">
                                      <form class="" style="background:#ffffff;" name="addca" ng-submit="submitformcourse()" novalidate>
                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('cauniversity',$fetchalluniversitydata,'', 'class="browser-default" ng-model="ca.cauniversity" id="cauniversity" onchange="selectcollege()" required'); ?>
                                            <span ng-show="submitted && addca.cauniversity.$error.required"  class="help-block has-error ng-hide">Please select university.</span>
                                            <span class="help-block has-error ng-hide" ng-show="cauniversityError">{{cauniversityError}}</span>
                                          </div>
                                        </div>
                                        
                                        <div class="row">
                                          <div class="input-field col s12">
                                            <select name="cacollege" class="browser-default" ng-model="ca.cacollege" id="cacollege" required>
                                              <option value="">Select College</option>
                                            </select>
                                            <span ng-show="submitted && addca.cacollege.$error.required"  class="help-block has-error ng-hide">Please select college.</span>
                                            <span class="help-block has-error ng-hide" ng-show="cacollegeError">{{cacollegeError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('cacourse',$fetchallcoursesdata,'', 'class="browser-default" ng-model="ca.cacourse" id="cacourse" required'); ?>
                                            <span ng-show="submitted && addca.cacourse.$error.required"  class="help-block has-error ng-hide">Please select course.</span>
                                            <span class="help-block has-error ng-hide" ng-show="cacourseError">{{cacourseError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('caspec',$fetchallspecdata,'', 'class="browser-default" ng-model="ca.caspec" id="caspec" required'); ?>
                                            <span ng-show="submitted && addca.caspec.$error.required"  class="help-block has-error ng-hide">Please select specialization.</span>
                                            <span class="help-block has-error ng-hide" ng-show="caspecError">{{caspecError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('cacredential',$fetchallcredentialdata,'', 'class="browser-default" ng-model="ca.cacredential" id="cacredential" required'); ?>
                                            <span ng-show="submitted && addca.cacredential.$error.required"  class="help-block has-error ng-hide">Please select credential.</span>
                                            <span class="help-block has-error ng-hide" ng-show="cacredentialError">{{cacredentialError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('cadegree',$fetchalldegreedata,'', 'class="browser-default" ng-model="ca.cadegree" id="cadegree" required'); ?>
                                            <span ng-show="submitted && addca.cadegree.$error.required"  class="help-block has-error ng-hide">Please select degree.</span>
                                            <span class="help-block has-error ng-hide" ng-show="cadegreeError">{{cadegreeError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('camode',$fetchallmodedata,'', 'class="browser-default" ng-model="ca.camode" id="camode" required'); ?>
                                            <span ng-show="submitted && addca.camode.$error.required"  class="help-block has-error ng-hide">Please select mode of study.</span>
                                            <span class="help-block has-error ng-hide" ng-show="camodeError">{{camodeError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <select class="browser-default" name="caduration" ng-model="ca.caduration" id="caduration" required>
                                              <option value="">Select Duration in Years</option>
                                              <option value="0.6">0.6</option>
                                              <option value="1">1</option>
                                              <option value="2">2</option>
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                              <option value="6">6</option>
                                            </select>
                                            <span ng-show="submitted && addca.caduration.$error.required"  class="help-block has-error ng-hide">Please select duration.</span>
                                            <span class="help-block has-error ng-hide" ng-show="cadurationError">{{cadurationError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('camedium',$fetchallmediumdata,'', 'class="browser-default" ng-model="ca.camedium" id="camedium" required'); ?>
                                            <span ng-show="submitted && addca.camedium.$error.required"  class="help-block has-error ng-hide">Please select medium.</span>
                                            <span class="help-block has-error ng-hide" ng-show="camediumError">{{camediumError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('carecognition',$fetchallrecognitiondata,'', 'class="browser-default" ng-model="ca.carecognition" id="carecognition" required'); ?>
                                            <span ng-show="submitted && addca.carecognition.$error.required"  class="help-block has-error ng-hide">Please select recognition.</span>
                                            <span class="help-block has-error ng-hide" ng-show="carecognitionError">{{carecognitionError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('cacoursestatus',$fetchallcoursestatusdata,'', 'class="browser-default" ng-model="ca.cacoursestatus" id="cacoursestatus" required'); ?>
                                            <span ng-show="submitted && addca.cacoursestatus.$error.required"  class="help-block has-error ng-hide">Please select course status.</span>
                                            <span class="help-block has-error ng-hide" ng-show="cacoursestatusError">{{cacoursestatusError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('caaccreditation',$fetchallaccreditationdata,'', 'class="browser-default" ng-model="ca.caaccreditation" id="caaccreditation" required'); ?>
                                            <span ng-show="submitted && addca.caaccreditation.$error.required"  class="help-block has-error ng-hide">Please select accreditation.</span>
                                            <span class="help-block has-error ng-hide" ng-show="caaccreditationError">{{caaccreditationError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <?php echo form_dropdown('caownership',$fetchallownershipdata,'', 'class="browser-default" ng-model="ca.caownership" id="caownership" required'); ?>
                                            <span ng-show="submitted && addca.caownership.$error.required"  class="help-block has-error ng-hide">Please select ownership.</span>
                                            <span class="help-block has-error ng-hide" ng-show="caownershipError">{{caownershipError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <input type="text" class="validate" id="cafees" name="cafees" ng-model="ca.cafees" ng-pattern="/^[0-9]*$/" required>
                                            <span ng-show="submitted && addca.cafees.$error.required"  class="help-block has-error ng-hide">Fees is required.</span>
                                            <span ng-show="submitted && addca.cafees.$error.pattern"  class="help-block has-error ng-hide">Please enter a valid number.</span>
                                            <span class="help-block has-error ng-hide" ng-show="cafeesError">{{cafeesError}}</span>
                                            <label for="list_name">Total Fees</label>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <textarea id="eligibility" name="eligibility" class="materialize-textarea" rows="4" cols="50" style="resize:none;" ng-model="ca.eligibility" required></textarea>
                                            <span ng-show="submitted && addca.eligibility.$error.required"  class="help-block has-error ng-hide">Eligibility is required.</span>
                                            <span class="help-block has-error ng-hide" ng-show="eligibilityError">{{eligibilityError}}</span>
                                            <label for="email">Eligibility</label>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <textarea id="coursestructure" name="coursestructure" class="materialize-textarea" rows="4" cols="50" style="resize:none;" ng-model="ca.coursestructure" required></textarea>
                                            <span ng-show="submitted && addca.coursestructure.$error.required"  class="help-block has-error ng-hide">Course structure is required.</span>
                                            <span class="help-block has-error ng-hide" ng-show="coursestructureError">{{coursestructureError}}</span>
                                            <label for="email">Course Structure</label>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12">
                                            <select id="enabled" name="enabled" ng-model="ca.enabled" class="browser-default" required>
                                              <option value="" disabled selected>Select Status</option>
                                              <option value="1">Active</option>
                                              <option value="0">Non-Active</option>
                                            </select>
                                            <span ng-show="submitted && addca.enabled.$error.required"  class="help-block has-error ng-hide">Please select status.</span>
                                            <span class="help-block has-error ng-hide" ng-show="enabledError">{{enabledError}}</span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="input-field col s12 v2-mar-top-40">
                                            <input class="input-btn" type="submit" value="Submit" ng-click="submitted=true">
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </li>
                                  <li>
                                    <div class="collapsible-header">View All</div><br>
                                    <div class="collapsible-body" style="display:none;">
                                      <table class="table table-striped table-bordered" datatable="ng" dt-options="vm.dtOptions">
                                        <thead>
                                          <tr>
                                            <th>Listing</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                            <th>Create Copy</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr ng-if="x.checkarray == 1" ng-repeat="x in names" ng-cloak>
                                            <td><span class="list-img" ng-cloak>{{ x.srno }}</span> </td>
                                            <td><span class="list-enq-name" ng-cloak>{{ x.name }} in {{ x.specializationname }}</span><!-- <span class="list-enq-city">Illunois, United States</span> --></td>
                                            <td><span class="db-list-ststus" ng-if="x.ascenabled == 1">Active</span><span class="db-list-ststus-na" ng-if="x.ascenabled == 0">Non-Active</span></td>
                                            <td> <a style="cursor:pointer;" data-toggle="modal" data-target="#myModal" ng-click="load_association(x.tcaid)" title="Edit"><i class="fa fa-edit"></i></a> </td>
                                            <td> <a href="#." ng-click="delete(x.tcaid)" title="Delete"><i class="fa fa-times"></i></a> </td>
                                            <td> <a href="#." ng-click="createcopy(x.tcaid)" title="Create Copy"><i class="fa fa-copy"></i></a> </td>
                                          </tr>
                                        </tbody> 
                                      </table>
                                    </div>
                                  </li>
                                </ul>
                              </div>
                            </div>

                            <!-- Modal -->
                            <!-- <div class="modal fade" id="myModal" role="dialog" style="padding-top:70px;">
                              <div class="modal-dialog">

                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">Edit Course Association</h5>
                                  </div>
                                  <div class="modal-body edit-content">
                                  </div>
                                </div>
                                
                              </div>
                            </div> -->
                            <!-- <div id="accreditation" class="col s12">
                              <p>2Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                            <div id="highlights" class="col s12">
                              <p>3Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div> -->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--== BOTTOM FLOAT ICON ==-->
  <!-- <section>
    <div class="fixed-action-btn vertical">
      <a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
      <ul>
        <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
        <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
        <li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
        <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
      </ul>
    </div>
  </section>