<!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
          <ul>
            <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> Dashboard</a> </li>
            <li class="page-back"><a href="#"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>Manage Listing</h4>
            <div class="tz-2-main-com bot-sp-20">
              <div class="tz-2-main-1 tz-2-main-admin" ng-repeat="x in names" ng-cloak>
                <div class="tz-2-main-2"> <i class="fa fa-university font-icons"></i><span>Total University</span>
                  <p ng-cloak>Total {{ x.universitycount }} university are created</p>
                  <a href="<?php echo base_url(); ?>admin/alluniversity"><h2 ng-cloak>{{ x.universitycount }}</h2></a>
                </div>
              </div>
              
              <div class="tz-2-main-1 tz-2-main-admin" ng-repeat="c in cnames" ng-cloak>
                <div class="tz-2-main-2"> <i class="fa fa-edit font-icons"></i><span>Total Courses</span>
                  <p ng-cloak>Total {{ c.coursecount }} courses are created</p>
                  <a href="<?php echo base_url(); ?>admin/allcourses"><h2 ng-cloak>{{ c.coursecount }}</h2></a>
                </div>
              </div>
              
              <div class="tz-2-main-1 tz-2-main-admin" ng-repeat="s in snames" ng-cloak>
                <div class="tz-2-main-2"> <i class="fa fa-file font-icons"></i><span>Total Specialization</span>
                  <p ng-cloak>Total {{ s.specializationcount }} specialization are created</p>
                  <a href="<?php echo base_url(); ?>admin/allspecialization"><h2 ng-cloak>{{ s.specializationcount }}</h2></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--== BOTTOM FLOAT ICON ==-->
  <!-- <section>
    <div class="fixed-action-btn vertical">
      <a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
      <ul>
        <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
        <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
        <li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
        <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
      </ul>
    </div>
  </section> -->