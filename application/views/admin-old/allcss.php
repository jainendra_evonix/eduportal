<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>EduPortal - Admin Panel</title>
  <!-- META TAGS -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- FAV ICON(BROWSER TAB ICON) -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/fav.ico" type="image/x-icon">
  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
  <!-- FONTAWESOME ICONS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
  <!-- ALL CSS FILES -->
  <link href="<?php echo base_url(); ?>assets/css/materialize.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/angular.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/angular-datatables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
  <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>

  <![endif]-->
  
</head>