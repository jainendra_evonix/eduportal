<!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
          <ul>
            <li><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> Add Course</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>Add New Course</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="#!">Add New</a> </li>
              <li><a href="#!">Edit</a> </li>
              <li><a href="#!">Update</a> </li>
              <li class="divider"></li>
              <li><a href="#!"><i class="material-icons">delete</i>Delete</a> </li>
              <li><a href="#!"><i class="material-icons">subject</i>View All</a> </li>
              <li><a href="#!"><i class="material-icons">play_for_work</i>Download</a> </li>
            </ul>
            <!-- Dropdown Structure -->
            <div class="split-row">
              <div class="col-md-12">
                <div class="box-inn-sp ad-inn-page">
                  <div class="tab-inn ad-tab-inn">
                    <div class="hom-cre-acc-left hom-cre-acc-right">
                      <div class="">
                        <?php echo $this->session->flashdata('successmsg');  ?>
                        <?php echo $this->session->flashdata('errormsg');  ?>
                        <form class="" name="addca" ng-submit="submitform()" novalidate>
                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('cauniversity',$fetchalluniversitydata,'', 'class="browser-default" ng-model="ca.cauniversity" id="cauniversity" required'); ?>
                              <span ng-show="submitted && addca.cauniversity.$error.required"  class="help-block has-error ng-hide">Please select university.</span>
                              <span class="help-block has-error ng-hide" ng-show="cauniversityError">{{cauniversityError}}</span>
                            </div>
                          </div>
                          
                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('cacollege',$fetchallcollegedata,'', 'class="browser-default" ng-model="ca.cacollege" id="cacollege" required'); ?>
                              <span ng-show="submitted && addca.cacollege.$error.required"  class="help-block has-error ng-hide">Please select college.</span>
                              <span class="help-block has-error ng-hide" ng-show="cacollegeError">{{cacollegeError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('cacourse',$fetchallcoursesdata,'', 'class="browser-default" ng-model="ca.cacourse" id="cacourse" required'); ?>
                              <span ng-show="submitted && addca.cacourse.$error.required"  class="help-block has-error ng-hide">Please select course.</span>
                              <span class="help-block has-error ng-hide" ng-show="cacourseError">{{cacourseError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('caspec',$fetchallspecdata,'', 'class="browser-default" ng-model="ca.caspec" id="caspec" required'); ?>
                              <span ng-show="submitted && addca.caspec.$error.required"  class="help-block has-error ng-hide">Please select specialization.</span>
                              <span class="help-block has-error ng-hide" ng-show="caspecError">{{caspecError}}</span>
                            </div>
                          </div>
                          
                          <?php //echo '<pre>'; print_r($fetchallspecdata); ?>
                          <!-- <div class="row">
                            <div class="input-field col s12">
                              <div class="checkheading">
                                <h5 class="checkname">Select Specialization</h5>
                              </div>
                              <div class="checkboxlabel">
                                <?php //foreach($fetchallspecdata as $k=>$v) {?>
                                <div>
                                <?php /*echo form_checkbox("", "", false, 'class="filled-in" id="spec_'.$v->id.'" checklist-model="ca.caspec" checklist-value="asd_'.$v->id.'" id="caspec" required');*/ ?><label for="spec_<?php echo $v->id; ?>"><?php echo $v->name; ?>&nbsp;&nbsp;</label>
                                </div>
                                <?php //}?>
                              </div>
                              <span ng-show="submitted && addca.caspec.$error.required"  class="help-block has-error ng-hide">Please select specialization.</span>
                              <span class="help-block has-error ng-hide" ng-show="caspecError">{{caspecError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                                <div class="checkheading">
                                  <h5 class="checkname">Select Specialization</h5>
                                </div>
                              <div class="checkboxlabel">
                                <div>
                                  <input type="checkbox" checklist-model="ca.chkweek" checklist-value="v1" class="filled-in" id="c1" />
                                  <label for="c1">English</label>
                                </div>&nbsp;&nbsp;
                                <div>
                                  <input type="checkbox" checklist-model="ca.chkweek" checklist-value="v2" class="filled-in" id="c2" />
                                  <label for="c2">French</label>
                                </div>
                              </div>
                            </div>
                          </div> -->

                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('cacredential',$fetchallcredentialdata,'', 'class="browser-default" ng-model="ca.cacredential" id="cacredential" required'); ?>
                              <span ng-show="submitted && addca.cacredential.$error.required"  class="help-block has-error ng-hide">Please select credential.</span>
                              <span class="help-block has-error ng-hide" ng-show="cacredentialError">{{cacredentialError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('cadegree',$fetchalldegreedata,'', 'class="browser-default" ng-model="ca.cadegree" id="cadegree" required'); ?>
                              <span ng-show="submitted && addca.cadegree.$error.required"  class="help-block has-error ng-hide">Please select degree.</span>
                              <span class="help-block has-error ng-hide" ng-show="cadegreeError">{{cadegreeError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('camode',$fetchallmodedata,'', 'class="browser-default" ng-model="ca.camode" id="camode" required'); ?>
                              <span ng-show="submitted && addca.camode.$error.required"  class="help-block has-error ng-hide">Please select mode of study.</span>
                              <span class="help-block has-error ng-hide" ng-show="camodeError">{{camodeError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select class="browser-default" name="caduration" ng-model="ca.caduration" id="caduration" required>
                                <option value="">Select Duration in Years</option>
                                <option value="0.6">0.6</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                              </select>
                              <span ng-show="submitted && addca.caduration.$error.required"  class="help-block has-error ng-hide">Please select duration.</span>
                              <span class="help-block has-error ng-hide" ng-show="cadurationError">{{cadurationError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('camedium',$fetchallmediumdata,'', 'class="browser-default" ng-model="ca.camedium" id="camedium" required'); ?>
                              <span ng-show="submitted && addca.camedium.$error.required"  class="help-block has-error ng-hide">Please select medium.</span>
                              <span class="help-block has-error ng-hide" ng-show="camediumError">{{camediumError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('carecognition',$fetchallrecognitiondata,'', 'class="browser-default" ng-model="ca.carecognition" id="carecognition" required'); ?>
                              <span ng-show="submitted && addca.carecognition.$error.required"  class="help-block has-error ng-hide">Please select recognition.</span>
                              <span class="help-block has-error ng-hide" ng-show="carecognitionError">{{carecognitionError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('cacoursestatus',$fetchallcoursestatusdata,'', 'class="browser-default" ng-model="ca.cacoursestatus" id="cacoursestatus" required'); ?>
                              <span ng-show="submitted && addca.cacoursestatus.$error.required"  class="help-block has-error ng-hide">Please select course status.</span>
                              <span class="help-block has-error ng-hide" ng-show="cacoursestatusError">{{cacoursestatusError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('caaccreditation',$fetchallaccreditationdata,'', 'class="browser-default" ng-model="ca.caaccreditation" id="caaccreditation" required'); ?>
                              <span ng-show="submitted && addca.caaccreditation.$error.required"  class="help-block has-error ng-hide">Please select accreditation.</span>
                              <span class="help-block has-error ng-hide" ng-show="caaccreditationError">{{caaccreditationError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('caownership',$fetchallownershipdata,'', 'class="browser-default" ng-model="ca.caownership" id="caownership" required'); ?>
                              <span ng-show="submitted && addca.caownership.$error.required"  class="help-block has-error ng-hide">Please select ownership.</span>
                              <span class="help-block has-error ng-hide" ng-show="caownershipError">{{caownershipError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <input type="text" class="validate" id="cafees" name="cafees" ng-model="ca.cafees" ng-pattern="/^[0-9]*$/" required>
                              <span ng-show="submitted && addca.cafees.$error.required"  class="help-block has-error ng-hide">Fees is required.</span>
                              <span ng-show="submitted && addca.cafees.$error.pattern"  class="help-block has-error ng-hide">Please enter a valid number.</span>
                              <span class="help-block has-error ng-hide" ng-show="cafeesError">{{cafeesError}}</span>
                              <label for="list_name">Total Fees</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <textarea id="eligibility" name="eligibility" class="materialize-textarea" rows="4" cols="50" style="resize:none;" ng-model="ca.eligibility" required></textarea>
                              <span ng-show="submitted && addca.eligibility.$error.required"  class="help-block has-error ng-hide">Eligibility is required.</span>
                              <span class="help-block has-error ng-hide" ng-show="eligibilityError">{{eligibilityError}}</span>
                              <label for="email">Eligibility</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <textarea id="coursestructure" name="coursestructure" class="materialize-textarea" rows="4" cols="50" style="resize:none;" ng-model="ca.coursestructure" required></textarea>
                              <span ng-show="submitted && addca.coursestructure.$error.required"  class="help-block has-error ng-hide">Course structure is required.</span>
                              <span class="help-block has-error ng-hide" ng-show="coursestructureError">{{coursestructureError}}</span>
                              <label for="email">Course Structure</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select id="enabled" name="enabled" ng-model="ca.enabled" class="browser-default" required>
                                <option value="" disabled selected>Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Non-Active</option>
                              </select>
                              <span ng-show="submitted && addca.enabled.$error.required"  class="help-block has-error ng-hide">Please select status.</span>
                              <span class="help-block has-error ng-hide" ng-show="enabledError">{{enabledError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12 v2-mar-top-40">
                              <input class="input-btn" type="submit" value="Submit" ng-click="submitted=true">
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--== BOTTOM FLOAT ICON ==-->
  <section>
    <div class="fixed-action-btn vertical">
      <a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
      <ul>
        <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
        <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
        <li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
        <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
      </ul>
    </div>
  </section>