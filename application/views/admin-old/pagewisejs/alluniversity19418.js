<script>
var postApp = angular.module('postApp', []);
postApp.controller('postController', function($scope, $http) {
   $http.get("<?php echo base_url();?>admin/getalluniversity")
   .then(function (response) {
    console.log(response);
    $scope.names = response.data;});
   //alert();

   $scope.linkurl = function(linkurlvalue){
          //alert(linkurlvalue);
          //window.location = linkurlvalue;
          window.open(linkurlvalue, '_blank');
        }

   $scope.edit = function(editingId){
        	//alert(editingId);
        	window.location = '<?php echo base_url() ?>admin/edituniversity/'+editingId;
        }

   $scope.delete = function(deletingId){
	//alert(deletingId);
	if (confirm("Are you sure you want to delete?")) {
		$http({
       method: 'POST',
       url: '<?php echo base_url() ?>admin/deleteuniversity',
      
       data: $.param({deletingId:deletingId}),
       headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function(data){
        //alert("deleted"+ deletingId);
        location.reload();
         })
    }
}
   
});

$("#university").addClass("menu-active");
  
</script>