<script>
    // Defining angularjs application.
    angular.module('postApp', ['postApp.controllers','datatables']);

    // Controller function and passing $http service and $scope var.
    //postApp.controller('postController', ['$scope', 'FileUploader', function ($scope, $http, FileUploader) {
    //postApp.controller('postController', ['$scope', 'FileUploader', function($scope, FileUploader, $http) {
        //postApp.controller('postController', ['$scope', 'FileUploader', '$http', function ($scope, FileUploader, $http) {
            angular.module('postApp.controllers', []).controller('postController', function($scope,$http,DTOptionsBuilder, DTColumnBuilder) {
    // create a blank object to handle form data.
    $scope.college = {};
            // calling our submit function.
            $scope.submitform = function () {
                //alert('submitform');

            // Posting data to php file
            if ($scope.addcollege.$valid) {
                //alert('if');
       			//alert('login');
                    $http({
                    method: 'POST',
                            dataType: 'json',
                            url: '<?php echo base_url() ?>admin/submitcollege',
                            data: $scope.college, //forms user object
                            headers: {'Content-Type': 'application/json'}
                    }).success(function (data) {
                        
                        
                   //console.log(data);
                        //alert(data.status);
            //console.log(data);
                    // console.log(data.error.password.error);
                    if (data.status == 1) {
                        $('.help-block').css('display','none');
                        /*setTimeout(function(){
                            //$('#message2').fadeOut();
                            window.location = '<?php echo base_url();?>admin/addcollege';
                        }, 5000);*/
                        window.location = '<?php echo base_url();?>admin/addcollege';
            } else if (data.status == 0)
            {
                    window.location = '<?php echo base_url();?>admin/addcollege'; 
                    
            } else
            {
                    window.location = '<?php echo base_url();?>admin/addcollege';
             }
            
            });
            }
            };

//ca
$scope.ca = {};
            // calling our submit function.
            $scope.submitformcourse = function () {
                //alert('submitformcourse');

            // Posting data to php file
            if ($scope.addca.$valid) {
                //alert('if');
       //alert('login');
                    $http({
                    method: 'POST',
                            dataType: 'json',
                            url: '<?php echo base_url() ?>admin/submitcourseassociation',
                            data: $scope.ca, //forms user object
                            headers: {'Content-Type': 'application/json'}
                    }).success(function (data) {
                        
                        
                   //console.log(data);
                        //alert(data.status);
            //console.log(data);
                    // console.log(data.error.password.error);
                    if (data.status == 1) {
                        $('.help-block').css('display','none');
                        /*setTimeout(function(){
                            //$('#message2').fadeOut();
                            window.location = '<?php echo base_url();?>admin/addcollege';
                        }, 5000);*/
                        window.location = '<?php echo base_url();?>admin/addcollege';
            } else if (data.status == 0)
            {
                    window.location = '<?php echo base_url();?>admin/addcollege'; 
                    
            } else
            {
                    window.location = '<?php echo base_url();?>admin/addcollege';
             }
            
            });
            }
            };
//ca

   $http.get("<?php echo base_url() ?>admin/getallasccourses").success(function(response){
      //console.log(response);
      $scope.names = response;  //ajax request to fetch data into vm.data
    });

   $scope.vm = {};

    $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
      .withOption('order', [0, 'asc']);

   $scope.delete = function(deletingId){
    //alert(deletingId);
    if (confirm("Are you sure you want to delete?")) {
        $http({
       method: 'POST',
       url: '<?php echo base_url() ?>admin/deleteasccourse',
      
       data: $.param({deletingId:deletingId}),
       headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function(data){
        //alert("deleted"+ deletingId);
        location.reload();
         })
    }
}

$scope.createcopy = function(copyId){
    //alert(copyId);
    if (confirm("Are you sure to create copy?")) {
        $http({
       method: 'POST',
       url: '<?php echo base_url() ?>admin/createcopyasccourse',
      
       data: $.param({copyId:copyId}),
       headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function(data){
        //alert("copied"+ copyId);
        location.reload();
         })
    }
}

//popup

$scope.load_association = function(eId){
    //alert(eId);
    $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/editcourseassociation');?>",
                data: "eId="+eId,
                success: function (response) {
                $(".displaycontent").html(response);
                  
                }
            });
}
//popup

    //}]);
    });

    $("#college").addClass("menu-active");
       
</script>

<script type="text/javascript">
function selectcollege()
    {
       //alert(1);
       var collegeuniversity=$('#cauniversity').val();
       //alert(collegeuniversity);
            
            $.post('<?php echo base_url();?>admin/getuniversitycollege/',
        {
            collegeuniversity:collegeuniversity
            
            },
            function(data) 
            {
            
            $('#cacollege').html(data);
            }); 
    }
</script>

<script type="text/javascript">
    $('#myModal').on('show.bs.modal', function(e) {
            
            var $modal = $(this),
                eId = e.relatedTarget.id;
            
            $.ajax({
                cache: false,
                type: 'POST',
                url: '<?php echo base_url();?>admin/editcourseassociation/',
                //data: eId,
                data: {eId: eId},
                dataType: 'json',
                success: function(data) 
                {
                  //alert(data);
                  $modal.find('.edit-content').html(data);
                  /*$modal.find('.edit-content').html('<form class="" style="background:#ffffff;" name="addcae" ng-submit="submitformeditcourse()" novalidate><input type="text" class="validate" id="caefees" name="caefees" ng-model="cae.caefees" ng-pattern="/^[0-9]*$/" value="'+data.totalfees+'" required><span ng-show="submitted && addcae.caefees.$error.required"  class="help-block has-error ng-hide">Fees is required.</span><span ng-show="submitted && addcae.caefees.$error.pattern"  class="help-block has-error ng-hide">Please enter a valid number.</span><span class="help-block has-error ng-hide" ng-show="caefeesError">{{caefeesError}}</span><label for="list_name">Total Fees</label><div class="row"><div class="input-field col s12 v2-mar-top-40"><input class="input-btn" type="submit" value="Submit" ng-click="submitted=true"></div></div></form>');*/
                }
            });
            
        })
</script>

<script type="text/javascript">
//$(".modal-dialog").hide();
/*function load_association(eId)
{
    $.ajax({
                type: "POST",
                url: "<?php echo base_url('admin/editcourseassociation');?>",
                data: "eId="+eId,
                success: function (response) {
                $(".displaycontent").html(response);
                  
                }
            });
}*/
</script>

<div class="modal fade displaycontent" id="myModal" style="padding-top:50px;">

<?php include('modal.php'); ?>