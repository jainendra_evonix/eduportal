<script>
    // Defining angularjs application.
    var postApp = angular.module('postApp', []);

    // Controller function and passing $http service and $scope var.
    //postApp.controller('postController', ['$scope', 'FileUploader', function ($scope, $http, FileUploader) {
    //postApp.controller('postController', ['$scope', 'FileUploader', function($scope, FileUploader, $http) {
        //postApp.controller('postController', ['$scope', 'FileUploader', '$http', function ($scope, FileUploader, $http) {
           
         /*directive for upload file*/

            

    /*
     A directive to enable two way binding of file field
     */

     //console.log('above')
    postApp.directive('demoFileModel', function ($parse) {

        console.log('here')
        return {
            restrict: 'A', //the directive can be used as an attribute only
 
            /*
             link is a function that defines functionality of directive
             scope: scope associated with the element
             element: element on which this directive used
             attrs: key value pair of element attributes
             */
            link: function (scope, element, attrs) {
                var model = $parse(attrs.demoFileModel),
                    modelSetter = model.assign; //define a setter for demoFileModel
 
                //Bind change event on the element
                element.bind('change', function () {
                    //Call apply on scope, it checks for value changes and reflect them on UI
                    scope.$apply(function () {
                        //set the model value
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    });


     
   
    postApp.controller('postController', function ($scope, fileUploadService) {
       console.log('here2')
        $scope.uploadFile = function () {
            alert('here')
            var file = $scope.myFile;
            var uploadUrl = "../server/service.php", //Url of webservice/api/server
                promise = fileUploadService.uploadFileToUrl(file, uploadUrl);

            promise.then(function (response) {
                $scope.serverResponse = response;
            }, function () {
                $scope.serverResponse = 'An error has occurred';
            })
        };
    });



    (function () {
    'use strict';
      
      console.log('service');
    postApp.service('fileUploadService', function ($http, $q) {
      console.log('service1')
        this.uploadFileToUrl = function (file, uploadUrl) {
             console.log('service2')
            //FormData, object of key/value pair for form fields and values
            var fileFormData = new FormData();
            fileFormData.append('file', file);

            var deffered = $q.defer();
            $http.post(uploadUrl, fileFormData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}

            }).success(function (response) {
                deffered.resolve(response);

            }).error(function (response) {
                deffered.reject(response);
            });

            return deffered.promise;
        }
    });
})();






            postApp.controller('postController', function ($scope, $http) {
    // create a blank object to handle form data.
    $scope.university = {};
            // calling our submit function.
            $scope.submitform = function () {
                //alert('submitform');

            // Posting data to php file
            if ($scope.adduniversity.$valid) {
                //alert('if');
       			//alert('login');
                    $http({
                    method: 'POST',
                            dataType: 'json',
                            url: '<?php echo base_url() ?>admin/submituniversity',
                            data: $scope.university, //forms user object
                            headers: {'Content-Type': 'application/json'}
                    }).success(function (data) {
                        
                        
                   //console.log(data);
                        //alert(data.status);
            //console.log(data);
                    // console.log(data.error.password.error);
                    if (data.status == 1) {
                        $('.help-block').css('display','none');
                        /*setTimeout(function(){
                            //$('#message2').fadeOut();
                            window.location = '<?php echo base_url();?>admin/adduniversity';
                        }, 5000);*/
                        window.location = '<?php echo base_url();?>admin/adduniversity';
            } else if (data.status == 0)
            {
                    window.location = '<?php echo base_url();?>admin/adduniversity'; 
                    
            } else
            {
                    window.location = '<?php echo base_url();?>admin/adduniversity';
             }
            
            });
            }
            };




    //}]);
    });

    $("#university").addClass("menu-active");
       
</script>