<section class="tz-register">
		<div class="log-in-pop-full">
			<div class="log-in-pop-center">
				<a href="#" class="pop-close" data-dismiss="modal"><img src="<?php echo base_url(); ?>assets/images/cancel.png" alt="" />
				</a>
				<h4>Forgot Password?</h4>
				<p>Enter your e-mail address below to reset your password.</p>
				<div id="message"></div>
				<?php echo $this->session->flashdata('successmsg');  ?>
                <?php echo $this->session->flashdata('errormsg');  ?>
				<form class="s12" name="userLogin" ng-submit="submitForm()" novalidate>
					<div>
						<div class="input-field s12">
							<input type="email" class="validate" id="emailaddress" ng-model="user.emailaddress" name="emailaddress" required>
							<label>Email ID (required)</label>
							<span ng-show="submitted && userLogin.emailaddress.$invalid" class="help-block has-error ng-cloak">Valid Email ID is required.</span>
      						<span class="help-block has-error ng-cloak" ng-show="emailaddressError">{{emailaddressError}}</span>
						</div>
					</div>
					<div>
						<div class="input-field s4">
							<!-- <input type="submit" value="Register" class="waves-effect waves-light log-in-btn"> -->
							<input type="submit" value="Submit" class="waves-effect waves-light log-in-btn" ng-click="submitted = true">
						</div>
					</div>
					<div>
						<div class="input-field s12"> <a href="<?php echo base_url();?>adminlogin">Are you a already member ? Login</a></div>
					</div>
				</form>
			</div>
		</div>
</section>