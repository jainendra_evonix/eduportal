  <?php

  


  //echo "<pre>sadas"; print_r($allCourseInfo); exit;

      
      if(!empty($postdata)){

      //  echo 'if';

       $id = isset($allCourseInfo) && !empty($postdata['id']) ? $allCourseInfo['id'] : '';
      $university = isset($postdata) && !empty($postdata['university']) ? $postdata['university'] : '';
      $universityName = isset($postdata) && !empty($postdata['universityName']) ? $postdata['university'] : '';
      $collegeName = isset($postdata) && !empty($postdata['collegeName']) ? $postdata['college'] : '';
      $college = isset($postdata) && !empty($postdata['college']) ? $postdata['college'] : '';
      $course = isset($postdata) && !empty($postdata['course']) ? $postdata['course'] : '';
      $courseSpec = isset($postdata) && !empty($postdata['courseSpec']) ? $postdata['courseSpec'] : '';
      $courseShortName = isset($postdata) && !empty($postdata['courseShortName']) ? $postdata['courseShortName'] : '';
      $duration = isset($postdata) && !empty($postdata['courseDuration']) ? $postdata['courseDuration'] : '';
      $courseFee = isset($postdata) && !empty($postdata['courseFee']) ? $postdata['courseFee'] : '';
      $feeStructure = isset($postdata) && !empty($postdata['feeStructure']) ? $postdata['feeStructure'] : '';
      //$course_type = isset($allCourseInfo) && !empty($allCourseInfo->course_type) ? $allCourseInfo->course_type : '';
      //$course_stream = isset($allCourseInfo) && !empty($allCourseInfo->course_stream) ? $allCourseInfo->course_stream : '';
      $study_mode = isset($allCourseInfo) && !empty($allCourseInfo['modeofstudy']) ? $allCourseInfo['study_mode'] : '';
      $courseEligibility = isset($postdata) && !empty($postdata['courseEligibility']) ? $postdata['courseEligibility'] : '';
       
      }



  else{
       
      $id = isset($allCourseInfo) && !empty($allCourseInfo->id) ? $allCourseInfo->id : '';
      $university = isset($allCourseInfo) && !empty($allCourseInfo->university_id) ? $allCourseInfo->university_id : '';
      $universityName = isset($allCourseInfo) && !empty($allCourseInfo->universityName) ? $allCourseInfo->universityName : '';
      $collegeName = isset($allCourseInfo) && !empty($allCourseInfo->colgName) ? $allCourseInfo->colgName : '';
      $college = isset($allCourseInfo) && !empty($allCourseInfo->college_id) ? $allCourseInfo->college_id : '';
      $course = isset($allCourseInfo) && !empty($allCourseInfo->course_name) ? $allCourseInfo->course_name : '';
      $courseSpec = isset($allCourseInfo) && !empty($allCourseInfo->course_specialization) ? $allCourseInfo->course_specialization : '';
      $courseShortName = isset($allCourseInfo) && !empty($allCourseInfo->course_shortname) ? $allCourseInfo->course_shortname : '';
      $duration = isset($allCourseInfo) && !empty($allCourseInfo->course_duration) ? $allCourseInfo->course_duration : '';
      $courseFee = isset($allCourseInfo) && !empty($allCourseInfo->course_total_fee) ? $allCourseInfo->course_total_fee : '';
      $feeStructure = isset($allCourseInfo) && !empty($allCourseInfo->course_fee_structure) ? $allCourseInfo->course_fee_structure : '';
      $course_type = isset($allCourseInfo) && !empty($allCourseInfo->course_type) ? $allCourseInfo->course_type : '';
      $course_stream = isset($allCourseInfo) && !empty($allCourseInfo->course_stream) ? $allCourseInfo->course_stream : '';
      $study_mode = isset($allCourseInfo) && !empty($allCourseInfo->study_mode) ? $allCourseInfo->study_mode : '';

      $courseEligibility = isset($allCourseInfo) && !empty($allCourseInfo->course_eligibility) ? $allCourseInfo->course_eligibility : '';

     // echo $courseEligibility; exit;
      
      }




   ?>


  <!-- == BODY INNER CONTAINER ==-->
        <div class="sb2-2">
          <!--== breadcrumbs ==-->
          <div class="sb2-2-2">
            <ul>
              <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
              <li class="active-bre"><a href="#"> Add Courses to University/College</a> </li>
              <li class="page-back"><a href="<?php echo base_url() ?>admin/allcourses"><i class="fa fa-backward" aria-hidden="true"></i> View All</a> </li>
            </ul>
          </div>
          <div class="tz-2 tz-2-admin">
            <div class="tz-2-com tz-2-main">
              <h4>Add Course to University/College</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
              <ul id="dr-list" class="dropdown-content">
                <li><a href="<?php echo base_url() ?>admin/addcourse">Add New</a> </li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url() ?>admin/allCollgeCourses"><i class="material-icons">subject</i>View All</a> </li>
              </ul>
              <!-- Dropdown Structure -->
              <div class="split-row">
                <div class="col-md-12">
                  <div class="box-inn-sp ad-inn-page">
                    <div class="tab-inn ad-tab-inn">
                      <div class="hom-cre-acc-left hom-cre-acc-right">
                        <div class="">
                        
                          <form class="" name="addcourse" action="<?php echo base_url(); ?>admin/editCourse" method="post" novalidate>
                                    

                                    <div class="row form-group">
                                        <label class="col-md-2">University</label>
                                        <div class="col-md-10">

                                           <input type="text" name="universityName" value="<?php echo $universityName; ?>" readonly>
                                           <input type="hidden" name="university" value="<?php echo $university; ?>" >
                                           <input type="hidden" name="id" value="<?php echo $id; ?>" >
                                            
                                            <?php echo form_error('university', '<div class="error">', '</div>'); ?>   

                                        </div>
                                    </div>



                                    <div class="row form-group">
                                        <label class="col-md-2">College</label>
                                        <div class="col-md-10">
                                          <input type="text" name="collegeName" value="<?php echo $collegeName; ?>" readonly>
                                           <input type="hidden" name="college" value="<?php echo $college; ?>" >
                                            
                                            <?php echo form_error('college', '<div class="error">', '</div>'); ?>  
                                          
                                        </div>
                                    </div>

                                    <div class="input_fields_wrap"> 
                                    <div class="row">
                                    <div class="col-md-3">
                                    <div class="row form-group">
                                       <!--  <label class="col-md-3">Course</label> -->
                                        <div class="" id="course-cust">
                                             <select class="" name="course" value=""  data-validation="required">
                                               <option>Select Stream</option>
                                              <?php foreach ($masterCourses as $key ){ ?>
                                                <option value="<?php echo $key->course_name;?>" <?php if($key->course_name ==$course){ echo 'selected'; } ?>><?php echo $key->course_name; ?></option>
                                              <?php   } ?>
                                            </select>
                                        </div>
                                        </div>
                                    </div>
                                      
                                       <div class="col-md-4 form-group">
                                         
                                              <input type="text" class="validate form-control" id="courseSpec" name="courseSpec" value="<?php echo $courseSpec;  ?>" placeholder="Enter course specialization name" data-validation="required"  data-validation-error-msg="Please enter course specialization" >
                                          <?php echo form_error('courseSpec', '<div class="error">', '</div>'); ?>   
                                    </div>

                                    <div class="col-md-4 form-group">
                                          <!-- <label>Course Short Name</label> -->
                                              <input type="text" class="validate form-control" id="courseShortName" name="courseShortName"  value="<?php echo $courseShortName; ?>" placeholder="Enter short name" data-validation="required"  data-validation-error-msg="Please enter course specialization" >
                                          <?php echo form_error('courseSpec', '<div class="error">', '</div>'); ?>   
                                    </div>
<!-- 
                                    <div class="col-md-1 form-group">
                                              <a href="javascript:void(0);" onclick="getStream();" ><i class="fa fa-plus-circle  add_field_button" aria-hidden="true"></i></a>
    
                                    </div> -->

                                    </div>

                                    <div class="row">
                                      <div class="col-md-3">
                                        <div class="row form-group">
                                          <div class="" id="course-duration">
                                             <select class="" name="courseDuration" value=""  data-validation="required">
                                               <option>Select Duration</option>
                                              <?php foreach ($courseDuration as $key ){ ?>
                                                <option value="<?php echo $key->duration;?>" <?php if($key->duration ==$duration){ echo 'selected'; } ?>><?php echo $key->duration; ?></option>
                                              <?php   } ?>
                                            </select>
                                        </div>
                                          
                                           </div>
                                         </div>


                                        <div class="col-md-4">
                                        <div class="row form-group">
                                          <div class="" id="course-duration">
                                             <select class="" name="modeofstudy" value=""  data-validation="required">
                                               <option>Mode of Study</option>
                                              <?php foreach ($modeofstudy as $key ){ ?>
                                                <option value="<?php echo $key->study_mode;?>" <?php if($key->study_mode ==$study_mode){ echo 'selected'; } ?>><?php echo $key->study_mode; ?></option>
                                              <?php   } ?>
                                            </select>
                                        </div>
                                          
                                           </div>
                                         </div>



                                         <div class="col-md-4 form-group">
                                         
                                              <input type="text" class="validate form-control" id="courseSpec" name="courseFee" value="<?php echo $courseFee;  ?>" placeholder="Enter total fee of course" data-validation="number"  data-validation-error-msg="Please enter course total fee" >
                                          <?php echo form_error('courseFee', '<div class="error">', '</div>'); ?>   
                                    </div>


                                   </div>
                                     
                                      <div class="row">
                                    <div class="col-md-12">
                                      <h3>Fee Structure</h3>
                                      <br>
                                      <textarea name="feeStructure"><?php echo html_entity_decode($feeStructure,ENT_QUOTES, "UTF-8"); ?></textarea>
                                    </div>
                                  </div>
                                   <br>

                                   <div class="row">
                                    <div class="col-md-12">
                                      <h3>Eligibility</h3>
                                      <br>
                                      <textarea name="courseEligibility"><?php echo html_entity_decode($courseEligibility,ENT_QUOTES, "UTF-8"); ?></textarea>
                                    </div>
                                  </div>
                                   <br>
                                   
                                   </div>

                                    <div class="row">
                                        <div class="input-field col s12 v2-mar-top-40">
                                          <input class="input-btn" type="submit" value="Save" >
                                        </div>
                                    </div>

                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--== BOTTOM FLOAT ICON ==-->
    <!-- <section>
      <div class="fixed-action-btn vertical">
        <a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
        <ul>
          <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
          <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
          <li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
          <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
        </ul>
      </div>
    </section> -->
   