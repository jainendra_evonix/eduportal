<!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
          <ul>
            <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> Update College</a> </li>
            <li class="page-back"><a href="<?php echo base_url() ?>admin/allcollege"><i class="fa fa-backward" aria-hidden="true"></i> View All</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>Update College</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="<?php echo base_url() ?>admin/addcollege">Add New</a> </li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url() ?>admin/allcollege"><i class="material-icons">subject</i>View All</a> </li>
            </ul>
            <!-- Dropdown Structure -->
            <div class="split-row">
              <div class="col-md-12">
                <div class="box-inn-sp ad-inn-page">
                  <div class="tab-inn ad-tab-inn">
                    <div class="hom-cre-acc-left hom-cre-acc-right">
                      <div class="">
                        <?php echo $this->session->flashdata('successmsg');  ?>
                        <?php echo $this->session->flashdata('errormsg');  ?>
                        <?php //print_r($getdata);  ?>
                        <form class="" name="addcollege" ng-submit="submitform()" novalidate>
                          <div class="row">
                            <div class="input-field col s12">
                              <?php echo form_dropdown('collegeuniversity',$fetchalluniversitydata,'', 'class="browser-default" ng-model="college.collegeuniversity" id="collegeuniversity" required'); ?>
                              <span ng-show="submitted && addcollege.collegeuniversity.$error.required"  class="help-block has-error ng-hide">Please select university.</span>
                              <span class="help-block has-error ng-hide" ng-show="collegeuniversityError">{{collegeuniversityError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <input type="text" class="validate" id="collegename" name="collegename" ng-model="college.collegename" maxlength="50" required>
                              <span ng-show="submitted && addcollege.collegename.$error.required"  class="help-block has-error ng-hide">Name is required.</span>
                              <span class="help-block has-error ng-hide" ng-show="collegenameError">{{collegenameError}}</span>
                              <label for="list_name">Name</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <input type="text" class="validate" id="collegeshortname" name="collegeshortname" ng-model="college.collegeshortname" maxlength="20" required>
                              <span ng-show="submitted && addcollege.collegeshortname.$error.required"  class="help-block has-error ng-hide">Short name is required.</span>
                              <span class="help-block has-error ng-hide" ng-show="collegeshortnameError">{{collegeshortnameError}}</span>
                              <label for="list_name">Short Name</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <input type="text" class="validate" id="collegeurl" name="collegeurl" ng-model="college.collegeurl" ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/" required>
                              <span ng-show="submitted && addcollege.collegeurl.$error.required"  class="help-block has-error ng-hide">URL is required.</span>
                              <span ng-show="submitted && addcollege.collegeurl.$error.pattern"  class="help-block has-error ng-hide">Please enter a valid url.</span>
                              <span class="help-block has-error ng-hide" ng-show="collegeurlError">{{collegeurlError}}</span>
                              <label for="list_phone">URL</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <textarea id="address" name="address" class="materialize-textarea" rows="4" cols="50" style="resize:none;" ng-model="college.address" required></textarea>
                              <span ng-show="submitted && addcollege.address.$error.required"  class="help-block has-error ng-hide">Address is required.</span>
                              <span class="help-block has-error ng-hide" ng-show="addressError">{{addressError}}</span>
                              <label for="email">Address</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select id="enabled" name="enabled" ng-model="college.enabled" class="browser-default" required>
                                <option value="" disabled selected>Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Non-Active</option>
                              </select>
                              <span ng-show="submitted && addcollege.enabled.$error.required"  class="help-block has-error ng-hide">Please select status.</span>
                              <span class="help-block has-error ng-hide" ng-show="enabledError">{{enabledError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12 v2-mar-top-40">
                              <input type="hidden" class="validate" id="hiddenid" name="hiddenid" ng-model="specialization.hiddenid">
                              <input class="input-btn" type="submit" value="Submit" ng-click="submitted=true">
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--== BOTTOM FLOAT ICON ==-->
  <!-- <section>
    <div class="fixed-action-btn vertical">
      <a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
      <ul>
        <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
        <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
        <li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
        <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
      </ul>
    </div>
  </section> -->