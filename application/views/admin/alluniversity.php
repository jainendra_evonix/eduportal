
<?php  if($this->session->flashdata('universitySaved')){   echo '<script> 
                   swal({
                        title: "University saved successfully!",
                       /* text: "Verify your email!",*/
                        icon: "success",
                        button: "Ok",
                      }); </script>';
  }

 if($this->session->flashdata('universityDelete')){   echo '<script> 
                   swal({
                        title: "Success!",
                        text: "University has been Deleted!",
                        icon: "success",
                        button: "Ok",
                      }); </script>';


                       }

if($this->session->flashdata('universityUpdated')){

    echo '<script> 
                   swal({
                        title: "Success!",
                        text: "University info has been updated successfully!",
                        icon: "success",
                        button: "Ok",
                      }); </script>';
  

}


                       ?>






<?php  // echo 'here'; exit; ?>
<!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <?php // $this->session->flashdata('universityDelete'); 


         ?>
        <?php // echo $this->session->flashdata('errormsg');  ?>

        <div class="sb2-2-2">
         
          <ul>
            <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> All University</a> </li>
            <li class="page-back"><a href="<?php echo base_url() ?>admin/adduniversity"><i class="fa fa-backward" aria-hidden="true"></i> Add New</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>All University</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="<?php echo base_url() ?>admin/adduniversity">Add New</a> </li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url() ?>admin/alluniversity"><i class="material-icons">subject</i>View All</a> </li>
            </ul>
            <!-- Dropdown Structure -->
            <div class="split-row">
              <div class="col-md-12">
                <div class="box-inn-sp ad-inn-page">
                  <div class="tab-inn ad-tab-inn">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered" datatable="ng" dt-options="vm.dtOptions">
                        <thead>
                          <tr>
                            <th>Listing</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Address</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="x in names" ng-cloak>
                            <td><span class="list-img" ng-cloak>{{ x.srno }}</span> </td>
                            <td><span class="list-enq-name" ng-cloak>{{ x.name }}</span><!-- <span class="list-enq-city">Illunois, United States</span> --></td>
                            <td ng-cloak>{{ x.day }} {{ x.month }} {{ x.year }}</td>
                            <td ng-cloak>{{ x.address }}<br><a class="atab-menu" ng-click="linkurl(x.url)" title="URL"><span class="list-enq-name">{{ x.url }}</span></a>
                            </td>
                            <td><span class="db-list-ststus" ng-if="x.enabled == 1">Active</span><span class="db-list-ststus-na" ng-if="x.enabled == 0">Non-Active</span></td>
                            <td> <a href="#." ng-click="edit(x.id)" title="Edit"><i class="fa fa-edit"></i></a> </td>
                            <td> <a href="#." ng-click="delete(x.id)" title="Delete"><i class="fa fa-times"></i></a> </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 