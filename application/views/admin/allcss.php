<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<html lang="en">

<head>
  <title>Studyatease - Admin Panel</title>
  <!-- META TAGS -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Free Web tutorials">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">
  <meta name="robots" content="noindex" />


  <!-- FAV ICON(BROWSER TAB ICON) -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/fav.ico" type="image/x-icon">
  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
  <!-- FONTAWESOME ICONS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link href="<?php echo base_url(); ?>assets/krajee/themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>
     <link href="<?php echo base_url(); ?>assets/krajee/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
  <!-- ALL CSS FILES -->
  <link href="<?php echo base_url(); ?>assets/css/materialize.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/jquery.steps.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/angular.min.js"></script>
  
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/js/sweetalert.min.js"></script>


  <script src="<?php echo base_url(); ?>assets/js/angular-datatables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/css/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
  <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=usyt2c5orpeupupr80falix22q7bn37pec4xw32kgsa6e50g"></script>


</head>