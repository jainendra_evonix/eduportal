<!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
          <ul>
            <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> Add College</a> </li>
            <li class="page-back"><a href="<?php echo base_url() ?>admin/allcollege"><i class="fa fa-backward" aria-hidden="true"></i> View All</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>Add New College</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="<?php echo base_url() ?>admin/addcollege">Add New</a> </li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url() ?>admin/allcollege"><i class="material-icons">subject</i>View All</a> </li>
            </ul>
            <!-- Dropdown Structure -->
            <div class="split-row">
              <div class="col-md-12">
                <div class="ad-inn-page">
                  <div class="tab-inn ad-tab-inn">
                    <div class="hom-cre-acc-left hom-cre-acc-right">
                      <div class="">
                        <?php echo $this->session->flashdata('successmsg');  ?>
                        <?php echo $this->session->flashdata('errormsg');  ?>
                        <div class="pg-elem">
                          <div class="pg-elem-inn ele-btn">
                            <div>
                              <ul class="tabs pg-ele-tab">
                                <li class="tab col s3"><a class="active" href="#basic">College Details</a> </li>
                                <li class="tab col s3"><a href="#course">Upload </a> </li>
                               <li class="tab col s3"><a href="#grade">Grade</a> </li>
                                <!-- <li class="tab col s3"><span>Accreditation</span> </li>
                                <li class="tab col s3"><span>Highlights</span> </li> -->
                              </ul>
                            </div>
                            <div id="basic" class="col s12">
                              <form class="" style="background:#ffffff;" name="addcollege" ng-submit="submitform()" novalidate>
                              		
                              		<div class="row form-group">
                               			<label class="col-md-3">Select University</label>
                               			<div class="col-md-9">
	                               			<select name="university" value="">
	                               				<option>Select universities</option>
                                        <?php foreach ($allUniversities as $key) { ?>

                                          <option value="<?php echo $key->id; ?>"><?php echo $key->name.' '.$key->shortname; ?></option>
                                         
                                    <?php     } ?>
	                               			</select>
	                               		</div>	
                              		</div>

                                <div class="row form-group">
                                      <label class="col-md-3">Full Name</label>
                                      <div class="col-md-9">
                                          <input type="text" name="" class="form-control">
                                      </div>
                                  </div>

                                  <div class="row form-group">
                                      <label class="col-md-3">Short Name</label>
                                      <div class="col-md-9">
                                          <input type="text" name="" class="form-control">
                                      </div>
                                  </div>

                                  <div class="row form-group">
                                      <label class="col-md-3">URL</label>
                                      <div class="col-md-9">
                                          <input type="text" name="" class="form-control">
                                      </div>
                                  </div>

                                <div class="row form-group">
                                      <label class="col-md-3">Address 1</label>
                                      <div class="col-md-9">
                                          <input type="text" name="" class="form-control">
                                      </div>
                                  </div>   

                                  <div class="row form-group">
                                      <label class="col-md-3">Address 2</label>
                                      <div class="col-md-9">
                                          <input type="text" name="" class="form-control">
                                      </div>
                                  </div>

                                  <div class="row form-group">
                                      <label class="col-md-3">Email Id</label>
                                      <div class="col-md-9">
                                          <input type="text" name="" class="form-control">
                                      </div>
                                  </div>

                                  <div class="row form-group">
                                      <label class="col-md-3">Phone No</label>
                                      <div class="col-md-9">
                                          <input type="text" name="" class="form-control">
                                      </div>
                                  </div>

                                  <div class="row form-group">
                                      <label class="col-md-3">Status</label>
                                      <div class="col-md-9">
                                          <select >
                                              <option value="1">Deactive</option>
                                              <option value="2">Active</option>
                                          </select>
                                      </div>
                                  </div>
                                <div class="row">
                                  <div class="input-field col s12 v2-mar-top-40">
                                    <input class="input-btn" type="submit" value="Submit" ng-click="submittedclg=true">
                                  </div>
                                </div>
                              </form>
                            </div>




                            <div id="course" class="col s12">
                                <form>
                                  <div class="row tz-file-upload form-group ">
                                      <label class="col-md-3">Logo Image</label>
                                    <div class="file-field input-field col-md-9 mt0">
                                      <div class="tz-up-btn"> <span>File</span>
                                        <input type="file"> </div>
                                      <div class="file-path-wrapper">
                                        <input class="file-path validate  form-control" type="text"> </div>
                                    </div>
                                  </div>

                                  <div class="row tz-file-upload form-group ">
                                      <label class="col-md-3">University Image</label>
                                    <div class="file-field input-field col-md-9 mt0">
                                      <div class="tz-up-btn"> <span>File</span>
                                        <input type="file"> </div>
                                      <div class="file-path-wrapper">
                                        <input class="file-path validate form-control" type="text"> </div>
                                    </div>
                                  </div>


                                  <div class="row">
                                      <div class="input-field col s12 v2-mar-top-40">
                                        <input class="input-btn" type="submit" value="Save" ng-click="submittedclg=true">
                                      </div>
                                  </div>

                              </form>    
                            </div>
                            <!-- upload Images -->


                            <!-- Grade -->
                             <div id="grade" class="col s12">
                                <form>
                              
                                  <div class="row form-group">
                                      <label class="col-md-3">Select Grade</label>
                                      <div class="col-md-8">
                                          <select>
                                          		<option>NAAC</option>
                                          		<option>NAAC 1</option>
                                          		<option>NAAC 2</option>
                                          </select>
                                      </div>
                                      <div class="col-md-1">
                                           <input class="input-btn" type="submit" value="+" ng-click="submittedclg=true">
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="input-field col s12 v2-mar-top-40">
                                        <input class="input-btn" type="submit" value="Save" ng-click="submittedclg=true">
                                      </div>
                                  </div>

                              </form>   
                             </div>
                            <!-- Grade -->



                           
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 