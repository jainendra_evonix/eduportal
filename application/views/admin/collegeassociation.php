<!-- <script src="<?php echo base_url(); ?>assets/js/angular.min.js"></script> -->
<body ng-app="postApp" ng-controller="postController">
  <!--== BODY CONTNAINER ==-->
  <div class="container-fluid sb2">
    <div class="row"><!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
          <ul>
            <li><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> Add Course</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>Add New Course</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="#!">Add New</a> </li>
              <li><a href="#!">Edit</a> </li>
              <li><a href="#!">Update</a> </li>
              <li class="divider"></li>
              <li><a href="#!"><i class="material-icons">delete</i>Delete</a> </li>
              <li><a href="#!"><i class="material-icons">subject</i>View All</a> </li>
              <li><a href="#!"><i class="material-icons">play_for_work</i>Download</a> </li>
            </ul>
            <!-- Dropdown Structure -->
            <div class="split-row">
              <div class="col-md-12">
                <div class="box-inn-sp ad-inn-page">
                  <div class="tab-inn ad-tab-inn">
                    <div class="hom-cre-acc-left hom-cre-acc-right">
                      <div class="">
                                                                        <form class="" name="addca" ng-submit="submitform()" novalidate>
                          <div class="row">
                            <div class="input-field col s12">
                              <select name="cauniversity" class="browser-default" ng-model="ca.cauniversity" id="cauniversity" required>
<option value="" selected="selected">Select University</option>
<option value="4">Amity University</option>
<option value="3">Symbiosis International University</option>
<option value="1">Bharati Vidyapeeth University</option>
</select>
                              <span ng-show="submitted && addca.cauniversity.$error.required"  class="help-block has-error ng-hide">Please select university.</span>
                              <span class="help-block has-error ng-hide" ng-show="cauniversityError">{{cauniversityError}}</span>
                            </div>
                          </div>
                          
                          <div class="row">
                            <div class="input-field col s12">
                              <select name="cacollege" class="browser-default" ng-model="ca.cacollege" id="cacollege" required>
<option value="" selected="selected">Select College</option>
<option value="3">Amity University, Jaipur</option>
<option value="2">Symbiosis Centre of Health Care</option>
<option value="1">Symbiosis Centre for Distance Learning</option>
</select>
                              <span ng-show="submitted && addca.cacollege.$error.required"  class="help-block has-error ng-hide">Please select college.</span>
                              <span class="help-block has-error ng-hide" ng-show="cacollegeError">{{cacollegeError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select name="cacourse" class="browser-default" ng-model="ca.cacourse" id="cacourse" required>
<option value="" selected="selected">Select Course</option>
<option value="4">Post Graduate Diploma in International Business</option>
<option value="3">Post Graduate Diploma in Business Administration</option>
<option value="2">Master of Arts</option>
<option value="1">Bachelor of Arts</option>
</select>
                              <span ng-show="submitted && addca.cacourse.$error.required"  class="help-block has-error ng-hide">Please select course.</span>
                              <span class="help-block has-error ng-hide" ng-show="cacourseError">{{cacourseError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select name="caspec" class="browser-default" ng-model="ca.caspec" id="caspec" required>
<option value="" selected="selected">Select Specialization</option>
<option value="6">Journalism and Mass Communication</option>
<option value="5">English</option>
<option value="4">Operations</option>
<option value="3">Human Resource</option>
<option value="2">Marketing</option>
<option value="1">Finance</option>
</select>
                              <span ng-show="submitted && addca.caspec.$error.required"  class="help-block has-error ng-hide">Please select specialization.</span>
                              <span class="help-block has-error ng-hide" ng-show="caspecError">{{caspecError}}</span>
                            </div>
                          </div>
                          
                                                    <!-- <div class="row">
                            <div class="input-field col s12">
                              <div class="checkheading">
                                <h5 class="checkname">Select Specialization</h5>
                              </div>
                              <div class="checkboxlabel">
                                                                <div>
                                <label for="spec_
<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: Notice</p>
<p>Message:  Undefined variable: v</p>
<p>Filename: admin/courseassociation.php</p>
<p>Line Number: 73</p>


  <p>Backtrace:</p>
  
    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/application/views/admin/courseassociation.php<br />
      Line: 73<br />
      Function: _error_handler      </p>

    
  
    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/application/controllers/Admin.php<br />
      Line: 965<br />
      Function: view      </p>

    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/index.php<br />
      Line: 315<br />
      Function: require_once      </p>

    
  

</div>
<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: Notice</p>
<p>Message:  Trying to get property of non-object</p>
<p>Filename: admin/courseassociation.php</p>
<p>Line Number: 73</p>


  <p>Backtrace:</p>
  
    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/application/views/admin/courseassociation.php<br />
      Line: 73<br />
      Function: _error_handler      </p>

    
  
    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/application/controllers/Admin.php<br />
      Line: 965<br />
      Function: view      </p>

    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/index.php<br />
      Line: 315<br />
      Function: require_once      </p>

    
  

</div>">
<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: Notice</p>
<p>Message:  Undefined variable: v</p>
<p>Filename: admin/courseassociation.php</p>
<p>Line Number: 73</p>


  <p>Backtrace:</p>
  
    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/application/views/admin/courseassociation.php<br />
      Line: 73<br />
      Function: _error_handler      </p>

    
  
    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/application/controllers/Admin.php<br />
      Line: 965<br />
      Function: view      </p>

    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/index.php<br />
      Line: 315<br />
      Function: require_once      </p>

    
  

</div>
<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: Notice</p>
<p>Message:  Trying to get property of non-object</p>
<p>Filename: admin/courseassociation.php</p>
<p>Line Number: 73</p>


  <p>Backtrace:</p>
  
    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/application/views/admin/courseassociation.php<br />
      Line: 73<br />
      Function: _error_handler      </p>

    
  
    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/application/controllers/Admin.php<br />
      Line: 965<br />
      Function: view      </p>

    
  
    
  
    
      <p style="margin-left:10px">
      File: /opt/lampp/htdocs/eduportal/index.php<br />
      Line: 315<br />
      Function: require_once      </p>

    
  

</div>&nbsp;&nbsp;</label>
                                </div>
                                                              </div>
                              <span ng-show="submitted && addca.caspec.$error.required"  class="help-block has-error ng-hide">Please select specialization.</span>
                              <span class="help-block has-error ng-hide" ng-show="caspecError">{{caspecError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                                <div class="checkheading">
                                  <h5 class="checkname">Select Specialization</h5>
                                </div>
                              <div class="checkboxlabel">
                                <div>
                                  <input type="checkbox" checklist-model="ca.chkweek" checklist-value="v1" class="filled-in" id="c1" />
                                  <label for="c1">English</label>
                                </div>&nbsp;&nbsp;
                                <div>
                                  <input type="checkbox" checklist-model="ca.chkweek" checklist-value="v2" class="filled-in" id="c2" />
                                  <label for="c2">French</label>
                                </div>
                              </div>
                            </div>
                          </div> -->

                          <div class="row">
                            <div class="input-field col s12">
                              <select name="cacredential" class="browser-default" ng-model="ca.cacredential" id="cacredential" required>
<option value="" selected="selected">Select Credential</option>
<option value="2">Degree</option>
<option value="1">Diploma</option>
</select>
                              <span ng-show="submitted && addca.cacredential.$error.required"  class="help-block has-error ng-hide">Please select credential.</span>
                              <span class="help-block has-error ng-hide" ng-show="cacredentialError">{{cacredentialError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select name="cadegree" class="browser-default" ng-model="ca.cadegree" id="cadegree" required>
<option value="" selected="selected">Select Degree</option>
<option value="2">PG</option>
<option value="1">UG</option>
</select>
                              <span ng-show="submitted && addca.cadegree.$error.required"  class="help-block has-error ng-hide">Please select degree.</span>
                              <span class="help-block has-error ng-hide" ng-show="cadegreeError">{{cadegreeError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select name="camode" class="browser-default" ng-model="ca.camode" id="camode" required>
<option value="" selected="selected">Select Mode of Study</option>
<option value="2">Full Time</option>
<option value="1">Part Time</option>
</select>
                              <span ng-show="submitted && addca.camode.$error.required"  class="help-block has-error ng-hide">Please select mode of study.</span>
                              <span class="help-block has-error ng-hide" ng-show="camodeError">{{camodeError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select class="browser-default" name="caduration" ng-model="ca.caduration" id="caduration" required>
                                <option value="">Select Duration in Years</option>
                                <option value="0.6">0.6</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                              </select>
                              <span ng-show="submitted && addca.caduration.$error.required"  class="help-block has-error ng-hide">Please select duration.</span>
                              <span class="help-block has-error ng-hide" ng-show="cadurationError">{{cadurationError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select name="camedium" class="browser-default" ng-model="ca.camedium" id="camedium" required>
<option value="" selected="selected">Select Medium</option>
<option value="2">Hindi</option>
<option value="1">English</option>
</select>
                              <span ng-show="submitted && addca.camedium.$error.required"  class="help-block has-error ng-hide">Please select medium.</span>
                              <span class="help-block has-error ng-hide" ng-show="camediumError">{{camediumError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select name="carecognition" class="browser-default" ng-model="ca.carecognition" id="carecognition" required>
<option value="" selected="selected">Select Recognition</option>
<option value="2">AICTE</option>
<option value="1">UGC</option>
</select>
                              <span ng-show="submitted && addca.carecognition.$error.required"  class="help-block has-error ng-hide">Please select recognition.</span>
                              <span class="help-block has-error ng-hide" ng-show="carecognitionError">{{carecognitionError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select name="cacoursestatus" class="browser-default" ng-model="ca.cacoursestatus" id="cacoursestatus" required>
<option value="" selected="selected">Select Course Status</option>
<option value="2">Affiliated to Central University</option>
<option value="1">Affiliated to Deemed University</option>
</select>
                              <span ng-show="submitted && addca.cacoursestatus.$error.required"  class="help-block has-error ng-hide">Please select course status.</span>
                              <span class="help-block has-error ng-hide" ng-show="cacoursestatusError">{{cacoursestatusError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select name="caaccreditation" class="browser-default" ng-model="ca.caaccreditation" id="caaccreditation" required>
<option value="" selected="selected">Select Accreditation</option>
<option value="2">NAAC Grade A+</option>
<option value="1">NAAC Grade A</option>
</select>
                              <span ng-show="submitted && addca.caaccreditation.$error.required"  class="help-block has-error ng-hide">Please select accreditation.</span>
                              <span class="help-block has-error ng-hide" ng-show="caaccreditationError">{{caaccreditationError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select name="caownership" class="browser-default" ng-model="ca.caownership" id="caownership" required>
<option value="" selected="selected">Select Ownership</option>
<option value="2">Public</option>
<option value="1">Private</option>
</select>
                              <span ng-show="submitted && addca.caownership.$error.required"  class="help-block has-error ng-hide">Please select ownership.</span>
                              <span class="help-block has-error ng-hide" ng-show="caownershipError">{{caownershipError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <input type="text" class="validate" id="cafees" name="cafees" ng-model="ca.cafees" ng-pattern="/^[0-9]*$/" required>
                              <span ng-show="submitted && addca.cafees.$error.required"  class="help-block has-error ng-hide">Fees is required.</span>
                              <span ng-show="submitted && addca.cafees.$error.pattern"  class="help-block has-error ng-hide">Please enter a valid number.</span>
                              <span class="help-block has-error ng-hide" ng-show="cafeesError">{{cafeesError}}</span>
                              <label for="list_name">Total Fees</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <textarea id="eligibility" name="eligibility" class="materialize-textarea" rows="4" cols="50" style="resize:none;" ng-model="ca.eligibility" required></textarea>
                              <span ng-show="submitted && addca.eligibility.$error.required"  class="help-block has-error ng-hide">Eligibility is required.</span>
                              <span class="help-block has-error ng-hide" ng-show="eligibilityError">{{eligibilityError}}</span>
                              <label for="email">Eligibility</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <textarea id="coursestructure" name="coursestructure" class="materialize-textarea" rows="4" cols="50" style="resize:none;" ng-model="ca.coursestructure" required></textarea>
                              <span ng-show="submitted && addca.coursestructure.$error.required"  class="help-block has-error ng-hide">Course structure is required.</span>
                              <span class="help-block has-error ng-hide" ng-show="coursestructureError">{{coursestructureError}}</span>
                              <label for="email">Course Structure</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <select id="enabled" name="enabled" ng-model="ca.enabled" class="browser-default" required>
                                <option value="" disabled selected>Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Non-Active</option>
                              </select>
                              <span ng-show="submitted && addca.enabled.$error.required"  class="help-block has-error ng-hide">Please select status.</span>
                              <span class="help-block has-error ng-hide" ng-show="enabledError">{{enabledError}}</span>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12 v2-mar-top-40">
                              <input class="input-btn" type="submit" value="Submit" ng-click="submitted=true">
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--== BOTTOM FLOAT ICON ==-->
  <script src="http://127.0.0.1/eduportal/assets/js/jquery.min.js"></script>
  <script src="http://127.0.0.1/eduportal/assets/js/bootstrap.js" type="text/javascript"></script>
  <script src="http://127.0.0.1/eduportal/assets/js/materialize.min.js" type="text/javascript"></script>
  <script src="http://127.0.0.1/eduportal/assets/js/custom.js"></script>
</body>

  <script>
    // Defining angularjs application.
    var postApp = angular.module('postApp', []);

    // Controller function and passing $http service and $scope var.
    //postApp.controller('postController', ['$scope', 'FileUploader', function ($scope, $http, FileUploader) {
    //postApp.controller('postController', ['$scope', 'FileUploader', function($scope, FileUploader, $http) {
        //postApp.controller('postController', ['$scope', 'FileUploader', '$http', function ($scope, FileUploader, $http) {
            postApp.controller('postController', function ($scope, $http) {
    // create a blank object to handle form data.
    $scope.ca = {};
            // calling our submit function.
            $scope.submitform = function () {
                //alert('submitform');

            // Posting data to php file
            if ($scope.addca.$valid) {
                //alert('if');
       //alert('login');
                    $http({
                    method: 'POST',
                            dataType: 'json',
                            url: 'http://127.0.0.1/eduportal/admin/submitcourseassociation',
                            data: $scope.ca, //forms user object
                            headers: {'Content-Type': 'application/json'}
                    }).success(function (data) {
                        
                        
                   //console.log(data);
                        //alert(data.status);
            //console.log(data);
                    // console.log(data.error.password.error);
                    if (data.status == 1) {
                        $('.help-block').css('display','none');
                        /*setTimeout(function(){
                            //$('#message2').fadeOut();
                            window.location = 'http://127.0.0.1/eduportal/admin/courseassociation';
                        }, 5000);*/
                        window.location = 'http://127.0.0.1/eduportal/admin/courseassociation';
            } else if (data.status == 0)
            {
                    window.location = 'http://127.0.0.1/eduportal/admin/courseassociation'; 
                    
            } else
            {
                    window.location = 'http://127.0.0.1/eduportal/admin/courseassociation';
             }
            
            });
            }
            };




    //}]);
    });

    $("#courseassociation").addClass("menu-active");
       
</script>