<!-- <div class="modal fade" tabindex="-1" id="myModal" role="dialog"> -->
<link href="<?php echo base_url(); ?>assets/css/materialize.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
  <!-- <script src="<?php echo base_url(); ?>assets/js/angular.min.js"></script> -->
  <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
  <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
<div ng-app="postApp" ng-controller="postCtrl">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Course Association</h4>
      </div>
      <div class="modal-body">
        <div class="split-row">
          <div class="col-md-12">
          <div class="box-inn-sp ad-inn-page" style="margin:15px -25px 0px -25px;">
          <div class="tab-inn ad-tab-inn">
          <div class="hom-cre-acc-left hom-cre-acc-right">
          <div class="">
        <?php
        error_reporting(0);
        //echo "<pre>"; print_r($info);
        //echo $info->totalfees;
        ?>
        <form id="editform" class="" style="background:#ffffff;" name="addcae" method="post" action="<?php echo base_url();?>admin/updatecourseassociation" novalidate>
          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caeuniversity',$fetchalluniversitydata,$info->universityid, 'class="browser-default" id="caeuniversity" onchange="selecteditcollege()" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <!-- <select name="caecollege" class="browser-default" id="caecollege" required>
                <option value="">Select College</option>
              </select> -->
              <?php echo form_dropdown('caecollege',$fetchallcollegedata,$info->collegeid, 'class="browser-default" id="caecollege" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caecourse',$fetchallcoursesdata,$info->courseid, 'class="browser-default" id="caecourse" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caespec',$fetchallspecdata,$info->specializationid, 'class="browser-default" id="caespec" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caecredential',$fetchallcredentialdata,$info->credentialid, 'class="browser-default" id="caecredential" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caedegree',$fetchalldegreedata,$info->degreeid, 'class="browser-default" id="caedegree" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caemode',$fetchallmodedata,$info->modeofstudyid, 'class="browser-default" id="caemode" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <select class="browser-default" name="caeduration" id="caeduration" required>
                <option value="">Select Duration in Years</option>
                <option <?php if($info->duration==0.6) echo 'selected'; ?> value="0.6">0.6</option>
                <option <?php if($info->duration==1) echo 'selected'; ?> value="1">1</option>
                <option <?php if($info->duration==2) echo 'selected'; ?> value="2">2</option>
                <option <?php if($info->duration==3) echo 'selected'; ?> value="3">3</option>
                <option <?php if($info->duration==4) echo 'selected'; ?> value="4">4</option>
                <option <?php if($info->duration==5) echo 'selected'; ?> value="5">5</option>
                <option <?php if($info->duration==3) echo 'selected'; ?> value="6">6</option>
              </select>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caemedium',$fetchallmediumdata,$info->mediumid, 'class="browser-default" id="caemedium" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caerecognition',$fetchallrecognitiondata,$info->recognitionid, 'class="browser-default" id="caerecognition" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caecoursestatus',$fetchallcoursestatusdata,$info->coursestatusid, 'class="browser-default" id="caecoursestatus" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caeaccreditation',$fetchallaccreditationdata,$info->accreditationid, 'class="browser-default" id="caeaccreditation" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <?php echo form_dropdown('caeownership',$fetchallownershipdata,$info->ownershipid, 'class="browser-default" id="caeownership" required'); ?>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <input type="text" class="validate" id="caefees" name="caefees" value="<?php echo $info->totalfees; ?>" required>
              <label for="caefees" class="active">Total Fees</label>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <textarea id="eligibility" name="eligibility" class="materialize-textarea" rows="4" cols="50" style="resize:none;" required><?php echo $info->eligibility; ?></textarea>
              <label for="eligibility" class="active">Eligibility</label>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <textarea id="coursestructure" name="coursestructure" class="materialize-textarea" rows="4" cols="50" style="resize:none;" required><?php echo $info->coursestructure; ?></textarea>
              <label for="coursestructure" class="active">Course Structure</label>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <select class="browser-default" name="enabled" id="enabled" required>
                <option value="">Select Status</option>
                <option <?php if($info->enabled==1) echo 'selected'; ?> value="1">Active</option>
                <option <?php if($info->enabled==0) echo 'selected'; ?> value="0">Non-Active</option>
              </select>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12 v2-mar-top-40">
              <input type="hidden" class="validate" id="hiddenid" name="hiddenid" value="<?php echo $info->course_association_id; ?>">
              <input class="input-btn" type="submit" value="Submit">
            </div>
          </div>
        </form>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>

      </div>
    </div>
  </div>
</div>

<script>
    // Defining angularjs application.
    var postApp = angular.module('postApp');

    // Controller function and passing $http service and $scope var.
    //myApp.controller('postCtrl', ['$scope', 'FileUploader', function ($scope, $http, FileUploader) {
    //myApp.controller('postCtrl', ['$scope', 'FileUploader', function($scope, FileUploader, $http) {
        //myApp.controller('postCtrl', ['$scope', 'FileUploader', '$http', function ($scope, FileUploader, $http) {
            postApp.controller('postCtrl', function ($scope, $http) {
    // create a blank object to handle form data.

    //cae
    $scope.cae = {};
            // calling our submit function.
            $scope.submitformeditcourse = function () {
                //alert('submitformeditcourse');

            // Posting data to php file
            if ($scope.addcae.$valid) {
                //alert('if');
                //alert('login');
                    $http({
                    method: 'POST',
                            dataType: 'json',
                            url: '<?php echo base_url() ?>admin/submitcourseassociation',
                            data: $scope.cae, //forms user object
                            headers: {'Content-Type': 'application/json'}
                    }).success(function (data) {
                        
                        
                    //console.log(data);
                    //alert(data.status);
                    //console.log(data);
                    // console.log(data.error.password.error);
                    if (data.status == 1) {
                        $('.help-block').css('display','none');
                        /*setTimeout(function(){
                            //$('#message2').fadeOut();
                            window.location = '<?php echo base_url();?>admin/addcollege';
                        }, 5000);*/
                        window.location = '<?php echo base_url();?>admin/addcollege';
                    } else if (data.status == 0)
                    {
                            window.location = '<?php echo base_url();?>admin/addcollege'; 
                            
                    } else
                    {
                            window.location = '<?php echo base_url();?>admin/addcollege';
                     }
                    
                    });
                    } else {
                      //alert('else');
                    }
                    };
    //cae

    //}]);

    });       
</script>

<!-- for validation -->
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>

<script type="text/javascript">
    $(function()
    {
      $("#editform").validate(
      {         
        // Rules for form validation
        rules:
        {
          
          "caeuniversity":
          {
            required: true,
          },
          "caecollege":
          {
            required: true,
          },
          "caecourse":
          {
            required: true,
          },
          "caespec":
          {
            required: true,
          },
          "caecredential":
          {
            required: true,
          },
          "caedegree":
          {
            required: true,
          },
          "caemode":
          {
            required: true,
          },
          "caeduration":
          {
            required: true,
          },
          "caemedium":
          {
            required: true,
          },
          "caerecognition":
          {
            required: true,
          },
          "caecoursestatus":
          {
            required: true,
          },
          "caeaccreditation":
          {
            required: true,
          },
          "caeownership":
          {
            required: true,
          },
          "caefees":
          {
            required: true,
            number: true,
          },
          "eligibility":
          {
            required: true,
          },
          "coursestructure":
          {
            required: true,
          },
          "enabled":
          {
            required: true,
          }
        },
                  
        // Messages for form validation
        messages:
        {
          "caeuniversity":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select university.</span>',
            
          },
          "caecollege":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select college.</span>',
            
          },
          "caecourse":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select course.</span>',
            
          },
          "caespec":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select specialization.</span>',
            
          },
          "caecredential":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select credential.</span>',
            
          },
          "caedegree":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select degree.</span>',
            
          },
          "caemode":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select mode of study.</span>',
            
          },
          "caeduration":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select duration.</span>',
            
          },
          "caemedium":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select medium.</span>',
            
          },
          "caerecognition":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select recognition.</span>',
            
          },
          "caecoursestatus":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select course status.</span>',
            
          },
          "caeaccreditation":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select accreditation.</span>',
            
          },
          "caeownership":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select ownership.</span>',
            
          },
          "caefees":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Fees is required.</span>',
            number: '<span style="color:#FF523F;font-style:normal;">Please enter a valid number.</span>',
            
          },
          "eligibility":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Eligibility is required.</span>',
            
          },
          "coursestructure":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Course structure is required.</span>',
            
          },
          "enabled":
          {
            required: '<span style="color:#FF523F;font-style:normal;">Please select status.</span>',
            
          }
        },          
        
        // Do not change code below
        errorPlacement: function(error, element)
        {
          //error.insertAfter(element.parent());
          if ($(element).attr("type") === "checkbox") {
            $("#checkBoxErrorHolder").html(error);
          } else {
            error.insertAfter(element.parent());
          }
          
        }
        
        
      });

    });
</script>

<script type="text/javascript">
function selecteditcollege()
    {
       //alert(2);
       var collegeuniversity=$('#caeuniversity').val();
       //alert(collegeuniversity);
            
            $.post('<?php echo base_url();?>admin/getuniversitycollege/',
        {
            collegeuniversity:collegeuniversity
            
            },
            function(data) 
            {
            
            $('#caecollege').html(data);
            }); 
    }
</script>