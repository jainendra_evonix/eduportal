<script>
    // Defining angularjs application.
    var postApp = angular.module('postApp', []);

   
            postApp.controller('postController', function ($scope, $http) {
    // create a blank object to handle form data.
    $scope.university = {};
            // calling our submit function.
            $scope.submitform = function () {
              

            if ($scope.adduniversity.$valid) {
                
                    $http({
                    method: 'POST',
                            dataType: 'json',
                            url: '<?php echo base_url() ?>admin/submituniversity',
                            data: $scope.university, //forms user object
                            headers: {'Content-Type': 'application/json'}
                    }).success(function (data) {
                        
                   
                    if (data.status == 1) {
                        $('.help-block').css('display','none');
                       
                        window.location = '<?php echo base_url();?>admin/adduniversity';
            } else if (data.status == 0)
            {
                    window.location = '<?php echo base_url();?>admin/adduniversity'; 
                    
            } else
            {
                    window.location = '<?php echo base_url();?>admin/adduniversity';
             }
            
            });
            }
            };




    //}]);
    });

    $("#college").addClass("menu-active");

       
</script>
<script>
  
   $(document).ready(function(){

   

     $.validate();

     


});


function uploadBannerDept()
{
         
         var collegeName = $('#departmentName').val();
         var type = 'college';


              if(collegeName){
           
            var data = new FormData();
              
           jQuery.each($('#bannerDept')[0].files, function(i, file) {
           // console.log(i)
            data.append('banner-'+i, file);
            data.append('collegeName',collegeName);
            
        });
           jQuery.ajax({
                        url: '<?php echo base_url();?>admin/uploadCollegeBanner',
                        data: data,collegeName,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        dataType:'json',
                       
                        success: function(data){
                         
                              if(data.status==1)
                              {
                                
                                swal("Oops!", "width and height should be 274*130 pixel", "error");
                             }
                             if(data.status==2)
                             {
                                 
                                 
                                 $('#bannerNameDept').val('');
                                 $('#bannerNameDept').val(data.fileName);
                                 
                                 swal({
                                title: "Banner image uploaded!",
                               /* text: "Verify your email!",*/
                                icon: "success",
                                button: "Ok",
                              });
                             }
                       }
           });
   }else{

        swal("Oops!", "Please enter department name first", "error");

                  return false;
   }   
}


function uploadBrochureDept()
{
         
         var collegeName = $('#departmentName').val();
         var type = 'college';


              if(collegeName){
           
            var data = new FormData();
              
           jQuery.each($('#brochureDept')[0].files, function(i, file) {
           // console.log(i)
            data.append('brochure-'+i, file);
            data.append('collegeName',collegeName);
            
        });
           jQuery.ajax({
                        url: '<?php echo base_url();?>admin/uploadCollegeBrochure',
                        data: data,collegeName,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        dataType:'json',
                       
                        success: function(data){
                         
                              if(data.status==1)
                              {
                                
                                swal("Oops!", "width and height should be 274*130 pixel", "error");
                             }
                             if(data.status==2)
                             {
                                 
                                 
                                 $('#brochurenameDept').val('');
                                 $('#brochurenameDept').val(data.fileName);
                                 
                                 swal({
                                title: "Brochure  uploaded!",
                               /* text: "Verify your email!",*/
                                icon: "success",
                                button: "Ok",
                              });
                             }
                       }
           });
   }else{

        swal("Oops!", "Please enter department name first", "error");

                  return false;
   }   
}



function getCities()
{

    // alert('here')
      
      var stateName = $('#stateName').val();

    //  alert(stateName);

       $.ajax({
            
            url:'<?php echo base_url(); ?>admin/getStateCities',
            data:{stateName:stateName},
            dataType:'json',
            method:'POST',
            success:function(data){

                console.log(data.cities)

                      $('#citiesOptions').html('');
                      $('#citiesOptions').html(data.cities);
                      $('#selectCities').show();

            }

       });


}



function uploadGalleryImages()
{

   
   
         var collegeName = $('#collegeName').val();



              if(collegeName){
           
            var data = new FormData();
              
           jQuery.each($('#gallery')[0].files, function(i, file) {
            console.log(i)
            data.append('gallery-'+i, file);
            data.append('collegeName',collegeName);
        });
           jQuery.ajax({
                        url: '<?php echo base_url();?>admin/uploadCollegeGalleryImages',
                        data: data,collegeName,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        dataType:'json',
                       
                        success: function(data){


                              if(data.error)
                              {
                                
                                swal("Oops!", "width and height should be 750*500 pixel", "error");
                              }
                             if(data.success)
                             {
                                 
                                 swal({
                                title: "Gallery images uploaded!",
                               /* text: "Verify your email!",*/
                                icon: "success",
                                button: "Ok",
                              });
                             }
                       }
           });
   }else{

        swal("Oops!", "Please enter college name first", "error");

                  return false;
   }   
}



tinymce.init({
  selector: 'textarea',
  height: 350,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });

</script>