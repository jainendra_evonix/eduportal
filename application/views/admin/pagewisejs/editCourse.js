<script>
angular.module('postApp', ['postApp.controllers','datatables']);
  
  angular.module('postApp.controllers', []).controller('postController', function($scope,$http,DTOptionsBuilder, DTColumnBuilder) {

    

     });

</script>

<script>

function getAffiliatedColleges()
{

 var universityId = $('#universityId').val();

  // alert(universityId);

     $.ajax({

          url: '<?php echo base_url(); ?>admin/getCollegeByUniversityID',
          method: 'POST',
          data:{universityId:universityId},
          dataType:'json',
          success:function(data)
          {

            $('#colleges').last().html();
            $('#colleges').last().html(data.colleges);
            $('#colllesOptions').show();
            


          }





    });
}
</script>

<script>

/*ajax for get streams*/
function getStream(){

     $.ajax({

        url:'<?php echo base_url(); ?>admin/getCoursesName',
        dataType:'json',
        method:'post',
        success:function(data){
              
              $('.courseOptions').last().html('');
              $('.courseOptions').last().html(data.courses);
              $('#course').show();
        }


     });
}





$(document).ready(function() {

  $.validate();





    var max_fields      = 10; //maximum input boxes allowed
  //  alert(max_fields)
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    


    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click

        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
         
          $('<div class="row"><div class="col-md-3"><div class="row form-group"><div class="" id="course-cust"><div class="courseOptions"></div></div></div></div><div class="col-md-4 form-group"><input type="text" class="validate form-control" id="courseSpec" name="courseSpec[]" value="" placeholder="Enter course specialization name" data-validation="required"  data-validation-error-msg="Please enter course specialization" ></div><div class="col-md-4 form-group"><input type="text" class="validate form-control" id="courseShortName" name="courseShortName[]" value="" placeholder="Enter course name" data-validation="required"  data-validation-error-msg="Please enter course short name" ></div><div class="col-md-1 "><a href="#" class="remove_field "><i class="fa fa-trash"></i></a></div></div><div class="clearfix"></div>').appendTo(wrapper); //add input box

           
          



            
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        
       e.preventDefault(); $(this).parent().parent().remove(); x--;
    })


});


tinymce.init({
  selector: 'textarea',
  height: 300,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });

</script>
