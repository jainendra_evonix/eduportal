<script>
//alert('here')
angular.module('postApp', ['postApp.controllers','datatables']);
  
  angular.module('postApp.controllers', []).controller('postController', function($scope,$http,DTOptionsBuilder, DTColumnBuilder) {

      $http.get("<?php echo base_url() ?>admin/getPrograms").success(function(response){
      console.log(response);
      $scope.names = response;  //ajax request to fetch data into vm.data
    });
      
    $scope.vm = {};

    $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
      .withOption('order', [0, 'asc']);

      $scope.linkurl = function(linkurlvalue){
          //alert(linkurlvalue);
          //window.location = linkurlvalue;
          window.open(linkurlvalue, '_blank');
        }

      $scope.edit = function(editingId){
          //alert(editingId);
          window.location = '<?php echo base_url() ?>admin/editProgram/'+editingId;
        }

        $scope.delete = function(deletingId){
  //alert(deletingId);
  if (confirm("Are you sure you want to delete?")) {
    $http({
       method: 'POST',
       url: '<?php echo base_url() ?>admin/deleteProgram',
      
       data: $.param({deletingId:deletingId}),
       headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function(data){
        //alert("deleted"+ deletingId);
        location.reload();
         })
    }
}

     });

  $("#programs").addClass("menu-active");

</script>