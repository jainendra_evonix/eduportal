<script>
    // Defining angularjs application.
    var postApp = angular.module('postApp', []);

   
            postApp.controller('postController', function ($scope, $http) {
    // create a blank object to handle form data.
    $scope.university = {};
            // calling our submit function.
            $scope.submitform = function () {
              

            if ($scope.adduniversity.$valid) {
                
                    $http({
                    method: 'POST',
                            dataType: 'json',
                            url: '<?php echo base_url() ?>admin/submituniversity',
                            data: $scope.university, //forms user object
                            headers: {'Content-Type': 'application/json'}
                    }).success(function (data) {
                        
                   
                    if (data.status == 1) {
                        $('.help-block').css('display','none');
                       
                        window.location = '<?php echo base_url();?>admin/adduniversity';
            } else if (data.status == 0)
            {
                    window.location = '<?php echo base_url();?>admin/adduniversity'; 
                    
            } else
            {
                    window.location = '<?php echo base_url();?>admin/adduniversity';
             }
            
            });
            }
            };




    //}]);
    });

    $("#university").addClass("menu-active");

       
</script>
<script type="text/javascript">
  
   $(document).ready(function(){

     $.validate();
   
      

   });


   function uploadLogo()
   {

              var universityName = $('#universityname').val();
              // alert(universityName);

              if(universityName){

                var data = new FormData();
                  
               jQuery.each($('#logo')[0].files, function(i, file) {
                console.log(i)
                data.append('logo-'+i, file);
                data.append('universityname',universityName);
            });
               

    
       

         jQuery.ajax({
                url: '<?php echo base_url();?>admin/uploadLogo',
                data: data,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                dataType:'json',
               
                success: function(data){
                 
                      if(data.status==1)
                      {
                        
                        swal("Oops!", "width and height should be 120*120 pixel", "error");
                     }
                     if(data.status==2)
                     {
                         $('#logoname').val('');
                         $('#logoname').val(data.fileName);
                         
                         swal({
                        title: "Logo uploaded!",
                       /* text: "Verify your email!",*/
                        icon: "success",
                        button: "Ok",
                      });
                     }
                  }
                    
            });
        }else{

               
               swal("Oops!", "Please enter university name first", "error");

                  return false;

        }     
}
   



   function uploadBrochure()
   {

              var universityName = $('#universityname').val();
              //alert(universityName);

              if(universityName){

                var data = new FormData();
                  
               jQuery.each($('#brochure')[0].files, function(i, file) {
                console.log(i)
                data.append('brochure-'+i, file);
                data.append('universityname',universityName);
            });
               

    
       

         jQuery.ajax({
                url: '<?php echo base_url();?>admin/uploadBrochure',
                data: data,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                dataType:'json',
               
                success: function(data){
                 
                      if(data.status==1)
                      {
                        
                        swal("Oops!", "width and height should be 120*120 pixel", "error");
                     }
                     if(data.status==2)
                     {
                         $('#brochurename').val('');
                         $('#brochurename').val(data.fileName);
                         
                         swal({
                        title: "Brochure uploaded!",
                       /* text: "Verify your email!",*/
                        icon: "success",
                        button: "Ok",
                      });
                     }
                  }
                    
            });
        }else{

               
               swal("Oops!", "Please enter university name first", "error");

                  return false;

        }     
}
  








function uploadBanner()
{
         
         var universityName = $('#universityname').val();



              if(universityName){
           
            var data = new FormData();
              
           jQuery.each($('#banner')[0].files, function(i, file) {
            console.log(i)
            data.append('banner-'+i, file);
            data.append('universityname',universityName);
        });
           jQuery.ajax({
                        url: '<?php echo base_url();?>admin/uploadBanner',
                        data: data,universityName,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        dataType:'json',
                       
                        success: function(data){
                         
                              if(data.status==1)
                              {
                                
                                swal("Oops!", "width and height should be 274*130 pixel", "error");
                             }
                             if(data.status==2)
                             {
                                 
                                 
                                 $('#bannerName').val('');
                                 $('#bannerName').val(data.fileName);
                                 
                                 swal({
                                title: "Banner image uploaded!",
                               /* text: "Verify your email!",*/
                                icon: "success",
                                button: "Ok",
                              });
                             }
                       }
           });
   }else{

        swal("Oops!", "Please enter university name first", "error");

                  return false;
   }   
}


function getCities()
{

    // alert('here')
      
      var stateName = $('#stateName').val();

    //  alert(stateName);

       $.ajax({
            
            url:'<?php echo base_url(); ?>admin/getStateCities',
            data:{stateName:stateName},
            dataType:'json',
            method:'POST',
            success:function(data){

                console.log(data.cities)

                      $('#citiesOptions').html('');
                      $('#citiesOptions').html(data.cities);
                      $('#selectCities').show();

            }

       });


}



function uploadGalleryImages()
{

   
   
         var universityName = $('#universityname').val();



              if(universityName){
           
            var data = new FormData();
              
           jQuery.each($('#gallery')[0].files, function(i, file) {
            console.log(i)
            data.append('gallery-'+i, file);
            data.append('universityname',universityName);
        });
           jQuery.ajax({
                        url: '<?php echo base_url();?>admin/uploadGalleryImages',
                        data: data,universityName,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        dataType:'json',
                       
                        success: function(data){


                              if(data.error)
                              {
                                
                                swal("Oops!", "width and height should be 750*500 pixel", "error");
                              }
                             if(data.success)
                             {
                                 
                                 swal({
                                title: "Gallery images uploaded!",
                               /* text: "Verify your email!",*/
                                icon: "success",
                                button: "Ok",
                              });
                             }
                       }
           });
   }else{

        swal("Oops!", "Please enter university name first", "error");

                  return false;
   }   
}


tinymce.init({
  selector: 'textarea',
  height: 350,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });


</script>