== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
          <ul>
            <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> Add University</a> </li>
            <li class="page-back"><a href="<?php echo base_url() ?>admin/alluniversity"><i class="fa fa-backward" aria-hidden="true"></i> View All</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>Add New University</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="<?php echo base_url() ?>admin/adduniversity">Add New</a> </li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url() ?>admin/alluniversity"><i class="material-icons">subject</i>View All</a> </li>
            </ul>
            <!-- Dropdown Structure -->
            <div class="split-row">
              <div class="col-md-12">
                <div class="box-inn-sp ad-inn-page">
                  <div class="tab-inn ad-tab-inn">
                    <div class="hom-cre-acc-left hom-cre-acc-right">
                      <div class="">
                        <?php echo $this->session->flashdata('successmsg');  ?>
                        <?php echo $this->session->flashdata('errormsg');  ?>
                        <form class="" name="adduniversity" ng-submit="submitform()" novalidate>
                          
                          <div class="row form-group">
								<label class="col-md-3">Name</label>
								<div class="col-md-9">
									<input type="text" name=" " class="form-control">	
								</div>
							
                            <!-- <div class="input-field col s12">
                              <input type="text" class="validate" id="universityname" name="universityname" ng-model="university.universityname" maxlength="50" required>
                              <span ng-show="submitted && adduniversity.universityname.$error.required"  class="help-block has-error ng-hide">Name is required.</span>
                              <span class="help-block has-error ng-hide" ng-show="universitynameError">{{universitynameError}}</span>
                              <label for="universityname">Name</label>
                            </div> -->
                          </div>

                          <div class="row form-group">
								<label class="col-md-3">Short Name</label>
								<div class="col-md-9">
									<input type="text" name=" " class="form-control">	
								</div>
                            <!-- <div class="input-field col s12">
                              <input type="text" class="validate" id="universityshortname" name="universityshortname" ng-model="university.universityshortname" maxlength="20" required>
                              <span ng-show="submitted && adduniversity.universityshortname.$error.required"  class="help-block has-error ng-hide">Short name is required.</span>
                              <span class="help-block has-error ng-hide" ng-show="universityshortnameError">{{universityshortnameError}}</span>
                              <label for="list_name">Short Name</label>
                            </div> -->
                          </div>

                          <div class="row form-group">
								<label class="col-md-3">URL</label>
								<div class="col-md-9">
									<input type="text" name=" " class="form-control">	
								</div>
                            <!-- <div class="input-field col s12">
                              <input type="text" class="validate" id="universityurl" name="universityurl" ng-model="university.universityurl" ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/" required>
                              <span ng-show="submitted && adduniversity.universityurl.$error.required"  class="help-block has-error ng-hide">URL is required.</span>
                              <span ng-show="submitted && adduniversity.universityurl.$error.pattern"  class="help-block has-error ng-hide">Please enter a valid url.</span>
                              <span class="help-block has-error ng-hide" ng-show="universityurlError">{{universityurlError}}</span>
                              <label for="list_phone">URL</label>
                            </div> -->
                          </div>

                          <div class="row form-group">
								              <label class="col-md-3">Address 1</label>
              								<div class="col-md-9">
              									 <input type="text" name=" " class="form-control"> 
              								</div>
                            <!-- <div class="input-field col s12">
                              <textarea id="address" name="address" class="materialize-textarea" rows="4" cols="50" style="resize:none;" ng-model="university.address" required></textarea>
                              <span ng-show="submitted && adduniversity.address.$error.required"  class="help-block has-error ng-hide">Address is required.</span>
                              <span class="help-block has-error ng-hide" ng-show="addressError">{{addressError}}</span>
                              <label for="email">Address</label>
                            </div> -->
                          </div>

                           <div class="row form-group">
                              <label class="col-md-3">Address 2</label>
                              <div class="col-md-9">
                                 <input type="text" name=" " class="form-control"> 
                              </div>
                          </div>
                              
                          <!-- <div class="row form-group">
                              <label class="col-md-3">Logo Image</label>
                              <div class="col-md-9">
                                  <input type="file" name=" " class="form-control"> 
                              </div>
                          </div> -->

                          <div class="row tz-file-upload form-group ">
                              <label class="col-md-3">Logo Image</label>
                            <div class="file-field input-field col-md-9 mt0">
                              <div class="tz-up-btn"> <span>File</span>
                                <input type="file"> </div>
                              <div class="file-path-wrapper">
                                <input class="file-path validate" type="text"> </div>
                            </div>
                          </div>

                          <div class="row tz-file-upload form-group ">
                              <label class="col-md-3">University Image</label>
                            <div class="file-field input-field col-md-9 mt0">
                              <div class="tz-up-btn"> <span>File</span>
                                <input type="file"> </div>
                              <div class="file-path-wrapper">
                                <input class="file-path validate" type="text"> </div>
                            </div>
                          </div>
                          
                         <!--  <div class="row form-group">
                              <label class="col-md-3">University Image</label>
                              <div class="col-md-9">
                                  <input type="file" name=" " class="form-control"> 
                              </div>
                          </div> -->

                          <div class="row form-group">
                              <label class="col-md-3">Description</label>
                              <div class="col-md-9 ">
                                 <textarea class="form-control mt0"></textarea>
                              </div>
                          </div>    

                          <div class="row form-group">
								<label class="col-md-3">Select Status</label>
								<div class="col-md-9">
									<select id="enabled" name="enabled" ng-model="university.enabled" class="browser-default" required>
                                <option value="" disabled selected>Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Non-Active</option>
                              </select>
                              <span ng-show="submitted && adduniversity.enabled.$error.required"  class="help-block has-error ng-hide">Please select status.</span>
                              <span class="help-block has-error ng-hide" ng-show="enabledError">{{enabledError}}</span>
								</div>
                            <!-- <div class="input-field col s12">
                              <select id="enabled" name="enabled" ng-model="university.enabled" class="browser-default" required>
                                <option value="" disabled selected>Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Non-Active</option>
                              </select>
                              <span ng-show="submitted && adduniversity.enabled.$error.required"  class="help-block has-error ng-hide">Please select status.</span>
                              <span class="help-block has-error ng-hide" ng-show="enabledError">{{enabledError}}</span>
                            </div> -->
                          </div>

                          <div class="row">
                            <div class="input-field col s12 v2-mar-top-40">
                              <input class="input-btn" type="submit" value="Submit" ng-click="submitted=true">
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--== BOTTOM FLOAT ICON ==-->
  <!-- <section>
    <div class="fixed-action-btn vertical">
      <a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
      <ul>
        <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
        <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
        <li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
        <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
      </ul>
    </div>
  </section> 