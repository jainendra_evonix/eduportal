<?php  if($this->session->flashdata('collegeSaved')){   echo '<script> 
                   swal({
                        title: "College saved successfully!",
                       /* text: "Verify your email!",*/
                        icon: "success",
                        button: "Ok",
                      }); </script>';
  } 


   if($this->session->flashdata('courseCreated'))
   {


       echo '<script> 
                   swal({
                        title: "Course added successfully!",
                       /* text: "Verify your email!",*/
                        icon: "success",
                        button: "Ok",
                      }); </script>';


   }
   if($this->session->flashdata('couseDeleted'))
   {
     
       echo '<script> 
                   swal({
                        title: "Course deleted successfully!",
                       /* text: "Verify your email!",*/
                        icon: "success",
                        button: "Ok",
                      }); </script>';


   }


  if($this->session->flashdata('courseUpdated'))
   {
     
       echo '<script> 
                   swal({
                        title: "Course Info Updated Successfully!",
                       /* text: "Verify your email!",*/
                        icon: "success",
                        button: "Ok",
                      }); </script>';


   }


  ?>

<!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
       
        <div class="sb2-2-2">
          <ul>
            <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#">College</a></li>
            <li class="active-bre"><a href="#"> All Courses</a></li>
            <li class="page-back"><a href="<?php echo base_url() ?>admin/addCourses/<?php echo $universityAndCollegeInfo->id.'/'.$universityAndCollegeInfo->universityId; ?>"><i class="fa fa-backward" aria-hidden="true"></i> Add new course</a> </li>
          </ul>
        </div>

        <div class="row">
           <div class="col-md-6">
             
             <h4>University Name:</h4>
             
           </div>

           <div class="col-md-6">
             <p><strong><?php echo $universityAndCollegeInfo->universityName; ?></strong></p>
           </div>

        </div>

         <div class="row">
           <div class="col-md-6">
             
             <h4>College Name:</h4>
             
           </div>

           <div class="col-md-6">
             <p><strong><?php echo $universityAndCollegeInfo->name; ?></strong></p>
           </div>

        </div>
        <br>
        
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>All Courses</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="<?php echo base_url() ?>admin/addCourses/<?php echo $universityAndCollegeInfo->id.'/'.$universityAndCollegeInfo->universityId; ?>">Add New</a> </li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url() ?>admin/viewCourses/<?php echo $universityAndCollegeInfo->id;?>"><i class="material-icons">subject</i>View All</a> </li>
            </ul>
            <!-- Dropdown Structure -->
            <div class="split-row">
              <div class="col-md-12">
                <div class="box-inn-sp ad-inn-page">
                  <div class="tab-inn ad-tab-inn">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered" datatable="ng" dt-options="vm.dtOptions">
                        <thead>
                          <tr>
                            <th>Listing</th>
                            <th>Course Type</th>
                            <th>Course Stream</th>
                            <th>Course Name</th>
                            <th>Course Specialization</th>
                            <th>Course Shortname</th>
                            
                            <th>Edit</th>
                            <th>Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="x in names" ng-cloak>
                            <td><span class="list-img" ng-cloak>{{ x.srno }}</span> </td>
                            <td><span class="list-enq-name" ng-cloak>{{ x.course_type }}</span></td>
                            <td><span class="list-enq-name" ng-cloak>{{ x.course_stream }}</span></td>
                            <td><span class="list-enq-name" ng-cloak>{{ x.course_name }}</span></td>
                            <td><span class="list-enq-name" ng-cloak>{{ x.course_specialization }}</span></td>
                            <td><span class="list-enq-name" ng-cloak>{{ x.course_shortname }}</span></td>
                            <td> <a href="#." ng-click="edit(x.id)" title="Edit"><i class="fa fa-edit"></i></a> </td>
                            <td> <a href="#." ng-click="delete(x.id)" title="Delete"><i class="fa fa-times"></i></a> </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 