<!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
          <ul>
            <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="#"> Add Specialization</a> </li>
            <li class="page-back"><a href="<?php echo base_url() ?>admin/allspecialization"><i class="fa fa-backward" aria-hidden="true"></i> View All</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>Add New Specialization</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="<?php echo base_url() ?>admin/addspecialization">Add New</a> </li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url() ?>admin/allspecialization"><i class="material-icons">subject</i>View All</a> </li>
            </ul>
            <!-- Dropdown Structure -->
            <div class="split-row">
              <div class="col-md-12">
                <div class="box-inn-sp ad-inn-page">
                  <div class="tab-inn ad-tab-inn">
                    <div class="hom-cre-acc-left hom-cre-acc-right">
                      <div class="">
                        <?php echo $this->session->flashdata('successmsg');  ?>
                        <?php echo $this->session->flashdata('errormsg');  ?>
                        <form class="" name="addspecialization" ng-submit="submitform()" novalidate>
                          <div class="row form-group">
                                      <label class="col-md-3">Specialization Name</label>
                                      <div class="col-md-9">
                                          <input type="text" name="" class="form-control">
                                      </div>
                                  </div>

                                  <div class="row form-group">
                                      <label class="col-md-3">Short Name</label>
                                      <div class="col-md-9">
                                          <input type="text" name="" class="form-control">
                                      </div>
                                  </div>

                                  

                                  <div class="row form-group">
                                      <label class="col-md-3">Status</label>
                                      <div class="col-md-9">
                                          <select >
                                              <option value="1">Deactive</option>
                                              <option value="2">Active</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="input-field col s12 v2-mar-top-40">
                                        <input class="input-btn" type="submit" value="Save" ng-click="submittedclg=true">
                                      </div>
                                  </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--== BOTTOM FLOAT ICON ==-->
  <!-- <section>
    <div class="fixed-action-btn vertical">
      <a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
      <ul>
        <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
        <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
        <li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
        <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
      </ul>
    </div>
  </section> -->