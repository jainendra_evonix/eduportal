<?php  //echo "<pre>"; print_r($errors); exit; 
$short_name = isset($postdata) && !empty($postdata['universityshortname']) ? $postdata['universityshortname'] : '';
$universityname = isset($postdata) && !empty($postdata['universityname']) ? $postdata['universityname'] : '';
$universityurl = isset($postdata) && !empty($postdata['universityurl']) ? $postdata['universityurl'] : '';
$address = isset($postdata) && !empty($postdata['address']) ? $postdata['address'] : '';
$country = isset($postdata) && !empty($postdata['country']) ? $postdata['country'] : '';
$stateName = isset($postdata) && !empty($postdata['stateName']) ? $postdata['stateName'] : '';
$city = isset($postdata) && !empty($postdata['city']) ? $postdata['city'] : '';
$email = isset($postdata) && !empty($postdata['email']) ? $postdata['email'] : '';
$contactNo = isset($postdata) && !empty($postdata['contactNo']) ? $postdata['contactNo'] : '';
$universityType = isset($postdata) && !empty($postdata['universityType']) ? $postdata['universityType'] : '';
$accreditions = isset($postdata) && !empty($postdata['accreditions']) ? $postdata['accreditions'] : '';
$approvedby = isset($postdata) && !empty($postdata['approvedby']) ? $postdata['approvedby'] : '';
$faxno = isset($postdata) && !empty($postdata['faxno']) ? $postdata['faxno'] : '';
$about = isset($postdata) && !empty($postdata['about']) ? $postdata['about'] : '';
$logoname = isset($postdata) && !empty($postdata['logoname']) ? $postdata['logoname'] : '';
$bannerName = isset($postdata) && !empty($postdata['bannerName']) ? $postdata['bannerName'] : '';
$established = isset($postdata) && !empty($postdata['established']) ? $postdata['established'] : '';
$metaKeyword = isset($postdata) && !empty($postdata['metaKeyword']) ? $postdata['metaKeyword'] : '';
$titleTag = isset($postdata) && !empty($postdata['titleTag']) ? $postdata['titleTag'] : '';
$metaDescription = isset($postdata) && !empty($postdata['metaDescription']) ? $postdata['metaDescription'] : '';
$metaRobot = isset($postdata) && !empty($postdata['metaRobot']) ? $postdata['metaRobot'] : '';
$brochurename = isset($postdata) && !empty($postdata['brochurename']) ? $postdata['brochurename'] : '';


?>
<style type="text/css">
  
  .error{

    color: red;
    font-size: 14px;
  }
</style>
<!--== BODY INNER CONTAINER ==-->
      <div class="sb2-2">
        <!--== breadcrumbs ==-->
        <div class="sb2-2-2">
          <ul>
            <li><a href="#."><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
            <li class="active-bre"><a href="<?php echo base_url() ?>admin/adduniversity"> Add University</a> </li>
            <li class="page-back"><a href="<?php echo base_url() ?>admin/allcollege"><i class="fa fa-backward" aria-hidden="true"></i> View All</a> </li>
          </ul>
        </div>
        <div class="tz-2 tz-2-admin">
          <div class="tz-2-com tz-2-main">
            <h4>Add New University</h4> <a class="dropdown-button drop-down-meta drop-down-meta-inn" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
            <ul id="dr-list" class="dropdown-content">
              <li><a href="<?php echo base_url() ?>admin/adduniversity">Add New</a> </li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url() ?>admin/alluniversity"><i class="material-icons">subject</i>View All</a> </li>
            </ul>
            <!-- Dropdown Structure -->





            <div class="split-row">
              <div class="col-md-12">
                <div class="ad-inn-page">
                  <div class="tab-inn ad-tab-inn">
                    <div class="hom-cre-acc-left hom-cre-acc-right">
                      <div class="">
                        <?php echo $this->session->flashdata('successmsg');  ?>
                        <?php echo $this->session->flashdata('errormsg');  ?>

                        
                          <div class="pg-elem-inn ele-btn col-md-12" >
  
                           <form id="example-advanced-form"  method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/adduniversity" novalidate>

                                <h3>Basic Info</h3>
                                <hr>
                                <fieldset>
                                  <div class="row">
                                   <div class="col-md-6 form-group">
                                        <label>Full Name </label>
                                           
                                          <input type="text" class="validate form-control" id="universityname" name="universityname"   value="<?php echo $universityname; ?>" data-validation="length" data-validation-length="max50"  data-validation-error-msg="Please enter university name"> 
                                        <?php echo form_error('universityname', '<div class="error">', '</div>'); ?>   
                                  </div>


                                  <div class="col-md-6 form-group">
                                      <label >Short Name </label>
                                      
                                          <input type="text" class="validate form-control" id="universityshortname" name="universityshortname" value="<?php echo $short_name; ?>" maxlength="20" data-validation="required" data-validation-error-msg="Please enter short name">
                                      <?php echo form_error('universityshortname', '<div class="error">', '</div>'); ?> 
                                  </div>

                                   </div>                          

                                   <div class="row">
                                  <div class="col-md-6 form-group">
                                      <label >Website </label>
                                      
                                           <input type="text" class="validate form-control" id="universityurl" name="universityurl"  value="<?php echo $universityurl; ?>" data-validation="url"  data-validation-error-msg="Ex. http://example.com">
                                      
                                      <?php echo form_error('universityurl', '<div class="error">', '</div>'); ?> 
                                     
                                  </div>
                                 
                                  <div class="col-md-6 form-group">
                                      <label >Address </label>
                                      
                                         <input type="text" class="validate form-control" id="address" name="address" value="<?php echo $address; ?>" data-validation="required" data-validation-error-msg="Please enter address" >
                                    
                                    <?php echo form_error('address', '<div class="error">', '</div>'); ?>  
                                  </div>
                                </div>
                                <div class="row">     
                                     <div class="col-md-6 form-group">
                                      <label >Country </label>
                                      
                                         
                                         <select class="choosen-select" name="country" value="" data-validation="required">
                                           
                                          <!--  <option value="">Select county</option> -->
                                           <option value="<?php echo $countries->name;?>" <?php if($countries->name == $country){echo 'selected';} ?> ><?php echo $countries->name; ?> </option>
                                         
                                         </select>
                                     <?php echo form_error('country', '<div class="error">', '</div>'); ?>
                                  </div>

                                    <div class="col-md-6 form-group">
                                      <label >State </label>
                                      
                                        
                                         <select  name="stateName" value="" id="stateName" data-validation="required" onchange="getCities();">
                                          <option value="">Please select </option>
                                           <?php foreach ($states as $key ) { ?>
                                           <option value="<?php echo $key->name;?>" <?php if($key->name == $stateName){ echo 'selected';} ?> ><?php echo $key->name; ?> </option>
                                         <?php  }?>
                                         </select>
                                 <?php echo form_error('stateName', '<div class="error">', '</div>'); ?>
                                     
                                  </div>
                                  </div>
                                <div class="row"> 
                                  <div class="col-md-6 form-group">
                                      <label >City  </label>
                                     
                                      <div class="citiesOptions" id="citiesOptions">                                      
                                         <select data-validation="required" value='' name="city"> 
                                         <?php if(empty($city)) { ?>
                                         <option value="">select city</option>
                                       <?php }else{?>
                                         <option value="<?php if($city){ echo $city; } ?>" <?php echo 'selected'; ?>  ><?php echo $city; ?></option>
                                       <?php }?>
                                         
                                       </select>
                                       </div>   
                                      <?php echo form_error('city', '<div class="error">', '</div>'); ?>
                                  </div>


                                  

                                     <div class="col-md-6 form-group">
                                      <label >Email Id </label>
                                      
                                          <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" data-validation="email" data-validation-error-msg="Please enter valid email id">
                                      <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                  </div>
                                  </div>
                                <div class="row"> 
                                  <div class="col-md-6 form-group">
                                      <label >Phone No </label>
                                      
                                          <input type="text" data-validation="number" name="contactNo" value="<?php echo $contactNo; ?>" class="form-control" data-validation-error-msg="Please enter valid mobile no.">
                                          <?php echo form_error('contactNo', '<div class="error">', '</div>'); ?>
                                      </div>


                                      <div class="col-md-6 form-group">
                                      <label>University Type</label>
                                      
                                          <select class="choosen-select" name="universityType" value=""  data-validation="required">
                                            
                                            <?php foreach ($universitiesType as $key ) { ?>

                                            <option value="<?php echo $key->university_type; ?>" <?php if($key->university_type==$universityType){ echo 'selected';} ?> ><?php echo $key->university_type; ?></option>
                                          <?php  }?>
                                          </select>
                                          <?php echo form_error('universityType', '<div class="error">', '</div>'); ?>
                                      </div>
                                      </div>
                                <div class="row"> 

                                      <div class="col-md-6 form-group">
                                      <label >Grade</label>
                                     
                                        <select name="accreditions" class="choosen-select" value="" data-validation="required">
                                              <option value="">Please select grade</option>
                                            <?php  foreach ($accreditionss as $key ) { ?>
                                              <option value="<?php echo $key->name;?>" <?php if($key->name==$accreditions){ echo 'selected';} ?> ><?php echo $key->name; ?></option>
                                          
                                          <?php   }?>
                                          </select>
                                           <?php echo form_error('accreditions', '<div class="error">', '</div>'); ?>
                                      </div>


                                       <div class="col-md-6 form-group">
                                      <label >Approved by</label>
                                      
                                          <select  class="choosen-select" name="approvedby" value="" data-validation="required">
                                            <option value="UGC">UGC</option>
                                          </select>
                                          <?php echo form_error('approvedby', '<div class="error">', '</div>'); ?>
                                      </div>
                                      </div>
                                <div class="row"> 
                                      
                                         <div class="col-md-6 form-group">
                                            <label>Fax No. </label>
                                          <!-- <input type="text" name="accreditedBy" value="" class="form-control" required> -->
                                         <input type="text" class="validate form-control" id="faxno" name="faxno" value="<?php echo $faxno;?>" data-validation="number"  data-validation-error-msg="Please enter valid fax no." >
                                         <?php echo form_error('faxno', '<div class="error">', '</div>'); ?>
                                      </div>
                                       <div class="col-md-6 form-group">
                                            <label>Established </label>
                                          <!-- <input type="text" name="accreditedBy" value="" class="form-control" required> -->
                                         <input type="text" class="validate form-control" id="established" name="established" value="<?php echo $established; ?>" data-validation="number"  data-validation-error-msg="Please enter year of Established" >
                                         <?php echo form_error('established', '<div class="error">', '</div>'); ?>
                                      </div>

                                      </div>
                                <div class="row"> 

                                      <div class="col-md-12 form-group">
                                      <label >About </label>
                                      
                                           <textarea id="about" name="about" class="materialize-textarea form-control" rows="2" cols="50"  value="" data-validation="required" data-validation-error-msg="Please enter about university"><?php echo $about; ?></textarea>
                              
                                      <?php echo form_error('about', '<div class="error">', '</div>'); ?>
                                     
                                  </div>
                                </div>

                                 <div class="row">
                                   <div class="col-md-6 form-group">
                                        <label>Meta Keywords </label>
                                           
                                          
                                          <input type="text" class="validate form-control" id="metaKeyword" name="metaKeyword"   value="<?php echo $metaKeyword; ?>" data-validation="required"  data-validation-error-msg="Please enter meta Keywords"> 
                                          <?php echo form_error('metaKeyword', '<div class="error">', '</div>'); ?>   
                                  </div>


                                  <div class="col-md-6 form-group">
                                      <label>Title Tag </label>
                                      
                                         
                                          <input type="text" class="validate form-control" id="titleTag" name="titleTag"   value="<?php echo $titleTag; ?>" data-validation="required"   data-validation-error-msg="Please enter meta title"> 
                                      <?php echo form_error('titleTag', '<div class="error">', '</div>'); ?> 
                                  </div>

                                   </div> 

                                    

                                   <div class="row">
                                   <div class="col-md-6 form-group">
                                        <label>Meta Description </label>
                                           
                                          <input type="text" class="validate form-control" id="metaDescription" name="metaDescription"   value="<?php echo $metaDescription; ?>" data-validation="required" data-validation-length="max50"  data-validation-error-msg="Please enter meta description name"> 
                                        <?php echo form_error('metaDescription', '<div class="error">', '</div>'); ?>   
                                  </div>


                                  <div class="col-md-6 form-group">
                                      <label>Meta Robots </label>
                                      
                                          <input type="text" class="validate form-control" id="metaRobot" name="metaRobot" value="<?php echo $metaRobot; ?>" maxlength="20" data-validation="required" data-validation-error-msg="Please enter meta robot">
                                      <?php echo form_error('metaRobot', '<div class="error">', '</div>'); ?> 
                                  </div>

                                   </div> 
                                  
                            </div> 





                            <div class="col-md-12">
                            <div class="pg-elem-inn ele-btn " style="background: #eaedef;    padding: 30px;">
                             
                                <h3>Uploads Images</h3>
                                <hr>
                                
                                  <div class="row tz-file-upload form-group ">
                                   
                                    <label class="col-md-12">Logo Image</label>
                                    
                                      <div class="col-md-6">
                                        <input type="file" name="logo" accept="image/*" id="logo">
                                        <input type="hidden"  name="logoname" id="logoname" class="form-control" value="<?php echo  $logoname;?>">
                                        <?php echo form_error('logoname', '<div class="error">', '</div>'); ?>
                                      </div>  
                                    <div class="col-md-3">
                                        <button type="button" class="btn" onclick="uploadLogo();">Upload</button>
                                    </div>
                                    
                                  </div>


                                   <div class="row tz-file-upload form-group ">
                                   
                                    <label  class="col-md-12">Banner Image</label>
                                       <div class="col-md-6">
                                        <input type="file" name="banner" accept="image/*" id="banner">
                                        <input type="hidden"  name="bannerName" id="bannerName" value="<?php echo $bannerName; ?>">
                                        <?php echo form_error('bannerName', '<div class="error">', '</div>'); ?>
                                      </div>  
                                        
                                    <div class="col-md-3">
                                         <button type="button" class="btn" onclick="uploadBanner();">Upload</button>
                                    </div>
                                    
                                  </div>
                                  <div class="row tz-file-upload form-group ">
                                   
                                    <label class="col-md-12">Brochure</label>
                                    
                                      <div class="col-md-6">
                                        <input type="file" name="brochure" accept="application/pdf/*" id="brochure">
                                        <input type="hidden"  name="brochurename" id="brochurename" class="form-control" value="<?php echo  $brochurename;?>">
                                        <?php echo form_error('brochurename', '<div class="error">', '</div>'); ?>
                                      </div>  
                                    <div class="col-md-3">
                                        <button type="button" class="btn" onclick="uploadBrochure();">Upload</button>
                                    </div>
                                    
                                  </div>
 


             

                                <div class="row tz-file-upload form-group ">
                                      <label  class="col-md-12">Gallery Images</label>
                                      <div class="col-md-6"> 
                                         <input type="file" name="gallery" accept="image/*" id="gallery" multiple> 
                                      </div>
                                         
                                      <div class="col-md-3">
                                         <button type="button" class="btn" onclick="uploadGalleryImages();">Upload</button>
                                      </div>
                                  </div>
                                  <hr>

                                                           
                              </div>
                            </div>  

                           



                            <div class="col-md-12 mTB30">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-block" type="Submit">Submit</button>
                                </div>    
                              </div>  
                            </form>

                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

