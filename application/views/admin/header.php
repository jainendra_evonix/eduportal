<?php // echo "<pre>"; print_r($userInfo); exit; ?>
<body ng-app="postApp" ng-controller="postController">
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!--== MAIN CONTRAINER ==-->
  <div class="container-fluid sb1">
    <div class="row">
      <!--== LOGO ==-->
      <div class="col-md-2 col-sm-3 col-xs-6 sb1-1"> <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a> <a href="#" class="atab-menu"><i class="fa fa-bars tab-menu" aria-hidden="true"></i></a>
        <a href="#." class="logo"><img src="<?php echo base_url(); ?>assets/images/logo1.png" alt="" /> </a>
      </div>
      <!--== SEARCH ==-->
      <div class="col-md-6 col-sm-6 mob-hide">
        <!-- <form class="app-search">
          <input type="text" placeholder="Search..." class="form-control"> <a href="#."><i class="fa fa-search"></i></a> </form> -->
      </div>
      <!--== NOTIFICATION ==-->
      <div class="col-md-2 tab-hide">
        <div class="top-not-cen"> <a class='waves-effect btn-noti' href='#'><i class="fa fa-commenting-o" aria-hidden="true"></i><span>5</span></a> <a class='waves-effect btn-noti' href='#'><i class="fa fa-envelope-o" aria-hidden="true"></i><span>5</span></a> <a class='waves-effect btn-noti' href='#'><i class="fa fa-tag" aria-hidden="true"></i><span>5</span></a> </div>
      </div>
      <!--== MY ACCCOUNT ==-->
      <div class="col-md-2 col-sm-3 col-xs-6">
        <!-- Dropdown Trigger -->
        <a class='waves-effect dropdown-button top-user-pro' href='#' data-activates='top-menu'><img src="<?php echo base_url(); ?>assets/images/users/2.png" alt="" />My Account <i class="fa fa-angle-down" aria-hidden="true"></i> </a>
        <!-- Dropdown Structure -->
        <ul id='top-menu' class='dropdown-content top-menu-sty'>
          <li><a href="<?php echo base_url();?>admin/dashboard" class="waves-effect"><i class="fa fa-tachometer"></i>Dashboard</a> </li>
          <li class="divider"></li>
          <li><a href="<?php echo base_url();?>admin/logout" class="ho-dr-con-last waves-effect"><i class="fa fa-sign-in" aria-hidden="true"></i> Logout</a> </li>
        </ul>
      </div>
    </div>
  </div>
  <!--== BODY CONTNAINER ==-->
  <div class="container-fluid sb2">
    <div class="row">
      <div class="sb2-1">
        <!--== USER INFO ==-->
        <div class="sb2-12">
          <ul>
            <li><img src="<?php echo base_url(); ?>assets/images/users/2.png" alt=""> </li>
            <li>
              <h5><?php echo $userInfo->fname.' '.$userInfo->lname;?> <span> <?php echo $userInfo->username;?></span></h5> </li>
            <li></li>
          </ul>
        </div>
        <!--== LEFT MENU ==-->
        <div class="sb2-13">
          <ul class="collapsible" data-collapsible="accordion">
            <li><a id="dashboard" href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a> </li>

            <li><a id="university" href="javascript:void(0)" class="collapsible-header"><i class="fa fa-university" aria-hidden="true"></i> University</a>
              <div class="collapsible-body left-sub-menu">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/alluniversity">All University</a> </li>
                  <li><a href="<?php echo base_url(); ?>admin/adduniversity">Add New University</a> </li>
                </ul>
              </div>
            </li>

           <li><a id="programs" href="javascript:void(0)" class="collapsible-header"><i class="fa fa-university" aria-hidden="true"></i> Program</a>
              <div class="collapsible-body left-sub-menu">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/addStream">Add Stream</a> </li>
                  <li><a id="dashboard" href="<?php echo base_url(); ?>admin/addPrograms"><i class="fa fa-tachometer" aria-hidden="true"></i> Add Programs</a> </li>
                  <li><a href="<?php echo base_url(); ?>admin/allPrograms">All Pragrams</a> </li>
                </ul>
              </div>
            </li>
 


          
         <!--  <li><a id="courses" href="javascript:void(0)" class="collapsible-header"><i class="fa fa-edit" aria-hidden="true"></i> Courses</a>
              <div class="collapsible-body left-sub-menu">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/allcourses">All Courses</a> </li>
                  <li><a href="<?php echo base_url(); ?>admin/addcourse">Add New Course</a> </li>
                </ul>
              </div>
            </li> -->

          <!--   <li><a id="specialization" href="javascript:void(0)" class="collapsible-header"><i class="fa fa-file" aria-hidden="true"></i> Specialization</a>
              <div class="collapsible-body left-sub-menu">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/allspecialization">All Specialization</a> </li>
                  <li><a href="<?php echo base_url(); ?>admin/addspecialization">Add New Specialization</a> </li>
                </ul>
              </div>
            </li> -->
            
            <li><a id="college" href="javascript:void(0)" class="collapsible-header"><i class="fa fa-building" aria-hidden="true"></i> College</a>
              <div class="collapsible-body left-sub-menu">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/allcollege">All College</a> </li>
                  <li><a href="<?php echo base_url(); ?>admin/addcollege">Add New College</a> </li>
                </ul>
              </div>
            </li>
              
           <!-- <li><a id="collegeCourses" href="<?php echo base_url(); ?>admin/addCourses"><i class="fa fa-tachometer" aria-hidden="true"></i> Add courses to University/College</a> </li> -->
             <!-- <li><a id="college" href="<?php echo base_url(); ?>admin/addSpecialization" class="collapsible-header"><i class="fa fa-building" aria-hidden="true"></i> Add Specializations to University</a>
              
            </li> -->


            <!-- <li><a id="courseassociation" href="<?php echo base_url(); ?>admin/courseassociation"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Course Association</a> </li> -->

            <li><a href="<?php echo base_url();?>admin/logout"><i class="fa fa-sign-in" aria-hidden="true"></i> Logout</a> </li>
          </ul>
        </div>
      </div>