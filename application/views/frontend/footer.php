	<!--FOOTER SECTION-->
	<footer id="colophon" class="site-footer clearfix">
		<div id="quaternary" class="sidebar-container " role="complementary">
			<div class="sidebar-inner">
				<div class="widget-area clearfix">
					<div id="azh_widget-2" class="widget widget_azh_widget">
						<div data-section="section">
							<div class="container">
								<div class="row">
									<!-- <div class="col-sm-4 col-md-3 foot-logo"> <img src="images/foot-logo.png" alt="logo">
										<p class="hasimg">Worlds's No. 1 Local Business Directory Website.</p>
										<p class="hasimg">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
									</div> -->
									<div class="col-sm-4 col-md-3">
										<h4>Top Colleges for</h4>
										<ul class="two-columns">
											<li> <a href="#">M.B.A</a> </li>
											<li> <a href="#">B.Tech/B.E</a> </li>
											<li> <a href="#">MCA</a> </li>
											<li> <a href="#">BCA </a> </li>
											<li> <a href="#">M.Tech</a> </li>
											<li> <a href="#">MA</a> </li>
											<li> <a href="#">BA</a> </li>
											<!-- <li> <a href="#">Quick Enquiry</a> </li>
											<li> <a href="#">Ratings </a> </li>
											<li> <a href="trendings.php">Top Trends</a> </li> -->
										</ul>
									</div>
									<div class="col-sm-4 col-md-3">
										<h4>Top Universities for</h4>
										<ul class="two-columns">
											<li> <a href="#">Engineering</a> </li>
											<li> <a href="#">Management</a> </li>
											<li> <a href="#">Medical</a> </li>
											<li> <a href="#">Law</a> </li>
											<li> <a href="#">Commerce</a> </li>
											<li> <a href="#">Science</a> </li>
											<li> <a href="#">Arts</a> </li>
											<!-- <li> <a href="#">Sports Events</a> </li>
											<li> <a href="#">Web Services </a> </li>
											<li> <a href="#">Skin Care</a> </li> -->
										</ul>
									</div>
									<div class="col-sm-4 col-md-3">
										<h4>Other Links</h4>
										<ul class="two-columns">
											<li> <a href="#">About Us</a> </li>
											<li> <a href="#">Contact Us</a> </li>
											<li> <a href="#">Advertising</a> </li>
											<li> <a href="#">Privacy </a> </li>
											<li> <a href="#">Terms & Conditions</a> </li>
											<!-- <li> <a href="#">Indianapolis</a> </li>
											<li> <a href="#">Las Vegas</a> </li>
											<li> <a href="#">Los Angeles</a> </li>
											<li> <a href="#">Louisville </a> </li>
											<li> <a href="#">Houston</a> </li> -->
										</ul>
									</div>

									<div class="col-sm-4 col-md-3 foot-social">
										<h4>Follow with us</h4>
										<ul>
											<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
											<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> </li>
											<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
											<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
											<!-- <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
											<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a> </li> -->
										</ul>
									</div>


								</div>
							</div>
						</div>

					

					</div>
				</div>
				<!-- .widget-area -->
			</div>
			<!-- .sidebar-inner -->
		</div>
		<!-- #quaternary -->
	</footer>
	<!--COPY RIGHTS-->
	<section class="copy">
		<div class="container">
			<p>Copyright © 2018 EduPortal. All rights reserved. Crafted By <a href="http://evonix.co/" target="_blank">Evonix Technology</a></p>
		</div>
	</section>
	<!--QUOTS POPUP-->
     
		<?php echo $add_js; ?>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6565945136965010",
    enable_page_level_ads: true
  });
</script>

</body>

</html>