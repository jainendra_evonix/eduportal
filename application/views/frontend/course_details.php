<?php error_reporting(0); //echo "<pre>"; print_r($allCourses); exit;  ?>
	<section >
		<div class="v3-list-ql">
			<div class="container">
				<div class="row">
					<div class="v3-list-ql-inn">
						<ul>
							<li class="active"><a href="#info"><i class="fa fa-user"></i> Details</a>
							</li>
							<li ><a href="#ld-abour"><i class="fa fa-user"></i> Eligibility</a>
							</li>
							<li><a href="#ld-ser"><i class="fa fa-cog"></i> Admissions</a>
							</li>
							<li><a href="#ld-gal"><i class="fa fa-photo"></i>  Fees  </a>
							</li>
							<li><a href="#ld-dat"><i class="fa fa-photo"></i>  Important Dates  </a>
							</li>
							<li><a href="#ld-rew"><i class="fa fa-edit"></i> Write Review</a>
							</li>
							<li><a href="#contact"><i class="fa fa-paper-plane-o"></i> Contact</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--LISTING DETAILS-->
	
	<section class="list-pg-bg" style="margin-top: 80px;">
		<div class="container">
			<div class="row" style="padding-top:40px">
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a> </li>
					<li class="active">Course Details</li>
				</ol>
			</div>
				
			<div class="row " >
				<div class="well" id="course">
					<div class="row">
						<div class="col-md-8">
							<p class="mb0"><?php echo $getAllCourseInfo->course_name; ?></p>
							<h2 class="mb10"><?php echo $getAllCourseInfo->course_name.' '.'('.$getAllCourseInfo->course_shortname.')'; ?></h2>
							

							<div class="list-room-type list-rom-ami">
								<ul>
									<li><?php echo $getAllCourseInfo->course_type; ?> Degree</li>
									<li>Full Time</li>
									<li>Duration - 2 years</li>
								</ul>
							</div>	
						</div>

						<div class="col-md-4 text-center course-price ">
							<span class="display1">  13.50 Lakh</span> <span><small class="display2">INR</small></span>
							<small class="display2">Total Fee*</small>

							<div class="mt20">
								<button class="btn btn-course"><i class="fa fa-download"></i> Brochure</button>
							</div>
						</div>
					</div>

					


				</div>	
			</div>	

			<div class="row">
				<div class="">
					<div class="list-pg-lt list-page-com-p">
						<!--Courses-->
						<div class="pglist-p1 pglist-bg pglist-p-com" id="ld-abour">
							<div class="pglist-p-com-ti">
								<h3><!-- <span>About</span>  -->Eligibility</h3> 
							</div>

							<div class="clearfixed">
								<div class="table-responsive  col-md-12 mt20">
									<table class="table table-bordered table-hover">
										<thead>
											<tr>
												<th>Qualification</th>
												<th>Minimum Eligibility to Apply</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Graduation</td>
												<td>50%</td>
											</tr>
											<tr>
												<td>12th</td>
												<td>50%</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>	
								
						</div>
						<!--END -->


						<!--Colleges-->
						<div class="pglist-p2 pglist-bg pglist-p-com " id="ld-ser">
							<div class="pglist-p-com-ti">
								<h3>Admission Process</h3> 
							</div>
							
							<div class="list-pg-inn-sp row college-view-row">
									
								<h5>Register and Apply</h5>
								<p>Interested candidates can apply online with the required information.</p>

								<h5>Entrance Test</h5>
								<p>Eligible candidates need to appear for Alliance Management Aptitude Test (AMAT) conducted by the college which is designed on the pattern of MAT/GMAT along with a component of Essay Writing.</p>


								<h5>Extempore Test</h5>
								<p>Eligible candidates are required to appear for an Oral Extempore Presentation before a panel, for a maximum duration of three minutes on a topic given on the spot. The candidate will be given five minutes of preparation before the presentation. The presentation will be evaluated on content, relevance, and clarity in communication.</p>

								<h5>Final Selection</h5>
								<p>Candidates are selected based on Alliance Management Aptitude Test (AMAT) score and performance in essay writing, oral extempore presentation and personal interview.</p>
							</div>
						<!--END -->
						</div>
						




						<!--Fees-->
						<div class="pglist-p3 pglist-bg pglist-p-com" id="ld-gal" style="overflow: auto;">
							<div class="pglist-p-com-ti">
								<h3><!-- <span>Photo</span>  -->Fees</h3> </div>
							<div class="list-pg-inn-sp">
								<h5>INR 13.50 Lakh</h5>
								<p>(Total Fee or Fees Components : Tuition, Registration Fee)</p>

								<p>In addition to the above-mentioned fees, all students are required to pay a refundable caution deposit of Rs10,000 + Rs 3,000 towards the membership of Alliance Alumni Association and Rs 1,500 towards the convocation fees along with the first installment of tuition fees.</p>

								<table class="table table-bordered table-hover">
									<tbody>
										<tr>
											<td>Double occupancy room with vegetarian meals for a period of six months</td>
											<td>75000/-</td>
										</tr>
										<tr>
											<td>Single occupancy room with vegetarian meals for a period of six months</td>
											<td>90000/-</td>
										</tr>
										<tr>
											<td>Laundry charges for six months</td>
											<td>6000/-</td>
										</tr>
										<tr>
											<td>Hostel Security Deposit</td>
											<td>Rs 15,000 (refundable amount upto a maximum of Rs 13000)</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!--END Fees-->


						<!--Important Dates-->
						<div class="pglist-p3 pglist-bg pglist-p-com" id="ld-dat" style="overflow: auto;">
							<div class="pglist-p-com-ti">
								<h3><!-- <span>Photo</span>  -->Important Dates</h3>
							</div>
								
							<div class="list-pg-inn-sp">
								<div class="clearfixed">
									<div class="table-responsive  col-md-12 ">
										<table class="table table-bordered table-hover">
											<thead>
												<tr>
													<th>Date</th>
													<th>Details</th>
												</tr>
											</thead>		
											<tbody>
												<tr>
													<td>Jun 4 , 2018	</td>
													<td>Admission Selection Process Delhi</td>
												</tr>
												<tr>
													<td>Jun 4 , 2018	</td>
													<td>ALLIANCE-AMAT - Admission Selection Process: Last Date</td>
												</tr>
												<tr>
													<td>Jun 28 , 2018	</td>
													<td>Course Commences</td>
												</tr>
											</tbody>
										</table>	
									</div>
								</div>		
							</div>	
						</div>	
					
						<!--END Important Dates-->



						<!-- Reviews -->
						<div class="pglist-p3 pglist-bg pglist-p-com" id="ld-rew" style="overflow: auto;">
							<div class="pglist-p-com-ti">
								<h3><span>Write Your</span> Reviews</h3> </div>
							<div class="list-pg-inn-sp">
								<div class="list-pg-write-rev">
									<form class="col">
										<p>Writing great reviews may help others discover the places that are just apt for them. Here are a few tips to write a good review:</p>
										<div class="row">
											<div class="col s12">
												<fieldset class="rating">
													<input type="radio" id="star5" name="rating" value="5">
													<label class="full" for="star5" title="Awesome - 5 stars"></label>
													<input type="radio" id="star4half" name="rating" value="4 and a half">
													<label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
													<input type="radio" id="star4" name="rating" value="4">
													<label class="full" for="star4" title="Pretty good - 4 stars"></label>
													<input type="radio" id="star3half" name="rating" value="3 and a half">
													<label class="half" for="star3half" title="Meh - 3.5 stars"></label>
													<input type="radio" id="star3" name="rating" value="3">
													<label class="full" for="star3" title="Meh - 3 stars"></label>
													<input type="radio" id="star2half" name="rating" value="2 and a half">
													<label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
													<input type="radio" id="star2" name="rating" value="2">
													<label class="full" for="star2" title="Kinda bad - 2 stars"></label>
													<input type="radio" id="star1half" name="rating" value="1 and a half">
													<label class="half" for="star1half" title="Meh - 1.5 stars"></label>
													<input type="radio" id="star1" name="rating" value="1">
													<label class="full" for="star1" title="Sucks big time - 1 star"></label>
													<input type="radio" id="starhalf" name="rating" value="half">
													<label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
												</fieldset>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s6">
												<input id="re_name" type="text" class="validate">
												<label for="re_name">Full Name</label>
											</div>
											<div class="input-field col s6">
												<input id="re_mob" type="number" class="validate">
												<label for="re_mob">Mobile</label>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s6">
												<input id="re_mail" type="email" class="validate">
												<label for="re_mail">Email id</label>
											</div>
											<div class="input-field col s6">
												<input id="re_city" type="text" class="validate">
												<label for="re_city">City</label>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s12">
												<textarea id="re_msg" class="materialize-textarea"></textarea>
												<label for="re_msg">Write review</label>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s12"> <a class="waves-effect waves-light btn-large full-btn" href="#!">Submit Review</a> </div>
										</div>
									</form>
								</div>
							</div>
						</div>



						<div class="pglist-p3 pglist-bg pglist-p-com" id="ld-rer">
							<div class="pglist-p-com-ti">
								<h3><span>User</span> Reviews</h3> </div>
							<div class="list-pg-inn-sp">
								<div class="lp-ur-all">
								
									<div class="lp-ur-all-right">
										<h5>Overall Ratings</h5>
										<p><span>4.5 <i class="fa fa-star" aria-hidden="true"></i></span> based on 242 reviews</p>
									</div>
								</div>
								<div class="lp-ur-all-rat">
									<h5>Reviews</h5>
									<ul>
										<li>
											<div class="lr-user-wr-img"> <img src="images/users/2.png" alt=""> </div>
											<div class="lr-user-wr-con">
												<h6>Jacob Michael <span>4.5 <i class="fa fa-star" aria-hidden="true"></i></span></h6> <span class="lr-revi-date">19th January, 2017</span>
												<p>Good service... nice and clean rooms... very good spread of buffet and friendly staffs. Located in heart of city and easy to reach any places in a short distance. </p>
												
											</div>
										</li>
										<li>
											<div class="lr-user-wr-img"> <img src="images/users/3.png" alt=""> </div>
											<div class="lr-user-wr-con">
												<h6>Gabriel Elijah <span>5.0 <i class="fa fa-star" aria-hidden="true"></i></span></h6> <span class="lr-revi-date">21th July, 2016</span>
												<p>The hotel is clean, convenient and good value for money. Staff are courteous and helpful. However, they need more training to be efficient.</p>
												
											</div>
										</li>
										<li>
											<div class="lr-user-wr-img"> <img src="images/users/4.png" alt=""> </div>
											<div class="lr-user-wr-con">
												<h6>Luke Mason <span>4.2 <i class="fa fa-star" aria-hidden="true"></i></span></h6> <span class="lr-revi-date">21th March, 2018</span>
												<p>Too much good experience with hospitality, cleanliness, facility and privacy and good value for money... To keep mind relaxing... Keep it up... </p>
												
											</div>
										</li>
										
									</ul>
								</div>
							</div>
						</div>

						<!-- End Reviews -->

						

						<!-- Contact -->
						<div class="pglist-p3 pglist-bg pglist-p-com" id="contact" style="overflow: auto;">
							<div class="pglist-p-com-ti">
								<h3><span>Contact</span> Us</h3> </div>
							<div class="list-pg-inn-sp">
								<div class="list-pg-write-rev">
									<div class="col-md-4 text-center">
										<h4><i class="fa fa-paper-plane-o"></i> Address:</h4>
										<hr>
										<p> Chikkahagade Cross<br> Chandapura - Anekal Main Road,<br>Bangalore ( Karnataka)</p>
									</div>
									
									<div class="col-md-4 text-center">
										<h4><i class="fa fa-globe"></i> Website:</strong> </h4>
										<hr>
										<p><a href="http://bus.alliance.edu.in/" target="_blank"><?php echo $getAllCourseInfo->url; ?></a></p>
									</div>

									<div class="col-md-4 text-center">
										<h4><i class="fa fa-phone"></i> Phone:</strong> </h4>
										<hr>
										
										<a href="#!" data-dismiss="modal" data-toggle="modal" data-target="#register" class="btn btn-primary"> Contact Us </a>
									</div>

								</div>
							</div>
						</div>
						<!--End Contact  -->



					</div>	




					<!--  Right Side bar -->
					<div class="list-pg-rt">
						<div class="pglist-p3 pglist-bg pglist-p-com">
							<div class="pglist-p-com-ti pglist-p-com-ti-right">
								<h3><span>Course </span> Listing</h3> </div>
							<div class="list-pg-inn-sp">
								<div class="list-pg-guar">
									<ul>
										<li>
											<div class="list-pg-guar-img"> <img src="<?php echo base_url(); ?>assets/frontend/images/icon/g1.png" alt="" /> </div>
											<h4>Service Guarantee</h4>
											<p>Upto 6 month of service</p>
										</li>
										<li>
											<div class="list-pg-guar-img"> <img src="<?php echo base_url(); ?>assets/frontend/images/icon/g2.png" alt="" /> </div>
											<h4>Professionals</h4>
											<p>100% certified professionals</p>
										</li>
										<li>
											<div class="list-pg-guar-img"> <img src="<?php echo base_url(); ?>assets/frontend/images/icon/g3.png" alt="" /> </div>
											<h4>Insurance</h4>
											<p>Upto $5,000 against damages</p>
										</li>
									</div>
							</div>
						</div>
						<!--END LISTING DETAILS: LEFT PART 7-->
						
						
					
					</div>
				</div>
			</div>
		</div>
	</section>
	






<!-- Pop Up -->
<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="register" role="dialog">
			<div class="modal-dialog model-lg">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Login</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body">
					
								<a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="" />
								</a>
								<!-- <h4>Login</h4>
								<p>Don't have an account? Create your account. It's take less then a minutes</p> -->
								<form class="s12">
									<div>
										<div class="form-group">
											<label>User name</label>
											<input type="text"  class="form-control">
											
										</div>
									</div>
									<div>
										<div class="form-group">
											<label>Password</label>
											<input type="password" class="form-control">
											
										</div>
									</div>
									<div>
										<div class="form-group">
											<input type="submit" value="Register" class="btn btn-danger"> </div>
									</div>
									<div>
										<div class="input-field s12"> <a href="forgot-pass.html">Forgot password</a> | <a href="<?php echo base_url(); ?>register">Create a new account</a> </div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>


<!-- Pop up -->