<!DOCTYPE html>
<html lang="en">

<head>
	<title>Edu Portal</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<!-- <link rel="shortcut icon" href="images/fav.ico" type="image/x-icon"> -->

	<!-- GOOGLE FONT -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet"> -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<?php echo $add_css; ?>
	<!-- FONTAWESOME ICONS -->
	<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/font-awesome.min.css"> -->
	<!-- ALL CSS FILES -->
	
	<!-- <link href="<?php echo base_url(); ?>assets/frontend/css/custom.css" rel="stylesheet"> -->
	<!-- <link href="<?php echo base_url(); ?>assets/frontend/css/materialize.css" rel="stylesheet"> -->
	<!-- <link href="<?php echo base_url(); ?>assets/frontend/css/style.css" rel="stylesheet"> -->
	<!-- <link href="<?php echo base_url(); ?>assets/frontend/css/bootstrap.css" rel="stylesheet" type="text/css" /> -->
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<!-- <link href="<?php echo base_url(); ?>assets/frontend/css/responsive.css" rel="stylesheet"> -->
	

</head>