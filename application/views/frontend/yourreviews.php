<!--DASHBOARD-->
	<div class="container">
		<div class="tz">
			<!--LEFT SECTION-->
			  <?php include('dashboardSideBar.php'); ?>

			<!--CENTER SECTION-->
			<div class="col-md-9">
				<div class="tz-2-com tz-2-main" style="background: #fff;">
					<h4>Your Reviews</h4>
					<div class="db-list-com tz-db-table">
						<div class="tz-mess">
							<ul>
								 <?php foreach ($userReviews as $key ) { ?>
								 	
							
								<li class="view-msg">
									<!--  <span class="tz-revi-star"><?php echo $key->user_rate; ?><i class="fa fa-star" aria-hidden="true"></i></span> -->
									<h5><a  target="__blank" href="<?php echo base_url(); ?>universityDetails/<?php echo strtolower(str_replace(' ','-',$key->name)).'/'.$key->university_id; ?>"><?php echo $key->name; ?></a></h5>

									   <p><span><?php echo $key->user_rate; ?> <i class="fa fa-star" aria-hidden="true"></i></span>
									<p><?php echo $key->review; ?></p>
									
								</li>
                            <?php 	 } ?>
								
							</ul>
						</div>
						

						
					</div>
				</div>
			</div>



		</div>
	</div>
	<!--END DASHBOARD-->
	