
<section class="tz-register">
    <div class="log-in-pop">
        <div class="log-in-pop-left">
                <h1>Hello... <!-- <span>{{ name1 }}</span> --></h1>
            <p>Don't have an account? Create your account. It's take less then a minutes</p>
            <h4>Login with social media</h4>
            <ul>
                <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a>
                </li>
                <li><a href="#"><i class="fa fa-google"></i> Google+</a>
                </li>
                <li><a href="#"><i class="fa fa-twitter"></i> Twitter</a>
                </li>
            </ul>
        </div>
        <div class="log-in-pop-right">
            <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="" />
            </a>
            <h4>Create an Account</h4>
            <p>Don't have an account? Create your account. It's take less then a minutes</p>
            <form class="s12" name="registration" method="post" action="<?php echo base_url(); ?>home/userInfo">
                <div>
                    <div class="input-field s12">
                        <input type="text" name="firstname" placeholder="firstname" >
                        <label>First name</label>
                    </div>
                </div>
                <div>
                    <div class="input-field s12">
                        <input type="text" name="lastname" placeholder="lastname" >
                        <label>Last name</label>
                    </div>
                </div>
                <div>
                    <div class="input-field s12">
                        <input type="email" name="email" id="emailId" onblur="checkEmail();" autocomplete="off" placeholder="john@doe.com">
                        <label>Email id</label>
                        <span id="msg" style="color:red"></span>
                    </div>
                </div>
                <div>
                    <div class="input-field s12">
                        <input type="password" name="password" id="password" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;"/>
                        <label>Password</label>
                    </div>
                </div>
                <div>
                    <div class="input-field s12">
                        <input type="password" name="cpass" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;" >
                        <label>Confirm password</label>
                    </div>
                </div>
                <div>
                    <div class="input-field s4">
                        <input type="submit" value="Register" class="waves-effect waves-light log-in-btn"> </div>
                </div>
                <div>
                    <div class="input-field s12"> <a href="<?php echo base_url(); ?>login">Are you a already member ? Login</a> </div>
                </div>
            </form>
        </div>
    </div>
</section>


