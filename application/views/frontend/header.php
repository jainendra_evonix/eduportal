
<!DOCTYPE html>
<html lang="en">
<?php // echo "<pre>"; print_r($this->session->userdata('user_id')); exit; ?>
<head>
	<title>Edu Portal</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/favicon.png">
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<?php echo $add_css; ?>
         <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script> -->
	
	

</head>
<body ng-app="postApp" ng-controller="postController as ctrl">
	<!-- start loader -->
	<!-- <div id="preloader">
		<div id="status">&nbsp;</div>
	</div> -->

	<!-- end Loader -->
	<!--TOP SEARCH SECTION-->
	<section class="bottomMenu dir-il-top-fix">
		<div class="container-fluid indexHeader top-search-main">
			<div class="row">
				<div class="ts-menu">
					<!--SECTION: LOGO-->
					<div class="ts-menu-1">
						<a href="<?php echo base_url(); ?>home"><img src="<?php echo base_url(); ?>assets/images/icone.png" width="auto" height="58px;" alt=""> </a>
					</div>
					<!--SECTION: BROWSE CATEGORY(NOTE:IT'S HIDE ON MOBILE & TABLET VIEW)-->
				
					<!--SECTION: SEARCH BOX-->
					<div class="ts-menu-3">
						<div class="">
							  <form class="tourz-search-form" action="<?php echo base_url(); ?>homesearch" method="get">
						
							      <select name="type"  id="select-search" value="" >
								<option value="" selected="selected">Select</option>
								<option value="university">University</option>
								<option value="college">College</option>
								
							</select>
							
						<div class="input-field">
							<div class="frmSearch" style="width: 100%;">
								<input type="text" id="search-box" class="form-control" placeholder="University/College Name" name="searchedKeyword" style="height: 56px; background: #fff;">
								<div id="suggesstion-box" style="width: 100%; text-align: -webkit-auto; z-index: 1;"></div>
							</div>
						</div>
						<div class="input-field">
							<input type="submit" value="search" class="waves-effect waves-light tourz-sear-btn"> </div>
					</form>
						</div>
					</div>
					<!--SECTION: REGISTER,SIGNIN AND ADD YOUR BUSINESS-->
					<div class="ts-menu-4">
						<div class="v3-top-ri" id="addButton">
							<ul> <?php if(!$this->session->userdata('user_id')){  ?>

								<li><a href="<?php echo base_url(); ?>login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Sign In</a> </li>
							<?php } else{  ?>
                                  <li><a href="<?php echo base_url(); ?>logout" class="v3-menu-sign"><i class="fa fa-sign-in"></i>Log out</a> </li>
							<?php } ?>
								<!-- <li><a href="db-listing-add.php" class="v3-add-bus"><i class="fa fa-plus" aria-hidden="true"></i> Add Listing</a> </li> -->
							</ul>
						</div>
					</div>

					<!--MOBILE MENU ICON:IT'S ONLY SHOW ON MOBILE & TABLET VIEW-->
					<div class="ts-menu-5"><span><i class="fa fa-bars" aria-hidden="true"></i></span> </div>
					<!--MOBILE MENU CONTAINER:IT'S ONLY SHOW ON MOBILE & TABLET VIEW-->
					<div class="mob-right-nav" data-wow-duration="0.5s">
						<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
						<h5>Log In</h5>
						<ul class="mob-menu-icon">
							<li><a href="<?php echo base_url(); ?>register" data-toggle="modal" data-target="#register">Register</a> </li>
						   <li><a href="<?php echo base_url(); ?>login" >Sign In</a> </li>
					    </ul>
					</div>
				</div>
			</div>
		</div>
	</section>