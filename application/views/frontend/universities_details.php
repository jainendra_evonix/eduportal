<?php 
error_reporting(0);
if($this->session->userdata('universityReviewsData')){
$reviewData = $this->session->userdata('universityReviewsData');
}

 if(!empty($reviewData))
 {
    //echo "<pre>"; print_r($reviewData); exit;
   $uniId =   isset($reviewData) && !empty($reviewData['university_id']) ? $reviewData['university_id'] : '';
   $user_rate =   isset($reviewData) && !empty($reviewData['user_rate']) ? $reviewData['user_rate'] : '';
   $fullname =   isset($reviewData) && !empty($reviewData['fullname']) ? $reviewData['fullname'] : '';
   $email_id =   isset($reviewData) && !empty($reviewData['email_id']) ? $reviewData['email_id'] : '';
   $mobile_no =   isset($reviewData) && !empty($reviewData['mobile_no']) ? $reviewData['mobile_no'] : '';
   $city =   isset($reviewData) && !empty($reviewData['city']) ? $reviewData['city'] : '';
   $review =   isset($reviewData) && !empty($reviewData['review']) ? $reviewData['review'] : '';

 }
 



   ?>
 <input type="hidden" id="uniId" value="<?php  echo $uniId; ?>">
 <input type="hidden" class="userRate" value="<?php  echo $user_rate; ?>">
 <input type="hidden" id="fullname" value="<?php  echo $fullname; ?>">
 <input type="hidden" id="email_id" value="<?php  echo $email_id; ?>">
 <input type="hidden" id="mobile_no" value="<?php  echo $mobile_no; ?>">
 <input type="hidden" id="review" value="<?php  echo $review; ?>">
 <input type="hidden" id="city" value="<?php  echo $city; ?>">

<section >
    <div class="v3-list-ql">
        <div class="container">
            <div class="row">
                <div class="v3-list-ql-inn">
                    <ul>
                        <li class="active"><a href="#info"><i class="fa fa-user"></i> About</a>
                        </li>
                        <li class="active"><a href="#ld-abour"><i class="fa fa-book"></i> Courses</a>
                        </li>
                        <li><a href="#ld-ser"><i class="fa fa-university"></i> Colleges</a>
                        </li>
                        <li><a href="#ld-gal"><i class="fa fa-photo"></i>  Gallery  </a>
                        </li>
                        <!-- <li><a href="#ld-vie"><i class="fa fa-street-view"></i> 360 View</a>
                        </li> -->
                        <li><a href="#ld-rew"><i class="fa fa-edit"></i> Write Review</a>
                        </li>
                        <li><a href="#contact"><i class="fa fa-paper-plane-o"></i> Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--LISTING DETAILS-->

<section class="list-pg-bg" style="margin-top: 80px;">
    <div class="container">
        <div class="row" style="padding-top:40px">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a> </li>
                <li class="active"><?php echo $universityInfo->name; ?></li>
            </ol>
        </div>

        <div class="row " >
            <div class="well" id="info">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="mb10"><img src="<?php echo base_url(); ?>uploads/logo/universities/<?php echo $universityInfo->logo_img; ?>" width="35px"> <?php echo $universityInfo->name; ?></h2>
                         <!-- <h4 class="mb10">115 Distance/Correspondence Diploma courses in India Offering 281 Courses</h4> -->
                        <p><?php echo html_entity_decode($universityInfo->about,ENT_QUOTES, "UTF-8"); ?></p>

                        <!-- <div class="list-room-type list-rom-ami">
                            <ul>
                                <li><img src="<?php echo base_url(); ?>uploads/banner/universites/<?php echo $universityInfo->banner_img; ?>" alt="">
                                    Colleges <span class="badge"><?php echo count($allColleges);  ?></span></li>

                                <li><img src="<?php echo base_url(); ?>assets/images/img/right-arrow.png" alt="">Courses <span class="badge"><?php echo count($allCourses); ?></span></li>
                            </ul>
                        </div> -->	
                    </div>

                    <div class="col-md-4">
                        <img src="<?php echo base_url(); ?>uploads/banner/universities/<?php echo $universityInfo->banner_img; ?>" class="img-thumbnail">
                        <div class="cus-ul">
                            <ul>
                                <li>
                                    <?php echo $universityInfo->approved_by;  ?> Approved </li>

                                <li> <?php echo $universityInfo->accredited_name; ?> </li>
                            </ul>
                        </div>

                    </div>
                </div>




            </div>	
        </div>	

        <div class="row">
            <div class="">
                <div class="list-pg-lt list-page-com-p">
                    <!--Courses-->
                    <div class="pglist-p1 pglist-bg pglist-p-com" id="ld-abour">
                        <div class="pglist-p-com-ti">
                                <h3>Courses</h3> 
                        </div>

                        <?php  //echo "<pre>"; print_r($allCourses); exit; ?>

                        <div class="col-md-12">
                            <span><strong>Browse by Courses: </strong></span>
                            <?php foreach($allCourses as $key => $val) { ?>
                            <a href="<?php echo base_url(); ?>university/<?php echo strtolower(str_replace(' ', '-',$universityInfo->name)).'/course/'.strtolower($val->course_name).'/'.strtolower(str_replace(' ','-',$universityInfo->id)).'/'.$val->tblUniColgCourseId; ?>" class="custom-ancr"><?php echo $val->course_name; ?></a>
                            <?php } ?>
                             
                        </div>
                 <hr>
         <?php //echo "<pre>"; print_r($allCourses); exit; ?>
                        <div class="col-md-12">
                            <span><strong>Browse by Streams: </strong></span>
                            <?php foreach ($allCourses as $key => $val) {  ?>
                               <a href="<?php echo base_url(); ?>university/<?php echo strtolower(str_replace(' ', '-',$universityInfo->name)).'/'.strtolower(str_replace(' ','-',$universityInfo->id)).'/'.'stream/'.strtolower($val->course_stream).'/'.$val->tblUniColgCourseId; ?>" class="custom-ancr"><?php echo $val->course_stream; ?></a>

                               <!-- <a href="<?php echo base_url(); ?>university/<?php echo strtolower(str_replace(' ', '-',$universityInfo->name)).'/stream/'.strtolower($val->course_stream).'/'.strtolower(str_replace(' ','-',$universityInfo->id)).'/'.$val->tblUniColgCourseId; ?>" class="custom-ancr"><?php echo $val->course_stream; ?></a> -->
                           <?php } ?>
                        </div>
                        <hr>	

                        <div class="list-pg-inn-sp">
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p> -->


                        </div>
                    </div>

                    <!--END Courses-->


                    <!--Colleges-->
                    <div class="pglist-p2 pglist-bg pglist-p-com " id="ld-ser">
                        <div class="pglist-p-com-ti">
                            <h3>Colleges</h3> 
                        </div>

                        <div class="list-pg-inn-sp row college-view-row">

                            <?php foreach ($allColleges as $key){ 
                                $uName = preg_replace('/[^A-Za-z0-9\-\']/', '-', $universityInfo->name); 
                              $cName= preg_replace('/[^A-Za-z0-9\-\']/', '-', $key->name); ?>
                            <div class="col-md-4 college-view">
                                <a href="<?php echo base_url() ?><?php echo 'university/'.strtolower($uName).'/'.'college/'.strtolower($cName).'/'.$key->id; ?>">
                                    <h3><img src="<?php echo base_url(); ?>assets/images/img/University-logo/Symbiosis.jpeg" class="title-img"> <?php echo $key->name.' '.'-'.'['.$key->shortname.']'.','.$key->city; ?></h3>
                                </a>
                            </div>

                            <?php } ?>

                             
                            
                            
                        </div>
                        <div class="text-center">
                                <a href="<?php echo base_url(); ?>india-universites" class="btn views_more ">View more</a>
                            </div>  
                        <div class="text-center row">

                        </div>
                    </div>
                    <!--END Colleges-->



                    <!-- Ad Box -->
                    <div class="col-md-12 fillter-ad1-728">
                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/728x90.jpg" alt="">
                    </div>
                    <!-- Ad Box -->



                    <!--Gallery-->
                    <div class="pglist-p3 pglist-bg pglist-p-com" id="ld-gal" style="overflow: auto;">
                        <div class="pglist-p-com-ti">
                                <h3><!-- <span>Photo</span>  -->Gallery</h3> </div>
                        <div class="list-pg-inn-sp">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel" data-slide-to="1"></li>
                                    <li data-target="#myCarousel" data-slide-to="2"></li>
                                    <li data-target="#myCarousel" data-slide-to="3"></li>
                                </ol>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <div class="item active"> <img src="<?php echo base_url(); ?>assets/frontend/images/slider/1.jpg" alt="Los Angeles"> </div>
                                    <div class="item"> <img src="<?php echo base_url(); ?>assets/frontend/images/slider/2.jpg" alt="Chicago"> </div>
                                    <div class="item"> <img src="<?php echo base_url(); ?>assets/frontend/images/slider/3.jpg" alt="New York"> </div>
                                    <div class="item"> <img src="<?php echo base_url(); ?>assets/frontend/images/slider/4.jpg" alt="New York"> </div>
                                </div>
                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <i class="fa fa-angle-left list-slider-nav" aria-hidden="true"></i> </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next"> <i class="fa fa-angle-right list-slider-nav list-slider-nav-rp" aria-hidden="true"></i> </a>
                            </div>
                        </div>
                    </div>
                    <!--END Gallery-->

                    <!-- Ad Box -->
                    <div class="col-md-12 fillter-ad1-728">
                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/728x90.jpg" alt="">
                    </div>
                    <!-- Ad Box -->
                         
                          <?php 
                           $max = 0;
                                  $n = count($userUniversityReview); // get the count of comments
                                
                                  foreach ($userUniversityReview as $rate ) { // iterate through array
                                  $max = $max+$rate->user_rate;
                                  } ?>
                    <!-- Reviews -->
                    <div class="pglist-p3 pglist-bg pglist-p-com" id="ld-rew" style="overflow: auto;">
                        <div class="pglist-p-com-ti">
                            <h3><span>Write Your</span> Reviews</h3> </div>
                        <div class="list-pg-inn-sp">
                            <div class="list-pg-write-rev">
                               <form class="col" name="userReviewForm" ng-submit="formSubmit()" novalidate>
                                    <p>Writing great reviews may help others discover the places that are just apt for them. Here are a few tips to write a good review:</p>
                                    <div class="row">
                                        <div class="col s12">
                                            <div class="click-callback" ></div>
                                            <input type="hidden" class="userRate" name="userrate" ng-model="userReview.userrate" >
                                            <input type="hidden" id="universityId" name="universityId"  value="<?php echo $universityInfo->id; ?>" >
                                            <input type="hidden" id="universityName" name="universityName"  value="<?php echo $universityInfo->name; ?>" >
                                            

                                            

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input  type="text" value="" name="fullname" ng-model="userReview.fullname" class="form-control" required>
                                            <label for="re_name">Full Name</label>
                             <span ng-show="submitted && userReviewForm.fullname.$invalid" class="help-block has-error warnig ng-hide">Full name is required.</span>
                            <span class="help-block has-error ng-hide warnig" ng-show="fullnameError">{{fullnameError}}</span>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="re_mob" type="text" value="" name="mobile" ng-model="userReview.mobile" class="form-control" required>
                                            <label for="re_mob">Mobile</label>
                                            <span ng-show="submitted && userReviewForm.mobile.$invalid" class="help-block has-error warnig ng-hide">Mobile No. is required.</span>
                                            <span class="help-block has-error ng-hide warnig" ng-show="mobileError">{{mobileError}}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input id="re_mail" type="email" value="" name="emailId" ng-model="userReview.emailId" class="form-control" required>
                                            <label for="re_mail">Email id</label>
                                             <span ng-show="submitted && userReviewForm.emailId.$invalid" class="help-block has-error warnig ng-hide">Email-id is required.</span>
                                            <span class="help-block has-error ng-hide warnig" ng-show="emailIdError">{{emailIdError}}</span>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="re_city" type="text" value="" name="city" ng-model="userReview.city" class="form-control" required>
                                            <label for="re_city">City</label>
                                            <span ng-show="submitted && userReviewForm.city.$invalid" class="help-block has-error warnig ng-hide">City name is required.</span>
                                            <span class="help-block has-error ng-hide warnig" ng-show="cityError">{{cityError}}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <textarea id="re_msg" class="materialize-textarea" name="review" ng-model="userReview.review" value="" required></textarea>
                                            <label for="re_msg">Write review</label>
                                             <span ng-show="submitted && userReviewForm.review.$invalid" class="help-block has-error warnig ng-hide">Review is required.  </span>
                                            <span class="help-block has-error ng-hide warnig" ng-show="reviewError">{{reviewError}}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12"> 
                                          
                                         <button type="submit" value="Submit"   ng-click="submitted=true" class="btn-large full-btn">Submit</button>
                                   
                                     </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
             
                    <div class="pglist-p3 pglist-bg pglist-p-com" id="ld-rer">
                        <div class="pglist-p-com-ti">
                            <h3><span>User</span> Reviews</h3> </div>
                        <div class="list-pg-inn-sp">
                            <div class="lp-ur-all">

                                <div class="lp-ur-all-right">
                                    <h5>Overall Ratings</h5>
                                    <p><span><?php echo round($max/$n); ?> <i class="fa fa-star" aria-hidden="true"></i></span> based on <?php echo $n;  ?> reviews</p>
                                </div>
                            </div>
                            <div class="lp-ur-all-rat">
                                <h5>Reviews</h5>
                                <ul>
                                      <?php $i=0; foreach ($userUniversityReview as $key) { $i++; ?>
                                 
                                    <li>
                                        <div class="lr-user-wr-img"> <img src="images/users/2.png" alt=""> </div>

                                        <div class="lr-user-wr-con">
                                            <h6><?php echo $key->fullname; ?> <span><?php echo $key->user_rate; ?> <i class="fa fa-star" aria-hidden="true"></i></span></h6> <span class="lr-revi-date"><?php echo date('d M Y',strtotime($key->created_date)); ?></span>
                                            <p><?php echo $key->review; ?> </p>

                                        </div>
                                    </li>
                               <?php  if($i==5) break; } ?>

                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- End Reviews -->

                    <!-- Ad Box -->
                    <div class="col-md-12 fillter-ad1-728">
                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/728x90.jpg" alt="">
                    </div>
                    <!-- Ad Box -->


                    <!-- Contact -->
                    <div class="pglist-p3 pglist-bg pglist-p-com" id="contact" style="overflow: auto;">
                        <div class="pglist-p-com-ti">
                            <h3><span>Contact</span> Us</h3> </div>
                        <div class="list-pg-inn-sp">
                            <div class="list-pg-write-rev">
                                <div class="col-md-4 text-center">
                                    <h4><i class="fa fa-paper-plane-o"></i> Address:</h4>
                                    <hr>
                                    <p> Ganeshkhind Road,<br> Pune ( Maharashtra)</p>
                                </div>

                                <div class="col-md-4 text-center">
                                    <h4><i class="fa fa-globe"></i> Website:</strong> </h4>
                                    <hr>
                                    <p><a href="<?php echo $universityInfo->url; ?>" target="_blank"> <?php echo $universityInfo->url; ?></a></p>
                                </div>

                                <div class="col-md-4 text-center">
                                    <h4><i class="fa fa-phone"></i> Phone:</strong> </h4>
                                    <hr>

                                    <a href="#!" data-dismiss="modal" data-toggle="modal" data-target="#register" class="btn btn-primary"> Contact Us </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--End Contact  -->

                    <!-- Ad Box -->
                    <div class="col-md-12 fillter-ad1-728">
                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/728x90.jpg" alt="">
                    </div>
                    <!-- Ad Box -->


                </div>	

                <!--  Right Side bar -->
                <div class="list-pg-rt">
                    <!--LISTING DETAILS: LEFT PART 7-->
                    <div class="pglist-p3 pglist-bg pglist-p-com">
                        <div class="pglist-p-com-ti pglist-p-com-ti-right">
                            <h3><span>Listing</span> Colleges</h3> </div>
                        <div class="list-pg-inn-sp">
                            <div class="list-pg-guar">
                                <ul>
                                    <li>
                                        <div class="list-pg-guar-img"> <img src="<?php echo base_url(); ?>assets/frontend/images/icon/g1.png" alt="" /> </div>
                                        <h4>Service Guarantee</h4>
                                        <p>Upto 6 month of service</p>
                                    </li>
                                    <li>
                                        <div class="list-pg-guar-img"> <img src="<?php echo base_url(); ?>assets/frontend/images/icon/g2.png" alt="" /> </div>
                                        <h4>Professionals</h4>
                                        <p>100% certified professionals</p>
                                    </li>
                                    <li>
                                        <div class="list-pg-guar-img"> <img src="<?php echo base_url(); ?>assets/frontend/images/icon/g3.png" alt="" /> </div>
                                        <h4>Insurance</h4>
                                        <p>Upto $5,000 against damages</p>
                                    </li>
                            </div>
                        </div>
                    </div>
                    <!--END LISTING DETAILS: LEFT PART 7-->

                    <div class="fillter-ad">
                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/ad1.jpg" alt="">

                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/300x600.jpg" alt="">

                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/300x250.jpg" alt="">
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>







<!-- Pop Up -->
<section>
    <!-- GET QUOTES POPUP -->
    <div class="modal fade dir-pop-com" id="register" role="dialog">
        <div class="modal-dialog model-lg">
            <div class="modal-content">
                <div class="modal-header dir-pop-head">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Login</h4>
                    <!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
                </div>
                <div class="modal-body">

                    <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="" />
                    </a>
                    <!-- <h4>Login</h4>
                    <p>Don't have an account? Create your account. It's take less then a minutes</p> -->
                    <form class="s12">
                        <div>
                            <div class="form-group">
                                <label>User name</label>
                                <input type="text"  class="form-control">

                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control">

                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <input type="submit" value="Register" class="btn btn-danger"> </div>
                        </div>
                        <div>
                            <div class="input-field s12"> <a href="forgot-pass.html">Forgot password</a> | <a href="<?php echo base_url(); ?>register">Create a new account</a> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- GET QUOTES Popup END -->
</section>


<!-- Pop up -->

