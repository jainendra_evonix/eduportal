<?php  error_reporting(0);
  // echo "<pre>page"; print_r($universityInfo); exit;   ?>
<section class=" dir-pa-sp-top">
    <div class="container">
        <div class="row" style="padding-top:20px">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a> </li>
                <li class="active"><?php echo $universityInfo->name; ?></li>
            </ol>
        </div>

        <div class="row " >
            <div class="well" id="info">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="mb10"><img src="<?php echo base_url(); ?>assets/frontend/images/img/University-logo/Symbiosis.jpeg" width="35px"> <?php echo $universityInfo->name; ?></h2>
                         <input type="hidden" id="univrID" name="uiverID" value="<?php echo $universityInfo->id; ?>">
                            <input type="hidden" id="uniName" name="uniName" value="<?php echo $universityInfo->name; ?>">

                         <p><?php echo html_entity_decode($universityInfo->about,ENT_QUOTES, "UTF-8"); ?></p>

                        <div class="list-room-type list-rom-ami">
                            <ul>
                                <li><img src="<?php echo base_url(); ?>assets/images/img/right-arrow.png" alt="">
                                    Colleges <span class="badge">5</span></li>

                                <li><img src="<?php echo base_url(); ?>assets/images/img/right-arrow.png" alt=""> Courses <span class="badge">15</span></li>
                            </ul>
                        </div>	
                    </div>

                    <div class="col-md-4">
                        <img src="<?php echo base_url(); ?>uploads/banner/universities/<?php echo $universityInfo->banner_img; ?>" class="img-thumbnail">
                        <div class="cus-ul">
                            <ul>
                                <li>
                                   <?php echo $universityInfo->approved_by; ?> </li>

                                <li> <?php echo $universityInfo->accredited_name; ?> </li>
                            </ul>
                        </div>

                    </div>
                </div>




            </div>	
        </div>	

        <div class="row">
            <div class="well col-md-12">
                <div class="col-md-3 dir-alp-con-left sidebar">
                    <!-- <div class="dir-alp-con-left-1">
                            <h3>Nearby Listings(07)</h3> </div> -->
                    <!-- hello -->
                    <div class="dir-alp-l3 dir-alp-l-com ">
                        <h4>Location</h4>


                        <?php // echo "<pre>"; print_r($cities); ?>
                        <div class="dir-alp-l-com1 dir-alp-p3">
                            <form action="#">
                                <ul>

                                    <?php error_reporting(0);

                                    foreach ($cities as $key) {
                                        ?>

                                        <div class="location" id="location">	
                                            <li>
                                                <input type="checkbox" id="<?php echo 'loc' . $key->id; ?>" <?php
                                                       if (in_array($key->city, str_replace('-',' ',$loc))) {
                                                           echo 'checked';
                                                       }
                                                       ?> value="<?php echo $key->city; ?>"  />

                                                <label for="<?php echo 'loc' . $key->id; ?>"><?php echo $key->city ?></label>
                                            </li>
                                            <input type="hidden" class="<?php echo 'location' . $key->city; ?>" value="<?php echo $key->id; ?>" >
                                        </div>
<?php } ?>

                                </ul>
                            </form> 
                        </div>
                    </div>
                    <!-- Location -->


                    <div class="dir-alp-l3 dir-alp-l-com">
                        <h4>Course Level</h4>
                        <div class="dir-alp-l-com1 dir-alp-p3">
                            <form action="#">
                                <ul>
<?php foreach ($courses as $key) { ?>

                                        <div class="courses" id="courses">
                                            <li>
                                                <input type="checkbox" id="<?php echo 'cour' . $key->id; ?>" value="<?php echo $key->course_type; ?>" <?php if (in_array($key->course_type, $cour)) {
        echo 'checked';
    }
    ?> />
                                                <label for="<?php echo 'cour' . $key->id; ?>"><?php echo $key->course_type; ?></label>
                                            </li>
                                        </div>
<?php } ?>
                                </ul>
                            </form> 
                        </div>
                    </div>


                    <div class="dir-alp-l3 dir-alp-l-com">
                        <h4>Specialization</h4>
                        <div class="dir-alp-l-com1 dir-alp-p3">

                            <ul>
                                <div class="specialization" id="specialization">
                                    <?php foreach ($specialization as $key) { ?>
                                        <li>
                                            <input type="checkbox" id="<?php echo 'spe' . $key->id ?>" value="<?php echo $key->course_name; ?>" <?php if (in_array($key->course_name, $spec)) {
                                            echo 'checked';
                                        }
                                        ?> />
                                            <label for="<?php echo 'spe' . $key->id ?>"><?php echo $key->course_name; ?></label>
                                        </li>
<?php } ?>
                                </div>
                            </ul>                        
                        </div>
                    </div>


                    <div class="fillter-ad">
                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/ad1.jpg" alt="">

                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/300x600.jpg" alt="">

                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/300x250.jpg" alt="">
                    </div>


                </div>
                <!-- fillters -->

                <div id="searchedUniversities">

                    <div class="col-md-9 dir-alp-con-right nopadding">
                        <div class="dir-alp-con-right-1">
                            <div class="row">

                                 

                                    <?php
                                          // echo "<pre>"; print_r($universities); exit;

                                    $i = 0;
                                    foreach ($colleges as $key) {
                                        $i++;
                                        ?>

                                                                        <!--LISTINGS-->
                                        <?php if ($i == 7 || $i == 11) { ?>

                                        <!-- Ad Box -->
                                        <div class="col-md-12 fillter-ad-728">
                                            <img src="<?php echo base_url(); ?>assets/frontend/images/img/728x90.jpg" alt="">
                                        </div>
                                        <!-- Ad Box -->

                                     <?php } ?>

                                    <div class="col-md-6" style="min-height: 390px;">
                                        <div class="home-list-pop list-spac list-spac-1 list-room-mar-o col-md-12">
                                            <div class="col-md-12 nopadding"> <img src="<?php echo base_url(); ?>uploads/banner/colleges/<?php echo $key->bannerName; ?>" alt=""> </div>

                                            <div class="col-md-12 nopadding home-list-pop-desc inn-list-pop-desc list-room-deta"> 
                               <a href="<?php echo base_url(); ?>universityDetails/<?php echo strtolower(str_replace(' ','-',$key->colgName)).'/'.$key->uId; ?>"><h3><img src="<?php echo base_url(); ?>/uploads/logo/colleges/<?php echo $key->logoname; ?>" class="title-img"><?php echo $key->colgName; ?></h3></a> 
                                            </div>	

                                            <small><i class="fa fa-university"></i><?php echo $key->city.', '.$key->g_name.', '.$key->accreditions; ?></small>

                                            <div class="list-room-type list-rom-ami">
                                                <ul>
                                                  
                                                    <li><img src="<?php echo base_url(); ?>/assets/frontend/images/img/right-arrow.png" alt=""> Full Time</li>
                                                    <li><img src="<?php echo base_url(); ?>/assets/frontend/images/img/right-arrow.png" alt=""> All Course </li>
                                                    <li><img src="<?php echo base_url(); ?>/assets/frontend/images/img/right-arrow.png" alt=""> Other</li>
                                                   
                                                </ul>
                                            </div> 
                                            <div class="list-enqu-btn">
                                                <ul>
                                                    <li><a href="universities_details.php"><i class="fa fa-file-text-o"></i> Apply Now</a> </li>

    
                                                        <li><a  data-dismiss="modal" data-toggle="modal" data-target="#register"><i class="fa fa-download"></i> Brochure</a> </li>
   
                                                </ul>
                                            </div>

                                        </div>
                                    </div>




<?php } ?>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="Ad">
    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-12 nopadding">
                <img src="<?php echo base_url(); ?>assets/frontend/images/img/ad.jpg" width="100%" height="auto">
            </div>
        </div>
</section>







<!-- Pop Up -->
<section>
  
    <div class="modal fade dir-pop-com" id="register" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header dir-pop-head">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Enter your details </h4>
                </div>
                <div class="modal-body" style="background:linear-gradient(0deg,rgba(109,81,184,0.5),rgba(255,255,255,0.5)),url('<?php echo base_url(); ?>assets/frontend/images/login-bg.jpg');>
                     
                    

                    <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="" />
                    </a>
                                                             
                    <form class="s12" name="instantRegister" ng-submit="submitForm()" novalidate>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Full Name</label>
                                <input type="text"  class="form-control" ng-model="guest.fullname" name="fullname" value="" required>
          <span ng-show="submitted && instantRegister.fullname.$invalid" class="help-block has-error warnig ng-hide">Full name is required.</span>
          <span class="help-block has-error ng-hide warnig" ng-show="fullnameError">{{fullnameError}}</span>

                            </div>
                        </div>
                        <div  class="row">
                            <div class="form-group col-md-6">
                                <label>Email</label>
                                <input type="email" class="form-control" ng-model="guest.email" name="email" value="" required>
                                <span ng-show="submitted && instantRegister.email.$invalid" class="help-block has-error warnig ng-hide">Valid Email ID is required.</span>
                                 <span class="help-block has-error ng-hide warnig" ng-show="emailError">{{emailError}}</span>

                            </div>
                            <div class="form-group col-md-6">
                                <label>Mobile Number</label>
                                <input type="email" class="form-control" ng-model="guest.mobileNo" name="mobileNo" value="" ng-pattern="/^[0-9]{10,10}$/;" required>
                               <span ng-show="submitted && instantRegister.mobileNo.$invalid" class="help-block has-error warnig ng-hide">Mobile Number is required</span>
                                 <span class="help-block has-error ng-hide warnig" ng-show="mobileNoError">{{mobileNoError}}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Select your city</label>
                                <input type="text"  class="form-control" ng-model="guest.city" name="city" value="" required>
          <span ng-show="submitted && instantRegister.city.$invalid" class="help-block has-error warnig ng-hide">City is required.</span>
          <span class="help-block has-error ng-hide warnig" ng-show="cityError">{{cityError}}</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Select a course</label>
                                <input type="text"  class="form-control" ng-model="guest.course" name="course" value="" required>
          <span ng-show="submitted && instantRegister.course.$invalid" class="help-block has-error warnig ng-hide">City is required.</span>
          <span class="help-block has-error ng-hide warnig" ng-show="courseError">{{courseError}}</span>
                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <input type="submit" ng-click="submitted = true" value="Submit" class="btn btn-danger"> </div>
                        </div>
                        <div>
                            <div class="input-field s12"> <a href="forgot-pass.html">Forgot password</a> | <a href="<?php echo base_url(); ?>register">Create a new account</a> </div>
                        </div>
                    </form>
                        
                   
                </div>
            </div>
        </div>
    </div><!-- model close -->
</div>
</div>
</section>