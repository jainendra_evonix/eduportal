<!--DASHBOARD-->
	<div class="container">
		<div class="tz">
			<!--LEFT SECTION-->
			 <?php include('dashboardSideBar.php'); ?>

			<!--CENTER SECTION-->
			<div class="col-md-9">
				<div class="tz-2-com tz-2-main" style="background: #fff;">
					<h4>Manage Profile</h4>
					<div class="db-list-com tz-db-table">
						<div class="form-group row">
							<label class="col-md-3">User Name</label>
							<div class="col-md-9">
								<input type="text" name="" value="<?php echo $userdata->first_name.' '.$userdata->last_name; ?>" class="form-control" >
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3">Email</label>
							<div class="col-md-9">
								<input type="email" name=""  value="<?php echo $userdata->user_email; ?>" class="form-control" >
							</div>
						</div>	
						<div class="form-group row">
							<label class="col-md-3">Phone</label>
							<div class="col-md-9">
								<input type="text" name="" class="form-control" >
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3">Date of Birth</label>
							<div class="col-md-9">
								<input type="text" name="" class="form-control" >
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3">Address</label>
							<div class="col-md-9">
								<input type="text" name="" class="form-control" >
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3">Pin Code</label>
							<div class="col-md-9">
								<input type="text" name="" class="form-control" >
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3">Message</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="5"></textarea>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3">&nbsp;</div>
							<div class="db-mak-pay-bot col-md-4">
								<a href="db-my-profile-edit.html" class="waves-effect waves-light btn-large">Edit my profile</a> 
							</div>
						</div>
					</div>
				</div>
			</div>



		</div>
	</div>
	<!--END DASHBOARD-->
	