<?php   ?>
<section class="tz-register">
    <div class="log-in-pop">
        <div class="log-in-pop-left">
                <h1>Hello... <!-- <span>{{ name1 }}</span> --></h1>
            <p>Don't have an account? Create your account. It's take less then a minutes</p>
            <h4>Login with social media</h4>
            <ul>
                <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a>
                </li>
                <li><a href="#"><i class="fa fa-google"></i> Google+</a>
                </li>
                <li><a href="#"><i class="fa fa-twitter"></i> Twitter</a>
                </li>
            </ul>
        </div>
        <div class="log-in-pop-right">
            <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="" />
            </a>
            <h4>Login</h4>
            <p>Don't have an account? Create your account. It's take less then a minutes</p>
            <form class="s12" name="userLogin" ng-submit="submitForm()" novalidate>
                <div>
                    <div class="input-field s12">
                        <input type="text"  class="validate" id="emailaddress" ng-model="user.emailaddress" name="emailaddress" required >
                        <label></label>
          <span ng-show="submitted && userLogin.emailaddress.$invalid" class="help-block has-error warnig ng-hide">Valid Email ID is required.</span>
          <span class="help-block has-error ng-hide warnig" ng-show="emailaddressError">{{emailaddressError}}</span>
                    </div>
                </div>
                <div>
                    <div class="input-field s12">
                        <input type="password" class="validate" id="password" ng-model="user.password" name="password" required>
                        <label></label>
                        <span ng-show="submitted && userLogin.password.$error.required"  class="help-block has-error warnig ng-hide">Password is required.</span>
               <span class="help-block has-error ng-hide warnig" ng-show="passwordError">{{passwordError}}</span>
                    </div>
                </div>
                <div>
                    <div class="input-field s4">
                        <input type="submit" value="Sing In" ng-click="submitted = true" class="waves-effect waves-light log-in-btn"> </div>
                </div>
                <div>
                    <div class="input-field s12"> <a href="forgot-pass.html">Forgot password</a> | <a href="<?php echo base_url(); ?>register">Create a new account</a> </div>
                </div>
            </form>
        </div>
    </div>
</section>



