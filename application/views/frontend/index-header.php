<?php //  echo "<pre>"; print_r($this->session->userdata('user_id')); exit; ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Edu Portal</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<?php echo $add_css; ?>
	
</head>
<body ng-app="myApp" ng-controller="myController as ctrl">
	<!--PRE LOADING-->
	<!-- <div id="preloader">
		<div id="status">&nbsp;</div>
	</div> -->
	<!--BANNER AND SERACH BOX-->
	<!-- <section id="background" class="dir1-home-head">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="dir-ho-tl">
						<ul>
							<li>
								<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/icone.png" width="auto" height="80px" alt=""> </a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="dir-ho-tr">
						<ul>
							<?php if(!$this->session->userdata('user_id')){ ?>
							<li><a href="<?php echo base_url(); ?>register">Register</a></li>
							
							<li><a href="<?php echo base_url(); ?>login">Sign In</a> </li>
						<?php }else { ?>
							<li><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
							<li><a href="<?php echo base_url(); ?>logout">Log out</a></li>
						<?php  }?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container dir-ho-t-sp">
			<div class="row">
				<div class="dir-hr1">
					<div class="dir-ho-t-tit">
						<h1>Your Career Path Begins Here</h1> 
						<p>Find colleges contact addresses, phone numbers<br> using eduportal</p>
					</div>
					<form class="tourz-search-form" action="<?php echo base_url(); ?>homesearch" method="get">
							<select name="type"  id="select-search" value="" >
								<option value="" selected="selected">Select</option>
								<option value="university">University</option>
								<option value="college">College</option>
							</select>
						<div class="input-field">
							<div class="frmSearch" style="width: 100%;">
								<input type="text" id="search-box" class="form-control" placeholder="University/College Name" name="searchedKeyword" style="height: 56px; background: #fff;">
								<div id="suggesstion-box" style="width: 100%; text-align: -webkit-auto; z-index: 1;"></div>
							</div>
						</div>
						<div class="input-field">
							<input type="submit" value="search" class="waves-effect waves-light tourz-sear-btn"> </div>
					</form>
				</div>
			</div>
		</div>
	</section> -->
	<!--TOP SEARCH SECTION-->
	<section>
         <div class="container-fluid indexHeader top-search-main">
			<div class="row">
				<div class="ts-menu">
					<!--SECTION: LOGO-->
					<div class="ts-menu-1">
						<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/icone.png" alt=""> </a>
					</div>
					<div class="ts-menu-3">
						<div class="">
						<form class="tourz-search-form" action="<?php echo base_url(); ?>homesearch" method="get">
						       <select name="type"  id="select-search" value="" >
										
										<option value="university">University</option>
										<option value="college">College</option>
								</select>
							
						<div class="input-field">
							<div class="frmSearch" style="width: 100%;">
								<input type="text" id="search-box" class="form-control" placeholder="University/College Name" name="searchedKeyword" style="height: 56px; background: #fff;">
								<div id="suggesstion-box" style="width: 100%; text-align: -webkit-auto; z-index: 1;"></div>
							</div>
						</div>
						<div class="input-field">
							<input type="submit" value="search" class="waves-effect waves-light tourz-sear-btn"> </div>
					</form>
						</div>
					</div>
					<!--SECTION: REGISTER,SIGNIN AND ADD YOUR BUSINESS-->
					<div class="ts-menu-4">
						<div class="v3-top-ri">
							<ul><?php if(!$this->session->userdata('user_id')){ ?>
								<li><a href="<?php echo base_url(); ?>login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Sign In</a> </li>
							<?php } ?>
								<!-- <li><a href="db-listing-add.php" class="v3-add-bus"><i class="fa fa-plus" aria-hidden="true"></i> Add Listing</a> </li> -->
							</ul>
						</div>
					</div>

					<!--MOBILE MENU ICON:IT'S ONLY SHOW ON MOBILE & TABLET VIEW-->
					<div class="ts-menu-5"><span><i class="fa fa-bars" aria-hidden="true"></i></span> </div>
					<!--MOBILE MENU CONTAINER:IT'S ONLY SHOW ON MOBILE & TABLET VIEW-->
					<div class="mob-right-nav" data-wow-duration="0.5s">
						<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
						<h5>Log In</h5>
						<ul class="mob-menu-icon">
							<li><a href="<?php echo base_url(); ?>register">Register</a> </li>
							<li><a href="<?php echo base_url(); ?>login" >Sign In</a> </li>
						</ul>
						
					</div>
				</div>
			</div>
		</div>

		  <div id="myCarousel" class="carousel slide" data-ride="carousel">
		    <!-- Indicators -->
		    <ol class="carousel-indicators">
		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		      <li data-target="#myCarousel" data-slide-to="1"></li>
		      <li data-target="#myCarousel" data-slide-to="2"></li>
		    </ol>

		    <!-- Wrapper for slides -->
		    <div class="carousel-inner">
		      <div class="item active">
		        <img src="<?php echo base_url(); ?>assets/frontend/images/slider-01.jpg" alt="Los Angeles" style="width:100%;">
		      </div>

		      <div class="item">
		        <img src="<?php echo base_url(); ?>assets/frontend/images/slider-02.jpg" alt="Chicago" style="width:100%;">
		      </div>
		    
		      <div class="item">
		        <img src="<?php echo base_url(); ?>assets/frontend/images/slider-03.jpg" alt="New york" style="width:100%;">
		      </div>
		    </div>

		    <!-- Left and right controls -->
		    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
		      <span class="glyphicon glyphicon-chevron-left"></span>
		      <span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control" href="#myCarousel" data-slide="next">
		      <span class="glyphicon glyphicon-chevron-right"></span>
		      <span class="sr-only">Next</span>
		    </a>
		  </div>
 </section>

	<section id="myID" class="bottomMenu hom-top-menu">
		<div class="container-fluid indexHeader top-search-main">
			<div class="row">
				<div class="ts-menu">
					<!--SECTION: LOGO-->
					<div class="ts-menu-1">
						<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/icone.png" alt=""> </a>
					</div>
					<div class="ts-menu-3">
						<div class="">
						<form class="tourz-search-form" action="<?php echo base_url(); ?>homesearch" method="get">
						       <select name="type"  id="select-search" value="" >
										
										<option value="university">University</option>
										<option value="college">College</option>
								</select>
							
						<div class="input-field">
							<div class="frmSearch" style="width: 100%;">
								<input type="text" id="search-box" class="form-control" placeholder="University/College Name" name="searchedKeyword" style="height: 56px; background: #fff;">
								<div id="suggesstion-box" style="width: 100%; text-align: -webkit-auto; z-index: 1;"></div>
							</div>
						</div>
						<div class="input-field">
							<input type="submit" value="search" class="waves-effect waves-light tourz-sear-btn"> </div>
					</form>
						</div>
					</div>
					<!--SECTION: REGISTER,SIGNIN AND ADD YOUR BUSINESS-->
					<div class="ts-menu-4">
						<div class="v3-top-ri">
							<ul><?php if(!$this->session->userdata('user_id')){ ?>
								<li><a href="<?php echo base_url(); ?>login" class="v3-menu-sign"><i class="fa fa-sign-in"></i> Sign In</a> </li>
							<?php } ?>
								<!-- <li><a href="db-listing-add.php" class="v3-add-bus"><i class="fa fa-plus" aria-hidden="true"></i> Add Listing</a> </li> -->
							</ul>
						</div>
					</div>

					<!--MOBILE MENU ICON:IT'S ONLY SHOW ON MOBILE & TABLET VIEW-->
					<div class="ts-menu-5"><span><i class="fa fa-bars" aria-hidden="true"></i></span> </div>
					<!--MOBILE MENU CONTAINER:IT'S ONLY SHOW ON MOBILE & TABLET VIEW-->
					<div class="mob-right-nav" data-wow-duration="0.5s">
						<div class="mob-right-nav-close"><i class="fa fa-times" aria-hidden="true"></i> </div>
						<h5>Log In</h5>
						<ul class="mob-menu-icon">
							<li><a href="<?php echo base_url(); ?>register">Register</a> </li>
							<li><a href="<?php echo base_url(); ?>login" >Sign In</a> </li>
						</ul>
						
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- Header -->