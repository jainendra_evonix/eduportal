<?php // echo "<pre>page"; print_r($universities); exit;

     

   ?>
<section class=" dir-pa-sp-top">
    <div class="container">
        <div class="row" style="padding-top:20px">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
                <li class="active">All Universities</li>
            </ol>
        </div>

    
        <div class="row">
            <div class="well col-md-12">
                <div class="col-md-3 dir-alp-con-left sidebar">
                    <div class="dir-alp-l3 dir-alp-l-com ">
                        <h4>Location</h4>
                        <div class="dir-alp-l-com1 dir-alp-p3">
                            <form action="#">
                                <ul>

                                    <?php error_reporting(0);

                                    foreach ($cities as $key) {
                                        ?>

                                        <div class="location" id="location">	
                                            <li>
                                                <input type="checkbox" id="<?php echo 'loc' . $key->id; ?>" <?php
                                                       if (in_array($key->city, str_replace('-',' ',$loc))) {
                                                           echo 'checked';
                                                       }
                                                       ?> value="<?php echo $key->city; ?>"  />

                                                <label for="<?php echo 'loc' . $key->id; ?>"><?php echo $key->city ?></label>
                                            </li>
                                            <input type="hidden" class="<?php echo 'location' . $key->city; ?>" value="<?php echo $key->id; ?>" >
                                        </div>
<?php } ?>

                                </ul>
                            </form> 
                        </div>
                    </div>
                    <!-- Location -->


                    <div class="dir-alp-l3 dir-alp-l-com">
                        <h4>Course Level</h4>
                        <div class="dir-alp-l-com1 dir-alp-p3">
                            <form action="#">
                                <ul>
<?php foreach ($courses as $key) { ?>

                                        <div class="courses" id="courses">
                                            <li>
                                                <input type="checkbox" id="<?php echo 'cour' . $key->id; ?>" value="<?php echo $key->course_type; ?>" <?php if (in_array($key->course_type, $cour)) {
        echo 'checked';
    }
    ?> />
                                                <label for="<?php echo 'cour' . $key->id; ?>"><?php echo $key->course_type; ?></label>
                                            </li>
                                        </div>
<?php } ?>
                                </ul>
                            </form> 
                        </div>
                    </div>


                    <div class="dir-alp-l3 dir-alp-l-com">
                        <h4>Specialization</h4>
                        <div class="dir-alp-l-com1 dir-alp-p3">

                            <ul>
                                <div class="specialization" id="specialization">
                                    <?php foreach ($specialization as $key) { ?>
                                        <li>
                                            <input type="checkbox" id="<?php echo 'spe' . $key->id ?>" value="<?php echo $key->course_name; ?>" <?php if (in_array($key->course_name, $spec)) {
                                            echo 'checked';
                                        }
                                        ?> />
                                            <label for="<?php echo 'spe' . $key->id ?>"><?php echo $key->course_name; ?></label>
                                        </li>
<?php } ?>
                                </div>
                            </ul>                        
                        </div>
                    </div>


                    <div class="fillter-ad">
                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/ad1.jpg" alt="">

                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/300x600.jpg" alt="">

                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/300x250.jpg" alt="">
                    </div>


                </div>
                <!-- fillters -->
                  <div class="holder">
                                <a class="jp-previous jp-disabled">← previous</a>
                                <a class="jp-current">1</a>
                                <span class="jp-hidden">...</span>
                                <a>2</a>
                                <a>3</a>
                                <a>4</a>
                                <a>5</a>
                                <a class="jp-hidden">6</a>
                                <a class="jp-hidden">7</a>
                                <a class="jp-hidden">8</a>
                                <a class="jp-hidden">9</a>
                                <span>...</span>
                                <a>10</a>
                                <a class="jp-next">next →</a>
                        </div>
                <div id="searchedUniversities">

                    <div class="col-md-9 dir-alp-con-right nopadding">
                        <div class="dir-alp-con-right-1">
                            <div class="row">
                                <?php $i = 0;
                                    foreach ($universities as $key) {
                                        $i++;
                                        ?>

                                        <?php if ($i == 7 || $i == 11) { ?>

                                        <!-- Ad Box -->
                                        <div class="col-md-12 fillter-ad-728">
                                            <img src="<?php echo base_url(); ?>assets/frontend/images/img/728x90.jpg" alt="">
                                        </div>
                                        <!-- Ad Box -->

                                     <?php } ?>

                                    <div class="col-md-6" style="min-height: 390px;">
                                        <div class="home-list-pop list-spac list-spac-1 list-room-mar-o col-md-12">
                                            <div class="col-md-12 nopadding img-min-height">
                                             <a href="<?php echo base_url(); ?>universityDetails/<?php echo strtolower(str_replace(' ','-',$key->uniName)).'/'.$key->uId; ?>">
                                             <img src="<?php echo base_url(); ?>uploads/banner/universities/<?php echo $key->banner_img; ?>" alt=""> </div>

                                            <div class="col-md-12 nopadding  home-list-pop-desc inn-list-pop-desc list-room-deta"> 
                               <h3><img src="<?php echo base_url(); ?>/uploads/logo/universities/<?php echo $key->logo_img; ?>" class="title-img"><?php echo $key->uniName; ?></h3></a> 
                                            </div>	

                                            <small><i class="fa fa-university"></i><?php echo $key->city.', '.$key->approved_by.', '.$key->accredited_name; ?></small>

                                            <div class="list-room-type list-rom-ami">
                                                <ul>
                                                  
                                                    <li><img src="<?php echo base_url(); ?>/assets/frontend/images/img/right-arrow.png" alt=""> Full Time</li>
                                                    <li><img src="<?php echo base_url(); ?>/assets/frontend/images/img/right-arrow.png" alt=""> All Course </li>
                                                    <li><img src="<?php echo base_url(); ?>/assets/frontend/images/img/right-arrow.png" alt=""> Other</li>
                                                   
                                                </ul>
                                            </div> 
                                            <div class="list-enqu-btn">
                                                <ul>
                                                    <li><a data-dismiss="modal" class="brochureInfo" data-id="<?php echo  $key->uId; ?>" data-toggle="modal" data-target="#apply"><i class="fa fa-file-text-o"></i> Apply Now</a> </li>

    
                                                        <li><a  data-dismiss="modal" class="brochureInfo" data-id="<?php echo  $key->uId; ?>" data-toggle="modal" data-target="#register" onclick="showModalForm();"><i class="fa fa-download"></i> Brochure</a> </li>
   
                                                </ul>
                                            </div>

                                        </div>
                                    </div>




<?php } ?>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="Ad">
    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-12 nopadding">
                <img src="<?php echo base_url(); ?>assets/frontend/images/img/ad.jpg" width="100%" height="auto">
            </div>
        </div>
</section>







<!-- Pop Up -->
<section>
  
    <div class="modal fade dir-pop-com" id="register" role="dialog">
        <div class="modal-dialog modal-lg model-custom" id="modalClass">
            <div class="modal-content">
               <div class="modal-body" id="modalBody">
                    <div class="row">
                     <div class="col-md-6">
                         <img src="<?php echo base_url(); ?>assets/frontend/images/brochurePopup.jpg" class="img-responsive">
                     </div>
                    
                     <div class="col-md-6">
                    <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="">
                    </a>
                   <h3 class="modal-title pull-left">Enter your details </h3>
                    <button type="button" class="close pull-right" data-dismiss="modal">×</button>
                                            
                    <form id="instantRegister" class="s12" name="instantRegister"  ng-submit="submitForm()"  novalidate>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Full Name</label>
                                <input type="text"  class="form-control input-lg"  name="fullname" ng-model="brochure.fullname" value="<?php echo $f_name.' '.$l_name; ?>" required>
                                
                              <span ng-show="submitted && instantRegister.fullname.$invalid" class="help-block has-error warnig ng-hide">Please enter full name</span>
          <span class="help-block has-error ng-hide warnig" ng-show="emailaddressError">{{fullnameError}}</span>
          

                            </div>
                        </div>
                        <div  class="row">
                            <div class="form-group col-md-6">
                                <label>Email</label>
                                <input type="email" class="form-control input-lg"  name="emailId"  ng-model="brochure.emailId" value="<?php echo $user_email; ?>" required>
                                <span ng-show="submitted && instantRegister.emailId.$invalid" class="help-block has-error warnig ng-hide">Please enter email id</span>
                            <span class="help-block has-error ng-hide warnig" ng-show="emailIdError">{{emailIdError}}</span>
                                </div>
                            <div class="form-group col-md-6">
                                <label>Mobile Number</label>
                                <input type="text" class="form-control input-lg"  name="mobileNo" value="" ng-model="brochure.mobileNo" required>
                              <span ng-show="submitted && instantRegister.mobileNo.$invalid" class="help-block has-error warnig ng-hide">Please enter mobile no.</span>
                            <span class="help-block has-error ng-hide warnig" ng-show="mobileNoError">{{mobileNoError}}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 hide-pop-select">
                                <label>Select your city</label>

                                 <select name="state" ng-model="brochure.city" class="form-control">  
                                        <option ng-repeat="city in cities" value="{{city.city}}">  
                                           {{city.city}}  
                                      </option>  
                                 </select>  
                               </div>
                            
                            <div class="form-group col-md-6  hide-pop-select">
                                <label>Select a course</label>
                                 <select name="course" ng-model="brochure.course" class="form-control">
                                    <option ng-repeat="cour in courses" value="{{cour.course}}">{{cour.course}}</option>
                                 </select>
                            </div>
                        </div>

                        <div class="row mt20">
                            <div class="form-group col-md-12">
                                <input type="submit" ng-click="submitted = true" value="Submit" class="btn btn-primary btn-block btn-lg"> </div>
                        </div>

                        <div class="row text-center">
                            <?php if(!$this->session->userdata('user_id')){ ?>
                            <div class="input-field s12">Already registered? <a href="<?php echo base_url(); ?>login"><strong>Click here to login</strong></a> </div>
                        <?php } ?>
                        </div>
                    </form>
                 </div>
              </div>
            </div>
                     <div id="brochureLoginForm" class="modal-body" style="display: none"></div>
                     <div id="modalSuccess" class="modal-body" style="display: none"></div>
            </div>
        </div>
    </div><!-- model close -->





<div class="modal fade dir-pop-com" id="apply" role="dialog">
        <div class="modal-dialog modal-lg" id="modalClass2">
            <div class="modal-content">
               <div class="modal-body" id="applyModalBody">
                    <div class="row">
                   <!--   <div class="col-md-6">
                         <img src="<?php echo base_url(); ?>assets/frontend/images/brochurePopup.jpg" class="img-responsive">
                     </div> -->
                    
                     
                     <div class="col-md-12">
                    <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="">
                    </a>
                   <h3 class="modal-title pull-left">Enter your details </h3>
                    <button type="button" class="close pull-right" data-dismiss="modal">×</button>
                                            
                    <form class="s12" name="instantApply" id="instantApply"  ng-submit="submitApplyForm()"  novalidate>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Full Name</label>
                                <input type="text"  class="form-control input-lg"  name="applyFullname" ng-model="apply.applyFullname" value="<?php echo $f_name.' '.$l_name; ?>" required>
                                
                              <span ng-show="submittedd && instantApply.applyFullname.$invalid" class="help-block has-error warnig ng-hide">Please enter full name</span>
          <span class="help-block has-error ng-hide warnig" ng-show="applyFullnameError">{{applyFullnameError}}</span>
          

                            </div>
                        </div>
                        <div  class="row">
                            <div class="form-group col-md-6">
                                <label>Email</label>
                                <input type="email" class="form-control input-lg"  name="applyEmailId"  ng-model="apply.applyEmailId" value="<?php echo $user_email; ?>" required>
                                <span ng-show="submittedd && instantApply.applyEmailId.$invalid" class="help-block has-error warnig ng-hide">Please enter email id</span>
                            <span class="help-block has-error ng-hide warnig" ng-show="applyEmailIdError">{{applyEmailIddError}}</span>
                                </div>
                            <div class="form-group col-md-6">
                                <label>Mobile Number</label>
                                <input type="text" class="form-control input-lg"  name="applyMobileNo" value="" ng-model="apply.applyMobileNo" required>
                              <span ng-show="submittedd && instantApply.applyMobileNo.$invalid" class="help-block has-error warnig ng-hide">Please enter mobile no.</span>
                            <span class="help-block has-error ng-hide warnig" ng-show="applyMobileNoError">{{applyMobileNoError}}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 hide-pop-select">
                                <label>Select your city</label>

                                 <select name="applyCity" ng-model="apply.applyCity" class="form-control">  
                                        <option ng-repeat="city in cities" value="{{city.city}}">  
                                           {{city.city}}  
                                      </option>  
                                 </select>  
                               </div>
                            
                            <div class="form-group col-md-6  hide-pop-select">
                                <label>Select a course</label>
                                 <select name="applyCourse" ng-model="apply.applyCourse" class="form-control">
                                    <option ng-repeat="cour in courses" value="{{cour.course}}">{{cour.course}}</option>
                                 </select>
                            </div>
                        </div>

                        <div class="row mt20">
                            <div class="form-group col-md-12">
                                <input type="submit" ng-click="submittedd = true" value="Submit" class="btn btn-primary btn-block btn-lg"> </div>
                        </div>

                        <div class="row text-center">
                            <?php if(!$this->session->userdata('user_id')){ ?>
                            <div class="input-field s12">Already registered? <a href="<?php echo base_url(); ?>login"><strong>Click here to login</strong></a> </div>
                        <?php } ?>
                        </div>
                    </form>
                     </div>
                     </div>
                      </div>
                       <div id="applyLoginForm" class="modal-body" style="display: none"></div>
                     <div id="applyModalSuccess" class="modal-body" style="display: none"></div>
            </div>
        </div>
    </div><!-- model close -->






</div>
</div>
<!-- GET QUOTES Popup END -->
</section>

