<?php error_reporting(0); // echo "<pre>"; print_r($courseInfo); exit; ?>


<section >
    <div class="v3-list-ql">
        <div class="container">
            <div class="row">
                <div class="v3-list-ql-inn">
                    <ul>
                        <li class="active"><a href="#info"><i class="fa fa-user"></i> About</a>
                        </li>
                        <li class="active"><a href="#ld-abour"><i class="fa fa-user"></i> Courses</a>
                        </li>
                       </li>
                                            
                        <li><a href="#contact"><i class="fa fa-paper-plane-o"></i> Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--LISTING DETAILS-->

<section class="list-pg-bg" style="margin-top: 80px;">
    <div class="container">
        <div class="row" style="padding-top:40px">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a> </li>
                <li class="active"><?php echo $courseInfo->course_name.' > '.'Courses'.' > '.$courseInfo->course_name.' in '.$courseInfo->course_specialization.', '.$courseInfo->colgName.', '.$courseInfo->colgCity.', '.$courseInfo->stateName; ?></li>
            </ol>
        </div>


        <div class="row">
            <div class="well" id="info">
                <div class="row">
                    <div class="col-md-8">
                     <p><img src="<?php echo base_url(); ?>uploads/logo/colleges/<?php echo $courseInfo->colgLogo; ?>" width="35px"> <?php echo $courseInfo->colgName.', '.$courseInfo->colgCity.', '.$courseInfo->stateName;; ?></p>

                        <h2 class="mb10"> <?php echo $courseInfo->course_name.' in '.$courseInfo->course_specialization; ?></h2>

                        <p><?php echo 'UG Degree'.' | '.' '.'Full Time' . ' | '.'Duration - 4 Years';  ?></p><br><br>
                        <h5><?php echo $courseInfo->g_name.' Approved'.' | '.$courseInfo->study_mode. ' | '.$courseInfo->course_duration;  ?></h5><br>
                        <p><strong>Affiliated to</strong>  <a href="<?php echo base_url(); ?>universityDetails/<?php echo strtolower(str_replace(' ','-',$courseInfo->universityName)).'/'.$courseInfo->uniId; ?>"><?php echo $courseInfo->universityName; ?></a></p>
                    </div>
                </div>




            </div>	
        </div>	


        <div class="row">
            <div class="">
                <div class="list-pg-lt list-page-com-p">
                    <!--Courses-->
                    <div class="pglist-p1 pglist-bg pglist-p-com" id="ld-abour">
                        <div class="pglist-p-com-ti">
                                <h3>Eligibility</h3> 
                        </div>

                     <div class="list-pg-inn-sp">
                           <p><?php echo html_entity_decode($courseInfo->course_eligibility,ENT_QUOTES, "UTF-8"); ?></p>


                        </div>
                    </div>

                    <div class="pglist-p1 pglist-bg pglist-p-com" id="ld-abour">
                        <div class="pglist-p-com-ti">
                                <h3>Fee Structure</h3> 
                        </div>

                     <div class="list-pg-inn-sp">
                           <p><?php echo html_entity_decode($courseInfo->course_fee_structure,ENT_QUOTES, "UTF-8"); ?></p>


                        </div>
                    </div>
                    <div class="col-md-12 fillter-ad1-728">
                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/728x90.jpg" alt="">
                    </div>
                  

                    <!-- Contact -->
                    <div class="pglist-p3 pglist-bg pglist-p-com" id="contact" style="overflow: auto;">
                        <div class="pglist-p-com-ti">
                            <h3><span>Contact</span> Us</h3> </div>
                        <div class="list-pg-inn-sp">
                            <div class="list-pg-write-rev">
                                <div class="col-md-4 text-center">
                                    <h4><i class="fa fa-paper-plane-o"></i> Address:</h4>
                                    <hr>
                                    <p> <?php echo $courseInfo->colgAdd; ?><br><?php echo $courseInfo->colgCity.', '.$courseInfo->stateName; ?></p>
                                </div>

                                <div class="col-md-4 text-center">
                                    <h4><i class="fa fa-globe"></i> Website:</strong> </h4>
                                    <hr>
                                    <p><a href="<?php echo $collegeInfo->url; ?>" target="_blank"> <?php echo $courseInfo->url; ?></a></p>
                                </div>

                                <div class="col-md-4 text-center">
                                    <h4><i class="fa fa-phone"></i> Phone:</strong> </h4>
                                    <hr>
                                     
                                     <p><?php echo $courseInfo->contactNo; ?></p>     
                                  
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--End Contact  -->

                 
                </div>	




                <!--  Right Side bar -->
                <div class="list-pg-rt">
                    <!--LISTING DETAILS: LEFT PART 7-->
                    <div class="pglist-p3 pglist-bg pglist-p-com">
                        <div class="pglist-p-com-ti pglist-p-com-ti-right">
                            <h3><span>Listing</span> Colleges</h3> </div>
                        <div class="list-pg-inn-sp">
                            <div class="list-pg-guar">
                                <ul>
                                    <li>
                                        <div class="list-pg-guar-img"> <img src="<?php echo base_url(); ?>assets/frontend/images/icon/g1.png" alt="" /> </div>
                                        <h4>Service Guarantee</h4>
                                        <p>Upto 6 month of service</p>
                                    </li>
                                    <li>
                                        <div class="list-pg-guar-img"> <img src="<?php echo base_url(); ?>assets/frontend/images/icon/g2.png" alt="" /> </div>
                                        <h4>Professionals</h4>
                                        <p>100% certified professionals</p>
                                    </li>
                                    <li>
                                        <div class="list-pg-guar-img"> <img src="<?php echo base_url(); ?>assets/frontend/images/icon/g3.png" alt="" /> </div>
                                        <h4>Insurance</h4>
                                        <p>Upto $5,000 against damages</p>
                                    </li>
                            </div>
                        </div>
                    </div>
                    <!--END LISTING DETAILS: LEFT PART 7-->

                    <div class="fillter-ad">
                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/ad1.jpg" alt="">

                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/300x600.jpg" alt="">

                        <img src="<?php echo base_url(); ?>assets/frontend/images/img/300x250.jpg" alt="">
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>







<!-- Pop Up -->
<section>
    <!-- GET QUOTES POPUP -->
    <div class="modal fade dir-pop-com" id="register" role="dialog">
        <div class="modal-dialog model-lg">
            <div class="modal-content">
                <div class="modal-header dir-pop-head">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Login</h4>
                    <!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
                </div>
                <div class="modal-body">

                    <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="" />
                    </a>
                    <!-- <h4>Login</h4>
                    <p>Don't have an account? Create your account. It's take less then a minutes</p> -->
                    <form class="s12">
                        <div>
                            <div class="form-group">
                                <label>User name</label>
                                <input type="text"  class="form-control">

                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control">

                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <input type="submit" value="Register" class="btn btn-danger"> </div>
                        </div>
                        <div>
                            <div class="input-field s12"> <a href="forgot-pass.html">Forgot password</a> | <a href="<?php echo base_url(); ?>register">Create a new account</a> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- GET QUOTES Popup END -->
</section>


<!-- Pop up -->