    <?php error_reporting(0); // echo "<pre>"; print_r($colleges); exit;   ?>
    <section class=" dir-pa-sp-top">
        <div class="container">
            <div class="row" style="padding-top:20px">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a> </li>
                    <li class="active"><?php echo $university->name; ?></li>
                    <input type="hidden" id="uniName" value="<?php echo  $university->name; ?>">
                    <input type="hidden" id="uniId" value="<?php echo  $university->id; ?>">
                </ol>

            </div>
 
            <div class="row " >
                <div class="well" id="info">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="mb10"><img src="<?php echo base_url(); ?>assets/frontend/images/img/University-logo/Symbiosis.jpeg" width="35px"> <?php echo $university->name; ?> </h2>


                            <h4 class="mb10">115 Distance/Correspondence Diploma courses in India Offering 281 Courses</h4>
                             <p><?php echo html_entity_decode($universities->about,ENT_QUOTES, "UTF-8"); ?></p>

                            <div class="list-room-type list-rom-ami">
                                <ul>
                                    <li><img src="<?php echo base_url(); ?>assets/images/img/right-arrow.png" alt="">
                                        Colleges <span class="badge">5</span></li>

                                    <li><img src="<?php echo base_url(); ?>assets/images/img/right-arrow.png" alt=""> Courses <span class="badge">15</span></li>
                                </ul>
                            </div>  
                        </div>

                        <div class="col-md-4">
                            <img src="<?php echo base_url(); ?>assets/frontend/images/img/university-img/pune.jpg" class="img-thumbnail">
                            <div class="cus-ul">
                                <ul>
                                    <li>
                                        <?php echo $universities->approved_by; ?> </li>

                                    <li> <?php echo $universities->accredited_name; ?> </li>
                                </ul>
                            </div>

                        </div>
                    </div>

               


                </div>  
            </div>  

           

            <div class="row">
                <div class="well col-md-12">
                    <div class="col-md-3 dir-alp-con-left sidebar">
                        <!-- <div class="dir-alp-con-left-1">
                                <h3>Nearby Listings(07)</h3> </div> -->
                        <!-- hello -->
                        <!-- <div class="dir-alp-l3 dir-alp-l-com ">
                            <h4>Location</h4>


                            <?php // echo "<pre>"; print_r($cities); ?>
                            <div class="dir-alp-l-com1 dir-alp-p3">
                                <form action="#">
                                    <ul>

                                        <?php error_reporting(0);

                                        foreach ($cities as $key) {
                                            ?>

                                            <div class="location" id="location">    
                                                <li>
                                                    <input type="checkbox" id="<?php echo 'loc' . $key->id; ?>" <?php
                                                           if (in_array($key->city, $loc)) {
                                                               echo 'checked';
                                                           }
                                                           ?> value="<?php echo $key->city; ?>"  />

                                                    <label for="<?php echo 'loc' . $key->id; ?>"><?php echo $key->city ?></label>
                                                </li>
                                                <input type="hidden" class="<?php echo 'location' . $key->city; ?>" value="<?php echo $key->id; ?>" >
                                            </div>
    <?php } ?>

                                    </ul>
                                </form> 
                            </div>
                        </div> -->
                        <!-- Location -->


                        <div class="dir-alp-l3 dir-alp-l-com">
                            <h4>Course Level</h4>
                            <div class="dir-alp-l-com1 dir-alp-p3">
                                <form action="#">
                                    <ul>
    <?php foreach ($courses as $key) { ?>

                                            <div class="courses" id="courses">
                                                <li>
                                                    <input type="checkbox" id="<?php echo 'cour' . $key->id; ?>" value="<?php echo $key->course_type; ?>" <?php if (in_array($key->course_type, $cour)) {
            echo 'checked';
        }
        ?> />
                                                    <label for="<?php echo 'cour' . $key->id; ?>"><?php echo $key->course_type; ?></label>
                                                </li>
                                            </div>
    <?php } ?>
                                    </ul>
                                </form> 
                            </div>
                        </div>


                        <div class="dir-alp-l3 dir-alp-l-com">
                            <h4>Specialization</h4>
                            <div class="dir-alp-l-com1 dir-alp-p3">

                                <ul>
                                    <div class="specialization" id="specialization">
                                        <?php foreach ($specialization as $key) { ?>
                                            <li>
                                                <input type="checkbox" id="<?php echo 'spe' . $key->id ?>" value="<?php echo $key->course_name; ?>" <?php if (in_array($key->course_name, $spec)) {
                                                echo 'checked';
                                            }
                                            ?> />
                                                <label for="<?php echo 'spe' . $key->id ?>"><?php echo $key->course_name; ?></label>
                                            </li>
    <?php } ?>
                                    </div>
                                </ul>                        
                            </div>
                        </div>


                        <div class="fillter-ad">
                            <img src="<?php echo base_url(); ?>assets/frontend/images/img/ad1.jpg" alt="">

                            <img src="<?php echo base_url(); ?>assets/frontend/images/img/300x600.jpg" alt="">

                            <img src="<?php echo base_url(); ?>assets/frontend/images/img/300x250.jpg" alt="">
                        </div>


                    </div>
                    <!-- fillters -->

                    <div id="searchedUniversities">

                        <div class="col-md-9 dir-alp-con-right nopadding">
                            <div class="dir-alp-con-right-1">
                                <div class="row">

                                     

                                        <?php
                                              // echo "<pre>"; print_r($colleges); exit;

                                        $i = 0;
                                        foreach ($colleges as $key) {

                                            $uName = preg_replace('/[^A-Za-z0-9\-\']/', '-', $key->uniName); 
                                            $cName = preg_replace('/[^A-Za-z0-9\-\']/', '-', $key->colgName); 
                                            $i++;
                                            if($key->type='department'){
                                                 $bannerUrl = 'uploads/banner/universities/'.$key->banner_img;
                                                 $logoUrl = 'uploads/logo/universities/'.$key->logo_img;
                                                 $approved_by = $key->uapproved;
                                                 $grade = $key->uaccredited;
                                                 $city=$key->ucity;
                                                 $state=$key->ustate;
                                             }else{
                                                  $bannerUrl = 'uploads/banner/colleges/'.$key->bannerName;
                                                 $logoUrl = 'uploads/logo/colleges/'.$key->logoname;
                                                 $approved_by = $key->g_name;
                                                 $grade=$key->caccreditions;
                                                 $city=$key->ccity;
                                                 $state=$key->cstate;
                                             }
                                            
                                            ?>

                                                                            <!--LISTINGS-->
                                            <?php if ($i == 7 || $i == 11) { ?>

                                            <!-- Ad Box -->
                                            <div class="col-md-12 fillter-ad-728">
                                                <img src="<?php echo base_url(); ?>assets/frontend/images/img/728x90.jpg" alt="">
                                            </div>
                                            <!-- Ad Box -->

                                         <?php } ?>

                                        <div class="col-md-6" style="min-height: 390px;">
                                            <div class="home-list-pop list-spac list-spac-1 list-room-mar-o col-md-12">
                                                <div class="col-md-12 nopadding"> <img src="<?php echo base_url().'/'.$bannerUrl; ?>" alt=""> </div>

                                                <div class="col-md-12 nopadding home-list-pop-desc inn-list-pop-desc list-room-deta"> 
                                   <a href="<?php echo base_url(); ?>university/<?php echo strtolower($uName).'/college/'.strtolower($cName).'/'.$key->colgId; ?>"><h3><img src="<?php echo base_url().'/'.$logoUrl; ?>" class="title-img"><?php echo $key->colgName; ?></h3></a> 
                                                </div>  

                                                <small><i class="fa fa-university"></i><?php echo $city.', '.$approved_by.', '.$grade; ?></small>

                                                <div class="list-room-type list-rom-ami">
                                                    <ul>
                                                        <!-- <li>
                                                                <strong>Amenities:</strong>
                                                        </li> -->
                                                        <li><img src="<?php echo base_url(); ?>/assets/frontend/images/img/right-arrow.png" alt=""> Full Time</li>
                                                        <li><img src="<?php echo base_url(); ?>/assets/frontend/images/img/right-arrow.png" alt=""> All Course </li>
                                                        <li><img src="<?php echo base_url(); ?>/assets/frontend/images/img/right-arrow.png" alt=""> Other</li>
                                                        <!-- <li><img src="images/img/right-arrow.png" alt=""> Bar</li> -->
                                                    </ul>
                                                </div> 
                                                <div class="list-enqu-btn">
                                                    <ul>
                                                        <li><a href="universities_details.php"><i class="fa fa-file-text-o"></i> Apply Now</a> </li>

        
                                                            <li><a  data-dismiss="modal" data-toggle="modal" class="brochureInfo" data-id="<?php echo  $key->uniId; ?>" data-target="#register"><i class="fa fa-download"></i> Brochure</a> </li>
       
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>




    <?php } ?>
                                </div>
 
                            </div>

                        </div>
                    </div><!-- id  searchedUniversities -->
                </div>
            </div>
        </div>
    </section>



    <section class="Ad">
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12 nopadding">
                    <img src="<?php echo base_url(); ?>assets/frontend/images/img/ad.jpg" width="100%" height="auto">
                </div>
            </div>
    </section>






<section>
    <!-- GET QUOTES POPUP -->
    <div class="modal fade dir-pop-com" id="register" role="dialog">
        <div class="modal-dialog modal-lg model-custom">
            <div class="modal-content">
                <!-- <div class="modal-header dir-pop-head">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Enter your details </h4>
                </div> -->
                <div class="modal-body" id="modalBody">
                    <div class="row">
                     <div class="col-md-6">
                         <img src="<?php echo base_url(); ?>assets/frontend/images/brochurePopup.jpg" class="img-responsive">
                     </div>
                    
                     <div class="col-md-6">
                    <a href="#" class="pop-close" data-dismiss="modal"><img src="images/cancel.png" alt="">
                    </a>
                   <h3 class="modal-title pull-left">Enter your details </h3>
                    <button type="button" class="close pull-right" data-dismiss="modal">×</button>
                                            
                    <form class="s12" name="instantRegister"  ng-submit="submitForm()"  novalidate>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Full Name</label>
                                <input type="text"  class="form-control input-lg"  name="fullname" ng-model="brochure.fullname" value="<?php echo $f_name.' '.$l_name; ?>" required>
                                
                              <span ng-show="submitted && instantRegister.fullname.$invalid" class="help-block has-error warnig ng-hide">Please enter full name</span>
          <span class="help-block has-error ng-hide warnig" ng-show="emailaddressError">{{fullnameError}}</span>
          

                            </div>
                        </div>
                        <div  class="row">
                            <div class="form-group col-md-6">
                                <label>Email</label>
                                <input type="email" class="form-control input-lg"  name="emailId"  ng-model="brochure.emailId" value="<?php echo $user_email; ?>" required>
                                <span ng-show="submitted && instantRegister.emailId.$invalid" class="help-block has-error warnig ng-hide">Please enter email id</span>
                            <span class="help-block has-error ng-hide warnig" ng-show="emailIdError">{{emailIdError}}</span>
                                </div>
                            <div class="form-group col-md-6">
                                <label>Mobile Number</label>
                                <input type="text" class="form-control input-lg"  name="mobileNo" value="" ng-model="brochure.mobileNo" required>
                              <span ng-show="submitted && instantRegister.mobileNo.$invalid" class="help-block has-error warnig ng-hide">Please enter mobile no.</span>
                            <span class="help-block has-error ng-hide warnig" ng-show="mobileNoError">{{mobileNoError}}</span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 hide-pop-select">
                                <label>Select your city</label>

                                 <select name="state" ng-model="brochure.city" class="form-control">  
                                        <option ng-repeat="city in cities" value="{{city.city}}">  
                                           {{city.city}}  
                                      </option>  
                                 </select>  
                               </div>
                            
                            <div class="form-group col-md-6  hide-pop-select">
                                <label>Select a course</label>
                                 <select name="course" ng-model="brochure.course" class="form-control">
                                    <option ng-repeat="cour in courses" value="{{cour.course}}">{{cour.course}}</option>
                                 </select>
                            </div>
                        </div>

                        <div class="row mt20">
                            <div class="form-group col-md-12">
                                <input type="submit" ng-click="submitted = true" value="Submit" class="btn btn-primary btn-block btn-lg"> </div>
                        </div>

                        <div class="row text-center">
                            <?php if(!$this->session->userdata('user_id')){ ?>
                            <div class="input-field s12">Already registered? <a href="<?php echo base_url(); ?>login"><strong>Click here to login</strong></a> </div>
                        <?php } ?>
                        </div>
                    </form>
                     </div>
                     </div>
                       

                       

                   
                </div>
            </div>
        </div>
    </div><!-- model close -->
</div>
</div>
<!-- GET QUOTES Popup END -->
</section>


    <!-- Pop up -->
